<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    public function run()
    {
        $this->call(RolePermissionTableSeeder::class);
        $this->call(CookiesTableSeeder::class);
        $this->call(EmailSettingsTableSeeder::class);
        $this->call(EmailTemplateVariablesTableSeeder::class);
        $this->call(EmailTemplatesTableSeeder::class);
        $this->call(FailedJobsTableSeeder::class);
        $this->call(GoogleMapsTableSeeder::class);
       
        $this->call(MenusTableSeeder::class);
        $this->call(PageBuildersTableSeeder::class);
        $this->call(PageComponentCategoriesTableSeeder::class);
        $this->call(PageComponentsTableSeeder::class);
        $this->call(PageHeadersTableSeeder::class);
        $this->call(PasswordResetsTableSeeder::class);
        $this->call(PaymentGatewaysTableSeeder::class);
        $this->call(PrivacyPoliciesTableSeeder::class);
        $this->call(ShippingCountrySeeder::class);
    }
}
