<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CookiesTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('cookies')->insert([
            'cookie_status' => '1',
            'cookie_button' => 'Accept',
            'cookie_confirmation' => 'Your experience on this site will be improved by allowing cookies.',
            'created_at' => now()->toDateTimeString(),
            'updated_at' => now()->toDateTimeString(),
        ]);
    }
}