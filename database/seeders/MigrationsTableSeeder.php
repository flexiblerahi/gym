<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MigrationsTableSeeder extends Seeder
{
    public function run()
    {
        DB::table('migrations')->insert([ 
            [
                'migration' => '2014_10_12_000000_create_users_table',
                'batch' => 1,
            ], [
                'migration' => '2014_10_12_100000_create_password_resets_table',
                'batch' => 1,
            ], [
                'migration' => '2019_08_19_000000_create_failed_jobs_table',
                'batch' => 1,
            ], [
                'migration' => '2019_12_14_000001_create_personal_access_tokens_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_02_055100_create_admins_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_04_094816_create_menus_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_06_030914_create_media_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_07_020018_create_settings_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_07_102319_create_cookies_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_08_091420_create_payment_gateways_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_09_011334_create_email_settings_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_09_100443_create_email_templates_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_12_010415_create_blogs_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_12_014008_create_blog_categories_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_12_020337_create_blog_comments_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_15_031130_create_email_template_variables_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_15_093111_create_subscribers_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_19_082530_create_page_builders_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_19_121719_create_page_items_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_19_121737_create_page_headers_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_20_014325_create_page_layouts_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_26_032311_create_page_components_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_26_032326_create_page_component_categories_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_29_152832_create_sliders_table',
                'batch' => 1,
            ], [
                'migration' => '2021_12_30_105259_create_faqs_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_01_075515_create_services_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_01_075555_create_abouts_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_01_075808_create_testimonials_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_09_044542_create_social_links_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_09_072839_create_footer_contact_items_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_11_073049_create_google_maps_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_12_085301_create_footer_information_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_13_152432_create_coursecategories_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_13_511246_create_courses_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_15_022000_create_product_categories_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_17_022038_create_orders_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_17_024924_create_transactions_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_17_621805_create_products_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_20_055231_create_product_reviews_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_23_002024_create_user_adresses_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_25_031857_create_shipping_countries_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_25_053149_create_shop_settings_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_30_092552_create_order_items_table',
                'batch' => 1,
            ], [
                'migration' => '2022_01_30_092940_create_order_addresses_table',
                'batch' => 1,
            ], [
                'migration' => '2022_02_09_160406_create_privacy_policies_table',
                'batch' => 1,
            ], [
                'migration' => '2022_02_09_160629_create_terms_of_uses_table',
                'batch' => 1,
            ], [
                'migration' => '2022_02_24_040405_create_contact_messages_table',
                'batch' => 1,
            ], [
                'migration' => '2022_02_24_051547_create_contact_pages_table',
                'batch' => 1,
            ], [
                'migration' => '2022_05_07_152055_create_permission_tables',
                'batch' => 1,
            ], [
                'migration' => '2022_05_25_062656_create_course_reviews_table',
                'batch' => 1,
            ], [
                'migration' => '2022_06_13_050400_create_trainers_table',
                'batch' => 1,
            ], [
                'migration' => '2022_06_18_045031_create_coupons_table',
                'batch' => 1,
            ], [
                'migration' => '2022_06_18_045540_create_shippings_table',
                'batch' => 1,
            ], [
                'migration' => '2022_07_02_063246_create_bill_addresses_table',
                'batch' => 1,
            ], [
                'migration' => '2022_07_14_044554_create_our_processes_table',
                'batch' => 1,
            ]
        ]);
    }
}