<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class RolePermissionTableSeeder extends Seeder
{
    public function run()
    {
        $allpermissions = array();
        $allrolehaspermissions = array();
        $nowtime = now()->toDateTimeString();
        $count_permissions = 0;
        $allpermissions = array();
        $allrolehaspermissions = array();
        $nowtime = now()->toDateTimeString();
        $count_permissions = 0;
        $permissions = array('about', 'admin-language', 'blog', 'blog-category', 'blog-comment', 'contact-message', 'contact-page', 'email-template', 'faq',
                             'footer-contact-item', 'frontend-language', 'media-manager', 'order', 'privacy-policy', 'product',
                             'product-category', 'product-review', 'send-email', 'service', 'settings', 'shipping-country', 'shop-settings', 'slider',
                             'social-link', 'subscriber', 'terms-of-use', 'testimonial', 'transaction', 'user', 'user-data', 'user-profile', 'role',
                             'course', 'course-category', 'ourprocess', 'sectiontitle', 'trainer');

        foreach ($permissions as $permission) foreach(array('index', 'create', 'edit', 'delete') as $route) {
            $allpermissions[] = array('name' => $permission.'-'.$route, 'guard_name' => 'admin', 'created_at' => $nowtime, 'updated_at' => $nowtime);
            $count_permissions = $count_permissions + 1;
        }
        
        DB::table('permissions')->insert($allpermissions);
        DB::table('roles')->insert(array('name' => 'admin', 'guard_name'=> 'admin', 'created_at' => $nowtime, 'updated_at' => $nowtime));
        
        for($i=1; $i<=$count_permissions; $i++) {
            $allrolehaspermissions[] = array('permission_id' => $i, 'role_id' => 1);
        }
        DB::table('role_has_permissions')->insert($allrolehaspermissions);
        $admin = Admin::create([
            'name' => 'Rahi',
            'avatar' => 'assets/uploads/user/avatar/media_1658141438.png',
            'email' => 'admin@gmail.com',
            'password' => Hash::make('1234'),
        ]);
        $admin->assignRole('admin');
    }
}
