<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class MenusTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        $nowdate = now()->toDateTimeString();
        DB::table('menus')->insert(array (
            0 => 
            array (
                'id' => 1,
                'title' => 'Home',
                'link' => '',
                'status' => 1,
                'order' => 0,
                'parent_id' => NULL,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ),
            1 => 
            array (
                'id' => 2,
                'title' => 'Blogs',
                'link' => 'blogs',
                'status' => 1,
                'order' => 2,
                'parent_id' => NULL,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ),
            2 => 
            array (
                'id' => 3,
                'title' => 'Shop',
                'link' => 'shop',
                'status' => 1,
                'order' => 3,
                'parent_id' => NULL,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ),
            3 => 
            array (
                'id' => 4,
                'title' => 'About Us',
                'link' => 'about',
                'status' => 1,
                'order' => 1,
                'parent_id' => NULL,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ),
            4 => 
            array (
                'id' => 5,
                'title' => 'Contact',
                'link' => 'contact',
                'status' => 1,
                'order' => 5,
                'parent_id' => NULL,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ),
            5 => 
            array (
                'id' => 6,
                'title' => 'Faq',
                'link' => 'faq',
                'status' => 1,
                'order' => 4,
                'parent_id' => NULL,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ),
        ));
        
        
    }
}