<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class EmailTemplateVariablesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('email_template_variables')->delete();
        
        \DB::table('email_template_variables')->insert([
            [
                'variable' => '{link}',
                'meaning' => 'Verification Link',
                'template_id' => 1,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{name}',
                'meaning' => 'Customer Name',
                'template_id' => 2,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{name}',
                'meaning' => 'Customer Name',
                'template_id' => 3,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{website}',
                'meaning' => 'Website Name',
                'template_id' => 3,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{name}',
                'meaning' => 'Name',
                'template_id' => 5,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{Phone}',
                'meaning' => 'Phone',
                'template_id' => 5,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{email}',
                'meaning' => 'Email',
                'template_id' => 5,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{subject}',
                'meaning' => 'Subject',
                'template_id' => 5,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{message}',
                'meaning' => 'Message',
                'template_id' => 5,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
            [
                'variable' => '{website}',
                'meaning' => 'Website Name',
                'template_id' => 6,
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ],
        ]);
        
        
    }
}