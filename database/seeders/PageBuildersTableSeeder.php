<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageBuildersTableSeeder extends Seeder
{
    public function run()
    {
        $nowdate = now()->toDateTimeString();
        DB::table('page_builders')->insert([
            [
                'title' => 'home',
                'slug' => 'home',
                'contents' => '3,4,5,6,8',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'title' => 'product',
                'slug' => 'product',
                'contents' => '9',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ],[
                'title' => 'About',
                'slug' => 'about',
                'contents' => '15,16,17,18',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ],[
                'title' => 'Faq',
                'slug' => 'faq',
                'contents' => '20',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ],[
                'title' => 'Privacy policy',
                'slug' => 'privacy-policy',
                'contents' => '22',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ],[
                'title' => 'Terms of use',
                'slug' => 'terms-of-use',
                'contents' => '23',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ],[
                'title' => 'Contact',
                'slug' => 'contact',
                'contents' => '24',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ],[
                'title' => 'Services',
                'slug' => 'services',
                'contents' => '13',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ],[
                'title' => 'Contact',
                'slug' => 'contact',
                'contents' => '24',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ]
        ]);
    }
}