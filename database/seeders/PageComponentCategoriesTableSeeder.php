<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageComponentCategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        DB::table('page_component_categories')->insert([
            [
                'name' => 'Page Title Components',
                'created_at' => now()->toDateString(),
                'updated_at' => now()->toDateString(),
            ], [
                'name' => 'Home Page Components',
                'created_at' => now()->toDateString(),
                'updated_at' => now()->toDateString(),
            ], [
                'name' => 'Sliders Components',
                'created_at' => now()->toDateString(),
                'updated_at' => now()->toDateString(),
            ], [
                'name' => 'Service Components',
                'created_at' => now()->toDateString(),
                'updated_at' => now()->toDateString(),
            ], [
                'name' => 'About Us Components',
                'created_at' => now()->toDateString(),
                'updated_at' => now()->toDateString(),
            ], [
                'name' => 'FAQ Components',
                'created_at' => now()->toDateString(),
                'updated_at' => now()->toDateString(),
            ], [
                'name' => 'Portfolio Components',
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ], [
                'name' => 'Privacy Policy Components',
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ], [
                'name' => 'Terms Of Use Components',
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ], [
                'name' => 'Contact Components',
                'created_at' => now()->toDateTimeString(),
                'updated_at' => now()->toDateTimeString(),
            ]
        ]);       
    }
}