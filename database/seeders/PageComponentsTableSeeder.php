<?php

namespace Database\Seeders;

use App\Models\Course;
use Carbon\Carbon;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PageComponentsTableSeeder extends Seeder
{
    public function run()
    {
        $created_at = Carbon::now()->format('Y-m-d H:i:s');
        $updated_at = Carbon::now()->format('Y-m-d H:i:s');
        DB::table('page_components')->insert([
            [
                'id' => 1,
                'name' => 'Page Title Left',
                'view' => 'frontend.components.titleComponents.titleTextLeft',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 2,
                'name' => 'Page Title Right',
                'view' => 'frontend.components.titleComponents.titleTextRight',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 3,
                'name' => 'Banner Section',
                'view' => 'frontend.frontpage.banner',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 4,
                'name' => 'About Section',
                'view' => 'frontend.frontpage.about',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 5,
                'name' => 'Service Section',
                'view' => 'frontend.frontpage.services',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 6,
                'name' => 'Training Section',
                'view' => 'frontend.frontpage.training',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 7,
                'name' => 'Pricing Section',
                'view' => 'frontend.frontpage.price',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 8,
                'name' => 'Blog Section',
                'view' => 'frontend.frontpage.blog',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 9,
                'name' => 'Footer Section',
                'view' => 'frontend.product.index',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 10,
                'name' => 'Product Details',
                'view' => 'frontend.product.details',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 11,
                'name' => 'Revolution Slider',
                'view' => 'frontend.components.sliderComponents.revolutionSlider',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 12,
                'name' => 'Owl Carousel',
                'view' => 'frontend.components.sliderComponents.owlCarousel',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 13,
                'name' => 'Service Style One',
                'view' => 'frontend.components.serviceComponents.serviceStyleOne',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 14,
                'name' => 'Service Style Two',
                'view' => 'frontend.components.serviceComponents.serviceStyleTwo',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 15,
                'name' => 'About Intro',
                'view' => 'frontend.components.aboutComponents.aboutIntro',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 16,
                'name' => 'About Cases',
                'view' => 'frontend.components.aboutComponents.aboutCases',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 17,
                'name' => 'About Parallax',
                'view' => 'frontend.components.aboutComponents.aboutParallax',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 18,
                'name' => 'About Brands',
                'view' => 'frontend.components.aboutComponents.aboutBrands',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 19,
                'name' => 'FAQ Style One',
                'view' => 'frontend.components.faqComponents.faqStyleOne',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 20,
                'name' => 'FAQ Style Two',
                'view' => 'frontend.components.faqComponents.faqStyleTwo',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 21,
                'name' => 'Portfolio Gallery',
                'view' => 'frontend.components.portfolioComponents.portfolioGallery',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 22,
                'name' => 'Privacy Policy Content',
                'view' => 'frontend.components.privacyPolicyComponents.privacyPolicy',
              'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 23,
                'name' => 'Terms Of Use',
                'view' => 'frontend.components.termsOfUSeComponents.termsOfUse',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 24,
                'name' => 'Contact',
                'view' => 'frontend.contact',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 25,
                'name' => 'Blog Grid',
                'view' => 'frontend.blog.list',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 26,
                'name' => 'About',
                'view' => 'frontend.about',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 27,
                'name' => 'Price',
                'view' => 'frontend.price',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 28,
                'name' => 'Services',
                'view' => 'frontend.services',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 29,
                'name' => 'Sign In',
                'view' => 'frontend.signin',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 30,
                'name' => 'Gallery',
                'view' => 'frontend.gallery',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 31,
                'name' => 'Terms & Condition',
                'view' => 'frontend.termscondition',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 32,
                'name' => 'Faq',
                'view' => 'frontend.faq',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 33,
                'name' => 'Checkout',
                'view' => 'frontend.checkout',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 34,
                'name' => 'Cart View',
                'view' => 'frontend.cartview',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 35,
                'name' => 'Privacy',
                'view' => 'frontend.privacy',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 36,
                'name' => 'Bloglist',
                'view' => 'frontend.blog.list',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 37,
                'name' => 'Blogdetails',
                'view' => 'frontend.blog.details',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 38,
                'name' => 'Course',
                'view' => 'frontend.course.index',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 39,
                'name' => 'Course Details',
                'view' => 'frontend.course.details',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 40,
                'name' => 'Reset Password',
                'view' => 'frontend.resetpassword',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ], [
                'id' => 41,
                'name' => 'Payment',
                'view' => 'frontend.payment.payment',
                'created_at' => $created_at,
                'updated_at' => $updated_at,
            ]
        ]);
       
    }
}
