<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class ShippingCountrySeeder extends Seeder
{
    public function run()
    {
        $nowtime = now()->toDateTimeString();
        DB::table('shipping_countries')->insert([
            [
                'country' => 'Bangladesh',
                'created_at' => $nowtime,
                'updated_at' => $nowtime
            ],
            [
                'country' => 'United States',
                'created_at' => $nowtime,
                'updated_at' => $nowtime
            ],
            [
                'country' => 'India',
                'created_at' => $nowtime,
                'updated_at' => $nowtime
            ],
        ]);
    }
}
