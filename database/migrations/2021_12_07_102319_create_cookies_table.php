<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCookiesTable extends Migration
{
    public function up()
    {
        Schema::create('cookies', function (Blueprint $table) {
            $table->id();
            $table->string('cookie_status');
            $table->string('cookie_button');
            $table->string('cookie_confirmation');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('cookies');
    }
}
