<?php

use App\Models\Setting;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSettingsTable extends Migration
{
    public function up()
    {
        Schema::create('settings', function (Blueprint $table) {
            $table->id();
            $table->string('site_name');
            $table->string('site_time_zone');
            $table->string('site_currency');
            $table->string('site_currency_icon');
            $table->tinyInteger('site_currency_direction')->default(Setting::LEFT);
            $table->string('site_direction');
            $table->string('site_logo')->nullable();
            $table->string('site_favicon')->nullable();
            $table->string('section_icon')->nullable();
            $table->string('footer_logo')->nullable();
            $table->string('breadcumb')->nullable();
            $table->string('user_avatar')->nullable();
            $table->string('testimonial_background')->nullable();
            $table->integer('blog_pagination')->default(2);
            $table->integer('blog_search_pagination')->default(2);
            $table->integer('blog_archive_pagination')->default(2);
            $table->integer('blog_tag_search_pagination')->default(2);
            $table->integer('blog_category_pagination')->default(2);
            $table->integer('blog_comment_pagination')->default(2);
            $table->integer('course_pagination')->default(2);
            $table->integer('course_search_pagination')->default(2);
            $table->integer('product_pagination')->default(2);
            $table->integer('product_search_pagination')->default(2);
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('settings');
    }
}
