<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSectionTitlesTable extends Migration
{
    public function up()
    {
        Schema::create('section_titles', function (Blueprint $table) {
            $table->id();
            $table->string('servicetitle')->nullable();
            $table->text('servicesubtitle')->nullable();
            $table->string('trainingtitle')->nullable();
            $table->text('trainingsubtitle')->nullable();
            $table->string('blogtitle')->nullable();
            $table->text('blogsubtitle')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('section_titles');
    }
}
