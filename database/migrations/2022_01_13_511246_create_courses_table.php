<?php

use App\Models\Course;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateCoursesTable extends Migration
{
    public function up()
    {
        Schema::create('courses', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('image')->nullable();
            $table->tinyInteger('status')->default(Course::ACTIVE);
            $table->double('price')->default(0);
            $table->double('offer_price')->default(0);
            $table->tinyInteger('level')->default(Course::BEGINNER);
            $table->tinyInteger('is_paid')->default(Course::PAID);
            $table->string('slug');
            $table->foreignId('coursecategory_id')->constrained('coursecategories')->cascadeOnDelete();
            $table->text('description')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('courses');
    }
}
