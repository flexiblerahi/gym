<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateShippingCountriesTable extends Migration
{
    public function up()
    {
        Schema::create('shipping_countries', function (Blueprint $table) {
            $table->id();
            $table->string('country');
            $table->timestamps();
        });
    }
    
    public function down()
    {
        Schema::dropIfExists('shipping_countries');
    }
}
