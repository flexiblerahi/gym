<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePageItemsTable extends Migration
{
    public function up()
    {
        Schema::create('page_items', function (Blueprint $table) {
            $table->id();
            $table->string('name');
            $table->string('view');
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('page_items');
    }
}
