<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateAboutsTable extends Migration
{
    public function up()
    {
        Schema::create('abouts', function (Blueprint $table) {
            $table->id();
            $table->Text('icon')->nullable();
            $table->string('title');
            $table->string('subtitle');
            $table->text('description');
            $table->string('bottom_text');
            $table->string('first_image');
            $table->string('second_image');
            $table->string('third_image');
            $table->string('signature')->nullable();
            $table->Text('first_video')->nullable();
            $table->Text('second_video')->nullable();
            $table->json('item')->nullable();
            $table->json('client_section')->nullable();
            $table->timestamps();
        });
    }

    public function down()
    {
        Schema::dropIfExists('abouts');
    }
}
