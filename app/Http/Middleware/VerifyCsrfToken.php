<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        '/user/sslcommerz-pay','/user/sslcommerz-pay-via-ajax','/user/sslcommerz-success','/user/sslcommerz-failed','/user/sslcommerz-cancel','/user/sslcommerz-ipn'
    ];
}
