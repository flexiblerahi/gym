<?php

namespace App\Http\Middleware;

use Illuminate\Auth\Middleware\Authenticate as Middleware;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Session;

class Authenticate extends Middleware
{

    protected function redirectTo($request)
    {
        $routes= array('checkout', 'user.productreview', 'payment');
        if($request->ajax()) Session::put('previousurl', $request->url);
        if (!$request->expectsJson()) {
            if(in_array(Route::currentRouteName(), $routes)) Session::put('previousurl', route(Route::currentRouteName()));
            return route('user.login');
        }
    }
}
