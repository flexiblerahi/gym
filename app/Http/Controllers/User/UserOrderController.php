<?php

namespace App\Http\Controllers\User;

use App\DataTables\User\OrderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Order;

class UserOrderController extends Controller
{
    
     public function index(OrderDataTable $orderDataTable) //view('user.order.index')
    {
        return $orderDataTable->render('user.order.index');
    }
    public function show(Order $order)
    {
        return view('user.order.show', compact('order'));
    }
}
