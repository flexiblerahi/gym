<?php

namespace App\Http\Controllers\User;

use App\DataTables\User\TransactionDataTable;
use App\Http\Controllers\Controller;

class TransactionController extends Controller
{

    public function index(TransactionDataTable $transactionDataTable) //view('user.transaction.transaction')
    {
        return $transactionDataTable->render('user.transaction.transaction');
    }

}
