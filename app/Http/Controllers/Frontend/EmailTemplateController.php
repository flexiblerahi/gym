<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Mail\SubscribeMail;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Mail;

class EmailTemplateController extends Controller
{
    public function subscription(Request $request)
    { 
        $checkMail = DB::table('subscribers')->where(['email' => $request->email, 'is_confirmed' => Subscriber::CONFIRMED])->get();
        if(count($checkMail)>0) return response()->json(['type' => 'error', 'message' => trans('Already Subscribed')]);
        
        $rules = [
            'email' => 'required|email',
        ];
        $customMessages = [
            'email.required' => trans('Email is required!'),
            'email.email' => trans('Valid email is required'),
        ];
        $this->validate($request, $rules, $customMessages);
        $data = [
            'title' => trans('For Verify Email'),
            'body' => trans('Please Click this button.'),
            'url' => route('verify', $request->email),
            'btn' => trans('Verify Email'),
            'greetings' => trans('Thanks,')
        ];
        // Mail::to($request->email)->send(new SubscribeMail($data));
        
        $subscriber  = Subscriber::create(['email' => $request->email, 'is_confirmed' => Subscriber::NOTCONFIRMED]);
        return response()->json(['type' => 'success', 'message' => trans('Please Check your Mail')]);
    }

    public function verify(Request $request)
    { 
        $subscriber = Subscriber::where('email', $request->email)->get();
        if($subscriber->count()>0) {
            $subscriber = $subscriber->first();
            $subscriber->is_confirmed = Subscriber::CONFIRMED;
            $subscriber->save();
        }
        return redirect()->route('home');
    }
}
