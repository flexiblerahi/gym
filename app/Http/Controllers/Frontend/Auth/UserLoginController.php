<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use App\Models\PageComponent;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Session;

class UserLoginController extends Controller
{
    public function __construct(PageComponent $pageComponent)
    {
        $this->pageComponent = $pageComponent;
        // $this->middleware('verified');
    }

    public function userLoginForm()
    {
        $signin = 29;
        $pagecomponents = $this->pageComponent->where('id', $signin)->get(['view']);     
        return view('frontend.dynamicPage', compact('pagecomponents'));
    }

    public function userCheckLogin(Request $request)
    {
        $this->validate($request, 
        [
            'email' => ['required', function ($attribute, $value, $fail) {
                $email = User::where('email', $value)->first();
                if ($email == null) {
                    $fail($attribute . ' does not exist.');
                }
            },
        ],
        'password' =>['required'],
        ]);
        $previousurl = (Session::get('previousurl')  == null) ? route('user.dashboard') : Session::get('previousurl');
        Session::forget('previousurl');
        if (Auth::guard('web')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {
            return response()->json(['success' => 'Login Successful!','previousurl' =>$previousurl]);
        }
        return redirect()->back()->withInput($request->only('email', 'remember'))->withErrors([
            'error' => 'Wrong password.',
        ]);
    }

    public function userLogout()
    {
        Session::flush();
        Auth::logout();
        return redirect('signin');
    }
}
