<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Models\PageComponent;
use App\Models\User;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\ResetsPasswords;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class ResetPasswordController extends Controller
{
    public function __construct(PageComponent $pageComponent)
    {
        $this->pageComponent = $pageComponent;
        // $this->middleware('verified');
    }
    /*
    |--------------------------------------------------------------------------
    | Password Reset Controller
    |--------------------------------------------------------------------------
    |
    | This controller is responsible for handling password reset requests
    | and uses a simple trait to include this behavior. You're free to
    | explore this trait and override any methods you wish to tweak.
    |
    */
    use ResetsPasswords;
    public function showResetForm(Request $request)
    {
        $token = $request->route()->parameter('token');
        $email = DB::table('password_resets')->where(['token' => $token])->first();
        if ($email) {
            $resetpassword = 41;
            $pagecomponents = $this->pageComponent->where('id', $resetpassword)->get(['view']);     
            return view('frontend.dynamicPage', compact('pagecomponents'))->with(
                ['token' => $token, 'email' => $email->email]
            );
        }
        alert()->error('Oops!', 'Invalid password reset link.');
        return redirect('/');
    }
    public function resetPassword(Request $request)
    {   
         $request->validate([
            'email' => 'required|email',
            'password' => 'required',
        ]);
        $password = $request->password;

        $tokenData = DB::table('password_resets')
            ->where('token', $request->token)->first();
        if (!$tokenData) return response()->json(['type'=> "error", "message" => "Invalid Token"]);
        $user = User::where('email', $tokenData->email)->first();
        if(!$user)  return response()->json(['type'=> "error", "message" => "Email not found"]); 
        // $user->password = Hash::make($password);
        $user->password = Hash::make($password);
        $user->update();
        Auth::login($user);
        DB::table('password_resets')->where('email', $user->email)
            ->delete();
        return response()->json(['type'=> "success", "message" => "Password Update Success fully"]);
    }
    /**
     * Where to redirect users after resetting their password.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;
}
