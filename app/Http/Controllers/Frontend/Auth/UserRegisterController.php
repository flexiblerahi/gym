<?php

namespace App\Http\Controllers\Frontend\Auth;

use App\Http\Controllers\Controller;
use App\Mail\SendEmail;
use App\Models\BillAddress;
use App\Models\EmailTemplate;
use App\Providers\RouteServiceProvider;
use App\Models\User;
use App\Models\PageComponent;
use Illuminate\Http\Request;
use Illuminate\Auth\Events\Registered;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;

class UserRegisterController extends Controller
{
     /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */
    
    protected $redirectTo = RouteServiceProvider::HOME;
    use RegistersUsers;

    public function __construct(PageComponent $pageComponent)
    {
        $this->pageComponent = $pageComponent;
        $this->middleware('guest');
    }
   
    public function index()
    {
        $signin = 29;
        $pagecomponents = $this->pageComponent->where('id', $signin)->get(['view']);     
        return view('frontend.dynamicPage', compact('pagecomponents'));
    }

    public function validation($request)
    {
        $rules = [
            'name' => ['required', 'min:2'],
            'email' => ['required', 'email', 'unique:users,email'],
            'password' => ['required'],
            'confirm_password' => ['same:password', 'required']
        ];
        $customMessages = [
            'name.required' => trans('name is required!'),
            'name.min' => trans('Minimum character is required'),
            'email.required' => trans('Email is required'),
            'email.email' => trans('Email must be valid email'),
            'email.unique' => trans('Email already exist.'),
            'password.required' => trans('Password is required!'),
            'confirm_password.required' => trans('Confirm Password is required'),
            'confirm_password.same' => trans('Password does not match.'),
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function store(Request $request) 
    {
        $this->validation($request);

        $user=User::create([
            'name' => $request->name,
            'email' => $request->email,
            'password' => Hash::make($request->password),
        ]);
        BillAddress::create([
            'user_id' =>$user->id,
        ]);

        $template = EmailTemplate::find(3);
        $body = str_replace('{name}', $user->name, $template->description);
        $body = str_replace('{website}', GetSetting('site_name'), $body);
        Mail::send('frontend.emailHtml', ['body' =>html_entity_decode($body)], function ($message) use ($user, $template) {
            $message->to($user->email);
            $message->subject($template->subject);
        }); //view('frontend.emailHtml')
        event(new Registered($user));
        auth()->login($user);
        return response()->json(['success' => 'Registration Successful! Please Verify Your mail']);
        return $user;
        
    }
}
