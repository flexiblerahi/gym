<?php

namespace App\Http\Controllers\frontend;

use App\Models\Course;
use App\Http\Controllers\Controller;
use App\Models\Coursecategory;
use App\Models\CourseReview;
use App\Models\PageComponent;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseController extends Controller
{
    public function __construct(PageComponent $pageComponent, CourseReview $coursereview)
    {
        $this->pageComponent = $pageComponent;
        $this->coursereview = $coursereview;
    }

    public function index(Request $request) //view('frontend.course.index')
    {
        $filterpaginate = array();
        $filterprevnext = '';
        $paginatenumber = 0;
        $level = $request->level;
        $is_paid = $request->is_paid;
        $coursecategory = $request->coursecategory;
        $requestarray = array('status' => Course::ACTIVE);
        $courses = Course::with('coursecategory', 'coursereviews');

        if ($coursecategory != null) {
            $filterpaginate['coursecategory'] = $coursecategory;
            $filterprevnext .= '&coursecategory='. $coursecategory;
            $requestarray['coursecategory_id'] = $coursecategory;
        }

        $courses = $courses->where($requestarray);

        if($request->course)  $courses = $courses->where('name', 'like', $request->course . '%');
        if(!empty($level)) {
            $filterpaginate['level'] = $level;
            $courses = $courses->whereIn('level', $level);
            foreach ($level as $key =>$le) {
                $filterprevnext .= '&level%5B%5D='.$le;
            }
        }

        if(!empty($is_paid)) {
            $filterpaginate['is_paid'] = $is_paid;
            $courses = $courses->whereIn('is_paid', $is_paid);
            foreach ($is_paid as $key =>$isp) {
                $filterprevnext .= '&is_paid%5B%5D='.$isp;
            }
        }

        if($request->sortby) $courses = $courses->orderBy(explode('_',($request->sortby))[0],explode('_',($request->sortby))[1]);
        else $courses = $courses->latest();

        $courses = $courses->paginate(GetSetting('course_pagination'));
        $paginatenumber = GetSetting('course_pagination');
        
        $currency = Setting::get(['site_currency_icon', 'site_direction'])->first();
        if($request->ajax()) return view('frontend.course.coursepaginate', compact('courses', 'currency', 'coursecategory', 'filterprevnext', 'filterpaginate', 'paginatenumber'))->render();
        
        $pagecomponents = $this->pageComponent->where('id', Course::COURSEPAGEID)->get(['view']);
        $coursecategories = Coursecategory::where('status', Coursecategory::ACTIVE)->get(['id', 'name']);

        return view('frontend.dynamicPage', compact('pagecomponents', 'currency', 'courses', 'coursecategories', 'coursecategory', 'filterprevnext', 'filterpaginate', 'paginatenumber'));
    }

    public function show($slug, Request $request) //view('frontend.course.details') //view('frontend.course.coursepaginate')
    {
        $userid = Auth::id();
        $course = Course::where('slug', $slug)->firstorfail();
        $currency = Setting::get(['site_currency_icon', 'site_direction'])->first();
        $pagecomponents = $this->pageComponent->where('id', Course::COURSEDETAILSPAGEID)->get(['view']);
        $coursecategories = Coursecategory::with('course')->where('status', Course::ACTIVE)->get(['id', 'name']);
        $recentcourses = Course::where('status', Course::ACTIVE)->whereNotIn('id', [$course->id])->latest()->take(3)->get(['id', 'slug', 'name', 'image', 'created_at']);
        $coursereviews = CourseReview::with('user')->where(['course_id' => $course->id, 'is_approved' => CourseReview::APPROVED])->paginate(GetSetting('course_pagination'));
        if($request->ajax()) return view('frontend.course.commentpage', compact('coursereviews'))->render();
        return view('frontend.dynamicPage', compact('pagecomponents', 'currency', 'course', 'recentcourses', 'coursecategories', 'coursereviews', 'userid'));
    }
}
