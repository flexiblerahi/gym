<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\ContactMessage;
use Illuminate\Http\Request;

class ContactMessageController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'name' => ['required'],
            'email' => ['required', 'email', 'max:255'],
            'phone' => ['required'],
            'message' => ['required'],
        ];
        $customMessages = [
            'name.required' => trans('Name is required!'),
            'email.required' => trans('Email is required!'),
            'email.email' => trans('Email must be type of mail'),
            'phone.required' => trans('Phone is Required!'),
            'message.required' => trans('Message is Required!'),
        ];
        $this->validate($request, $rules, $customMessages);

        $input['name']    = $request->name;
        $input['email']   = $request->email;
        $input['phone']   = $request->phone;
        $input['message'] = $request->message;
        ContactMessage::create($input);
        return response()->json(['success' => 'Message Created Successfully', 200]);
    }
}
