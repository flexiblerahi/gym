<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\BillAddress;
use Illuminate\Http\Request;
use App\Models\Coupon;
use App\Models\Course;
use App\Models\OrderItem;
use App\Models\Product;
use App\Models\Shipping;
use App\Models\ShopSetting;
use App\Models\PageComponent;
use App\Models\Setting;
use App\Models\ShippingCountry;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Session;

class BillingController extends Controller
{
    public function checkout() //view('frontend.checkout')
    {
        $checkout = 33;
        $currency = Setting::get(['site_currency_icon', 'site_currency_direction'])->first();
        $currency->is_left = Setting::LEFT; 
        $pagecomponents = PageComponent::where('id', $checkout)->get(['view']);
        $products = (Session::get('products')  == null) ? array() : Session::get('products');
        $courses = (Session::get('courses')  == null) ? array() : Session::get('courses');

        if (count($products) <1 && count($courses) < 1) {
            Session::put('errormsg', trans('Your cart is empty'));
            return redirect()->route('product.index'); 
        }

        $coupon = Session::get('coupon');
        $shipping = (Session::get('shipping')  == null) ? array() : Session::get('shipping');
        $shippingCountries = ShippingCountry::all(['country']);
        $is_paid = array('free' => Course::NONPAID);
        $tax= ShopSetting::first(['tax']);
        $billaddress = BillAddress::where('user_id', Auth::id())->get()->first();
        return view('frontend.dynamicPage', compact('pagecomponents', 'products', 'currency', 'courses', 'coupon', 'shipping', 'is_paid', 'tax', 'shippingCountries', 'billaddress'));
    }

    public function couponapply(Request $request)
    {
        $coupon = Coupon::where(['code' => $request->code, 'status' => Coupon::ACTIVE])->get(['amount', 'type']);
        if($coupon->count() > 0) return response()->json(array('coupon' => $coupon->first(), 'inprice' => Coupon::INPRICE));
        return response()->json(array('error' => trans(Coupon::NOTFOUNDMSG)));
    }

    public function cartview() //view('frontend.cartview')
    {
        $cartview = 34;
        $currency = Setting::get(['site_currency_icon', 'site_currency_direction'])->first();
        $currency->is_left = Setting::LEFT; 
        $pagecomponents = PageComponent::where('id', $cartview)->get(['view']);
        $products =  (Session::get('products')  == null) ? array() : Session::get('products')->toArray();
        $courses =  (Session::get('courses')  == null) ? array() : Session::get('courses')->toArray();
        if (count($products) <1 && count($courses) < 1) {
            Session::put('errormsg', trans('Your cart is empty'));
            return redirect()->route('product.index'); 
        }
        $shippings = Shipping::where('status', Shipping::ACTIVE)->get();
        $is_paid = array('free' => Course::NONPAID);
        $tax = ShopSetting::first(['tax']);
        return view('frontend.dynamicPage', compact('pagecomponents', 'products', 'courses', 'currency', 'is_paid', 'shippings', 'tax'));
    }

    public function addcart(Request $request)
    {
        if($request->type == 'product') {
            $products = Session::get('products') == null ? array() : Session::get('products')->toArray();
            if(in_array($request->product_id, array_column($products, 'id'))) return response()->json(['type'=> 'warning', 'message' => 'Already added to the cart.']);
            $product = Product::where('id', $request->product_id)->get(['id', 'name', 'image', 'price', 'discount_price', 'stock', 'category_id'])->first();
            if($product->stock<1) return response()->json(['type'=> 'error', 'message' => 'Out of Stock']);
            $product->quantity = $request->quantity;
            $product->price = ($product->discount_price > 0) ? $product->discount_price : $product->price;
            $product->category = $product->category_id == null ? null : $product->getCategory->name; 
            unset($product->getCategory, $product->discount_price);
            array_push($products, $product);
            Session::put('products', collect($products));
        } else {
            $courses = Session::get('courses') == null ? array() : Session::get('courses')->toArray();
            if(in_array($request->course_id, array_column($courses, 'id'))) return response()->json(['type'=> 'warning', 'message' => 'Already added to the cart.']);
            $course = Course::where('id', $request->course_id)->get(['id', 'name', 'image', 'price', 'offer_price', 'is_paid'])->first();
            $course->price = ($course->offer_price>0) ? $course->offer_price : $course->price;
            array_push($courses, $course);
            Session::put('courses', collect($courses));
        }
        $totalproduct = Session::get('products')  == null ? 0 : Session::get('products')->count();
        $totalcourse = Session::has('courses') == null ? 0 : Session::get('courses')->count();
        return response()->json(['type'=> 'success', 'message' => 'Add to Cart Successfully', 'total' => $totalproduct + $totalcourse]);
    }

    public function removecart(Request $request)
    {
        $products =Session::get('products') == null ? array() : Session::get('products')->toArray();
        $courses = Session::get('courses') == null ? array() : Session::get('courses')->toArray();

        if($request->type == "product") {
            unset($products[array_search($request->product_id, array_column($products, 'id'))]);
            Session::put('products', collect($products));
        } else if($request->type == "course") {
            unset($courses[array_search($request->course_id, array_column($courses, 'id'))]);
            Session::put('courses', collect($courses));
        } else {
            Session::forget(['courses', 'products']);
            return response()->json(['total' => 0]);
        }
        $total = count($products) + count($courses);
        return response()->json(['total' => $total]);
    }

    public function proceedcheckout(Request $request)
    {
        $shipping = $request->shipping;
        $products = $request->products;
        if($request->coupon) Session::put('coupon', $request->coupon);
        if($request->shipping) Session::put('shipping', ['id' => $shipping['id'], 'name'=> $shipping['title'], 'cost' => $shipping['cost']]);
        if($request->products) Session::put('products', collect($products));
        if($request->courses) Session::put('courses', collect($request->courses));
        $msg = trans('Add To cart view Successfully');
        return response()->json(['type' => 'success', 'message' => $msg, 'products' => $products, 'courses' => $request->courses]);
    }

    public function payment() //view('frontend.payment.payment')
    {
        if(!Session::get('courses')) if(!Session::get('products'))return redirect()->route('product.index');
        if(!Session::get('shipping')) return redirect()->route('cartview');
        $total = 0;
        $subtotal = 0;
        $totaltax = 0;
        $cart = array();
        
        $tax = ShopSetting::first(['tax']);
        $tax = ($tax)? $tax : array('tax'=>1); 
        
        $products = (Session::get('products')  == null) ? array() : Session::get('products')->toArray();
        if(count($products)> 0) {
            foreach($products as $product) {
                $product['type'] =  OrderItem::PRODUCT;
                $total = $total + (intval($product['price']) * intval($product['quantity']));
                $totaltax = $totaltax + ((floatval($product['quantity'] * $product['price'])/100) * ($tax['tax']));
                array_push($cart, $product);
            }
        }

        $courses =  (Session::get('courses')  == null) ? array() : Session::get('courses')->toArray();
        if(count($courses)> 0) {
            foreach($courses as $course) {
                $course['type'] =  OrderItem::COURSE;
                $total = ($course['is_paid'] == Course::PAID) ? ($total + intval($course['price'])) : $total + 0;
                $totaltax = $totaltax + ((floatval($course['price'])/100) * ($tax['tax']));
                array_push($cart, $course);
            }
        }

        $subtotal = $total;
        $total = $total + $totaltax;

        $shipping = Session::get('shipping');
        $total = $total + intval($shipping['cost']);

        $coupon = Session::get('coupon');
        if($coupon) {
            $cpndata = $coupon['coupon'];
            $total = ($cpndata['type'] == Coupon::INPRICE) ? $total - intval($cpndata['amount']) : ($total - (($total/100) * intval($cpndata['amount'])));
        } else $coupon = array('coupon'=> ['amount' => 0, 'type' => Coupon::INPRICE], 'inprice' => Coupon::INPRICE);
        
        $pagecomponents = PageComponent::where('id', 41)->get(['view']);
        return view('frontend.dynamicPage', compact('pagecomponents', 'total', 'shipping', 'coupon', 'subtotal', 'totaltax', 'cart'));
    }

    public function placeorder(Request $request)
    {
        $rules = [
            'billing_name' => ['required'],
            'billing_phone' => ['required'],
            'billing_email' => ['required'],
            'billing_country' => ['required'],
            'billing_city' => ['required'],
            'billing_post_code' => ['required'],
            'billing_address' => ['required'],
        ];
        $customMessages = [
            'billing_name.required' => trans('Billing Name is required'),
            'billing_phone.required' => trans('Billing Phone is required'),
            'billing_email.required' => trans('Billing Email is required'),
            'billing_country.required' => trans('Billing Country is required'),
            'billing_city.required' => trans('Billing City is required'),
            'billing_post_code.required' => trans('Billing post-code is required'),
            'billing_address.required' => trans('Billing Address is required'),
        ];
        $this->validate($request, $rules, $customMessages);
        $is_shipping = $request->is_shipping ? BillAddress::NOTDIFFERENT : BillAddress::DIFFERENTSHIPPING; 
        $bill = BillAddress::where('user_id', auth()->id())->get()->first();
        // dd($bill);
        $bill->id = $bill->id;
        $bill->billing_name = $request->input('billing_name');
        $bill->billing_phone = $request->input('billing_phone');
        $bill->billing_email = $request->input('billing_email');
        $bill->billing_country = $request->input('billing_country');
        $bill->billing_city = $request->input('billing_city');
        $bill->billing_post_code = $request->input('billing_post_code');
        $bill->billing_address = $request->input('billing_address');
        $bill->is_shipping = $is_shipping;
        $bill->shipping_name = ($is_shipping == BillAddress::NOTDIFFERENT)  ? $request->input('shipping_name') : $request->input('billing_name');
        $bill->shipping_phone = ($is_shipping == BillAddress::NOTDIFFERENT)  ? $request->input('shipping_phone'): $request->input('billing_phone');
        $bill->shipping_email = ($is_shipping == BillAddress::NOTDIFFERENT)  ? $request->input('shipping_email') : $request->input('billing_email');
        $bill->shipping_country = ($is_shipping == BillAddress::NOTDIFFERENT)  ? $request->input('shipping_country'): $request->input('billing_country');
        $bill->shipping_city = ($is_shipping == BillAddress::NOTDIFFERENT)  ? $request->input('shipping_city') : $request->input('billing_city');
        $bill->shipping_post_code = ($is_shipping == BillAddress::NOTDIFFERENT)  ? $request->input('shipping_post_code') : $request->input('billing_post_code');
        $bill->shipping_address = ($is_shipping == BillAddress::NOTDIFFERENT)  ? $request->input('shipping_address') : $request->input('billing_address');
        $bill->save();
        return redirect()->route('payment');
    }
}
