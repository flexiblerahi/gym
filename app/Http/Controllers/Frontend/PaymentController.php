<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\EmailTemplate;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Stripe;
use Razorpay\Api\Api;
use Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;
use Mollie\Laravel\Facades\Mollie;

use App\Models\Transaction as ModelsTransaction;
use Illuminate\Support\Facades\Mail;

class PaymentController extends Controller
{
    public function stripe(Request $request)
    {
        $cents = $request->total * 100;
        Stripe\Stripe::setApiKey(PaymentGateway('stripe_api_secret_key'));
        $response = Stripe\Charge::create([
            "amount" => $cents,
            "currency" => GetSetting('site_currency'),
            "source" => $request->stripeToken,
            "description" => env('APP_NAME')
        ]);
        $responseJson = $response->jsonSerialize();
        $transaction_id = $responseJson['balance_transaction'];
        $total_amount = round($request->total / PaymentGateway('stripe_rate') , 2);
        $tax = $request->tax;
        return payment($tax, $total_amount, $transaction_id, $request, 'Stripe');    
    }

    public function razorpay(Request $request)
    {
        $input = $request->except(['total', 'cart']);
        $api = new Api(PaymentGateway('razorpay_key_id'), PaymentGateway('razorpay_key_secret'));
        $payment = $api->payment->fetch($input['razorpay_payment_id']);
        if (count($input) && !empty($input['razorpay_payment_id'])) {
            try {
                $response = $api->payment
                    ->fetch($input['razorpay_payment_id'])
                    ->capture(array('amount' => $payment['amount']));
                $payId = $response->id;
                $total_amount = round($request->total / PaymentGateway('razorpay_rate') , 2);
                $tax = $request->tax;
                return payment($tax, $total_amount, $payId, $request, 'Razorpay');
            } catch (Exception $e) {
                toast(trans('frontend.Payment Error!'), 'error')->width('350px');
                return Redirect()->back();
            }
        }
    }
    
    public function mollie(Request $request)
    {
        Mollie::api()->setApiKey(PaymentGateway('mollie_api_key'));
        $mollie_price = round($request->total, 2);
        $payment = Mollie::api()->payments()->create([
            'amount' => [
                'currency' => GetSetting('site_currency'),
                'value' => '' . sprintf('%0.2f', $mollie_price) . '',
            ],
            'description' => env('APP_NAME'),
            'redirectUrl' => route('user.mollie-notify'),
        ]);
        $payment = Mollie::api()->payments()->get($payment->id);
        session()->put('payment_id', $payment->id);
        session()->put('cart', $request->cart);
        session()->put('total', $request->total);
        session()->put('tax', $request->tax);
        return redirect($payment->getCheckoutUrl(), 303);
    }

    public function mollie_notify(Request $request)
    {
        Mollie::api()->setApiKey(PaymentGateway('mollie_api_key'));
        $payment = Mollie::api()->payments->get(session()->get('payment_id'));
        $total_amount = round($request->total / PaymentGateway('mollie_rate') , 2);
        $tax = session()->get('tax');
        if ($payment->isPaid()) return payment($tax, $total_amount, session()->get('payment_id'), $request, 'Mollie');
        toast(trans('frontend.Payment Failed!'), 'error')->width('350px');
        return  redirect()->route('user.dashboard'); 
    }

    public function paystack(Request $request)
    {
        $reference = $request->reference;
        $secret_key = PaymentGateway('paystack_secret_key');
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://api.paystack.co/transaction/verify/$reference",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_SSL_VERIFYHOST => 0,
            CURLOPT_SSL_VERIFYPEER => 0,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "Authorization: Bearer $secret_key",
                "Cache-Control: no-cache",
            ),
        ));
        $response = curl_exec($curl);
        curl_close($curl);
        $final_data = json_decode($response);
        $total_amount = round($request->get('amp;total') / PaymentGateway('paystack_rate') , 2);
        $tax = $request->get('amp;tax');
        if ($final_data->status == true) return payment($tax, $total_amount, $request->trx_id, $request, 'PayStack');   
        else {
            toast(trans('frontend.Payment Failed!'), 'error')->width('350px');
            return redirect()->route('user.dashboard');
        }
    }
    
    public function bank(Request $request)
    {
        $rules = [
            'bank_transaction_info' => 'required',
        ];
        $customMessages = [
            'bank_transaction_info.required' => trans('Bank Transaction Info is required!'),
        ];
        $this->validate($request, $rules, $customMessages);
        $total_amount = $request->total;
        $tax = $request->tax;   

        return payment($tax, $total_amount, 'N/A', $request, 'Bank');   
    }

    public function freepurches(Request $request)
    {
        $cart = $request->courses;
        $order = new Order();
        $order->user_id = auth()->id();
        $order->status = 'pending';
        $order->bank_info = 'Free';
        $order->total_amount = 0;
        $order->tax = 0;
        $order->shipping_fee = 0;
        $order->save();
        foreach ($cart as $product) {
            $item = new OrderItem();
            $item->order_id = $order->id;
            $item->item_id = $product['id'];
            $item->item_price = 0;
            $item->type = OrderItem::COURSE;
            $item->item_qty = OrderItem::COUSEQUANTITY;
            $item->save();
        }
        $transaction = new ModelsTransaction();
        $transaction->order_id = $order->id;
        $transaction->payment_method = 'Free';
        $transaction->transaction_id = 'Free';
        $transaction->user_id = auth()->id();
        $transaction->amount = 0;
        $transaction->currency = GetSetting('site_currency');
        $transaction->save();
        $template = EmailTemplate::find(2);
        $body = str_replace('{name}', auth()->user()->name, $template->description);
        if(str_contains($body, '{transaction_id}')) $body = str_replace('{transaction_id}', 'Free', $body);
        if(str_contains($body, '{total_amount}')) $body = str_replace('{total_amount}' , currencyPosition(0), $body);
        if(str_contains($body, '{payment_method}')) $body = str_replace('{payment_method}', 'Free', $body);
        Mail::send('frontend.emailHtml', ['body' =>html_entity_decode($body)], function ($message) use ($request, $template) {
            $message->to(auth()->user()->email);
            $message->subject($template->subject);
        });
        Session::forget(['courses', 'products', 'shipping']);
        if(Session::get('coupon') != null) Session::forget('coupon');

        Session::put('successmsg', trans('Your Payment Successfully'));
        return response()->json(['type' => 'success', 'msg'=> trans('Course Buy Successfully')]);
    }
}