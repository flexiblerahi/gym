<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\ProductReview;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProductReviewController extends Controller
{
    public function store(Request $request)
    {
        $rules = [
            'rating' => ['required'],
            'comment' => ['required']
        ];
        $customMessages = [
            'rating.required' => trans('Rating is Required!'),
            'comment.required' => trans('Comment is Required!'),
        ];
        $this->validate($request, $rules, $customMessages);
        $input = array();
        $input['user_id'] =Auth::id();
        $input['comment'] = $request->comment;
        $input['rating'] = $request->rating;
        $input['product_id'] = $request->product_id;
        $input['status'] = ProductReview::PENDING;
        $ispurches = Order::whereRelation('items', ['order_items.item_id'=>  $request->product_id, 'order_items.type' => OrderItem::PRODUCT])
        ->with('items', fn ($query) => $query->where(['order_items.item_id'=>  $request->product_id, 'order_items.type' => OrderItem::PRODUCT]))
        ->where('user_id', auth()->id())->exists();
        if(!$ispurches) return response()->json(['error' => trans(ProductReview::ISPURCHASE)]);
        if(ProductReview::where(['product_id' => $input['product_id'], 'user_id' => $input['user_id']])->count() > 0) {
            return response()->json(['error' => trans(ProductReview::ALREADYCOMMENT)]);
        }
        ProductReview::create($input);
        return response()->json(['success' => trans(ProductReview::SUCCESSCREATE)]);
    }

}
