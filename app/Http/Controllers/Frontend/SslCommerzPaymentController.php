<?php

namespace App\Http\Controllers\Frontend;

use DB;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Library\SslCommerz\SslCommerzNotification;
use App\Models\BillAddress;

class SslCommerzPaymentController extends Controller
{
    public function index(Request $request)
    {
        $post_data = array();
        $post_data['total_amount'] = $request->total; # You cant not pay less than 10
        $post_data['currency'] = PaymentGateway("ssl_commerz_currency");
        $post_data['tran_id'] = uniqid(); // tran_id must be unique

        $bill = BillAddress::where('user_id', auth()->id())->get()->first();
        # CUSTOMER INFORMATION]
        $post_data['cus_name'] = $bill->billing_name;
        $post_data['cus_email'] = $bill->billing_phone;
        $post_data['cus_add'] = $bill->billing_address;
        $post_data['cus_city'] = $bill->billing_city;
        $post_data['cus_postcode'] = $bill->billing_post_code;
        $post_data['cus_country'] = $bill->billing_country;
        $post_data['cus_phone'] = $bill->billing_phone;
        # SHIPMENT INFORMATION
        $bill = BillAddress::where('user_id', auth()->id())->get()->first();
        $post_data['ship_name'] = $bill->shipping_name;
        $post_data['ship_add1'] = $bill->billing_address;
        $post_data['ship_city'] = $bill->shipping_city;
        $post_data['ship_state'] = $bill->shipping_city;
        $post_data['ship_postcode'] = $bill->shipping_post_code;
        $post_data['ship_phone'] = $bill->shipping_phone;
        $post_data['ship_country'] = $bill->shipping_country;

        $post_data['shipping_method'] = "NO";

        $sslc = new SslCommerzNotification();
        # initiate(Transaction Data , false: Redirect to SSLCOMMERZ gateway/ true: Show all the Payement gateway here )
        $payment_options = $sslc->makePayment($post_data, 'hosted');

        if (!is_array($payment_options)) {
            print_r($payment_options);
            $payment_options = array();
        }
    }

    public function success(Request $request)
    {
        $tran_id = $request->input('tran_id');
        $post_data['tran_id'] = uniqid();
        $total_amount = round($request->total / PaymentGateway('ssl_commerz_rate') , 2);
        $tax = session()->get('tax');
        return payment($tax, $total_amount, $tran_id, $request, 'sslcommerz');
    }

    public function fail(Request $request)
    {
        toast(trans('frontend.Payment Faild!'), 'error')->width('350px');
        return redirect()->route('user.dashboard');
    }

    public function cancel(Request $request)
    {
        toast(trans('frontend.Payment Canceled!'), 'error')->width('350px');
        return redirect()->route('user.dashboard');
    }
}
