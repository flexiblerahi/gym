<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogComment;
use Illuminate\Http\Request;

class BlogCommentController extends Controller
{
    public function store(Request $request)
    {    
        $rules = [
            'name' => ['required'],
            'email' => ['required', 'email', 'max:255'],
            'comment' => ['required'],
        ];
        $customMessages = [
            'name.required' => trans('Name is required!'),
            'email.required' => trans('Email field is required!'),
            'email.email' => trans('Email must be type of mail'),
            'comment.required' => trans('Comment is Required!'),
        ];

        $this->validate($request, $rules, $customMessages);
        $blog = Blog::where('slug', $request->slug)->first();
        $input['blog_id']  = $blog->id;
        $input['name']    = $request->name;
        $input['email']   = $request->email;
        $input['status']  = BlogComment::CLOSE;
        $input['comment'] = $request->comment;
        BlogComment::create($input);
        return response()->json(['success' => 'Comment Created Successfully', 200]);
    }
}
