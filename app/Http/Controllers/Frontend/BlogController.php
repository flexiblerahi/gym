<?php

namespace App\Http\Controllers\frontend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\BlogCategory;
use App\Models\BlogComment;
use App\Models\PageComponent;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function __construct(PageComponent $pageComponent)
    {
        $this->pageComponent = $pageComponent;
    }

    public function index(Request $request)
    {
        $blogrid = 25;
        $pagecomponents = $this->pageComponent->where('id', $blogrid)->get(['view']);
        $blogs = Blog::latest()->where('status', Blog::ACTIVE);
        if ($request->category_id) $blogs = $blogs->where('category_id', $request->category_id);
        if ($request->blog_name) $blogs = $blogs->where('title', 'LIKE', "%{$request->blog_name}%");
        $blogs = $blogs->paginate(GetSetting('blog_pagination'));

        return view('frontend.dynamicPage', compact('pagecomponents', 'blogs'));
    }

    public function show($slug, Request $request) //view('frontend.blog.details')
    {
        $blogdetailsid = 37;
        $pagecomponents = $this->pageComponent->where('id', $blogdetailsid)->get(['view']);
        $blogdetails = Blog::where('slug', $slug)->firstorfail();
        $blogcategories = BlogCategory::with('getBlogs')->get();
        $newblogs = Blog::latest()->where('status', Blog::ACTIVE)->whereNotIn('id', [$blogdetails->id])->take(4)->get((['id', 'slug', 'title', 'image', 'created_at']));
        $blogcomments = BlogComment::where(['blog_id' => $blogdetails->id, 'status' => BlogComment::ACTIVE])->paginate(GetSetting('blog_comment_pagination'));
        // dd($blogdetails->id);
        if($request->ajax()) return view('frontend.blog.commentpage', compact('blogcomments'))->render();
        
        return view('frontend.dynamicPage', compact('pagecomponents', 'blogdetails', 'blogcomments', 'blogcategories', 'newblogs'));
    }

    public function search(Request $request)
    {
        $search = $request->get('query');
        $blogs = Blog::where(['status' => 1])
            ->where('title', 'LIKE', "%{$search}%")
            ->orWhere('description', 'LIKE', "%{$search}%")
            ->paginate(GetSetting('blog_pagination'));
        return view('frontend.blogSearch', compact('blogs'));
    }

    public function archive(Request $request)
    {
        $blogs = Blog::where(['status' => 1])
            ->where('created_at', 'LIKE', "%{$request->range}%")
            ->paginate(GetSetting('blog_pagination'));
        return view('frontend.blogArchive', compact('blogs'));
    }

    public function search_tag(Request $request)
    {
        $blogs = Blog::where(['status' => 1])->select("blogs.*")
            ->whereRaw("find_in_set('" . $request->tag . "',blogs.tags)")
            ->paginate(GetSetting('blog_pagination'));
        return view('frontend.blogTag', compact('blogs'));
    }

    public function category(Request $request)
    {
        $blogs = BlogCategory::where('slug', $request->slug)
            ->firstOrFail()
            ->getBlogs()
            ->where(['status' => 1])
            ->paginate(GetSetting('blog_pagination'));
        return view('frontend.blogCategory', compact('blogs'));
    }

    public function comment(Request $request)
    {
        $comment = new BlogComment();
        $comment->blog_id  = $request->blog;
        $comment->name    = $request->name;
        $comment->email   = $request->email;
        $comment->phone = $request->phone;
        $comment->status  = 0;
        $comment->comment = $request->comment;
        $comment->save();

        toast(trans('frontend.Comment added!'), 'success')->width('350px');
        return redirect()->back();
    }
}
