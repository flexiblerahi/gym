<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use Illuminate\Http\Request;
use App\Models\PageBuilder;
use App\Models\PageComponent;
use App\Models\ProductCategory;
use App\Models\ProductReview;
use App\Models\Setting;
use Illuminate\Support\Facades\Auth;

class ProductController extends Controller
{
    function __construct(PageComponent $pageComponent, PageBuilder $pagebuilder, Product $product, ProductReview $productreview)
    {
        $this->pageComponent = $pageComponent;
        $this->pagebuilder = $pagebuilder;
        $this->product = $product;
        $this->productreview = $productreview;
    }

    public function index(Request $request) //view('frontend.product.index') //view('frontend.product.productpaginate')
    {
        $page = $this->pagebuilder->where(['slug' => 'product'])->firstOrFail(['contents']);
        $elements = explode(',', $page->contents);
        $pagecomponents = $this->pageComponent->whereIn('id', $elements)->get(['view']);
       
        $category_id = $request->category_id; //if category id in search index
        $searchproduct = $request->product; // if product name in search index
        $products = $this->product->where('status', Product::ACTIVE); // start query

        if($category_id) $products =$products->where('category_id', $category_id);  
        if($searchproduct) $products = $products->where('name', 'like', '%'. $searchproduct . '%');
        if($request->sortby) $products = $products->orderBy(explode('_',($request->sortby))[0],explode('_',($request->sortby))[1]);
        else $products = $products->latest();
        
        $products = $products->paginate(GetSetting('product_pagination'));

        if($request->ajax()) return view('frontend.product.productpaginate', compact('products', 'searchproduct', 'category_id'))->render();
        $categories = ProductCategory::all(['id', 'name']); 
        return view('frontend.dynamicPage', compact('pagecomponents', 'products', 'page', 'categories', 'searchproduct', 'category_id')); 
    }

    public function show($slug , Request $request) //view('frontend.product.details')
    {
        $userid = Auth::id();
        $product = Product::with('getCategory')->where('slug', $slug)->firstorfail();
        $currency = Setting::get(['site_currency_icon', 'site_direction'])->first();

        $pagecomponents = $this->pageComponent->where('id', Product::DETAILS)->get(['view']);

        $productreview =  $this->productreview->with('customer')
        ->where(['product_id' => $product->id, 'status' => ProductReview::APPROVED])->paginate(GetSetting('product_pagination'));
        
        if($request->ajax()) return view('frontend.product.commentpage', compact('productreview'))->render();
        return view('frontend.dynamicPage', compact('pagecomponents', 'currency', 'product', 'productreview','userid'));
    }
}
