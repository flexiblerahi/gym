<?php

namespace App\Http\Controllers\frontend;

use App\Models\CourseReview;
use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderItem;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CourseReviewController extends Controller
{
    public function __construct(CourseReview $coursereview) {
        $this->coursereview = $coursereview;
    }

    public function store(Request $request)
    {
        $rules = [
            'rating' => ['required'],
            'comment' => ['required'],
        ];
        $customMessages = [
            'rating.required' => trans('Rating is Required!'),
            'comment.required' => trans('Comment is Required!'),
        ];
        $this->validate($request, $rules, $customMessages);

        $input['user_id'] = Auth::id();
        $input['comment'] = $request->comment;
        $input['rating'] = $request->rating;
        $input['course_id'] = $request->course_id;
        $input['status'] = CourseReview::PENDING;
        $ispurches = Order::whereRelation('items', ['order_items.item_id'=>  $request->course_id, 'order_items.type' => OrderItem::COURSE])
        ->with('items', fn ($query) => $query->where(['order_items.item_id'=>  $request->course_id, 'order_items.type' => OrderItem::COURSE]))
        ->where('user_id', auth()->id())->exists();
        if(!$ispurches) return response()->json(['error' => trans(CourseReview::ISPURCHASE)]);

        if($this->coursereview->where(['course_id' => $input['course_id'], 'user_id' => $input['user_id']])->count() > 0) {
            return response()->json(['error' => trans(CourseReview::ALREADYCOMMENT)]);
        }
        $this->coursereview->create($input);
        return response()->json(['success' => trans(CourseReview::SUCCESSCREATE)]);
    }
}
