<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Blog;
use App\Models\EmailTemplate;
use App\Models\Faq;
use App\Models\FooterInformation;
use App\Models\GoogleMap;
use App\Models\OurProcess;
use App\Models\PageBuilder;
use App\Models\PageComponent;
use App\Models\PrivacyPolicy;
use App\Models\SectionTitle;
use App\Models\Service;
use App\Models\Slider;
use App\Models\Subscriber;
use App\Models\TermsOfUse;
use App\Models\Testimonial;
use App\Models\Trainer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class DynamicPageController extends Controller
{
    public function __construct(PageComponent $pageComponent, PageBuilder $pagebuilder) {
        $this->pageComponent = $pageComponent;
        $this->pagebuilder = $pagebuilder;
    }
    
    public function index()
    {
        $page = $this->pagebuilder->where(['slug' => 'home'])->firstOrFail(['contents']);
        $elements = explode(',', $page->contents);
        $pagecomponents = $this->pageComponent->whereIn('id', $elements)->get(['view']);
        $sliders = Slider::all();
        $abouthomepage = About::get()->first();
        $ourprocess = OurProcess::first();
        $sectiontitle = SectionTitle::first();
        $testimonials = Testimonial::all();
        $trainers = Trainer::all();
        $services = Service::all(['icon', 'title', 'description']);

        $blogs = Blog::where('status', Blog::ACTIVE)->latest()->take(3)->get();
        return view('frontend.dynamicPage', compact('pagecomponents', 'sliders', 'abouthomepage', 'ourprocess', 'services','testimonials', 'blogs','trainers','sectiontitle'));
    }

    public function page($slug)
    {
        $page = PageBuilder::where(['slug' => $slug])->firstOrFail();
        $elements = explode(',', $page->contents);
        return view('frontend.dynamicPage', compact('elements', 'page'));
    }

    public function about()
    {
         $aboutid = 26;
        $pagecomponents = $this->pageComponent->where('id', $aboutid)->get(['view']);   
        $aboutus = About::get()->first();
        $testimonials = Testimonial::all();
        return view('frontend.dynamicPage', compact('pagecomponents','aboutus', 'testimonials'));
    }

    public function contact()
    {
        $contact = 24;
        $googlemap = GoogleMap::first();
        $pagecomponents = $this->pageComponent->where('id', $contact)->get(['view']);     
        return view('frontend.dynamicPage', compact('pagecomponents' , 'googlemap'));
    }

    public function services()
    {
        $serviceid = 28;
        $pagecomponents = $this->pageComponent->where('id', $serviceid)->get(['view']);     
        $services = Service::all(['description', 'title', 'icon']);
        $sectiontitle = SectionTitle::first();
        return view('frontend.dynamicPage', compact('pagecomponents', 'services','sectiontitle'));
    }

    public function signin()
    {
        $signin = 29;
        $pagecomponents = $this->pageComponent->where('id', $signin)->get(['view']);     
        return view('frontend.dynamicPage', compact('pagecomponents'));
    }

    public function gallery()
    {
        $gallery = 30;
        $pagecomponents = $this->pageComponent->where('id', $gallery)->get(['view']);     
        return view('frontend.dynamicPage', compact('pagecomponents'));
    }

    public function termcondition()
    {
        $termconditionid = 31;
        $termcondition = TermsOfUse::first();
        $pagecomponents = $this->pageComponent->where('id', $termconditionid)->get(['view']);     
        return view('frontend.dynamicPage', compact('pagecomponents', 'termcondition'));
    }

    public function faq()
    {
        $faqid = 32;
        $pagecomponents = $this->pageComponent->where('id', $faqid)->get(['view']);     
        $faqs = Faq::all();
        return view('frontend.dynamicPage', compact('pagecomponents', 'faqs'));
    }

    public function privacy(Request $request)
    {
        $privacyid = 35;
        $pagecomponents = $this->pageComponent->where('id', $privacyid)->get(['view']);
        $privacy =PrivacyPolicy::first();
        return view('frontend.dynamicPage', compact('pagecomponents', 'privacy'));
    }

    public function bloglist()
    {
        $bloglist = 36;
        $pagecomponents = $this->pageComponent->where('id', $bloglist)->get(['view']);
        return view('frontend.dynamicPage', compact('pagecomponents'));
    }

    public function blogdetails()
    {
        $bloglist = 37;
        $pagecomponents = $this->pageComponent->where('id', $bloglist)->get(['view']);
        return view('frontend.dynamicPage', compact('pagecomponents'));
    }

    public function courses()
    {
        $bloglist = 38;
        $pagecomponents = $this->pageComponent->where('id', $bloglist)->get(['view']);
        return view('frontend.dynamicPage', compact('pagecomponents'));
    }
}
