<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\RoleDataTable;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\DB;
use Spatie\Permission\Models\Role;
use Illuminate\Support\Str;
use Spatie\Permission\Models\Permission;

class RoleController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:role-index|role-create|role-edit|role-delete', ['only' => ['index','store']]);
         $this->middleware('permission:role-create', ['only' => ['create','store']]);
         $this->middleware('permission:role-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:role-delete', ['only' => ['destroy']]);
    }

    public function index(RoleDataTable $roleDataTable) //view('admin.role.index')
    {
        return $roleDataTable->render('admin.role.index');
    }

    public function create()
    {
        $permission = Permission::get();
        return view('admin.role.create',compact('permission'));
    }

    public function validation($request, $id=null)
    {
        $rules = [
            'name' => (is_null($id)) ? ['required', 'unique:roles,name'] : ['required', 'unique:roles,name,'.$id],
            'permission' => 'required',
        ];
        $customMessages = [
            'name.required' => trans('Name is required!'),
            'name.unique' => trans('Name already exist'),
            'permission.required' => trans('Permission is required!'),
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function store(Request $request)
    {
        $this->validation($request);
        $role = Role::create(['name' => $request->input('name')]);
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('admin.roles.index');

    }

    public function edit($id)
    {
        $role = Role::find($id);
        $permission = Permission::get();
        $rolePermissions = DB::table("role_has_permissions")->where("role_has_permissions.role_id",$id)
            ->pluck('role_has_permissions.permission_id','role_has_permissions.permission_id')
            ->all();
        return view('admin.role.edit',compact('role','permission','rolePermissions'));
    }

    public function update(Request $request, $id)
    {
        $this->validation($request, $id);
        $role = Role::find($id);
        $role->name = $request->input('name');
        $role->save();
        $role->syncPermissions($request->input('permission'));
        return redirect()->route('admin.roles.index');
    }

    public function destroy($id)
    {
        DB::table("roles")->where('id',$id)->delete();
    }
}
