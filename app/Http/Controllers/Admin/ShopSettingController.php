<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\ShopSetting;
use Illuminate\Http\Request;

 class ShopSettingController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:shop-settings-index|shop-settings-create|shop-settings-edit|shop-settings-delete', ['only' => ['index','show']]);
         $this->middleware('permission:shop-settings-create', ['only' => ['create','store']]);
         $this->middleware('permission:shop-settings-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:shop-settings-delete', ['only' => ['destroy']]);
    }

    public function index()
    { 
        $setting = ShopSetting::first();
        return view('admin.shopSettingIndex', compact('setting'));
    }

    public function update(Request $request, $id)
    {
        $setting = ShopSetting::first();
        if (!$setting) $setting = new ShopSetting();
        $setting->tax = $request->tax;
        $setting->save();   
        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }
}
