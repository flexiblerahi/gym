<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CouponDataTable;
use App\Http\Controllers\Controller;
use App\Models\Coupon;
use Illuminate\Http\Request;

class CouponController extends Controller
{
    public function index(CouponDataTable $dataTables)
    {
        return $dataTables->render('admin.coupon.index');
    }

    public function create()
    {
        return view('admin.coupon.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'name' => 'required',
            'amount' => 'required',
            'code' => 'required|unique:coupons',
        ];
        $customMessages = [
            'code.required' => trans('code is required!'),
            'amount.required' => trans('Amount is required!'),
            'name.required' => trans('Name is required!'),
            'code.unique' => trans('Code already exist!'),
        ];
        $this->validate($request, $rules, $customMessages);

        $coupon = new Coupon();
        $coupon->name = $request->name;
        $coupon->code = $request->code;
        $coupon->amount = $request->amount;
        $coupon->type = $request->type;
        ($request->status == 'on') ? $coupon->status = Coupon::ACTIVE : $coupon->status = Coupon::CLOSE;
        $coupon->save();

        $notification = trans('Coupon Created Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.coupon.index')->with($notification);
    }

    public function edit(Coupon $coupon)
    {
        $coupon->active = Coupon::ACTIVE;
        return view('admin.coupon.edit', compact('coupon'));
    }

    public function update(Request $request, Coupon $coupon)
    {
        $rules = [
            'name' => 'required',
            'code' => 'required|unique:coupons,code,'.$coupon->id,
        ];
        $customMessages = [
            'code.required' => trans('code is required!'),
            'name.required' => trans('Name is required!'),
            'code.unique' => trans('Code already exist!'),
        ];
        $this->validate($request, $rules, $customMessages);
        
        $coupon->name = $request->name;
        $coupon->code = $request->code;
        $coupon->amount = $request->amount;
        ($request->status == 'on') ? $coupon->status = Coupon::ACTIVE : $coupon->status = Coupon::CLOSE;
        $coupon->type = $request->type;
        $coupon->save();

        $notification = trans('Coupon updated Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.coupon.index')->with($notification);
    }

    public function destroy(Coupon $coupon)
    {
        $coupon->delete();
    }
}
