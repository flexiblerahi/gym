<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use stdClass;

class PagePreviewController extends Controller
{
    public function store(Request $request)
    {
        if ($request->data) {
            $elements = $request->data;
            $elements = explode(',', $elements);
            $page = new stdClass();
            $page->title = $request->title;
            return view('frontend.layouts.pageAjax', compact('elements', 'page'));
        }
        return response([ 'Error' => 'No Element Selected']);
    }
}
