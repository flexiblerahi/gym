<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SliderDataTable;
use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;

class SliderController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:slider-index|slider-create|slider-edit|slider-delete', ['only' => ['index','show']]);
         $this->middleware('permission:slider-create', ['only' => ['create','store']]);
         $this->middleware('permission:slider-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:slider-delete', ['only' => ['destroy']]);
    }

    public function index(SliderDataTable $sliderDataTable)
    {
        return $sliderDataTable->render('admin.sliderIndex');
    }

    public function create()
    {
        return view('admin.sliderCreate');
    }
   
    public function store(Request $request)
    {
        $rules = ['title' => 'required'];
        $customMessages = ['title.required' => trans('Title is required!')];
        $this->validate($request, $rules, $customMessages);
        $slider = new Slider();
        $slider->title = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->description = $request->description;
        $slider->image = $request->image;
        $slider->button = $request->button;
        $slider->button_link = $request->button_link;
        $slider->save();

        $notification = trans('admin.Created Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.slider.index')->with($notification);

    }

    public function edit(Slider $slider)
    {
        return view('admin.sliderEdit', compact('slider'));
    }
 
    public function update(Request $request, $id)
    {
        $rules = ['title' => 'required'];
        $customMessages = ['title.required' => trans('Title is required!')];
        $this->validate($request, $rules, $customMessages);
        $slider =  Slider::findOrFail($id);
        $slider->title = $request->title;
        $slider->subtitle = $request->subtitle;
        $slider->description = $request->description;
        $slider->image = $request->image;
        $slider->button = $request->button;
        $slider->button_link = $request->button_link;
        $slider->save();

        $notification = trans('admin.Updated Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.slider.index')->with($notification);
    }
  
    public function destroy(Slider $slider)
    {
        $slider->delete();
    }
}
