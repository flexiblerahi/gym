<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\About;
use Illuminate\Http\Request;
class AboutController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:about-index|about-create|about-edit|about-delete', ['only' => ['index','show']]);
         $this->middleware('permission:about-create', ['only' => ['create','store']]);
         $this->middleware('permission:about-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:about-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $about = About::first();
        return view('admin.aboutIndex', compact('about'));
    }

    public function validation($request)
    {
        $rules = [
            'title' => ['required'],
            'icon' => ['required'],
            'subtitle' => ['required'],
            'description' => ['required'],
            'clienttitle' => ['required'],
            'clientsubtitle' => ['required'],
            'itemicon.*' => ['required'],
            'itemtitle.*' => ['required'],
            'itemsubtitle.*' => ['required'],
            'bottom_text' => ['required']
        ];
        $customMessages = [
            'title.required' => trans('Title is required!'),
            'icon.required' => trans('Icon is required'),
            'subtitle.required' => trans('Sub title is required'),
            'description.required' => trans('Description is required'),
            'clienttitle.required' => trans('Client title is required'),
            'clientsubtitle.required' => trans('Client sub title is required'),
            'itemicon.*.required' => trans('Item icon is required'),
            'itemtitle.*.required' => trans('Item title is required'),
            'itemsubtitle.*.required' => trans('Item subtitle is required'),
            'bottom_text.required' => trans('admin.Bottom Text is required')
        ];
        $this->validate($request, $rules, $customMessages);
    }
 
    public function update(Request $request)
    {
        $this->validation($request);
        $item = array();
        $itemcondition = ($request->itemicon) ? count($request->itemicon) : 0;
        for ($i=0; $i < $itemcondition && $itemcondition > 0; $i++) { 
            $item[] = ['icon' => $request->itemicon[$i], 'title' => $request->itemtitle[$i], 'subtitle' => $request->itemsubtitle[$i]];
        } 
        $client_section = ['title' => $request->clienttitle, 'subtitle' => $request->clientsubtitle];

        $about = About::first();
        if (!$about) $about = new About();
        $about->icon = $request->icon;
        $about->title = $request->title;
        $about->subtitle = $request->subtitle;
        $about->description = $request->description;
        $about->bottom_text = $request->bottom_text;
        $about->first_image = $request->first_image;
        $about->second_image = $request->second_image;
        $about->third_image = $request->third_image;
        $about->signature = $request->signature;
        $about->first_video = $request->first_video;
        $about->second_video = $request->second_video;
        $about->item = json_encode($item);
        $about->client_section = json_encode($client_section);
        $about->save();
        return redirect()->route('admin.about.index')->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }
}
