<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\FaqDataTable;
use App\Http\Controllers\Controller;
use App\Models\Faq;
use Illuminate\Http\Request;

class FaqController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:faq-index|faq-create|faq-edit|faq-delete', ['only' => ['index','show']]);
        $this->middleware('permission:faq-create', ['only' => ['create','store']]);
        $this->middleware('permission:faq-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:faq-delete', ['only' => ['destroy']]);
    }

    public function index(FaqDataTable $faqDataTable) //view('admin.faq.index')
    {
        return $faqDataTable->render('admin.faq.index');
    }

    public function create()
    {
        return view('admin.faq.create');
    }

    public function validation($request)
    {
        $rules = [
            'question' => 'required',
            'answer' => 'required',
        ];
        $customMessages = [
            'question.required' => trans('admin.Question is required!'),
            'answer.required' => trans('admin.Answer is required!'),
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function store(Request $request)
    {
        $this->validation($request);
        $faq = new Faq();
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();
        return redirect()->route('admin.faq.index')->with(['message' => trans('admin.Created Successfully'), 'alert-type' => 'success']);
    }

    public function edit(Faq $faq)
    {
        return view('admin.faq.edit', compact('faq'));
    }

    public function update(Request $request, $id)
    {
        $this->validation($request);
        $faq = Faq::findOrFail($id);
        $faq->question = $request->question;
        $faq->answer = $request->answer;
        $faq->save();
        return redirect()->route('admin.faq.index')->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }

    public function destroy(Faq $faq)
    {
        $faq->delete();
    }
}
