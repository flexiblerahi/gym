<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ShippingDataTable;
use App\Http\Controllers\Controller;
use App\Models\Shipping;
use Illuminate\Http\Request;

class ShippingController extends Controller
{
    public function index(ShippingDataTable $dataTables)
    {
        return $dataTables->render('admin.shipping.index');
    }

    public function create()
    {
        return view('admin.shipping.create');
    }

    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'short_description' => 'required',
            'cost' => 'required',
        ];
        $customMessages = [
            'title.required' => trans('Title is required!'),
            'short_description.required' => trans('Short Description is required!'),
            'cost.required' => trans('Cost is required!'),
        ];
        $this->validate($request, $rules, $customMessages);

        $shipping = new Shipping();
        $shipping->title = $request->title;
        $shipping->short_description = $request->short_description;
        $shipping->cost = $request->cost;
        ($request->status == 'on') ? $shipping->status = Shipping::ACTIVE : $shipping->status = Shipping::CLOSE;
        $shipping->save();

        $notification = trans('shipping Created Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.shipping.index')->with($notification);
    }

    public function edit(Shipping $shipping)
    {
        $shipping->active = Shipping::ACTIVE;
        return view('admin.shipping.edit', compact('shipping'));
    }

    public function update(Request $request, Shipping $shipping)
    {
        $rules = [
            'title' => 'required',
            'short_description' => 'required',
            'cost' => 'required',
        ];
        $customMessages = [
            'title.required' => trans('Title is required!'),
            'short_description.required' => trans('Short Description is required!'),
            'cost.required' => trans('Cost is required!'),
        ];
        $this->validate($request, $rules, $customMessages);

        $shipping->title = $request->title;
        $shipping->short_description = $request->short_description;
        $shipping->cost = $request->cost;
        ($request->status == 'on') ? $shipping->status = Shipping::ACTIVE : $shipping->status = Shipping::CLOSE;
        $shipping->save();

        $notification = trans('shipping Updated Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.shipping.index')->with($notification);
    }

    public function destroy(Shipping $shipping)
    {
        $shipping->delete();
    }
}
