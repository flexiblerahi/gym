<?php

namespace App\Http\Controllers\Admin\Auth;

use App\Http\Controllers\Controller;
use App\Providers\RouteServiceProvider;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AdminLoginController extends Controller
{


    use AuthenticatesUsers;
    protected $redirectTo = RouteServiceProvider::ADMIN;

    public function __construct()
    {
        $this->middleware('guest:admin')->except('adminLogout');
    }


    public function adminLoginForm()
    {
        return view('admin.loginIndex');
    }

    public function validation($request)
    {
        $rules = [
            'email'   => ['required', 'email'],
            'password' => ['required', 'min:4']
        ];
        $customMessages = [
            'email.required' => trans('Email is required.'),
            'email.email' => trans('Email must by type of Email.'),
            'password.required' => trans('Password is required'),
            'password.min' => trans('Password length must be 4'),
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function adminCheckLogin(Request $request)
    {

        $this->validation($request);

        if (Auth::guard('admin')->attempt(['email' => $request->email, 'password' => $request->password], $request->get('remember'))) {

            return redirect()->intended(RouteServiceProvider::ADMIN);
        }
        return back()->withInput($request->only('email', 'remember'));
    }

    public function adminLogout()
    {
        Auth::guard('admin')->logout();
        return redirect()->route('admin.login');
    }
}
