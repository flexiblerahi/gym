<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductCategoryDataTable;
use App\Http\Controllers\Controller;
use App\Models\ProductCategory;
use Illuminate\Http\Request;

class ProductCategoryController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:product-category-index|product-category-create|product-category-edit|product-category-delete', ['only' => ['index','show']]);
        $this->middleware('permission:product-category-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-category-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-category-delete', ['only' => ['destroy']]);
    }

    public function index(ProductCategoryDataTable $productCategoryDataTable) //view('admin.product.category.index');
    {
        return $productCategoryDataTable->render('admin.product.category.index');
    }
 
    public function create()
    {
        return view('admin.product.category.create');
    }
  
    public function store(Request $request)
    {
        $customMessages = ['name.required' => trans('Name is required'), 'name.unique' => trans('Name already exist')];
        $this->validate($request, ['name' => ['required', 'unique:product_categories']], $customMessages);
        ProductCategory::create(['name' => $request->name]);
        return redirect()->route('admin.product-category.index')->with(['message' => trans('admin.Created Successfully'), 'alert-type' => 'success']);
    }

    public function edit($id)
    {
        $category = ProductCategory::find($id);
        return view('admin.product.category.edit', compact('category'));
    }

    public function update(Request $request, $id)
    {
        $customMessages = ['name.required' => trans('Name is required'), 'name.unique' => trans('Name already exist')];
        $this->validate($request, ['name' => ['required', 'unique:product_categories,name,'. $id]], $customMessages);
        $category = ProductCategory::find($id);
        $category->name = $request->name;
        $category->save();
        return redirect()->route('admin.product-category.index')->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }
    
    public function destroy($id)
    {
        $count = ProductCategory::findOrFail($id)->products->count();
        if($count>0) return response(['status' => 'failed']);
        ProductCategory::findOrFail($id)->delete();
        return response(['status' => 'success']);
    }
}
