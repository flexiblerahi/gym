<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Cookie;
use App\Models\EmailSetting;
use App\Models\EmailTemplate;
use App\Models\GoogleMap;
use App\Models\PaymentGateway;
use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Session;

class SettingsController extends Controller
{

    function __construct()
    {
         $this->middleware('permission:settings-index|settings-create|settings-edit|settings-delete', ['only' => ['index','show']]);
         $this->middleware('permission:settings-create', ['only' => ['create','store']]);
         $this->middleware('permission:settings-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:settings-delete', ['only' => ['destroy']]);
    }

    public function  settings()
    {
        $setting= Setting::first();
        return view('admin.settings.settings', compact('setting'));
    }

    public function savegeneralsettings(Request $request)
    {
        $general = Setting::first();
        $general->site_name         = $request->site_name;
        $general->site_currency     = $request->site_currency;
        $general->site_currency_icon = $request->site_currency_icon;
        $general->site_currency_direction    = $request->site_currency_direction;
        $general->site_direction    = $request->site_direction;
        $general->site_time_zone    = $request->site_time_zone;
        $general->site_logo = $request->site_logo;
        $general->site_favicon = $request->site_favicon;
        $general->section_icon       = $request->section_icon;
        $general->footer_logo       = $request->footer_logo;
        $general->breadcumb = $request->breadcumb;
        $general->user_avatar = $request->user_avatar;
        $general->testimonial_background = $request->testimonial_background;
        $general->save();
        if(Session::get('currency')) Session::forget('currency');
        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }

    public function cookiesettings()
    {
        return view('admin.cookiesettings');
    }

    public function savecookiesettings(Request $request)
    {
        $cookie = Cookie::first();
        $cookie->cookie_status = $request->cookie_status;
        $cookie->cookie_button = $request->cookie_button;
        $cookie->cookie_confirmation = $request->cookie_confirmation;
        $cookie->save();
        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }

    public function savepaymentsettings(Request $request)
    {
        $paymentsettings = PaymentGateway::first();
        $paymentsettings->paypal_api_mode            = $request->paypal_api_mode;
        $paymentsettings->paypal_api_client          = $request->paypal_api_client;
        $paymentsettings->paypal_api_secret          = $request->paypal_api_secret;

        $paymentsettings->paypal_country          = $request->paypal_country;
        $paymentsettings->paypal_currency          = $request->paypal_currency;
        $paymentsettings->paypal_rate          =  $request->paypal_rate;
        $paymentsettings->paypal_api_status = ($request->paypal_api_status == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;

        $paymentsettings->ssl_commerz_store_id       = $request->ssl_commerz_store_id;
        $paymentsettings->ssl_commerz_store_password = $request->ssl_commerz_store_password;
        $paymentsettings->ssl_commerz_country          = $request->ssl_commerz_country;
        $paymentsettings->ssl_commerz_currency          = $request->ssl_commerz_currency;
        $paymentsettings->ssl_commerz_rate          = $request->ssl_commerz_rate;
        $paymentsettings->ssl_commerz_is_sandbox = ($request->ssl_commerz_is_sandbox == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;
        $paymentsettings->ssl_commerz_status = ($request->ssl_commerz_status == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;

        $paymentsettings->stripe_api_publishable_key = $request->stripe_api_publishable_key;
        $paymentsettings->stripe_api_secret_key      = $request->stripe_api_secret_key;
        $paymentsettings->stripe_country          = $request->stripe_country;
        $paymentsettings->stripe_currency          = $request->stripe_currency;
        $paymentsettings->stripe_rate          = $request->stripe_rate;
        $paymentsettings->stripe_api_status = ($request->stripe_api_status == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;

        $paymentsettings->razorpay_key_id = $request->razorpay_key_id;
        $paymentsettings->razorpay_key_secret      = $request->razorpay_key_secret;
        $paymentsettings->razorpay_country          = $request->razorpay_country;
        $paymentsettings->razorpay_currency          = $request->razorpay_currency;
        $paymentsettings->razorpay_rate          = $request->razorpay_rate;
        $paymentsettings->razorpay_status = ($request->razorpay_status == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;

        $paymentsettings->mollie_email = $request->mollie_email;
        $paymentsettings->mollie_api_key      = $request->mollie_api_key;
        $paymentsettings->mollie_country          = $request->mollie_country;
        $paymentsettings->mollie_currency          = $request->mollie_currency;
        $paymentsettings->mollie_rate          = $request->mollie_rate;
        $paymentsettings->mollie_status = ($request->mollie_status == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;
  
        $paymentsettings->paystack_public_key = $request->paystack_public_key;
        $paymentsettings->paystack_secret_key      = $request->paystack_secret_key;
        $paymentsettings->paystack_country          = $request->paystack_country;
        $paymentsettings->paystack_currency          = $request->paystack_currency;
        $paymentsettings->paystack_rate          = $request->paystack_rate;
        $paymentsettings->paystack_status = ($request->paystack_status == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;

        $paymentsettings->bank_account_details       = $request->bank_account_details;
        $paymentsettings->bank_account_status = ($request->bank_account_status == 'on') ? PaymentGateway::ACTIVE : PaymentGateway::CLOSE;
        
        $paymentsettings->save();
        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }

    public function savemailsettings(Request $request)
    {
        $mail =  EmailSetting::first();
        $mail->mail_host = $request->mail_host;
        $mail->smtp_username = $request->smtp_username;
        $mail->smtp_password = $request->smtp_password;
        $mail->mail_port = $request->mail_port;
        $mail->mail_sent_from = $request->mail_sent_from;
        $mail->mail_sent_from_email = $request->mail_sent_from_email;
        $mail->mail_encryption = $request->mail_encryption;
        $mail->save();
        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }

    public function mapsettings(Request $request)
    {
        $map =  GoogleMap::first();
        $map->link = $request->link;
        $map->save();
        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }

    public function editemailtemplate(Request $request)
    {
        $template = EmailTemplate::findOrFail($request->id);
        return view('admin.editemailtemplate', compact('template'));
    }

    public function contact_message()
    {
        $setting = Setting::first();
        if (GetSetting('save_contact_message') == 1) {
            $setting->save_contact_message = 0;
            $setting->save();
            return response([
                'message' => trans('admin.Message Setting Updated!')
            ]);
        }
        $setting->save_contact_message = 1;
        $setting->save();
        return response(['message' => trans('admin.Message Setting Updated!')]);
    }

    public function paginationsettings(Request $request)
    {
        $general = Setting::first();
        $general->blog_pagination = $request->blog_pagination;
        $general->blog_comment_pagination = $request->blog_comment_pagination;
        $general->course_pagination = $request->course_pagination;
        $general->product_pagination = $request->product_pagination;
        $general->save();

        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }
}
