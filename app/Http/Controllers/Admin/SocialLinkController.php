<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\SocialLinkDataTable;
use App\Http\Controllers\Controller;
use App\Models\SocialLink;
use Illuminate\Http\Request;

class SocialLinkController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:social-link-index|social-link-create|social-link-edit|social-link-delete', ['only' => ['index','show']]);
         $this->middleware('permission:social-link-create', ['only' => ['create','store']]);
         $this->middleware('permission:social-link-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:social-link-delete', ['only' => ['destroy']]);
    }

    public function index(SocialLinkDataTable $dataTable)
    {
        return $dataTable->render('admin.headerSocialLinkIndex');
    }
 
    public function create()
    {
        return view('admin.headerSocialLinkCreate');
    }
   
    public function store(Request $request)
    {
        $rules = [
            'link' => 'required',
            'icon' => 'required',
        ];
        $customMessages = [
            'link.required' => trans('Link is required'),
            'icon.required' => trans('Icon is required'),
        ];

        $this->validate($request, $rules, $customMessages);
        $social = new SocialLink();
        $social->on_footer = ($request->on_footer == 'on') ? SocialLink::ONFOOTER : SocialLink::CLOSE;
        $social->icon = $request->icon;
        $social->link = $request->link;
        $social->save();

        $notification = trans('admin.Created Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.social-link.index')->with($notification);
    }

    public function edit($id)
    {
        $social =  SocialLink::findOrFail($id);
        return view('admin.headerSocialLinkEdit', compact('social'));
    }

    public function update(Request $request, $id)
    {
        $rules = [
            'link' => 'required',
            'icon' => 'required',
        ];
        $customMessages = [
            'link.required' => trans('Link is required'),
            'icon.required' => trans('Icon is required'),
        ];
        $this->validate($request, $rules, $customMessages);
        $social =  SocialLink::findOrFail($id);
        $social->on_footer = ($request->on_footer == 'on') ? SocialLink::ONFOOTER : SocialLink::CLOSE;
        $social->icon = $request->icon;
        $social->link = $request->link;
        $social->save();
        $notification = trans('admin.Updated Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.social-link.index')->with($notification);
    }

    public function destroy($id){
        SocialLink::findOrFail($id)->delete();
    }
}
