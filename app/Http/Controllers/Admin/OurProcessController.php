<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\OurProcess;
use Illuminate\Http\Request;

class OurProcessController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:ourprocess-index|ourprocess-update', ['only' => ['index','update']]);
         $this->middleware('permission:ourprocess-index', ['only' => ['index']]);
         $this->middleware('permission:ourprocess-update', ['only' => ['update']]);
    }

    public function index()
    {
        $ourprocess = OurProcess::first();
        // dd($ourprocess);
        return view('admin.layouts.ourprocess', compact('ourprocess'));
    }

    public function update(Request $request, $id)
    {
        // dd($request->all());
        $rules = ['title' => 'required', 'description' => 'required'];

        $customMessages = [
            'description.required' => trans('Description is required'),
            'title.required' => trans('Title is required'),
        ];
        $this->validate($request, $rules, $customMessages);
        $items = array();
        $itemcondition = ($request->itemicon) ? count($request->itemicon) : 0;
        for ($i=0; $i < $itemcondition && $itemcondition > 0; $i++) { 
            $items[] = [
                'icon' => $request->itemicon[$i],
                'title' => $request->itemtitle[$i],
            ];
        } 
        $ourprocess = OurProcess::first();
        if (!$ourprocess) $ourprocess = new OurProcess();
        $ourprocess->title = $request->title;
        $ourprocess->description = $request->description;
        $ourprocess->items = json_encode($items);
        $ourprocess->save();
        $notification = trans('Our Process Updated Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->route('admin.ourprocess.index')->with($notification);
    }
}
