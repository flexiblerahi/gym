<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CourseDataTable;
use App\Models\Course;
use App\Http\Controllers\Controller;
use App\Models\Coursecategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class CourseController extends Controller
{
    public function __construct()
    {
        $this->middleware('permission:course-index|course-create|course-edit|course-delete', ['only' => ['index','show']]);
        $this->middleware('permission:course-create', ['only' => ['create','store']]);
        $this->middleware('permission:course-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:course-delete', ['only' => ['destroy']]);
    }

    public function index(CourseDataTable $dataTables) //view('admin.course.index')
    {
        return $dataTables->render('admin.course.index');
    }

    public function create()
    {
        $courseCategories = Coursecategory::where('status', Coursecategory::ACTIVE)->get(['id', 'name']);
        return view('admin.course.create', compact('courseCategories'));
    }

    public function store(Request $request)
    {
        $this->validation($request);
        $course = new Course();
        $course->name = $request->name;
        $course->slug = ($request->filled('slug')) ? Str::slug($request->slug) : Str::slug($request->name);
        $course->image = $request->image;
        $course->status = ($request->status == 'on') ? Course::ACTIVE : Course::CLOSE;
        $course->price = $request->price;
        $course->offer_price = $request->offer_price;
        $course->level = $request->level;
        $course->is_paid = $request->is_paid;
        $course->description = $request->description;
        $course->coursecategory_id = $request->coursecategory_id;
        $course->save();
        return redirect()->route('admin.course.index')->with(['message' => trans('Course Create Successfully'), 'alert-type' => 'success']);
    }

    public function validation($request, $id=null)
    {
        $rules = [
            'image' => ['required'],
            'price' => ['required'],
            'level' => ['required'],
            'is_paid' => ['required'],
            'description' => ['required'],
            'name' => (is_null($id)) ? ['required', 'unique:courses,name'] : ['required', 'unique:courses,name,'.$id],
            'slug' => (is_null($id)) ? ['required', 'unique:courses,slug'] : ['required', 'unique:courses,slug,'.$id],
            'coursecategory_id' => ['required'],
        ];
        
        $customMessages = [
            'coursecategory_id.required' => trans('Course Category is required!'),
            'price.required' => trans('Price tag is required!'),
            'level.required' => trans('level tag is required!'),
            'is_paid.required' => trans('this tag is required!'),
            'image.required' => trans('Image field is Required!'),
            'coursecategory_id.required' => trans('Course Category field is Required!'),
            'name.required' => trans('Course Name is required!'),
            'name.unique' => trans('Course Name already exist!'),
            'slug.required' => trans('Slug is required!'),
            'slug.required' => trans('Slug already exist!'),
            'description.required' => trans('Description is required!'),
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function edit(Course $course)
    {
        $courseCategories = Coursecategory::where('status', Coursecategory::ACTIVE)->get(['id', 'name']);
        return view('admin.course.edit', compact('course', 'courseCategories'));
    }

    public function update(Request $request, $id)
    {
        $this->validation($request, $id);
        $course = Course::find($id);
        $course->slug = ($request->filled('slug')) ? Str::slug($request->slug) : Str::slug($request->name);
        $course->name = $request->name;
        $course->image = $request->image;
        $course->status = ($request->status == 'on') ? Course::ACTIVE : Course::CLOSE;
        $course->price = $request->price;
        $course->offer_price = $request->offer_price;
        $course->level = $request->level;
        $course->is_paid = $request->is_paid;
        $course->description = $request->description;
        $course->coursecategory_id = $request->coursecategory_id;
        $course->save();
        return redirect()->route('admin.course.index')->with(['message' => trans('Course Update Successfully'), 'alert-type' => 'success']);
    }

    public function destroy(Course $course)
    {
        $course->delete();
    }
}
