<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ServiceDataTable;
use App\Http\Controllers\Controller;
use App\Models\Service;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ServiceController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:service-index|service-create|service-edit|service-delete', ['only' => ['index','show']]);
         $this->middleware('permission:service-create', ['only' => ['create','store']]);
         $this->middleware('permission:service-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:service-delete', ['only' => ['destroy']]);
    }

    public function index(ServiceDataTable $serviceDataTable) //view('admin.service.index')
    {
        return $serviceDataTable->render('admin.service.index');
    }

    public function create()
    {
        return view('admin.service.create');
    }

    public function validation($request, $id=null)
    {
        $rules = [
            'title' => (is_null($id)) ? ['required', 'unique:services'] : ['required', 'unique:services,title,'.$id],
            'slug'  => (is_null($id)) ? ['required', 'unique:services'] : ['required', 'unique:services,slug,'.$id],
            'description' => 'required',
            'icon' => 'required',
        ];

        $customMessages = [
            'title.required' => trans('Title is required'),
            'title.unique' => trans('Title must be unique'),
            'description.required' => trans('Description is required'),
            'icon.required' => trans('Icon is required'),
            'slug.required' => trans('Slug is required'),
            'slug.unique' => trans('Slug must be unique')
        ];

        $this->validate($request, $rules, $customMessages);
    }

    public function store(Request $request)
    {
        $this->validation($request);
        $service = new Service();
        $this->saveorupdate($service, $request);
        return redirect()->route('admin.service.index')->with(['message' => trans('admin.Created Successfully'), 'alert-type' => 'success']);
    }

    public function edit($id)
    {
        $service = Service::findOrFail($id);
        return view('admin.service.edit', compact('service'));
    }

    public function saveorupdate($service, $request)
    {
        $service->title = $request->title;
        $service->slug = ($request->filled('slug')) ?Str::slug($request->slug) : Str::slug($request->title);
        $service->description = $request->description;
        $service->icon = $request->icon;
        $service->save();
    }
   
    public function update(Request $request, $id)
    {
        $this->validation($request, $id);
        $service =  Service::findOrFail($id);
        $this->saveorupdate($service, $request);
        return redirect()->route('admin.service.index')->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }
    
    public function destroy($id)
    {
        $service = Service::findOrFail($id);
        $service->delete();
    }
}
