<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductReviewDataTable;
use App\Http\Controllers\Controller;
use App\Models\ProductReview;
use Illuminate\Http\Request;

class ProductReviewController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:product-review-index|product-review-create|product-review-edit|product-review-delete', ['only' => ['index','show']]);
        $this->middleware('permission:product-review-create', ['only' => ['create','store']]);
        $this->middleware('permission:product-review-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:product-review-delete', ['only' => ['destroy']]);
    }

    public function index(ProductReviewDataTable $productReviewDataTable) //view('admin.productReviewIndex')
    {
        return $productReviewDataTable->render('admin.productReviewIndex');
    }
   
    public function edit(ProductReview $productReview)
    {
        $productReview->status = ($productReview->status == ProductReview::APPROVED) ? ProductReview::PENDING : ProductReview::APPROVED;
        $productReview->save();

        $notification = trans('admin.Updated Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->back()->with($notification);

    }
  
    public function destroy($id)
    {
        ProductReview::find($id)->delete();
    }
}
