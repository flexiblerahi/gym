<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\ProductDataTable;
use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\ProductCategory;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ProductController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:product-index|product-create|product-edit|product-delete', ['only' => ['index','show']]);
         $this->middleware('permission:product-create', ['only' => ['create','store']]);
         $this->middleware('permission:product-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:product-delete', ['only' => ['destroy']]);
    }

    public function index(ProductDataTable $productDataTable) //view('admin.product.index');
    {
        // dd($productDataTable);
        return $productDataTable->render('admin.product.index');
    }

    public function create()
    {
        $categories = ProductCategory::all(['id', 'name']);
        return view('admin.product.create', compact('categories'));
    }

    public function store(Request $request)
    {
        $this->validation($request);
        $product = new Product();
        $this->saveorupdate($product, $request);
        return redirect()->route('admin.product.index')->with(['message' => trans('admin.Created Successfully'), 'alert-type' => 'success']);
    }

    public function update(Request $request, $id)
    {
        $this->validation($request, $id);
        $product = Product::findOrFail($id);
        $this->saveorupdate($product, $request);
        return redirect()->route('admin.product.index')->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }
  
    public function edit($id)
    {
        $product = Product::findOrFail($id);
        $categories = ProductCategory::all(['id', 'name']);
        return view('admin.product.edit', compact('product', 'categories'));
    }

    public function validation($request, $id=null)
    {
        $rules = [
            'description' => 'required',
            'name' => (is_null($id)) ? 'required|unique:products' : 'required|unique:products,name,'.$id,
            'price' => 'required'
        ];
        $customMessages = [
            'name.required' => trans('Name is required!'),
            'name.unique' => trans('Name already exist!'),
            'description.required' => trans('Description is required!'),
            'price.required' => trans('Price is required')
        ];
        $this->validate($request, $rules, $customMessages);
    }

    public function saveorupdate($product, $request)
    {
        $product->slug = ($request->filled('slug')) ? Str::slug($request->slug) : Str::slug($request->name);
        $product->name = $request->name;
        $product->image = $request->media;
        $product->price = $request->price;
        $product->discount_price = $request->discount_price;
        $product->description = $request->description;
        $product->sku = $request->sku;
        $product->brand = $request->brand;
        $product->weight = $request->weight;
        $product->stock = $request->stock;
        $product->category_id = $request->category_id;
        $product->status = $request->status == 'on' ? Product::ACTIVE : Product::CLOSE;
        $product->save();
    }
  
    public function destroy($id)
    {
        if (Product::findOrFail($id)->orders()->count() > 0)  Product::findOrFail($id)->onlyTrashed();
        Product::findOrFail($id)->reviews()->delete();
        Product::findOrFail($id)->delete();
    }
}
