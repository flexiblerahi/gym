<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\SectionTitle;
use Illuminate\Http\Request;

class SectionTitleController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:sectiontitle-index|sectiontitle-update', ['only' => ['index','update']]);
         $this->middleware('permission:sectiontitle-index', ['only' => ['index']]);
         $this->middleware('permission:sectiontitle-update', ['only' => ['update']]);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sectiontitle = SectionTitle::first();
        return view('admin.sectionTitlcreate',compact('sectiontitle'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        // dd($request->all());
        $rules = [
            'servicetitle' => 'required',
            'trainingtitle' => 'required',
            'blogtitle' => 'required',
        ];
        $customMessages = [
            'servicetitle.required' => trans('Service Title is required!'),
            'trainingtitle.required' => trans('Training Title is required!'),
            'blogtitle.required' => trans('Blog Title Title is required!'),
        ];
        $this->validate($request, $rules, $customMessages);
        $sectiontitle = SectionTitle::first();
        if (!$sectiontitle) $sectiontitle = new SectionTitle();
        $sectiontitle->servicetitle = $request->servicetitle;
        $sectiontitle->trainingtitle = $request->trainingtitle;
        $sectiontitle->blogtitle = $request->blogtitle;
        $sectiontitle->servicesubtitle = $request->servicesubtitle;
        $sectiontitle->trainingsubtitle = $request->trainingsubtitle;
        $sectiontitle->blogsubtitle = $request->blogsubtitle;
        $sectiontitle->save();
        $notification = trans('Updated Successfully');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return back()->with($notification);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
