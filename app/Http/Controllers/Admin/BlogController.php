<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\BlogDataTable;
use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class BlogController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:blog-index|blog-create|blog-edit|blog-delete', ['only' => ['index','show']]);
        $this->middleware('permission:blog-create', ['only' => ['create','store']]);
        $this->middleware('permission:blog-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:blog-delete', ['only' => ['destroy']]);
    }

    public function index(BlogDataTable $dataTables) //view('admin.blog.index')
    {
        return $dataTables->render('admin.blog.index');
    }
 
    public function create()
    {
        return view('admin.blog.post');
    }

    public function validation($request, $id=null)
    {
        $rules = [
            'title' => (is_null($id)) ? ['required', 'unique:blogs'] : ['required', 'unique:blogs,title,' . $id],
            'slug' => (is_null($id)) ? ['required', 'unique:blogs'] : ['required', 'unique:blogs,slug,' . $id],
            'category_id' => 'required',
        ];
        $customMessages = [
            'category_id.required' => trans('admin.Category is required!'),
            'title.required' => trans('admin.Title is required!'),
            'slug.required' => trans('admin.Slug is required!'),
            'title.unique' => trans('admin.Title already exist!'),
            'slug.unique' => trans('admin.Slug already exist!'),
        ];
        $this->validate($request, $rules, $customMessages);
    }
 
    public function store(Request $request)
    {
        $this->validation($request);
        $blog = new Blog();
        $this->saveorupdate($request, $blog);
        return redirect()->route('admin.blog.index')->with(['message' => trans('admin.Posted Successfully'), 'alert-type' => 'success']);
    }
   
    public function edit($id)
    {
        $blog = Blog::findOrFail($id);
        return view('admin.blog.edit', compact('blog'));
    }

    public function saveorupdate($request, $blog)
    {
        $blog->image = $request->blog_image;
        $blog->title = $request->title;
        $blog->slug = ($request->filled('slug')) ? Str::slug($request->slug) : Str::slug($request->title);
        $blog->category_id = $request->category_id;
        $blog->description = $request->description;
        $blog->seo_title = $request->seo_title;
        $blog->seo_description = $request->seo_description;
        $blog->status = ($request->status == 'on') ? Blog::ACTIVE : Blog::CLOSE;
        $blog->comment_status = ($request->comment_status == 'on') ? Blog::ACTIVE : Blog::CLOSE;
        $blog->save();
    }
   
    public function update(Request $request, $id)
    {
        $this->validation($request, $id);
        $blog =  Blog::findOrFail($id);
        $this->saveorupdate($request, $blog);
        return redirect()->route('admin.blog.index')->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }
   
    public function destroy(Blog $blog)
    {
        $blog->comments()->delete();
        $blog->delete();
    }
}