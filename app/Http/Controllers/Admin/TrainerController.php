<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\TrainerDataTable;
use App\Http\Controllers\Controller;
use App\Models\Trainer;
use Illuminate\Http\Request;

class TrainerController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:trainer-index|trainer-create|trainer-edit|trainer-delete', ['only' => ['index','show']]);
        $this->middleware('permission:trainer-create', ['only' => ['create','store']]);
        $this->middleware('permission:trainer-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:trainer-delete', ['only' => ['destroy']]);
    }

    public function index(TrainerDataTable $dataTables)
    {
        return $dataTables->render('admin.trainer.index');
    }

    public function create()
    {
        return view('admin.trainer.create');
    }

    public function store(Request $request)
    {
        $this->validation($request);
        $social_item = array();
        $sociacondition =  count($request->socialicon);
        for ($i = 0; $i < $sociacondition; $i++) {
            $social_item[] = ['socialicon' => $request->socialicon[$i], 'sociallink' => $request->sociallink[$i]];
        }
        $trainer = new Trainer();
        $trainer->name = $request->name;
        $trainer->image = $request->image;
        $trainer->social_item = json_encode($social_item);
        $trainer->save();
        return redirect()->route('admin.trainer.index')->with(['message' => trans('Trainer Created Successfully'), 'alert-type' => 'success']);
    }

    public function edit(Trainer $trainer)
    {
        return view('admin.trainer.edit', compact('trainer'));
    }

    public function validation($request)
    {
        $rules = ['name' => 'required',];
        $customMessages = ['name.required' => trans('Name is required')];
        $this->validate($request, $rules, $customMessages);
    }

    public function update(Request $request, Trainer $trainer)
    {
        $this->validation($request);
        $social_item = array();
        $sociacondition =  count($request->socialicon);
        for ($i = 0; $i < $sociacondition; $i++) {
            $social_item[] = ['socialicon' => $request->socialicon[$i], 'sociallink' => $request->sociallink[$i]];
        }
        $trainer->name = $request->name;
        $trainer->image = $request->image;
        $trainer->social_item = json_encode($social_item);
        $trainer->save();
        return redirect()->route('admin.trainer.index')->with(['message' => trans('Trainer Updated Successfully'), 'alert-type' => 'success']);
    }

    public function destroy(Trainer $trainer)
    {
        $trainer->delete();
    }
}
