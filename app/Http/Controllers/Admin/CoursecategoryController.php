<?php

namespace App\Http\Controllers\Admin;

use App\DataTables\CourseCategoryDataTable;
use App\Models\Coursecategory;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;

class CoursecategoryController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:course-category-index|course-category-create|course-category-edit|course-category-delete', ['only' => ['index','show']]);
         $this->middleware('permission:course-category-create', ['only' => ['create','store']]);
         $this->middleware('permission:course-category-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:course-category-delete', ['only' => ['destroy']]);
    }

    public function index(CourseCategoryDataTable $dataTables) //view('admin.course.category.index')
    {
        return $dataTables->render('admin.course.category.index');
    }

    public function create()
    {
        return view('admin.course.category.create');
    }

    public function store(Request $request)
    {
        $customMessages = ['name.required' => trans('Name is required'), 'name.unique' => trans('Name already exist')];
        $this->validate($request, ['name' => ['required', 'unique:coursecategories,name']], $customMessages);
        $category = new Coursecategory();
        $this->saveorupdate($category, $request);
        return redirect()->route('admin.coursecategory.index')->with(['message' => trans('Course Category Create Successfully'), 'alert-type' => 'success']);
    }

    public function edit(Coursecategory $coursecategory)
    {
        return view('admin.course.category.edit', compact('coursecategory'));
    }

    public function update(Request $request, $id)
    {
        $customMessages = ['name.required' => trans('Name is required'), 'name.unique' => trans('Name already exist')];
        $this->validate($request, ['name' => ['required', 'unique:coursecategories,name,'.$id]], $customMessages);
        $category = Coursecategory::findorfail($id);
        $this->saveorupdate($category, $request);
        return redirect()->route('admin.coursecategory.index')->with(['message' => trans('Course Category Update Successfully'), 'alert-type' => 'success']);
    }

    public function saveorupdate($category, $request)
    {
        $category->name = $request->name;
        $category->status = ($request->status == 'on') ? Coursecategory::ACTIVE : Coursecategory::CLOSE;
        $category->save();
    }

    public function destroy(Coursecategory $coursecategory)
    {
        $course_category = $coursecategory->course;
        if(count($course_category)>0) return response(['status' => 'failed']);    
        $coursecategory->delete();
        return response(['status' => 'success']);
    }
}
