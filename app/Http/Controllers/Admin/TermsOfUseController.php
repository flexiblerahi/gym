<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\TermsOfUse;
use Illuminate\Http\Request;

class TermsOfUseController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:terms-of-use-index|terms-of-use-create|terms-of-use-edit|terms-of-use-delete', ['only' => ['index','show']]);
         $this->middleware('permission:terms-of-use-create', ['only' => ['create','store']]);
         $this->middleware('permission:terms-of-use-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:terms-of-use-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        $tos = TermsOfUse::first();
        return view('admin.termsOfUseIndex', compact('tos'));
    }

    public function store(Request $request)
    {
        $rules = ['content' => 'required'];
        $customMessages = ['content.required' => trans('Content is required!')];
        $this->validate($request, $rules, $customMessages);
        $tos = TermsOfUse::first();
        if (!$tos) $tos = new TermsOfUse();
        $tos->content = $request->content;
        $tos->save();
        return redirect()->back();
    }
}
