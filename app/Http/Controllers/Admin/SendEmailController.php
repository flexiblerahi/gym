<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class SendEmailController extends Controller
{
    function __construct()
    {
        $this->middleware('permission:send-email-index|send-email-create|send-email-edit|send-email-delete', ['only' => ['index','show']]);
        $this->middleware('permission:send-email-create', ['only' => ['create','store']]);
        $this->middleware('permission:send-email-edit', ['only' => ['edit','update']]);
        $this->middleware('permission:send-email-delete', ['only' => ['destroy']]);
    }

    public function index()
    {
        return view('admin.sendEmailIndex');
    }
 
    public function store(Request $request)
    {
        $rules = [
            'title' => 'required',
            'body' => 'required',
        ];
        $customMessages = [
            'title.required' => trans('Subject is required!'),
            'body.required' => trans('Message is required!'),
        ];
        $this->validate($request, $rules, $customMessages);
        $data = [
            'title' => $request->subject,
            'body' => $request->message,
        ];
        $emails = [];
        foreach (\App\Models\Subscriber::where(['is_confirmed' => 1])->get() as  $subscriber) {
            $emails[] = $subscriber->email;
        }
        Mail::to($emails)->send(new \App\Mail\SendEmail($data));

        $notification = trans('admin.Email Sent to All Subscribers!');
        $notification = ['message' => $notification, 'alert-type' => 'success'];
        return redirect()->back()->with($notification);
    }
}
