<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\About;
use App\Models\Setting;
use App\Models\Shipping;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\DB;
use ZipArchive;

class UserDataController extends Controller
{
    function __construct()
    {
         $this->middleware('permission:user-data-index|user-data-create|user-data-edit|user-data-delete', ['only' => ['index','show']]);
         $this->middleware('permission:user-data-create', ['only' => ['create','store']]);
         $this->middleware('permission:user-data-edit', ['only' => ['edit','update']]);
         $this->middleware('permission:user-data-delete', ['only' => ['destroy']]);
    }

    public function backup()
    {
        $tables = DB::select('SHOW TABLES');
        $paths = getAllResourceFiles(database_path('seeders/'));
        foreach ($paths as $key => $path) {
            if (!str_contains($path, 'DatabaseSeeder.php')) {
                unlink($path);
            }
        }

        foreach ($tables as $key) {
            $databaseName = Config::get('database.connections.' . Config::get('database.default'));
            $database = 'Tables_in_' . $databaseName['database'];
            $table = $key->{$database};
            try {
                Artisan::call("iseed $table");
            } catch (\Throwable $th) {
                return $th;
            }
        }
        $zip_file = 'gym_backup_' . date('y-m-d') . '.zip';
        $zip = new \ZipArchive();
        $zip->open($zip_file, \ZipArchive::CREATE | \ZipArchive::OVERWRITE);
        $database = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(database_path('seeders/'))
        );
        foreach ($database as $name => $file) {
            if (!$file->isDir()) {
                $filePath     = $file->getRealPath();
                $relativePath = 'database/seeders/' .  basename($filePath);
                $zip->addFile($filePath, $relativePath);
            }
        }
        $files = new \RecursiveIteratorIterator(
            new \RecursiveDirectoryIterator(public_path('assets/uploads/images/'))
        );
        foreach ($files as $name => $file) {
            if (!$file->isDir()) {
                $filePath = $file->getRealPath();

                $relativePath = 'public/assets/uploads/images/' .  basename($filePath);
                $zip->addFile($filePath, $relativePath);
            }
        }
        $zip->close();
        return response()->download($zip_file)->deleteFileAfterSend(true);
    }

    public function restore(Request $request)
    {
        $zip = new ZipArchive;
        $res = $zip->open($request->file('backup'));
        if ($res === TRUE) {
            $zip->extractTo(base_path('/'));
            $zip->close();
            Artisan::call("db:seed");
            return redirect()->back()->with(['message' => trans('admin.Restored Successfully'), 'alert-type' => 'success']);
        } else {
            return redirect()->back()->with(['message' => trans('admin.Error restoring'), 'alert-type' => 'error']);
        }
    }

    public function maintenance_mode(Request $request)
    {   
        if (checkMaintenance()) Artisan::call('up');
        else Artisan::call('down');
        return redirect()->back()->with(['message' => trans('admin.Updated Successfully'), 'alert-type' => 'success']);
    }

    public function reset()
    {  
        $nowdate = now()->toDateTimeString(); 
        $paths = getAllResourceFiles(public_path('assets/uploads/images'));
        $userimagepaths = getAllResourceFiles(public_path('assets/uploads/user/avatar'));
        $admin = auth()->user();
        $setting = Setting::first();
        $aboutus = About::first();
        Artisan::call('migrate:fresh');
        
        // $migrations = array('users', 'menus', 'products', 'password_resets', 'failed_jobs', 'personal_access_tokens', 'menus', 'media', 'settings', 'cookies', 'payment_gateways', 'email_settings', 'email_templates', 'blogs', 'blog_categories', 'blog_comments', 'email_template_variables', 'subscribers', 'page_builders', 'page_items', 'page_headers', 'page_layouts', 'page_components', 'page_component_categories', 'sliders', 'faqs', 'services', 'abouts', 'testimonials', 'social_links', 'footer_contact_items', 'google_maps', 'footer_information', 'coursecategories', 'courses', 'product_categories', 'orders', 'transactions', 'products', 'product_reviews', 'user_adresses', 'shipping_countries', 'shop_settings', 'order_items', 'order_addresses', 'privacy_policies', 'terms_of_uses', 'contact_messages', 'contact_pages', 'course_reviews', 'trainers', 'coupons', 'shippings', 'bill_addresses', 'our_processes');
        // foreach($migrations as $migration) {
        //     DB::table($migration)->delete();
        //     DB::raw("ALTER TABLE `".$migration."` AUTO_INCREMENT = 0");
        // }
        Artisan::call('db:seed');
        DB::table('abouts')->insert([
            'icon' => 'fas fa-dumbbell',
            'title' => 'About Us',
            'subtitle' => 'We Inspire and Help Our Customers',
            'description' => '&lt;p&gt;Welcome to our gym &amp;amp; fitness training center. Sore today, strong tomorrow. Improve your fitness today. We are providing the best gym and fitness facilities.&lt;/p&gt;',
            'bottom_text' => 'Lorem Ipsum is not simply random text',
            'first_image' => $aboutus->first_image,
            'second_image' => $aboutus->second_image,
            'third_image' => $aboutus->third_image,
            'first_video' => 'https://youtu.be/pzhzS7FDFQg',
            'second_video' => 'https://youtu.be/8D_ItZBvbmk',
            'item' => '[{"icon": "far fa-running", "title": "Functional ", "subtitle": "With 8 detectors to pick from various"}, {"icon": "fas fa-dumbbell", "title": "Weight &amp;amp; Boxing lifting", "subtitle": "You can have peace of mind"}, {"icon": "far fa-skiing-nordic", "title": "Weight fuck lifting", "subtitle": "You fuck set Safewatch to detect"}]',
            'client_section' => '{"title": "Our Happy Clients", "subtitle": "Welcome to our gym and fitness training center. Sore today, strong tomorrow. Improve your fitness today."}',
            'created_at' => $nowdate,
            'updated_at' => $nowdate,
        ]);
        
        DB::table('shippings')->insert([
            [
                'title' => 'Free Shipping',
                'short_description' =>'Shipment will be within 10-15 Days',
                'cost' => 0,
                'status' => Shipping::ACTIVE,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'title' => 'Standard Shipping',
                'short_description' =>'Shipment will be within 5-10 Day.',
                'cost' => 5,
                'status' => Shipping::ACTIVE,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'title' => '2-Day Shipping',
                'short_description' =>'Shipment will be within 2 Days.',
                'cost' => 10,
                'status' => Shipping::ACTIVE,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'title' => 'Same day delivery',
                'short_description' =>'Shipment will be within 1 Day',
                'cost' => 20,
                'status' => Shipping::ACTIVE,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ]
        ]);
        DB::table('media')->insert([
            [
                'file_name' => 'Site Favicon',
                'file_path' => $setting->site_favicon,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'file_name' => 'Section Icon',
                'file_path' => $setting->section_icon,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'file_name' => 'header logo',
                'file_path' => $setting->site_logo,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'file_name' => 'Breacumb',
                'file_path' => $setting->breadcumb,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'file_name' => 'Admin Avatar',
                'file_path' => $admin->avatar,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'file_name' => 'About Us Image 1',
                'file_path' => $aboutus->first_image,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'file_name' => 'About Us Image 2',
                'file_path' => $aboutus->second_image,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ], [
                'file_name' => 'About Us Image 3',
                'file_path' => $aboutus->third_image,
                'file_type' => 'image',
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ]
        ]);
        DB::table('settings')->insert([
            'site_name' => 'Gym',
            'site_time_zone' => 'Asia/Dhaka',
            'site_currency' => 'USD',
            'site_currency_icon' => '$',
            'site_direction' => 'LTR',
            'site_logo' => $setting->site_logo,
            'site_favicon' => $setting->site_favicon,
            'section_icon' => $setting->section_icon,
            'footer_logo' => $setting->footer_logo,
            'breadcumb' => $setting->breadcumb,
            'user_avatar' => $setting->user_avatar,
            'testimonial_background' => $setting->testimonial_background,
            'blog_pagination' => 2,
            'blog_comment_pagination' => 2,
            'course_pagination' => 2,
            'product_pagination' => 2,
        ]);

        DB::table('shop_settings')->insert([
            [
                'tax' => 1,
                'status' => 1,
                'created_at' => $nowdate,
                'updated_at' => $nowdate,
            ]
        ]);
       
        foreach($paths as $path) {
            if (str_contains($path, $setting->site_logo)) ''; 
            else if (str_contains($path, $setting->site_favicon)) ''; 
            else if (str_contains($path, $setting->section_icon)) ''; 
            else if (str_contains($path, $setting->footer_logo)) ''; 
            else if (str_contains($path, $setting->breadcumb)) ''; 
            else if (str_contains($path, $setting->testimonial_background)) ''; 
            else if (str_contains($path, $setting->user_avatar)) ''; 
            else if (str_contains($path, $aboutus->first_image))  ''; 
            else if (str_contains($path, $aboutus->second_image))  ''; 
            else if (str_contains($path, $aboutus->third_image))  ''; 
            else unlink($path);
        }

        foreach($userimagepaths as $userimagepath) {
            if(str_contains($userimagepath, $admin->avatar)) {}
            else unlink($userimagepath);
        }
        return redirect()->back()->with(['message' => trans('admin.Reset Successfully'), 'alert-type' => 'success']);
    }
}