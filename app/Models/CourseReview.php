<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CourseReview extends Model
{
    use HasFactory;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['is_approved', 'user_id', 'course_id', 'rating', 'comment'];
    const CANCEL = 0, PENDING = 1, APPROVED = 2;
    const ALREADYCOMMENT = 'You Have Already Review in This Course', 
    SUCCESSCREATE = 'Course Review Created successfully.',
    ISPURCHASE = 'Please Purchase This Course Before Review It';

    public function user()
    {
        return $this->belongsTo(User::class);
        // return $this->belongsTo(Coursecategory::class);
    }
    
    public function course()
    {
        return $this->hasOne(Course::class, 'id', 'course_id');
    }
}
