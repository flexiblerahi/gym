<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderAddress extends Model
{
    use HasFactory;
    const DIFFERENTSHIPPING = 1, NOTDIFFERENT = 0;
    protected $fillable = [
        'id','user_id','order_id', 'billing_name', 'billing_phone', 'billing_email','billing_country', 'billing_city', 'billing_post_code', 'billing_address',
        'is_shipping', 'shipping_name', 'shipping_phone', 'shipping_email','shipping_country', 'shipping_city', 'shipping_post_code', 'shipping_address',
    ];
}
