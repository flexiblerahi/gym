<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Shipping extends Model
{
    use HasFactory;
    protected $fillable = [
        'title', 'short_description', 'cost', 'status',
    ];

    const ACTIVE = 1, CLOSE = 2;
    
}
