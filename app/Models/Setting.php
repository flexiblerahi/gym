<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
    use HasFactory;
    const LEFT = 1, RIGHT = 2;
    protected $fillable = ['blog_pagination', 'blog_search_pagination', 'blog_comment_pagination', 'course_pagination', 'course_search_pagination', 'product_pagination', 'product_search_pagination'];
}
