<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class SocialLink extends Model
{
    use HasFactory;
    const ONFOOTER = 1;
    const ACTIVE = 1, CLOSE = 0;
}
