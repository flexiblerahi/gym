<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Blog extends Model
{
    use HasFactory;
    const ACTIVE =  1, CLOSE = 0;

    public function comments()
    {
        return $this->hasMany(BlogComment::class);
    }
}
