<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ProductReview extends Model
{
    use HasFactory;

    protected $fillable = ['status', 'user_id', 'product_id', 'rating', 'comment'];
    const PENDING = 0, APPROVED = 1, CANCEL = 2;
    const ALREADYCOMMENT = 'You Have Already Review in This Course',
    SUCCESSCREATE = 'Product Review Created successfully.',
    ISPURCHASE = 'Please Purchase This Product Before Review It';
    
    public function customer()
    {
        return $this->hasOne(User::class, 'id', 'user_id');
    }

    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'product_id');
    }
}
