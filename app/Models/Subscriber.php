<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Subscriber extends Model
{
    use HasFactory;
    const CONFIRMED = 1, NOTCONFIRMED = 0;
    protected $fillable = ['email', 'is_confirmed'];
}
