<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coursecategory extends Model
{
    use HasFactory;
    const ACTIVE = 1, CLOSE =0;
    protected $fillable = ['name', 'description', 'status'];

    public function course()
    {
        return $this->hasMany(Course::class);
    }
}
