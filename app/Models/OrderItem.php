<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    use HasFactory;
    const PRODUCT = 1, COURSE = 2, COUSEQUANTITY = 0; 


    public function product()
    {
        return $this->hasOne(Product::class, 'id', 'item_id');
    }

    public function course()
    {
        return $this->hasOne(Course::class, 'id', 'item_id');
    }
}
