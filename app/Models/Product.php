<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Product extends Model
{
    use HasFactory;
    const DETAILS = 10;
    const ACTIVE = 1, CLOSE=0;
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = ['name', 'image', 'slug', 'price', 'discount_price', 'description', 'sku', 'brand', 'weight', 'stock', 'category_id', 'status'];
    protected $table = 'products';

    public function getCategory()
    {
        return $this->hasOne(ProductCategory::class, 'id', 'category_id');
    }

    public function reviews()
    {
        return $this->hasMany(ProductReview::class, 'product_id', 'id');
    }

    public function orders()
    {
        return $this->hasMany(OrderItem::class, 'item_id', 'id');
    }


}
