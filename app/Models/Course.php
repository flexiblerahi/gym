<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    use HasFactory;
    const ACTIVE = 1, CLOSE = 0;
    const COURSEPAGEID = 38, COURSEDETAILSPAGEID = 39;
    const BEGINNER = 1, INTERMEDIATE = 2, ADVANCE = 3, PRO = 4;
    const PAID = 1, NONPAID = 2;
    protected $fillable = ['name', 'image', 'status', 'price', 'offer_price', 'level', 'is_paid', 'slug', 'description', 'coursecategory_id'];

    public function coursecategory()
    {
        return $this->belongsTo(Coursecategory::class);
    }

    public function coursereviews()
    {
        return $this->hasMany(CourseReview::class);
    }
}
