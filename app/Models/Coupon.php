<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Coupon extends Model
{
    use HasFactory;
    protected $fillable = [
        'name', 'code', 'status', 'amount', 'type'
    ];
    
    const ACTIVE = 1, CLOSE = 0;
    const INPRICE = 1, INPERCENTAGE = 0;
    const NOTFOUNDMSG = 'Coupon does not match';
}
 