<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class ShopSetting extends Model
{
    use HasFactory;
    protected $fillable = [
        'default_shipping_fee',
        'tax',
        'status',
    ];
    const VISIBLE = 1, DISABLE = 0;
}
