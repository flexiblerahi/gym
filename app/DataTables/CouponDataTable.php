<?php

namespace App\DataTables;

use App\Models\Coupon;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CouponDataTable extends DataTable
{
    public function dataTable($query)
    {    
        return datatables()
            ->eloquent($query)
            ->addColumn('status', function ($status) {
                if ($status->status == Coupon::ACTIVE) return '<div class="btn btn-success btn-sm"> '.trans('active').' </div>';
                return '<div class="btn btn-danger btn-sm"> '.trans('close').' </div>';
            })
            ->addColumn('type', function ($type) {
                if ($type->type == Coupon::INPRICE) return '<div class="btn btn-primary btn-sm"> '.trans('In Price').' </div>';
                return '<div class="btn btn-warning btn-sm"> '.trans('In Percentage').' </div>';
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.coupon.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.coupon.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['name', 'code', 'status', 'type', 'created_at', 'action']);
    }

    public function query(Coupon $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('coupon-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('name')->title(trans('Name')),
            Column::make('code')->title(trans('Code')),
            Column::make('amount')->title(trans('Amount')),
            Column::make('status')->title(trans('Status'))->addClass('text-center'),
            Column::make('type')->title(trans('Type'))->addClass('text-center'),
            Column::make('created_at')->title(trans('Created At')),
            Column::computed('action', trans('Action'))
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')
        ];
    }

    protected function filename()
    {
        return 'coupon_' . date('YmdHis');
    }
}
