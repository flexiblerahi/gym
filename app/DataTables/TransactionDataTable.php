<?php

namespace App\DataTables;

use App\Models\Transaction;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TransactionDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('customer_name', function($query) {
                return $query->customer->name;
            })
            ->addColumn('amount', function ($query)
            { 
                return currencyPosition($query->amount);
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->rawColumns(['customer_name', 'amount', 'created_at']);
    }

    public function query(Transaction $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('transaction-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('customer_name')->title(trans('Customer Name')),
            Column::make('amount')->title(trans('Amount')),
            Column::make('transaction_id')->title(trans('Transaction Id'))->width(100),
            Column::make('payment_method')->title(trans('Payment Method')),
            Column::make('created_at')->title(trans('Created At'))
        ];
    }

    protected function filename()
    {
        return 'Transaction_' . date('YmdHis');
    }
}
