<?php

namespace App\DataTables;

use App\Models\Blog;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BlogDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('image', function ($image) {
                return '<img src=' . asset(MediaImage($image->image, 1)) . ' border="0" width="100" class="img-rounded" align="center" />';
            })
            ->addColumn('status', function ($status) {
                if ($status->status == 1) return '<div class="btn btn-success btn-sm"> Published </div>';
                return '<div class="btn btn-secondary btn-sm"> Draft </div>';
            })
            ->addColumn('comment_status', function ($status) {
                if ($status->comment_status == 1) return '<div class="btn btn-primary btn-sm"> Enabled </div>';
                return '<div class="btn btn-danger btn-sm"> Disabled </div>';
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.blog.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.blog.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['image', 'created_at', 'action', 'status', 'comment_status', 'on_featured']);
    }

    public function query(Blog $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('blog-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('image')->title(trans('Image')),
            Column::make('title')->title(trans('Title'))->width(100),
            Column::make('status')->title(trans('Status')),
            Column::make('comment_status')->title(trans('Comment Status')),
            Column::make('created_at')->title(trans('Created At')),
            Column::computed('action', trans('Action'))
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')
        ];
    }

    protected function filename()
    {
        return 'Blog_' . date('YmdHis');
    }
}
