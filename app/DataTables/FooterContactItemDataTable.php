<?php

namespace App\DataTables;

use App\Models\FooterContactItem;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class FooterContactItemDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('icon', function ($icon) {
                return '<i class="'.$icon->icon.'"></i>';
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.footer-contact-item.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.footer-contact-item.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })

            ->rawColumns(['action', 'icon']);
    }

    public function query(FooterContactItem $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('footercontactitem-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                        Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset')),

                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('Id'))->width(100),
            Column::make('name')->title(trans('Name')),
            Column::make('icon')->title(trans('Icon')),
            Column::computed('action', trans('Action'))
            ->exportable(false)
            ->printable(false)
            ->width(100)
            ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'FooterContactItem_' . date('YmdHis');
    }
}
