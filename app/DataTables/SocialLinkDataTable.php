<?php

namespace App\DataTables;

use App\Models\SocialLink;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class SocialLinkDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('icon', function ($icon) {
                return '<i class="'.$icon->icon.'"></i>';
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.social-link.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.social-link.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['action', 'icon']);
    }

    public function query(SocialLink $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('headersociallink-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                        Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('Id'))->width(100),
            Column::make('icon')->title(trans('Icon')),
            Column::make('link')->title(trans('Link')),
            Column::computed('action', trans('Action'))
            ->exportable(false)
            ->printable(false)
            ->width(100)
            ->addClass('text-center'),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'SocialLink_' . date('YmdHis');
    }
}
