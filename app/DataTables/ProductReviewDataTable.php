<?php

namespace App\DataTables;

use App\Models\ProductReview;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductReviewDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($action) {
                return '
                <a class="btn-sm text-white ' . ($action->status === ProductReview::PENDING ? "btn-warning" : "btn-success") . ' mr-2" href="' . route('admin.product-review.edit', $action->id) . '">
                    <i class="' . ($action->status === ProductReview::PENDING ? "fas fa-eye-slash" : "fas fa-eye") . '"></i>
                </a>
                <a class="btn-sm btn-danger delete" href="' . route('admin.product-review.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->addColumn('customer_name', function ($customer_name) {
                return $customer_name->customer->name;
            })
            ->addColumn('rating', function ($rating) {
                $star = null;
                for ($i = 0; $i < $rating->rating; $i++) {
                    $star .= '<i class="fas fa-star    "></i>';
                }
                return $star;
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->addColumn('product', function ($product) {

                return view('admin.reviewProduct', compact('product'));
            })
            ->rawColumns(['status', 'action', 'customer_name', 'rating', 'product']);
    }

    public function query(ProductReview $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('productreview-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset')),
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('rating')->width(100),
            Column::make('comment')->title(trans('Comment')),
            Column::make('product')->title(trans('Product')),
            Column::make('customer_name')->width(150),
            Column::make('created_at')->width(100)->title('Date'),
            Column::make('action')
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'ProductReview_' . date('YmdHis');
    }
}
