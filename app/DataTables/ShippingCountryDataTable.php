<?php

namespace App\DataTables;

use App\Models\ShippingCountry;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ShippingCountryDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.shipping-country.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.shipping-country.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['action']);
    }

    public function query(ShippingCountry $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('shippingcountry-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset')),
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('country')->title(trans('Country')),
            Column::computed('action', trans('Action'))
            ->exportable(false)
            ->printable(false)
            ->width(100)
            ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'ShippingCountry_' . date('YmdHis');
    }
}
