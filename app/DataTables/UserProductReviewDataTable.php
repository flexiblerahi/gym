<?php

namespace App\DataTables;

use App\Models\ProductReview;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UserProductReviewDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('product', function ($query) {
                return '<p>'.$query->product->name.'</p>';
            })
            ->addColumn('rating', function ($rating) {
                $star = null;
                for ($i = 0; $i < $rating->rating; $i++) {
                    $star .= '<i class="fas fa-star    "></i>';
                }
                return $star;
            })
            ->addColumn('status', function ($query) {
                if($query->status == ProductReview::APPROVED) return '<div class="btn btn-success btn-sm"> ' . trans('Approved') .' </div>';
                else if($query->status == ProductReview::PENDING) return '<div class="btn btn-warning btn-sm"> ' . trans('Pending') .' </div>';
                else return '<div class="btn btn-danger btn-sm"> '. trans('Cancel') .' </div>';
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->rawColumns(['status', 'product', 'rating', 'created_at']);
    }

    public function query(ProductReview $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('course-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1);
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('product')->title(trans('Product')),
            Column::computed('status', trans('Is Approved')),
            Column::make('rating')->title(trans('Rating')),
            Column::make('comment')->title(trans('Comment')),
            Column::make('created_at')->title(trans('Created At'))
        ];
    }

    protected function filename()
    {
        return 'Course_' . date('YmdHis');
    }
}
