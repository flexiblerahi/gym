<?php

namespace App\DataTables;

use App\Models\Subscriber;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class EmailSubscriberDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            });
    }

    public function query()
    {
        return Subscriber::where(['is_confirmed' => 1]);
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('emailsubscriber-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('email')->title(trans('Email')),
            Column::make('created_at')->title(trans('Created At'))->addClass('text-right'),
        ];
    }

    protected function filename()
    {
        return 'EmailSubscriber_' . date('YmdHis');
    }
}
