<?php

namespace App\DataTables;

use App\Models\Product;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ProductDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('image', function ($image) {
                $media = MediaImage($image->image, 1);
                $url = asset("$media");
                return '<img src=' . $url . ' border="0" width="100" class="img-rounded" align="center" />';
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->addColumn('status', function ($action) {
                if($action->status == Product::ACTIVE) {
                    return '<p class="btn btn-primary">'.trans('Active').'</p>';
                } else return '<p class="btn btn-second-secondary">'.trans('Deactive').'</p>';
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.product.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.product.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['image', 'created_at', 'status', 'action']);
    }

    public function query(Product $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('product-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),

                        Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset')),
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('name')->title(trans('Name')),
            Column::make('image')->title(trans('Image')),
            Column::make('stock')->title(trans('Stock')),
            Column::make('created_at')->title(trans('Created At')),
            Column::make('status')->title(trans('Status')),
            Column::computed('action', trans('Action'))
                  ->exportable(false)
                  ->printable(false)
                  ->width(100)
                  ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Product_' . date('YmdHis');
    }
}
