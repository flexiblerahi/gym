<?php

namespace App\DataTables;

use App\Models\CourseReview;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UserCourseReviewDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('course', function ($query) {
                return '<p>'.$query->course->name.'</p>';
            })
            ->addColumn('rating', function ($rating) {
                $star = null;
                for ($i = 0; $i < $rating->rating; $i++) {
                    $star .= '<i class="fas fa-star    "></i>';
                }
                return $star;
            })
            ->addColumn('is_approved', function ($query) {
                if ($query->is_approved == CourseReview::APPROVED) return '<div class="btn btn-success btn-sm"> ' . trans('Approved') .' </div>';
                else if ($query->is_approved == CourseReview::PENDING) return '<div class="btn btn-warning btn-sm"> ' . trans('Pending') .' </div>';
                else return '<div class="btn btn-danger btn-sm"> '. trans('Cancel') .' </div>';
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->rawColumns(['is_approved', 'rating', 'course', 'created_at']);
    }

    public function query(CourseReview $model)
    {
        return $model->newQuery()->where('user_id', auth()->id());
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('course-review-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->orderBy(1);
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('course')->title(trans('Course')),
            Column::make('is_approved')->title(trans('Is Approved')),
            Column::make('rating')->title(trans('Rating')),
            Column::make('comment')->title(trans('Comment')),
            Column::make('created_at')->title(trans('Created At'))
        ];
    }

    protected function filename()
    {
        return 'User_Course_Review_' . date('YmdHis');
    }
}
