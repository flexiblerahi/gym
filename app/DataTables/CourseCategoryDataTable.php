<?php

namespace App\DataTables;

use App\Models\Coursecategory;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class CourseCategoryDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('status', function ($status) {
                if ($status->status == 1) return '<div class="btn btn-success btn-sm"> ' . trans('active') .' </div>';
                return '<div class="btn btn-secondary btn-sm"> '. trans('close') .' </div>';
            })
            ->addColumn('name', function ($name) {
                return $name->name;
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.coursecategory.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.coursecategory.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['status', 'name', 'created_at', 'action']);
    }

    public function query(Coursecategory $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('course-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('status')->title(trans('Status')),
            Column::make('name')->title(trans('Name')),
            Column::make('created_at')->title(trans('Created At')),
            Column::computed('action', trans('Action'))
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')
        ];
    }

    protected function filename()
    {
        return 'Coursecategory_' . date('YmdHis');
    }
}
