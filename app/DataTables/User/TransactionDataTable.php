<?php

namespace App\DataTables\User;

use App\Models\Transaction;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class TransactionDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', 'transaction.action')
            ->addColumn('customer_name', function($query){
                return $query->customer->name;
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->rawColumns(['customer_name']);
    }

    public function query(Transaction $model)
    {
        $user = Auth::user()->id;
        return $model->where('user_id',$user)->newQuery();
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('transaction-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('customer_name')->title(trans('Customer Name')),
            Column::make('amount')->title(trans('Amount')),
            Column::make('currency')->title(trans('Currency')),
            Column::make('transaction_id')->title(trans('Transaction Id')),
            Column::make('payment_method')->title(trans('Payment Method')),
            Column::make('created_at')->title(trans('Created At'))->title('Date'),

        ];
    }

    protected function filename()
    {
        return 'Transaction_' . date('YmdHis');
    }
}
