<?php

namespace App\DataTables\User;

use App\Models\Order;
use Illuminate\Support\Facades\Auth;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class OrderDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function($action){
                return '<a class="btn-sm btn-primary" href="'.route('user.order.show', $action->id).'"><i class="fas fa-eye "></i> View</a>';
            })
            ->addColumn('items', function($order){
                return view('admin.order.item', compact('order'));
            })
            ->addColumn('total_amount', function($action){
                return numberFormat($action->total_amount);
            })
            ->addColumn('status', function ($status) {
                if ($status->status !== 'complete') return '<div class="btn btn-danger btn-sm"> '.$status->status.' </div>';
                return '<div class="btn btn-secondary btn-sm"> '.$status->status.' </div>';
            })
            ->rawColumns(['status' ,'action','items','total_amount']);
    }

    public function query(Order $model)
    {
        $user = Auth::user()->id;
        return $model->where('user_id',$user)->newQuery();
    }

    public function html()
    {
        return $this->builder()
                    ->setTableId('order-table')
                    ->columns($this->getColumns())
                    ->minifiedAjax()
                    ->dom('Bfrtip')
                    ->orderBy(1)
                    ->buttons(
                        Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset')),
                    );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('Id'))->width(100),
            Column::make('items')->title(trans('Items')),
            Column::make('status')->title(trans('Status'))->width(100),
            Column::make('total_amount')->title(trans('Total Amount'))->width(100),
            Column::computed('action', trans('Action'))
            ->exportable(false)
            ->printable(false)
            ->width(100)
            ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'Order_' . date('YmdHis');
    }
}
