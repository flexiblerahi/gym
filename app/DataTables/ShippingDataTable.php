<?php

namespace App\DataTables;

use App\Models\Shipping;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ShippingDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('status', function ($status) {
                if ($status->status == Shipping::ACTIVE) return '<div class="btn btn-success btn-sm"> '.trans('active').' </div>';
                else return '<div class="btn btn-danger btn-sm"> '.trans('close').' </div>';
            })
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.shipping.edit',  $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.shipping.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['title', 'cost', 'status', 'created_at', 'action']);
    }

    public function query(Shipping $model)
    {
        return $model->newQuery()->orderby('id', 'asc');
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('shipping-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('title')->title(trans('Title')),
            Column::make('cost')->title(trans('Cost')),
            Column::make('status')->title(trans('Status'))->addClass('text-center'),
            Column::make('created_at')->title(trans('Created At')),
            Column::computed('action', trans('Action'))
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')
        ];
    }

    protected function filename()
    {
        return 'shipping_' . date('YmdHis');
    }
}
