<?php

namespace App\DataTables;

use App\Models\Admin;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class UserDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('action', function ($query) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.users.edit',  $query->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.users.destroy', $query->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->addColumn('role', function ($query) {
                $user = Admin::find($query->id);
                $userRole = $user->roles->pluck('name', 'name')->all();
                $badge = '';
                foreach ($userRole as $key => $value) {
                    $badge .= '<span class="badge badge-primary">'.$value.'</span>';
                }
                return $badge;
            })
            ->rawColumns(['role', 'created_at', 'action']);
    }

    public function query(Admin $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('user-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset')),
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID'))->width(100),
            Column::make('name')->title(trans('Name')),
            Column::make('role')->title(trans('Role')),
            Column::make('created_at')->title(trans('Registration Date')),
            Column::computed('action', trans('Action'))
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'User_' . date('YmdHis');
    }
}
