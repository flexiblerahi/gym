<?php

namespace App\DataTables;

use App\Models\BlogComment;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class BlogCommentDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->order(function ($query) {
                    $query->orderBy('id', 'asc');
            })
            ->addColumn('blog', function($query) {
                return  '<div class=""> '.$query->blog->title.' </div>';
            })
            ->addColumn('action', function ($action) {
                return '
                <a class="btn-sm text-white '.($action->status === 0 ? "btn-warning" : "btn-success").' mr-2" href="'.route('admin.blog-comment.edit', $action->id).'">
                    <i class="'.($action->status === 0 ? "fas fa-eye-slash" : "fas fa-eye").'"></i>
                </a>
                <a class="btn-sm btn-danger delete" href="'.route('admin.blog-comment.destroy', $action->id).'"><i class="far fa-trash-alt"></i></a>';
            })
            ->addColumn('status', function ($status) {
                if ($status->status == 1) {
                    return '<div class="btn btn-success btn-sm"> Active </div>';
                } else return '<div class="btn btn-secondary btn-sm"> Deactivated </div>';
            })
            ->rawColumns(['status', 'action', 'blog']);
    }

    public function query(BlogComment $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('blogcomment-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('blog')->title(trans('Blog')),
            Column::make('name')->title(trans('Name'))->width(150),
            Column::make('email')->title(trans('Email')),
            Column::make('comment')->title(trans('Comment')),
            Column::computed('action', trans('Action'))
                ->exportable(false)
                ->printable(false)
                ->width(100)
                ->addClass('text-center')
        ];
    }

    protected function filename()
    {
        return 'BlogComment_' . date('YmdHis');
    }
}
