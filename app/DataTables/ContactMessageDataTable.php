<?php

namespace App\DataTables;

use App\Models\ContactMessage;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class ContactMessageDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('created_at', function ($query) {
                return \Carbon\Carbon::parse($query->created_at)->format('M d - Y h:i a');
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.contact-messages.show',  $action->id) . '"><i class="fas fa-eye"></i> View</a>';
            })
            ->rawColumns(['created_at', 'action']);
    }

    public function query(ContactMessage $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('contactmessage-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset')),
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('name')->title(trans('Name')),
            Column::make('email')->title(trans('Email')),
            Column::make('created_at')->title(trans('Created At')),
            Column::computed('action', trans('Action'))
                  ->exportable(false)
                  ->printable(false)
                  ->width(100)
                  ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'ContactMessage_' . date('YmdHis');
    }
}
