<?php

namespace App\DataTables;

use App\Models\PageBuilder;
use Yajra\DataTables\Html\Button;
use Yajra\DataTables\Html\Column;
use Yajra\DataTables\Services\DataTable;

class PageBuilderDataTable extends DataTable
{
    public function dataTable($query)
    {
        return datatables()
            ->eloquent($query)
            ->addColumn('link', function ($action) {
                return '<div class="input-group mb-3">
                            <input type="text" class="form-control" data-input="'.url("$action->slug").'" value="'.url("$action->slug").'"/>
                            <div class="input-group-append">
                            <button data-button="'.url("$action->slug").'" class="btn btn-outline-secondary copy" type="button">
                            <i class="fas fa-paperclip    "></i> Copy</button>
                            </div>
                        </div>';
            })
            ->addColumn('action', function ($action) {
                return '<a class="btn-sm btn-primary" href="' . route('admin.page-builder.edit', $action->id) . '"><i class="far fa-edit"></i></a>
                        <a class="btn-sm btn-danger delete" href="' . route('admin.page-builder.destroy', $action->id) . '"><i class="far fa-trash-alt"></i></a>';
            })
            ->rawColumns(['action', 'link']);
    }

    public function query(PageBuilder $model)
    {
        return $model->newQuery()->oldest();
    }

    public function html()
    {
        return $this->builder()
            ->setTableId('pagebuilder-table')
            ->columns($this->getColumns())
            ->minifiedAjax()
            ->dom('Bfrtip')
            ->orderBy(1)
            ->buttons(
                Button::make('create')->text('<i class="fa fa-plus"></i>&nbsp;'.trans('Create')),
                Button::make('reset')->text('<i class="fa fa-undo"></i>&nbsp;'.trans('Reset'))
            );
    }

    protected function getColumns()
    {
        return [
            Column::make('id')->title(trans('ID')),
            Column::make('title')->title(trans('Title')),
            Column::make('slug')->title(trans('slug')),
            Column::make('link')->title(trans('link')),
            Column::computed('action', trans('Action'))
                ->exportable(false)
                ->printable(false)
                ->width(150)
                ->addClass('text-center'),
        ];
    }

    protected function filename()
    {
        return 'PageBuilder_' . date('YmdHis');
    }
}
