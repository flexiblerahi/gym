<?php

use App\Http\Controllers\Admin\Auth\AdminLoginController;
use App\Http\Controllers\Admin\DashboardController;
use App\Http\Controllers\Admin\SettingsController;
use App\Http\Controllers\Admin\MediaManagerController;
use App\Http\Controllers\Admin\UserDataController; 

use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['XSS', 'HtmlSpecialchars']], function () {
    Route::get('lang', function () {
        generateLangFrontend('views/frontend', 'lang/en/frontend.php');
        generateLangBackend('views/admin', 'lang/en/admin.php');
    });
    // Admin Routes
    Route::get('/login',   [AdminLoginController::class, 'adminLoginForm'])->name('admin.login');
    Route::post('/login',  [AdminLoginController::class, 'adminCheckLogin'])->name('admin.login');
    Route::post('/logout', [AdminLoginController::class, 'adminLogout'])->name('admin.logout');

    Route::group(['as' => 'admin.', 'middleware' => 'auth:admin'], function () {
        // Dashoboard
        Route::get('/',          [DashboardController::class, 'index'])->name('dashboard');
        Route::get('/dashboard', [DashboardController::class, 'dashboard'])->name('dashboard');
        //Media Manager Routes
        Route::get('/showmanager',          [MediaManagerController::class, 'showmanager'])->name('showmanager');
        Route::get('/mediamanager',         [MediaManagerController::class, 'index'])->name('mediamanager');
        Route::post('/mediamanager/upload', [MediaManagerController::class, 'upload'])->name('mediaupload');
        Route::get('/mediamanager/images',  [MediaManagerController::class, 'images'])->name('mediaimages');
        Route::get('/mediamanager/delete/', [MediaManagerController::class, 'delete'])->name('media.delete');

        Route::get('/settings',             [SettingsController::class,     'settings'])->name('settings');
        Route::resource('roles', App\Http\Controllers\Admin\RoleController::class);
        // Settings Routes
        Route::get('/contact-message',      [SettingsController::class, 'contact_message'])->name('contact_message');
        Route::put('/genral/settings',      [SettingsController::class, 'savegeneralsettings'])->name('generalsettings');
        Route::put('/pagination/settings',      [SettingsController::class, 'paginationsettings'])->name('paginationsettings');
        Route::put('/cookie/settings',      [SettingsController::class, 'savecookiesettings'])->name('cookiesettings');
        Route::put('/payment/settings',     [SettingsController::class, 'savepaymentsettings'])->name('paymentsettings');
        Route::put('/email/settings',       [SettingsController::class, 'savemailsettings'])->name('mailsettings');
        Route::put('/google-map/settings',  [SettingsController::class, 'mapsettings'])->name('mapsettings');

        Route::get('/reset',      [UserDataController::class, 'reset'])->name('reset');
        Route::get('/backup',      [UserDataController::class, 'backup'])->name('backup');
        Route::post('/restore',     [UserDataController::class, 'restore'])->name('restore');
        Route::get('/maintenance',      [UserDataController::class, 'maintenance_mode'])->name('maintenance');
        Route::resource('page-preview',   App\Http\Controllers\Admin\PagePreviewController::class);
        // Localization
        Route::resource('frontend-language',   App\Http\Controllers\Admin\FrontendLanguageController::class);
        Route::resource('admin-language',   App\Http\Controllers\Admin\AdminLanguageController::class);
        // Email
        Route::resource('email-template', App\Http\Controllers\Admin\EmailTemplateController::class);
        Route::resource('subscriber',     App\Http\Controllers\Admin\SubscriberController::class);
        Route::resource('send-email',     App\Http\Controllers\Admin\SendEmailController::class);
        // Blog
        Route::resource('blog',            App\Http\Controllers\Admin\BlogController::class);
        Route::resource('blog-category',   App\Http\Controllers\Admin\BlogCategoryController::class);
        Route::resource('blog-comment',    App\Http\Controllers\Admin\BlogCommentController::class);
        Route::resource('contact-messages', App\Http\Controllers\Admin\ContactMessageController::class);
        // Components
        Route::resource('slider',           App\Http\Controllers\Admin\SliderController::class);
        Route::resource('faq',              App\Http\Controllers\Admin\FaqController::class);
        Route::resource('service',          App\Http\Controllers\Admin\ServiceController::class);
        Route::resource('testimonial',      App\Http\Controllers\Admin\TestimonialController::class);
        Route::resource('about',            App\Http\Controllers\Admin\AboutController::class);
        Route::resource('trainer',          App\Http\Controllers\Admin\TrainerController::class);
        Route::resource('sectitontitle',    App\Http\Controllers\Admin\SectionTitleController::class);
        Route::resource('ourprocess',      App\Http\Controllers\Admin\OurProcessController::class);
        // Shop
        Route::resource('course', App\Http\Controllers\Admin\CourseController::class);
        Route::resource('coursecategory', App\Http\Controllers\Admin\CoursecategoryController::class);
        Route::resource('product',          App\Http\Controllers\Admin\ProductController::class);
        Route::resource('product-review',   App\Http\Controllers\Admin\ProductReviewController::class);
        Route::resource('product-category', App\Http\Controllers\Admin\ProductCategoryController::class);
        Route::resource('order',            App\Http\Controllers\Admin\OrderController::class);
        Route::resource('transaction',      App\Http\Controllers\Admin\TransactionController::class);

        Route::resource('shipping', App\Http\Controllers\Admin\ShippingController::class);
        Route::resource('shop-setting',     App\Http\Controllers\Admin\ShopSettingController::class);
        Route::resource('shipping-country', App\Http\Controllers\Admin\ShippingCountryController::class);
        Route::resource('coupon', App\Http\Controllers\Admin\CouponController::class);
        // Footer
        Route::resource('social-link',         App\Http\Controllers\Admin\SocialLinkController::class);
        Route::resource('footer-contact-item', App\Http\Controllers\Admin\FooterContactItemController::class);

        Route::resource('terms-of-use',      App\Http\Controllers\Admin\TermsOfUseController::class);
        Route::resource('privacy-policy',    App\Http\Controllers\Admin\PrivacyPolicyController::class);
        Route::resource('user-profile',      App\Http\Controllers\Admin\UserProfileController::class);
        Route::resource('users', App\Http\Controllers\Admin\UserController::class); 
    });
});
