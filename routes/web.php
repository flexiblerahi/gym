<?php

use App\Http\Controllers\Frontend\Auth\ResetPasswordController;
use App\Http\Controllers\Frontend\Authr\ForgotPasswordController;
use App\Http\Controllers\Frontend\DynamicPageController;
use App\Http\Controllers\Frontend\ShopController;
use App\Http\Controllers\Frontend\UserPanelController;
use App\Http\Controllers\Frontend\Auth\UserLoginController;
use App\Http\Controllers\Frontend\Auth\UserRegisterController;
use App\Http\Controllers\Frontend\PaymentController;
use App\Http\Controllers\Frontend\PaypalController;
use App\Http\Controllers\Frontend\Auth\VerificationController;
use App\Http\Controllers\Frontend\SslCommerzPaymentController;
use App\Http\Controllers\User\MediaManagerController as UserMediaManagerController;
use App\Http\Controllers\User\TransactionController as UserTransactionController;
use App\Http\Controllers\User\UserOrderController;
use Illuminate\Support\Facades\Route;

Route::group(['middleware' => ['XSS', 'HtmlSpecialchars']], function () {
    Route::group(['middleware' => ['auth']], function () {
        Route::get('/email/verify', [VerificationController::class, 'show'])->name('verification.notice');
        Route::get('/email/verify/{id}/{hash}', [VerificationController::class, 'verify'])->name('verification.verify')->middleware(['signed']);
        Route::post('/email/resend', [VerificationController::class, 'resend'])->name('verification.resend');
    });
    // Frontend
    Route::get('/subscribe',                [App\Http\Controllers\Frontend\EmailTemplateController::class, 'subscription'])->name('subscribe');
    Route::get('/verify/{email}',                [App\Http\Controllers\Frontend\EmailTemplateController::class, 'verify'])->name('verify');

    Route::get('/',                     [DynamicPageController::class,  'index'])->name('home');
    Route::get('/about',                     [DynamicPageController::class,  'about'])->name('about');
    Route::get('/contact',                     [DynamicPageController::class,  'contact'])->name('contact');
    Route::get('/services',                     [DynamicPageController::class,  'services'])->name('services');
    Route::get('/signin',                     [DynamicPageController::class,  'signin'])->name('signin');
    Route::get('/term-condition',                     [DynamicPageController::class,  'termcondition'])->name('termcondition');
    Route::get('/faq',                     [DynamicPageController::class,  'faq'])->name('faq');
    Route::get('/privacy',                     [DynamicPageController::class,  'privacy'])->name('privacy');
    Route::get('/bloglist',                     [DynamicPageController::class,  'bloglist'])->name('bloglist');
    Route::get('/blog-details',                     [DynamicPageController::class,  'blogdetails'])->name('blogdetails');
    Route::get('/course/details/{slug}', [App\Http\Controllers\Frontend\CourseController::class,  'show'])->name('course.show');
    Route::get('/course', [App\Http\Controllers\Frontend\CourseController::class,  'index'])->name('course.index');

    //billing
    Route::get('/checkout',                     [App\Http\Controllers\Frontend\BillingController::class,  'checkout'])->middleware('auth')->name('checkout');
    Route::get('/coupon-apply',                     [App\Http\Controllers\Frontend\BillingController::class,  'couponapply'])->name('couponapply');
    Route::get('/cart-view',                     [App\Http\Controllers\Frontend\BillingController::class,  'cartview'])->name('cartview');
    Route::get('/add-to-cart',  [App\Http\Controllers\Frontend\BillingController::class, 'addcart'])->name('addcart');
    Route::get('/remove-from-cart', [App\Http\Controllers\Frontend\BillingController::class, 'removecart'])->name('removecart');
    Route::post('/proceed/checkout', [App\Http\Controllers\Frontend\BillingController::class, 'proceedcheckout'])->name('proceedcheckout');
    Route::group(['middleware' => ['auth', 'verified']], function() {
        Route::post('/freepurches', [App\Http\Controllers\Frontend\PaymentController::class,  'freepurches'])->name('freepurches');
        Route::get('/payment',                     [App\Http\Controllers\Frontend\BillingController::class,  'payment'])->name('payment');
    });
    Route::post('/placeorder', [App\Http\Controllers\Frontend\BillingController::class, 'placeorder'])->name('placeorder');

    Route::get('product/{slug}', [App\Http\Controllers\frontend\ProductController::class, 'show'])->name('product.show');
    Route::resource('/product', App\Http\Controllers\Frontend\ProductController::class);
    Route::get('/productsearch', [App\Http\Controllers\Frontend\ProductController::class, 'searchproduct'])->name('productsearch');
    Route::get('blog/{slug}', [App\Http\Controllers\frontend\BlogController::class, 'show'])->name('blog.show');
    Route::resource('blog', App\Http\Controllers\Frontend\BlogController::class);
    Route::post('blogcomment', [App\Http\Controllers\Frontend\BlogCommentController::class, 'store'])->name('blogcomment.store');
    Route::resource('contactmessage', App\Http\Controllers\Frontend\ContactMessageController::class);

    Route::get('cart-page',             [ShopController::class,     'cart_page'])->name('cart_page');
    // // Login Routes
    Route::get('login',                     [UserLoginController::class,  'userLoginForm'])->name('user.login');
    Route::post('/login',               [UserLoginController::class,    'userCheckLogin'])->name('user.login');
    Route::resource('registrations', UserRegisterController::class);
    Route::post('/logout',              [UserLoginController::class,    'userLogout'])->name('user.logout');
    Route::get('/forgot',               [ForgotPasswordController::class, 'forgot'])->name('user.forgot');
    Route::post('/forgot',              [ForgotPasswordController::class, 'resetLink'])->name('user.forgot');
    Route::get('/password-reset/{token}', [ResetPasswordController::class, 'showResetForm'])->name('user.reset');
    Route::post('/password-reset',      [ResetPasswordController::class, 'resetPassword'])->name('user.reset');
    Route::get('{slug}',                [DynamicPageController::class, 'page'])->name ('page');
 
    Route::group(['as' => 'user.','middleware' => ['auth', 'verified'], 'prefix' => 'user'], function () {
        Route::get('/mediamanager',         [UserMediaManagerController::class, 'index'])->name('mediamanager');
        Route::post('/mediamanager/upload', [UserMediaManagerController::class, 'upload'])->name('mediaupload');
        Route::get('/mediamanager/images',  [UserMediaManagerController::class, 'images'])->name('mediaimages');
        Route::get('/mediamanager/delete/', [UserMediaManagerController::class, 'delete'])->name('media.delete');
        Route::post('/productreview', [App\Http\Controllers\Frontend\ProductReviewController::class, 'store'])->name('productreview');
        Route::post('/coursereview', [App\Http\Controllers\Frontend\CourseReviewController::class, 'store'])->name('coursereview');
        Route::get('shop/cart',         [ShopController::class,      'cart'])->name('cart');
        Route::post('/update-profile',  [UserPanelController::class, 'update_profile'])->name('update');
        Route::post('/shipping-address',  [UserPanelController::class, 'shipping_address'])->name('shipping-address');
        Route::get('/dashboard',        [UserPanelController::class, 'dashboard'])->name('dashboard');
        Route::get('/profile',          [UserPanelController::class, 'profile'])->name('profile');
        Route::post('/updateprofile',  [UserPanelController::class, 'store'])->name('updateprofile'); 
        Route::post('/updateaddress',  [UserPanelController::class, 'update_address'])->name('updateaddress');
        Route::resource('order',            UserOrderController::class);
        Route::get('transactions',     [UserTransactionController::class, 'index'])->name('transactions');

        Route::post('/sslcommerz-pay',    [SslCommerzPaymentController::class, 'index'])->name('sslcommerz-pay');
        Route::post('/sslcommerz-pay-via-ajax', [SslCommerzPaymentController::class, 'payViaAjax'])->name('sslcommerz-pay-ajax');
        Route::post('/sslcommerz-success', [SslCommerzPaymentController::class,   'success'])->name('sslcommerz-success');
        Route::post('/sslcommerz-failed', [SslCommerzPaymentController::class,   'fail'])->name('sslcommerz-failed');
        Route::post('/sslcommerz-cancel', [SslCommerzPaymentController::class,   'cancel'])->name('sslcommerz-cancel');
        Route::post('/sslcommerz-ipn', [SslCommerzPaymentController::class, 'ipn'])->name('sslcommerz-ipn');

        Route::get('/pay-with-paypal',       [PaypalController::class,   'paypal_pay'])->name('pay-with-paypal');
        Route::get('/paypal-payment-success',       [PaypalController::class,   'paypal_success'])->name('paypal-payment-success');
        Route::get('/paypal-payment-cancled',       [PaypalController::class,   'paypal_cancel'])->name('paypal-payment-cancled');

        Route::post('/bank-pay',        [PaymentController::class,   'bank'])->name('bank-pay');
        Route::post('/stripe-pay',      [PaymentController::class,   'stripe'])->name('stripe-pay');
        Route::post('/razorpay-pay',    [PaymentController::class,   'razorpay'])->name('razorpay-pay');
        Route::post('/mollie-pay',      [PaymentController::class,   'mollie'])->name('mollie-pay');
        Route::get('/mollie-notify',    [PaymentController::class,   'mollie_notify'])->name('mollie-notify');
        Route::post('/instamojo-pay',   [PaymentController::class,   'instamojo'])->name('instamojo-pay');
        Route::get('/instamojo-verify', [PaymentController::class,   'instamojo_verify'])->name('instamojo-verify');
        Route::get('/paystack-pay',     [PaymentController::class,   'paystack'])->name('paystack-pay');
        Route::get('/flutterwave-pay', [PaymentController::class,   'flutterwave'])->name('flutterwave-pay');
        Route::get('/review/history/course', [UserPanelController::class, 'review_history_course'])->name('review.history.course');
        Route::get('/review/history/product', [UserPanelController::class, 'review_history_product'])->name('review.history.product');
    });
});
