@component('mail::message')
# {{$data['title']}}

{{$data['body']}}

@component('mail::button', ['url' => $data['url']])
{{$data['btn']}}
@endcomponent

{{$data['greetings']}}<br>
{{ config('app.name') }}
@endcomponent
