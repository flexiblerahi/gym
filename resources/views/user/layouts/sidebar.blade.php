<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand px-5 mb-4">
            <a href="{{ url('/user/dashboard') }}">
                <img class="w-100 mt-2" src="{{ url(GetSetting('site_logo')) }}">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/user/dashboard') }}">
                <img src="{{ url(GetSetting('site_favicon')) }}">
            </a>
        </div>

        <ul class="sidebar-menu">
            <li class="menu-header">{{__('Basics')}}</li>
            <li class="{{request()->routeIs('user.dashboard') ? 'active': ''}}">
                <a class="nav-link" href="{{ route('user.dashboard') }}">
                    <i class="fas fa-fire"></i>
                    <span>{{__('Dashboard')}}</span>
                </a>
            </li>
            <li class="{{request()->routeIs('user.profile') ? 'active': ''}}">
                <a class="nav-link" href="{{ route('user.profile') }}">
                    <i class="fas fa-user"></i>
                    <span>{{__('Profile')}}</span>
                </a>
            </li>
            <li class="{{request()->routeIs('user.order.*') ? 'active': ''}}">
                <a class="nav-link" href="{{ route('user.order.index') }}">
                    <i class="fas fa-shopping-cart"></i>
                    <span>{{__('Orders')}}</span>
                </a>
            </li>
            <li class="{{request()->routeIs('user.transactions') ? 'active': ''}}">
                <a class="nav-link" href="{{ route('user.transactions') }}">
                    <i class="fas fa-credit-card"></i>
                    <span>{{__('Transaction')}}</span>
                </a>
            </li>
            <li class="nav-item dropdown {{request()->is('/user/review/history*') ? 'active': ''}}">
                <a href="#" class="nav-link has-dropdown"> 
                    <i class="fas fa-history"></i><span>{{__('Review History')}}</span>
                </a>
                <ul class="dropdown-menu">
                    <li class="{{(request()->routeIs('user.review.history.course')) ? 'active': ''}}">
                        <a class="nav-link" href="{{route('user.review.history.course')}}">{{__('Course')}}</a>
                    </li>
                    <li class="{{(request()->routeIs('user.review.history.product')) ? 'active': ''}}">
                        <a class="nav-link" href="{{route('user.review.history.product')}}">{{__('Product')}}</a>
                    </li>
                </ul>
            </li>
        </ul>
    </aside>
</div>
