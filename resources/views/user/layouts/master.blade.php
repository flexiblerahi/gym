<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta content="width=device-width, initial-scale=1, maximum-scale=1, shrink-to-fit=no" name="viewport">
    <title>@if (!empty($page_title)) {{$page_title}} @endif</title>
    <meta name="csrf-token" content="{{csrf_token()}}">
    @include('user.layouts.styles')
    @stack('styles')
    <script src="{{asset('assets/admin/js/sweetalert.js')}}"></script>
</head>
<body>
    <div id="app">
            <div class="navbar-bg"></div>
            @include('user.layouts.navbar')
            @include('user.layouts.sidebar')
            <div class="main-wrapper">
                @yield('content')
                @include('user.layouts.footer')
            </div>

    </div>
    @include('user.layouts.scripts')
    @stack('scripts')
</body>
</html>
