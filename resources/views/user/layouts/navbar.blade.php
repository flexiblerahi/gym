<nav class="navbar navbar-expand-lg main-navbar">
    <div class=" mr-auto">
      <ul class="navbar-nav mr-3">
        <li><a href="#" data-toggle="sidebar" class="nav-link nav-link-lg"><i class="fas fa-bars"></i></a></li>
      </ul>
    </div>
    <ul class="navbar-nav navbar-right">
        <li ><a href="{{url('/')}}" onclick="event.preventDefault(); window.open('{{url('/')}}')" class="nav-link nav-link-lg"> <i class="fas fa-globe-asia    "></i> </a></li>
      <li class="dropdown"><a href="#" data-toggle="dropdown" class="nav-link dropdown-toggle nav-link-lg nav-link-user">
        
        <img style="border-radius: 50%; height:40px; width: 40px" alt="image" src="{{Auth::user()->avatar ? asset(Auth::user()->avatar) : asset(GetSetting('user_avatar'))}}" class="rounded-circle mr-1">
        <div class="d-sm-none d-lg-inline-block">{{__('Hi,')}} {{Auth::user()->name}}</div></a> 
            
        <div class="dropdown-menu dropdown-menu-right">
          <a href="{{route('user.profile')}}" class="dropdown-item has-icon">
            <i class="far fa-user"></i> {{trans('Profile')}}
          </a>
          <a href="{{route('user.profile')}}" class="dropdown-item has-icon">
            <i class="far fa-user"></i> {{trans('update Password')}}
          </a>
          <a href="{{route('user.logout')}}" onclick="event.preventDefault();
          document.getElementById('logout-form').submit();" class="dropdown-item has-icon text-danger">
            <i class="fas fa-sign-out-alt"></i> {{trans('logout')}}
          </a>
          <form id="logout-form" action="{{route('user.logout')}}" method="POST" class="d-none">
            @csrf </form>
        </div>
      </li>
    </ul>
  </nav>
