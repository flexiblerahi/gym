@php
    $page_title = trans('User | Profile');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #e4e6fc;
            border-radius: 5px;
            background: #fdfdff;
        }

        .input-group-text,
        select.form-control:not([size]):not([multiple]),
        .form-control:not(.form-control-sm):not(.form-control-lg) {
            font-size: 14px;
            padding: 10px 15px;
            height: 48px;
        }
        .upload-preview {
        width: 150px;
        height: 150px;
        margin-bottom: 1rem;
        border-radius: 10px;
        }

    .upload-preview img {
        border-radius: 10px;
        width: 150px;
        height: 150px;
        object-fit: cover;
        }

    </style>
@endpush
@extends('user.layouts.master')
@section('content')
@if(Session::has('errormsg') || Session::has('successmsg')) 
    @push('script')
        <script>
            if('{{ Session::get("errormsg")}}') toastr.error('{{ Session::get("errormsg")}}');
            else toastr.success('{{ Session::get("successmsg")}}');
        </script>
    @endpush
    {{Session::forget('errormsg')}}
    {{Session::forget('successmsg')}}
@endif
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>{{__('Profile')}}</h1>
        </div>
        <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                class="fas fa-arrow-circle-left"></i> {{trans('Back')}}</a>
        <div class="section-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <form action="{{route('user.updateprofile')}}" method="POST" enctype="multipart/form-data">
                                @csrf
                                @method('POST')
                                <div class="form-group">
                                    <label for="formFile" class="form-label">{{__('Avatar')}}</label>
                                    <div class="upload-preview">
                                        <img src="{{@$user->avatar ? asset(@$user->avatar) : asset(GetSetting('user_avatar'))}}" alt="{{__('Avatar')}}" class="avatar-preview">
                                    </div>
                                    <input class="form-control" name="avatar" type="file" id="formFile" onchange="previewFile(this)">
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('Name')}}</label>
                                    <input type="text" class="form-control" name="name" value="{{@$user->name}}">
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('frontend.Email')}}</label>
                                    <input type="email" class="form-control" name="email"
                                         value="{{@$user->email}}" disabled>
                                </div>
                                <h4>{{__('Update Password')}}</h4>
                                <div class="form-group">
                                    <label for="">{{__('New Password')}}</label>
                                    <input type="password" class="form-control" name="password" value="">
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('Confirm Password')}}</label>
                                    <input type="password" class="form-control" name="confirm_password" value="">
                                </div>
                                <button type="submit" class="btn btn-primary btn-block" role="button">
                                    {{trans('Save')}} </button>
                            </form>
                        </div>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="card">
                        <div class="card-body">
                            <form>
                                <h4>{{__('Update Address')}}</h4>
                                <div class="form-group">
                                    <label for="">{{__('Country')}}</label>
                                    <select class="form-control select2" id="country" name="country" tabindex="-1" aria-hidden="true">
                                        <option>{{__('--Select Option--')}}</option>
                                        @foreach ($shippingCountries as $shippingCountry)
                                            @if ($shippingCountry->country == ($address ? $address->country : '') ) 
                                                <option  value="{{$shippingCountry->country}}" selected>{{__($shippingCountry->country)}}</option>
                                            @else
                                                <option  value="{{$shippingCountry->country}}">{{__($shippingCountry->country)}}</option>
                                            @endif
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('City')}}</label>
                                    <input id="city" type="text" class="form-control" name="city" value="{{@$address->city}}">
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('Post Code')}}</label>
                                    <input id="postcode" type="text" class="form-control" name="post_code" value="{{@$address->post_code}}">
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('Address')}}</label>
                                    <input id="address" type="text" class="form-control" name="address" value="{{@$address->address}}">
                                </div>
                                <button id="addresssubmit" type="submit" class="btn btn-primary btn-block" role="button"> {{trans('Save')}} </button>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
@push('script')
    <script>
        function previewFile(input)
        {
            var file = $("input[type=file]").get(0).files[0];
            if(file){
                var reader = new FileReader();
                reader.onload = function() {
                    $(".avatar-preview").attr("src", reader.result);
                }
                reader.readAsDataURL(file);
            }
        }

        $(function() {
            $('#addresssubmit').on('click', function(e) {
                e.preventDefault();
                let formData = {
                    "_token": "{{ csrf_token() }}",
                    'country': $('#country').val(),
                    'city': $('#city').val(),
                    'post_code': $('#postcode').val(),
                    'address': $('#address').val(),
                };
                
                jQuery.ajax({
                    type: 'POST',
                    url: '{{ route('user.updateaddress') }}',
                    data: formData,
                    dataType: 'JSON',
                    success: function(data) {
                        data.success ? toastr.success(data.success) : toastr.error(data.error);
                    },
                    error: function(data) {
                        let allerrors = null;
                        if(data.responseJSON.errors) {
                            allerrors = data.responseJSON.errors;
                            if(allerrors.country) toastr.error(allerrors.country[0])
                            if(allerrors.city) toastr.error(allerrors.city[0])
                            if(allerrors.post_code) toastr.error(allerrors.post_code[0])     
                            if(allerrors.address) toastr.error(allerrors.address[0])     
                        }
                    }
                });
            })
        });
    </script>
@endpush
@include('user.layouts.mediaModal')
@endsection