@php
$page_title = trans('User | Dashboard');
@endphp
@extends('user.layouts.master')
@section('content')
{{-- Main Content --}}
@if(Session::has('errormsg') || Session::has('successmsg')) 
    @push('script')
        <script>
            if('{{ Session::get("errormsg")}}') toastr.error('{{ Session::get("errormsg")}}');
            else toastr.success('{{ Session::get("successmsg")}}');
        </script>
    @endpush
    {{Session::forget('errormsg')}}
    {{Session::forget('successmsg')}}
@endif
<div class="main-content">
<section class="section">
    <div class="section-header">
    <h1>{{__('Dashboard')}}</h1>
    </div>
    <div class="section-body">
        <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <a href="{{ route('user.order.index') }}">
                  <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                      <i class="fas fa-shopping-cart"></i>
                    </div>
                    <div class="card-wrap">
                      <div class="card-header">
                        <h4>{{__('Orders')}}</h4>
                      </div>
                      <div class="card-body">
                        {{$orders->count()}}
                      </div>
                    </div>
                  </div>
              </a>
            </div>  
            <div class="col-lg-3 col-md-6 col-sm-6 col-12">
              <a href="{{ route('user.transactions') }}">
                <div class="card card-statistic-1">
                  <div class="card-icon bg-info">
                    <i class="fas fa-credit-card"></i>
                  </div>
                  <div class="card-wrap">
                    <div class="card-header">
                      <h4>{{__('Transection')}}</h4>
                    </div>
                    <div class="card-body">
                      {{$transactions->count()}}
                    </div>
                  </div>
                </div>
              </a>
            </div>         
        </div>
    </div>
</section>
</div>
@endsection

