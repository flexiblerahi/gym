<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{ __('Privacy policy') }}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{ __('home') }}</a></li>
                        <li><a href="">{{ __('Privacy policy') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__privacy mt_120 xs_mt_70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="wsus__terms_con_text privacy_text">
                    <h2>{{ __('Privacy policy') }}</h2>
                    <div class="mt-3"> {!!html_entity_decode($privacy->content)!!}</div>
                </div>
            </div>
        </div>
    </div>
</section>