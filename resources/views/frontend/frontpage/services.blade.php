@if (count($services)>0 || $sectiontitle)
    <section class="wsus__services mt_120 xs_mt_70">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 col-md-8 m-auto">
                    <div class="wsus__heading_area text-center mb_35">
                        <h2>{{($sectiontitle)?$sectiontitle->servicetitle : ''}}</h2>
                        <p>{{($sectiontitle)?$sectiontitle->servicesubtitle : ''}}</p>
                    </div>
                </div>
            </div>
            <div class="row service_slider">
                @foreach($services as $service)
                <div class="col-xl-3">
                    <div class="wsus__single_services">
                        <span><i class="{{ $service->icon }}"></i></span>
                        <h4>{{__($service->title)}}</h4>
                        <div id="service{{ $service->id }}">
                            {!! Str::limit(htmlspecialchars_decode(nl2br($service->description)), 50, '...') !!}
                        </div>
                    </div>
                </div>
                @endforeach          
            </div>
        </div>
    </section>
@endif