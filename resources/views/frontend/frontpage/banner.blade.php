<section class="wsus__banner">
    <div class="container">
        <div class="wsus__banner_slider_area">
            <div class="row banner_slider">
                @foreach ($sliders as $slider)
                    <div class="col-12">
                        <div class="row">
                            <div class="col-xl-6">
                                <div class="wsus__banner_text_area">
                                    <div class="wsus__banner_text">
                                        <h5>{{($slider->title)}}</h5>
                                        <h1>{{($slider->subtitle)}}</h1>
                                        <p>{!!html_entity_decode($slider->description)!!}
                                        </p>
                                        <ul class="banner_btn d-flex flex-wrap align-items-center">
                                            @if($slider->button != '')
                                            <li><a class="common_btn" href="{{($slider->button_link)}}">{{($slider->button)}}</a></li>
                                            @endif
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <div class="wsus__banner_img">
                                    <img src="{{ asset($slider->image) }}" alt="{{__('banner')}}" class="img-fluid w-100">
                                </div>
                            </div>
                        </div>
                    </div>  
                @endforeach
            </div>
        </div>
    </div>
</section>

{{-- OUR PROCESS START --}}
@if ($ourprocess)
    <section class="wsus__process mt_120 xs_mt_70">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 m-auto">
                    <div class="wsus__heading_area text-center mb_35">
                        <h2>{{($ourprocess)?$ourprocess->title : ''}}</h2>
                        <p>{!! ($ourprocess) ? clean(html_entity_decode($ourprocess->description)) : '' !!}</p>
                    </div>
                </div>
            </div>
            @php
            $items =($ourprocess)? json_decode($ourprocess->items) : array();
            @endphp
            <div class="row">
                @foreach ( $items as $key => $item )
                    @if (!empty($item->icon))
                        <div class="col-xl-3">
                            <div class="wsus__single_process @if (count($items)-1 ==$key) last @endif ">
                                <span><i class="{{$item->icon}}"></i></span>
                                <p>{{$item->title}}</p>
                            </div>
                        </div>
                    @endif
                @endforeach 
            </div>
        </div>
    </section>
@endif
