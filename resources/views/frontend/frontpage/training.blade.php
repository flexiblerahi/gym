@if (count($trainers)>0 || $sectiontitle)
    <section class="wsus__training mt_120 xs_mt_70 pt_30">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 m-auto">
                    <div class="wsus__heading_area text-center mb_35">
                        <h2>{{($sectiontitle)?$sectiontitle->trainingtitle : ''}}</h2>
                        <p>{{($sectiontitle)?$sectiontitle->trainingsubtitle : ''}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($trainers as $trainer )
                <div class="col-xl-3">
                    <div class="wsus__single_training">
                        <img src="{{ asset($trainer->image) }}" alt="{{__('training')}}" class="img-fluid w-100">
                        <div class="wsus__training_text">
                            <h4 class="text-center">{{$trainer->name}}</h4>
                            {{-- <p>500 Member</p> --}}
                            <div class="wsus__footer_content">
                                <ul class="footer_link d-flex flex-wrap justify-content-center">
                                    @php
                                    $socialitems = json_decode($trainer->social_item);   
                                    // dd($socialitems);
                                    @endphp
                                    @foreach ($socialitems as $socialitem)
                                    <li><a href="https://{{$socialitem->sociallink}}"><i class="{{$socialitem->socialicon}}"></i></a></li>
                                    @endforeach                               
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                @endforeach
            </div>
        </div>
    </section>
@endif
@php
    $client_section =  $abouthomepage ? json_decode($abouthomepage->client_section) : '';
@endphp
@if ($testimonials || $client_section)
    <section class="wsus__information mt_130 xs_mt_80"
        style="background: url({{ asset(GetSetting('testimonial_background')) }});">
        <div class="wsus__information_overlay">
            <div class="container">
                <div class="row info_slider">
                    @foreach ($testimonials as $testimonial )
                    <div class="col-xl-6 m-auto">
                        <div class="wsus__info_text text-center wsus__info_text_2">
                            <span class="icon"><i class="fas fa-quote-left"></i></span>
                            <h2>{{( $client_section->title)}}</h2>
                            <p>{{($testimonial->comment)}}</p>
                            <div class="wsus__info_center">
                                <img src="{{ asset($testimonial->image) }}" alt="{{__('infomaration')}}" class="img-fluid">
                                <h4>{{($testimonial->name)}}</h4>
                                <p>{{($testimonial->designation)}}</p>
                            </div>
                        </div>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </section>  
@endif