@if($abouthomepage)
    <section class="wsus__about pt_130 xs_mt_80">
        <div class="container">
            <div class="row justify-content-between">
                <div class="col-xl-5">
                    <div class="wsus__about_img">
                        <img src="{{ asset($abouthomepage->third_image) }}" alt="{{__('about')}}" class="img-fluid w-100 about_1">
                        <img src="{{ asset($abouthomepage->second_image) }}" alt="{{__('about')}}" class="img-fluid w-100 about_2">
                        <img src="{{ asset($abouthomepage->first_image) }}" alt="{{__('about')}}" class="img-fluid w-100 about_3">
    
                        <a class="venobox about_play_1" data-autoplay="true" data-vbtype="video" href="{{($abouthomepage->first_video)}}">
                            <span><i class=" fas fa-play"></i></span>
                        </a>
                        <a class="venobox about_play_2" data-autoplay="true" data-vbtype="video" href="{{($abouthomepage->second_video)}}">
                            <span><i class=" fas fa-play"></i></span>
                        </a>
                    </div>
                </div>
                <div class="col-xl-6">
                    <div class="wsus__heading_area">
                        <h5> <i class="{{($abouthomepage->icon)}}" aria-hidden="false"></i>{{($abouthomepage->title)}}</h5>
                        <h2>{{($abouthomepage->subtitle)}}</h2>
                        {!!html_entity_decode($abouthomepage->description)!!}
                    </div>
                    <div class="wsus__about_text">
                        <ul>
                            @foreach (json_decode($abouthomepage->item) as $key => $item)
                            <li>
                                <span class="icon"><i class="{{($item->icon)}}"></i></span>
                                <div class="text">
                                    <h4>{{($item->title)}}</h4>
                                    <p>{{($item->subtitle)}}</p>
                                </div>
                            </li>
                            @endforeach 
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endif