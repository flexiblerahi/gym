
@if(count($blogs)>0 || $sectiontitle)
    <section class="wsus__blog mt-4">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 m-auto">
                    <div class="wsus__heading_area text-center mb_35">
                        <h2>{{($sectiontitle)?$sectiontitle->blogtitle : ''}}</h2>
                        <p>{{($sectiontitle)?$sectiontitle->blogsubtitle : ''}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($blogs as $blog )
                <div class="col-xl-4">
                    <div class="wsus__single_blog">
                        <div class="wsus__blog_img">
                            <img src="{{ asset($blog->image) }}" alt="{{__('blog')}}" class="img-fluid w-100">
                        </div>
                        <div class="wsus__blog_text">
                            <ul class="wsus__blog_header">
                                <li> {{__(date_format($blog->created_at, 'd, F Y'))}}</li>
                                <li>{{__("Author")}}</li>
                            </ul>
                            <a href="{{ route('blog.show',$blog->slug) }}">{{($blog->title)}}</a>
                            {!! clean(Str::limit(html_entity_decode($blog->description), 50, ' ...')) !!}
                        </div>
                    </div>
                </div>   
                @endforeach  
            </div>
        </div>
    </section>    
@endif