
<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('checkout')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="{{route('checkout')}}">{{__('checkout')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__checkout mt_120 xs_mt_70">
    <div class="container">
        @if($errors->any())
            @push('scripts')
                <script>
                    let errors = @json($errors->all());
                    errors.map(error => toastr.error(error))
                </script>
            @endpush
        @endif
        <form action="{{route('placeorder')}}" method="post">
            @csrf
            <div class="row">
                <div class="col-xl-8">
                    <div class="wsus__checkout_form">
                        <div class="row">
                            <div class="col-12 mb_20">
                                <h2>{{__('Billing Address')}}</h2>
                            </div>
                            <div class="col-xl-6">
                                <label>{{__('Name')}}</label>
                                <input type="text" name="billing_name" value="{{$billaddress->billing_name}}">                               
                            </div>
                            <div class="col-xl-6">
                                <label>{{__('Email address')}}</label>
                                <input type="text" name="billing_email" value="{{$billaddress->billing_email}}">                              
                            </div>
                            <div class="col-xl-6">
                                <label>{{__('Phone number')}}</label>
                                <input type="text" name="billing_phone" value="{{$billaddress->billing_phone}}">                               
                            </div>
                            <div class="col-xl-6">
                                <label>{{__('Country')}}</label>
                                <div class="wsus__select_area">
                                    <select class="select_2" name="billing_country">
                                        @foreach ($shippingCountries as $shippingCountry)
                                            <option  value="{{$shippingCountry->country}}" @if ($shippingCountry->country == $billaddress->billing_country )selected 
                                            @endif>{{__($shippingCountry->country)}}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="col-xl-6">
                                <label>{{__('City')}}</label>
                                <input type="text" name="billing_city" value="{{$billaddress->billing_city}}">                               
                            </div>
                            <div class="col-xl-6">
                                <label>{{__('Post Code')}}</label>
                                <input type="text" name="billing_post_code" value="{{$billaddress->billing_post_code}}">                             
                            </div>
                            <div class="col-xl-6">
                                <label>{{__('Address')}}</label>
                                <input type="text" name="billing_address" value="{{$billaddress->billing_address}}">                             
                            </div>
                        </div>
                        <div class="col-12 mt_20 mb_25">
                            <h2>{{__('Shipping Address')}}</h2>
                            <div class="form-check">
                                <input class="form-check-input" type="checkbox" @if ($billaddress->is_shipping == APP\Models\BillAddress::DIFFERENTSHIPPING)
                                    checked
                                @endif name="is_shipping" id="flexCheckDefault">
                                <label class="form-check-label collapsed" for="flexCheckDefault" type="button"
                                    data-bs-toggle="collapse" data-bs-target="#flush-collapseOne" aria-expanded="false"
                                    aria-controls="flush-collapseOne">{{__('Ship to a different address?')}}
                                </label>
                            </div>
                            <div id="flush-collapseOne" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    <div class="row">
                                        <div class="col-12 mb_20">
                                            <h2>{{__('Shipping Address')}}</h2>
                                        </div>
                                        <div class="col-xl-6">
                                            <label>{{__('Name')}}</label>
                                            <input type="text" name="shipping_name" value="{{$billaddress->shipping_name}}">
                                        </div>
                                        <div class="col-xl-6">
                                            <label>{{__('Email address')}}</label>
                                            <input type="text" name="shipping_email" value="{{$billaddress->shipping_email}}">
                                        </div>
                                        <div class="col-xl-6">
                                            <label>{{__('Phone number')}}</label>
                                            <input type="text" name="shipping_phone" value="{{$billaddress->shipping_phone}}">
                                        </div>
                                        <div class="col-xl-6">
                                            <label>{{__('Country')}}</label>
                                            <div class="wsus__select_area">
                                                <select class="select_2" name="shipping_country">
                                                    @foreach ($shippingCountries as $shippingCountry)
                                                        <option  value="{{$shippingCountry->country}}" @if ($shippingCountry->country == $billaddress->shipping_country )selected 
                                                        @endif>{{__($shippingCountry->country)}}
                                                        </option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                        <div class="col-xl-6">
                                            <label>{{__('City')}}</label>
                                            <input type="text" name="shipping_city" value="{{$billaddress->shipping_city}}">
                                        </div>
                                        <div class="col-xl-6">
                                            <label>{{__('Post Code')}}</label>
                                            <input type="text" name="shipping_post_code" value="{{$billaddress->shipping_post_code}}">
                                        </div>
                                        <div class="col-xl-6">
                                            <label>{{__('Address')}}</label>
                                            <input type="text" name="shipping_address" value="{{$billaddress->shipping_address}}">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <a href="{{ route('cartview') }}"> 
                                <span class="me-2"><i class="far fa-angle-left"></i></span>
                                {{__('Back to cart')}}
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-4">
                    <div class="wsus__check_sidebar">
                        @foreach ($courses as $course)
                            @php
                                $image = explode(',',$course['image']);
                            @endphp
                            <div class="wsus__check_item_single">
                                <img src="{{ asset($image[0]) }}" alt="{{__('item')}}" class="img-fluid w-100 ">
                                <div class="wsus__check_item_text">
                                    <h4>{{__($course['name'])}}</h4>
                                    <p>{{__('Price')}}&nbsp;:&nbsp;{{$course['price']}}</p>
                                    <p>{{__('Is Paid')}} : @if ($course['is_paid'] == App\Models\Course::PAID) {{__('Paid')}} @else {{__('Free')}} @endif</p>
                                </div>
                            </div>
                        @endforeach
                        @foreach ($products as $product)
                            @php
                                $image = explode(',',$product['image']);
                                intval($product['quantity']);
                            @endphp
                            <div class="wsus__check_item_single">
                                <img src="{{ asset($image[0]) }}" alt="{{__('item')}}" class="img-fluid w-100 ">
                                <div class="wsus__check_item_text">
                                    <h4>{{__($product['name'])}}</h4>
                                    <p>{{__('Price')}}&nbsp;:&nbsp;{{$product['price']}}</p>
                                    <span>{{__('Quantity')}}&nbsp;:&nbsp;{{$product['quantity']}}</span>
                                </div>
                            </div>
                        @endforeach
                        <ul>
                            <li> <span>{{__('Sub-total')}}</span> <span id="subtotal"></span> </li>
                            <li> <span>{{__('Shipping')}}</span> <span id="shipping"></span> </li>
                            <li> <span>{{__('Coupon')}}</span> <span id="coupon"></span> </li>
                            <li> <span>{{__('Tax')}}</span> <span id=tax></span> </li>
                            <li> <span>{{__('Total')}}</span> <span id="total"></span> </li>
                        </ul>
                        <button type="submit" class="common_btn">
                            {{__('Place an order')}} <i class="far fa-arrow-right"></i>
                        </button>
                    </div>
                </div>
            </div>
        </form>
    </div>
</section>
@push('scripts')
    <script>
        $(function() {
            let products = @json($products);
            let courses = @json($courses);
            let is_paid = @json($is_paid);
            let shipping = @json($shipping);
            let coupon = @json($coupon);
            let total = parseInt(shipping.cost);
            let tax =@json($tax);
            tax = parseInt(tax.tax);
            let totaltax = 0;
            products.map(product => {
                product.price = parseInt(product.price);
                product.quantity = parseInt(product.quantity);
                product.stock = parseInt(product.stock) - product.quantity;
                $('#product_stock_'+product.id).text(product.stock);
                total = total + (product.price * product.quantity);
                totaltax = totaltax + ((parseFloat (product.quantity * product.price)/100) * (tax));
            });
            function currencyPosition(amount) {
                let currency = @json($currency);
                return (currency.site_currency_direction == currency.is_left) ? currency.site_currency_icon + parseFloat(amount).toFixed(2) : parseFloat(amount).toFixed(2) + currency.site_currency_icon;
            }
            
            courses.map(course=> {
                if(is_paid['free'] != course.is_paid) {
                    totaltax = totaltax + ((parseFloat (course.price)/100) * (tax));
                    total = total + parseInt(course.price)
                };
            });
            $('#subtotal').text(currencyPosition(total));

            if(coupon) {
                total = (coupon.coupon.type == coupon.inprice) ? total - parseInt(coupon.coupon.amount) : (total - ((total/100) * parseInt(coupon.coupon.amount)));
                (coupon.coupon.type == coupon.inprice) ? $('#coupon').text(currencyPosition(coupon.coupon.amount)) :  $('#coupon').text(`${coupon.coupon.amount}%`);
            } else $('#coupon').text(currencyPosition (0));

            total = total + totaltax;
            $('#tax').text(currencyPosition(totaltax));
            (shipping.cost>0) ? $('#shipping').text(currencyPosition(shipping.cost)) : $('#shipping').text('{{__("Free")}}');
            $('#total').text(currencyPosition(total));
        });
    </script>
@endpush