<section class="wsus__signin mt_130 xs_mt_80">
    <div class="container">
        <div class="row">
            <div id="registerform" class="col-xl-5 m-auto">
                <div class="wsus__login_form">
                    <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                        aria-labelledby="pills-home-tab">
                        <form>
                            <div class="wsus__heading_area text-center mb_20">
                                <h4>{{__('Reset Password')}}</h4>
                            </div>
                            <label>{{__('Email Address')}}</label>
                            <div class="wsus__single_input">
                                <input type="email" id="email" value="{{$email}}" name="email" disabled>
                                <span id="remailerr" class="text-danger"></span>
                            </div>
                            <label>{{__('password')}}</label>
                            <div class="wsus__single_input">
                                <input type="password" name="password" id="password">
                                <span id="showpassword"><i class="far fa-eye-slash"></i></span>
                            </div>
                            <span id="rpassworderr" class="text-danger"></span>
                            <label>{{__('confirm password')}}</label>
                            <div class="wsus__single_input">
                                <input type="password" id="confirm_password" name="confirm_password">
                                <span><i class="far fa-eye-slash"></i></span>
                            </div>
                            <span id="rconfirmpassworderr" class="text-danger"></span>
                            <a id="resetbtn" class="common_btn">{{__('sign up')}} <i class="far fa-arrow-right"></i></a>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script>
    $(function() {
            $('#resetbtn').on('click', function() {
                let registerurl = "{{ route('user.reset') }}";
                let url = window.location.href;
                let verifytoken= url.split("/");
                verifytoken = verifytoken[verifytoken.length - 1];
                console.log('verifytoken :>> ', verifytoken);
                jQuery.ajax({
                    type: 'POST',
                    url: registerurl,
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'email': $('#email').val(),
                        'password': $('#password').val(),
                        'confirm_password': $('#confirm_password').val(),
                        'token' : verifytoken
                     },
                    dataType: 'JSON',
                    success: function(response) {
                        toastr[response.type](response.message);
                        window.location = "{{ route('signin') }}";
                    },
                    error: function(data) {
                        if(data.responseJSON.errors) {   
                            const allerrors = data.responseJSON.errors; 
                            if(allerrors.confirm_password) $('#rconfirmpassworderr').text(allerrors.confirm_password[0])
                            if(allerrors.email) $('#remailerr').text(allerrors.email[0])
                            if(allerrors.password) $('#rpassworderr').text(allerrors.password[0])        
                        }
                    }
                });
            });

            $('#forgetPassword').on('click', function (e) { 
                e.preventDefault();
                $('#registerform').addClass('d-none');
                $('#forgetpassword').removeClass('d-none');
            });

            $('#sendbtn').on('click', function (e) {
                e.preventDefault();

                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("subscribe")}}',
                     data: {
                        'email': $('#mail').val(),
                     },
                     success: function(data) {
                        toastr[data.type](data.message);
                        if(data.type == 'success') {
                            $('#iscartitemexists').removeClass('d-none');
                            $('#cartitemtotal').text(data.total);
                        }
                     },
                 });
             });

            $(document).on('click', '#showpassword', function() {
                if ($('#password').attr("type") == "text") { 
                    $('#password').attr('type', 'password');
                    $('#showpassword i').addClass("fa-eye-slash");
                    $('#showpassword i').removeClass("fa-eye");
                } else if ($('#password').attr("type") == "password") {
                    $('#password').attr('type', 'text');
                    $('#showpassword i').removeClass("fa-eye-slash");
                    $('#showpassword i').addClass("fa-eye");
                }
            });
        });
</script>
@endpush