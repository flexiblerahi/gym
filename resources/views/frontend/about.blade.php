
<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}})">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('About us')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="{{route('about')}}">{{__('about')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@if ($aboutus)
<section class="wsus__about pt_130 xs_mt_80">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-xl-5">
                <div class="wsus__about_img">
                    <img src="{{ asset($aboutus->third_image) }}" alt="{{__('about')}}" class="img-fluid w-100 about_1">
                    <img src="{{ asset($aboutus->second_image) }}" alt="{{__('about')}}" class="img-fluid w-100 about_2">
                    <img src="{{ asset($aboutus->first_image) }}" alt="{{__('about')}}" class="img-fluid w-100 about_3">

                    <a class="venobox about_play_1" data-autoplay="true" data-vbtype="video" href="{{($aboutus->first_video)}}">
                        <span><i class=" fas fa-play"></i></span>
                    </a>
                    <a class="venobox about_play_2" data-autoplay="true" data-vbtype="video" href="{{($aboutus->second_video)}}">
                        <span><i class=" fas fa-play"></i></span>
                    </a>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="wsus__heading_area">
                    <h5> <i class="{{($aboutus->icon)}}" aria-hidden="false"></i>{{($aboutus->title)}}</h5>
                    <h2>{{($aboutus->subtitle)}}</h2>
                    {!!html_entity_decode($aboutus->description)!!}
                </div>
                <div class="wsus__about_text">
                    <ul>
                        @foreach (json_decode($aboutus->item) as $key => $item)
                        <li>
                            <span class="icon"><i class="{{($item->icon)}}"></i></span>
                            <div class="text">
                                <h4>{{($item->title)}}</h4>
                                <p>{{($item->subtitle)}}</p>
                            </div>
                        </li>
                        @endforeach 
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@endif
@php
    $client_section = json_decode($aboutus->client_section);
@endphp
@if (count($testimonials)>0 || $aboutus)
<section class="wsus__testimonial mt_120 xs_mt_70">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 m-auto">
                <div class="wsus__heading_area text-center mb_35">
                    <h2>{{( $client_section->title)}}</h2>
                    <p>{{( $client_section->subtitle)}}</p>
                </div>
            </div>
        </div>
        <div class="row test_slider">
            @foreach ($testimonials as $testimonial )
                <div class="col-xl-6">
                    <div class="wsus__testimonial_item">
                        <div class="wsus__testimonial_text">
                            <span class="left_icon"><i class="fas fa-quote-left"></i></span>
                            <p>{{($testimonial->comment)}}</p>
                            <span class="right_icon"><i class="fas fa-quote-right"></i></span>
                        </div>
                        <div class="wsus__testimonial_img_area">
                            <div class="wsus__testimonial_img">
                                <img src="{{ asset($testimonial->image) }}" alt="{{__('clients')}}" class="img-fluid w-100">
                            </div>
                            <div class="wsus__testimonial_title">
                                <h3>{{($testimonial->name)}}</h3>
                                <p>{{($testimonial->designation)}}</p>
                            </div>
                        </div>
                    </div>
                </div>       
            @endforeach
        </div>
    </div>
</section>
@endif