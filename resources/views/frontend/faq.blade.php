<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('faq')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="">{{__('faq')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__faq mt_120 xs_mt_70">
    <div class="container">
        <div class="row">
            <div class="col-xl-6 m-auto">
                <div class="wsus__heading_area text-center mb_35">
                    <h2>{{__('FAQs')}}</h2>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xl-12">
                <div class="wsus__faq_items">
                    <div class="accordion" id="accordionExample">
                        @foreach ($faqs as $faq)
                            <div class="accordion-item">
                                <h2 class="accordion-header" id="heading{{ $faq->id }}">
                                    <button class="accordion-button" type="button" data-bs-toggle="collapse" data-bs-target="#collapse{{ $faq->id }}" aria-expanded="true" aria-controls="collapse{{ $faq->id }}">
                                        {{__($faq->question)}}
                                    </button>
                                </h2>
                                <div id="collapse{{ $faq->id }}" class="accordion-collapse collapse @if($faqs->first()->id == $faq->id)show @endif" aria-labelledby="heading{{ $faq->id }}" data-bs-parent="#accordionExample">
                                    <div class="accordion-body">
                                        <p>{{__($faq->answer)}}
                                        </p>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
