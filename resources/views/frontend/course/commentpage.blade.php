    @foreach ($coursereviews as $review)
        <div class="wsus__single_review">
            @if ($review->user->avatar)
            <div class="col-2">
                <img src="{{ asset($review->user->avatar) }}" alt="{{__('img')}}" class="img-fluid w-100">
            </div>
            @else
            <div class="col-2">
                <img src="{{ asset('assets/frontend/none.jpg') }}" alt="{{__('img')}}" class="img-fluid w-100">
            </div>
            @endif
            <div class="col px-0 wsus__review_text">
                <h5>{{__($review->user->name)}}</h5>
                <span><i class="fal fa-clock"></i> {{__(date_format($review->created_at, 'd F Y'))}}</span>
                <p>{{__($review->comment)}}</p>
            </div>
        </div>
    @endforeach
@if($coursereviews->total()>=3)
@include('frontend.course.commentpaginate')
@endif
