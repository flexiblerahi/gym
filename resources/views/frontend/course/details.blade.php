<section class="wsus__breadcrumb"  style="background: url({{asset(GetSetting('breadcumb'))}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{ __('courses details') }}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('course.index')}}">{{ __('courses') }}</a></li>
                        <li><a href="">{{ __('courses details') }}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>

<section class="wsus__courses_details mt_105 xs_mt_55">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                <div class="wsus__blog_details_area">
                    <div class="wsus__blog_det_img">
                        <img src="{{ asset($course->image) }}" alt="{{__('course')}}" class="img-fluid w-100">
                        <p class="rating">
                            @php
                                $totaltrate = 5;
                                $courserating = $coursereviews->avg('rating') + 1;
                            @endphp
                            @while ($totaltrate > 0)
                                @php
                                    $courserating--;
                                    $totaltrate--;
                                @endphp
                                @if ($courserating >= 0.1 && $courserating < 1)
                                    <i class="fas fa-star-half-alt"></i>
                                @elseif($courserating >= 1)
                                    <i class="fas fa-star"></i>
                                @else
                                    <i class="fal fa-star"></i>
                                @endif
                            @endwhile
                            <span>{{__(round($coursereviews->avg('rating'), 1))}}</span>
                        </p>
                    </div>
                    <div class="wsus__blog_det_text">
                        <ul class="top_icon d-flex fles-wrap">
                            <li><a class="btn btn-success addcart" data-course_id="{{$course->id}}">{{__('add to cart')}}</a></li>
                            <li><i class="fas fa-list-ul"></i> {{__($course->coursecategory->name)}}</li>
                            <li><i class="fal fa-money-check-edit-alt"></i>
                                @if($course->is_paid == App\Models\Course::NONPAID)
                                    {{__('Free')}}
                                @elseif (intval($course->offer_price)> 0) 
                                    {{currencyPosition($course->offer_price)}}
                                @else
                                    {{currencyPosition($course->price)}}
                                @endif
                            </li>
                        </ul>
                        <h4 class="title">{{__($course->name)}}</h4> 
                       <div class="course-details mb-3">
                        {!! html_entity_decode($course->description) !!}
                       </div>
                        <div class="wsus__blog_det_share">
                            <div class="wsus__blog_det_share_icon d-flex flex-wrap">
                                <span>{{__('share')}}:</span>
                                <ul class="d-flex flex-wrap mb-0">
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="http://www.twitter.com/share?url={{url()->current()}}"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="https://vk.com/share.php?url={{url()->current()}}"><i class="fab fa-vk"></i></a></li>
                                    <li><a href="https://www.linkedin.com/sharing/share-offsite/?url={{url()->current()}}"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="http://pinterest.com/pin/create/bookmarklet?url={{url()->current()}}"><i class="fab fa-pinterest-p"></i></a></li>
                                </ul>
                            </div>
                        </div>
                        <div class="wsus__course_review">
                            <div class="row">
                                <div class="col-xl-12">
                                    @if($coursereviews->total()>=1)
                                    <h3>{{__('coursereviews')}} - {{$coursereviews->total()}}</h3>
                                    @endif
                                </div>
                                <div class="col-xl-12" id="coursecommentpage">
                                    @include('frontend.course.commentpage')
                                </div>
                                <div class="col-12">
                                    <div class="wsus__blog_comment_post">
                                        <h2>{{__('Leave A Review')}}</h2>
                                        <form>
                                            <div class="row">
                                                <div class="col-12">
                                                    <div class="wsus__select_review">
                                                        <p>
                                                            <span>{{__('Your Rating')}}:</span>
                                                            <i id="rate_1" class="fas fa-star rating" data-rate="1"></i>
                                                            <i id="rate_2" class="fas fa-star rating" data-rate="2"></i>
                                                            <i id="rate_3" class="fas fa-star rating" data-rate="3"></i>
                                                            <i id="rate_4" class="fas fa-star rating" data-rate="4"></i>
                                                            <i id="rate_5" class="fas fa-star rating" data-rate="5"></i>
                                                        </p>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="course_id" id="id" value="{{$course->id}}">
                                                <div class="col-xl-12">
                                                    <textarea rows="5" id="review_comment" placeholder="{{__('Write a comment') }}"></textarea>
                                                    <button id="review_submit" type="submit" class="common_btn">{{__('submit review')}}</button>
                                                </div>
                                            </div>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="wsus__sidebar" id="sticky_sidebar">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="sidebar_item wsus__blog_serch">
                                <h3>{{__('Search')}}</h3>
                                <form>
                                    <input id="searchcourse" type="text" placeholder="Search">
                                    <button id="search" class="common_btn"><i class="far fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-6 col-lg-12">
                            <div class="sidebar_item wsus__blog_categori pb_20">
                                <h3>{{__('Course Categories')}}</h3>
                                <ul>
                                    @foreach ($coursecategories as $coursecategory)
                                        <li><a href="{{ url('/course?coursecategory='. $coursecategory->id) }}">{{__($coursecategory->name)}} <span>{{__($coursecategory->course->count())}}</span></a></li>
                                    @endforeach 
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-12 col-md-6 col-lg-12">
                            <div class="sidebar_item">
                                <h3>{{__('Recent Course')}}</h3>
                                <ul class="wss__sedebar_blog_item">
                                    @foreach ($recentcourses as $recentcourse)
                                        <li>
                                            <img src="{{ asset($recentcourse->image) }}" alt="{{__('blog')}}" class="img-fluid w-100">
                                            <div class="text">
                                                <a href="{{ route('course.show', $recentcourse->slug) }}">{{__($recentcourse->name)}}</a>
                                                <p><i class="far fa-calendar-alt"></i> {{__(date_format($recentcourse->created_at, 'F d, Y'))}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
    <script>
        $(function() {
            let rating = 0;
            $('.rating').on('click', function() {
                let rate = $(this).attr('data-rate');
                rating = rate;
                console.log('rate', rate)
                $('.rating').removeClass('text-warning');
                for (i = 0; i <= rate; i++) {
                    $('#rate_' + i).addClass('text-warning');
                }
            });
             //Product Review
            $('#review_submit').on('click', function(e) {
                e.preventDefault();
                let userId = @json($userid);
                let courseId = $('#id').val();
                let formData = {
                    "_token": "{{ csrf_token() }}",
                    'user_id': userId, 
                    'comment': $('#review_comment').val(),
                    'rating': rating,
                    'course_id': courseId,
                    'url': window.location.href
                };
                console.log('formData :>> ', formData);
                jQuery.ajax({
                    type: 'POST',
                    url: '{{ route('user.coursereview') }}',
                    data: formData,
                    dataType: 'JSON',
                    success: function(data) {
                        console.log('data', data);
                        $('#review_comment').val('');
                        data.success ? toastr.success(data.success) : toastr.error(data.error);
                    },
                    error: function(data) {
                        if (data.responseJSON.error== "Unauthenticated.") {
                        window.location = "{{route('signin')}}";
                        }
                        let allerrors = null;
                        if(data.responseJSON.errors) {
                        allerrors = data.responseJSON.errors;
                        if(allerrors.comment) toastr.error(allerrors.comment[0]); 
                    }
                    }
                });
            })
            
            $(document).on('click', '.paginations', function() {
                commentpaginationId = $(this).attr('id');
                console.log('commentpaginationId :>> ', commentpaginationId);
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    data: {
                        'page':$(this).data('page'),
                    },
                    success: function(data) {
                        $('#coursecommentpage').html(data);
                    }
                });
            });

            $(document).on('click', '#nextpage', function() {
                nextpageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('nextpage'),
                    data: {
                        'page':$(this).data('page'),
                    },
                    success: function(data) {
                        $('#coursecommentpage').html(data);
                    }
                });
            });
            
            $(document).on('click', '#previouspage', function() {
                previouspageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('previouspage'),
                    data: {
                        'page':$(this).data('page'),
                    },
                    success: function(data) {
                        $('#coursecommentpage').html(data);
                    }
                });
            });

            $('#search').on('click',function (e) { 
                e.preventDefault();
                let search = $('#searchcourse').val();
                window.location.href = window.location.origin+'/course?course='+search;
            });

            $(document).on('click', '.addcart', function () {  
                let course_id = $(this).data('course_id');
                $('.addcart').css('cursor','pointer');
                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("addcart")}}',
                     data: {
                        'course_id': course_id,
                        'type' : 'course',
                     },
                     success: function(data) {
                        toastr[data.type](data.message);
                        if(data.type == 'success') {
                            $('#iscartitemexists').removeClass('d-none');
                            $('#cartitemtotal').text(data.total);
                            
                        }
                     },
                 });
             });
        });
    </script>
@endpush
