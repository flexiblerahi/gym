<div class="row" id="allcourses">
    <div class="col-xl-12">
        <div class="wsus__product_topbar">
            <p>{{__('Showing' . ' ' . $courses->firstItem() . '-' . $courses->lastItem() . ' ' . 'of' . ' ' . $courses->total() . ' ' . 'Results')}}</p>
            <div class="wsus__select_area">
                <select class="select_2" id="sortby">
                    <option value="" >{{__('short by') }}</option>
                    <option value="price_asc">{{__('Sort by Price (low to high)')}}</option>
                    <option value="price_desc">{{__('Sort by Price (High to low)')}}</option>
                    <option value="name_asc">{{__('Sort by Name (A to Z)')}}</option>
                    <option value="name_desc">{{__('Sort by Name (Z to A)')}}</option>
                </select>
            </div>
        </div>
    </div>
    @php
        $colorloop = 4;
    @endphp
    @foreach ($courses as $course)
        @php
            if ($colorloop == 0) {
                $colorloop = 4;
            }
        @endphp
        <div class="col-xl-6 col-md-6 course">
            <div class="wsus_courses">
                <div class="wsus_courses_img">         
                        <img src="{{ asset($course->image) }}" alt="{{__('product')}}" class="img-fluid w-100">
                    <ul>
                        @if ($colorloop == 4)
                            <li><a class="bg-primary">{{__($course->coursecategory->name)}}</a></li>
                        @elseif ($colorloop == 3)
                            <li><a class="bg-success">{{__($course->coursecategory->name)}}</a></li>
                        @elseif ($colorloop == 2)
                            <li><a class="bg-warning">{{__($course->coursecategory->name)}}</a></li>
                        @else
                            <li><a class="bg-danger">{{__($course->coursecategory->name)}}</a></li>
                        @endif
                    </ul>
                </div>
                <div class="wsus_courses_text">
                    <p class="rating">
                        @php
                            $totaltrate = 5;
                            $courserating = $course->coursereviews->avg('rating') + 1;
                        @endphp
                        @while ($totaltrate > 0)
                            @php
                                $courserating--;
                                $totaltrate--;
                            @endphp
                            @if ($courserating >= 0.1 && $courserating < 1)
                                <i class="fas fa-star-half-alt"></i>
                            @elseif($courserating >= 1)
                                <i class="fas fa-star"></i>
                            @else
                                <i class="fal fa-star"></i>
                            @endif
                        @endwhile
                        <span>{{__(round($course->coursereviews->avg('rating'), 1))}}</span>
                    </p>
                    <a class="title" href="{{ route('course.show', $course->slug) }}">{{__($course->name)}}</a>
                    {!! clean(Str::limit(html_entity_decode($course->description), 50, ' ...')) !!}
                    <ul>
                        <li>
                            @if($course->is_paid == App\Models\Course::NONPAID)
                                <span>{{__('Free')}}</span>
                            @elseif(intval($course->offer_price)>0)
                                <del> {{currencyPosition($course->price)}} </del><span>&nbsp;{{currencyPosition($course->offer_price)}}</span> 
                            @else 
                                <span>{{currencyPosition($course->price)}}</span>
                            @endif 
                        </li>
                        <li><a class="common_btn addcart" data-course_id="{{$course->id}}">{{__('add to cart')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        @php
            $colorloop--;
        @endphp
    @endforeach
    @if($courses->count()>=1)
        @include('frontend.course.paginate')
    @else
        <h1 class="text-center">{{__('COURSE NOT AVAILABE')}}</h1>
    @endif
</div>
