<section class="wsus__courses_page mt_130 xs_mt_80">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8" id="coursepaginatepage">
                @include('frontend.course.coursepaginate')
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="wsus__sidebar" id="sticky_sidebar">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="sidebar_item wsus__blog_categori product_category pb_20">
                                <h3>{{__('Categories')}}</h3>
                                <ul>
                                    @foreach ($coursecategories as $coursecategory)
                                        <li class="coursecategory" id="{{ $coursecategory->id }}"><a class="categories" id="category_{{$coursecategory->id}}">{{__($coursecategory->name)}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="sidebar_item product_color pb_15">
                                <h3>{{__('Level')}}</h3>
                                <div class="form-check">
                                    <input class="form-check-input levelfilter" type="checkbox" value="{{ App\Models\Course::BEGINNER }}" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        {{__('Beginner')}}
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input levelfilter" type="checkbox" value="{{ App\Models\Course::INTERMEDIATE }}" id="flexCheckDefault1">
                                    <label class="form-check-label" for="flexCheckDefault1">
                                        {{__('Intermediate')}}
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input levelfilter" type="checkbox" value="{{ App\Models\Course::ADVANCE }}" id="flexCheckDefault2">
                                    <label class="form-check-label" for="flexCheckDefault2">
                                        {{__('Advance')}}
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input levelfilter" type="checkbox" value="{{ App\Models\Course::PRO }}" id="flexCheckDefault3">
                                    <label class="form-check-label" for="flexCheckDefault3">
                                        {{__('Pro')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="sidebar_item product_color pb_15 mb-0">
                                <h3>{{__('course price')}}</h3>
                                <div class="form-check">
                                    <input class="form-check-input ispaid" type="checkbox" value="{{ App\Models\Course::PAID }}" id="flexCheckDefault6">
                                    <label class="form-check-label" for="flexCheckDefault6">
                                        {{__('Paid Course')}}
                                    </label>
                                </div>
                                <div class="form-check">
                                    <input class="form-check-input ispaid" type="checkbox" value="{{ App\Models\Course::NONPAID }}" id="flexCheckDefault7">
                                    <label class="form-check-label" for="flexCheckDefault7">
                                        {{__('Free Course')}}
                                    </label>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
    <script>
        $(function() {
            $('.common_btn').css('cursor','pointer');
            $('.paginations').css('cursor','pointer');
            let url = window.location.href;
            let courseCategoryId = '';
            if(url.includes('coursecategory=')) {
                const  urlsplit = url.split('coursecategory=');
                courseCategoryId = urlsplit[1];
            }
            let levelArr = [];
            let filterispaidarr = [];
            let sortvalue='';

            $(document).on('change', '#sortby', function () {  
                sortvalue = $(this).val();
                jQuery.ajax({
                     type: 'GET',
                     url: window.location.href,
                     data: {
                        'level': levelArr,
                        'is_paid': filterispaidarr,
                        'coursecategory':courseCategoryId,
                        'sortby': sortvalue == '' ? null : sortvalue
                     },
                     success: function(data) {
                        $('#coursepaginatepage').html(data);
                        $('.select_2').select2();
                        $('#sortby').val(sortvalue);
                        $('#sortby').trigger('change.select2');
                        $('.common_btn').css('cursor','pointer');
                        $('.paginations').css('cursor','pointer');
                     }
                 });
            });

            $('.coursecategory').on('click', function() {
                courseCategoryId = $(this).attr('id');
                $('.categories').css({'color':'white', 'border-color': 'white'});
                $('#category_'+courseCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                jQuery.ajax({
                    type: 'GET',
                    url: window.location.href,
                    data: {
                        'level': levelArr,
                        'is_paid': filterispaidarr,
                        'coursecategory':courseCategoryId,
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#coursepaginatepage').html(data);
                        $('.select_2').select2();
                        $('.common_btn').css('cursor','pointer');
                        $('.paginations').css('cursor','pointer');
                    }
                });
            });

            $('.ispaid').on('click', function() {
                console.log('courseCategoryId :>> ', courseCategoryId);
                let ispaidnumber = filterispaidarr.includes($(this).val());
                if (ispaidnumber) {
                    const levelIndex = filterispaidarr.indexOf($(this).val());
                    filterispaidarr.splice(levelIndex, 1);
                } else filterispaidarr.push($(this).val());
                jQuery.ajax({
                    type: 'GET',
                    url: window.location.href,
                    data: {
                        'coursecategory':courseCategoryId,
                        'level': levelArr,
                        'is_paid': filterispaidarr,
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#coursepaginatepage').html(data);
                        $('.select_2').select2();
                        $('.common_btn').css('cursor','pointer');
                        $('.paginations').css('cursor','pointer');
                    }
                });
            });

            $('.levelfilter').on('click', function() {
                let number = levelArr.includes($(this).val());
                if (number) {
                    const levelIndex = levelArr.indexOf($(this).val());
                    levelArr.splice(levelIndex, 1);
                } else levelArr.push($(this).val());
                jQuery.ajax({
                    type: 'GET',
                    url: window.location.href,
                    data: {
                        'coursecategory':courseCategoryId,
                        'level': levelArr,
                        'is_paid': filterispaidarr,
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#coursepaginatepage').html(data);
                        $('.select_2').select2();
                        $('.common_btn').css('cursor','pointer');
                        $('.paginations').css('cursor','pointer');
                    }
                });
            });

            $(document).on('click', '.addcart', function () {  
                let course_id = $(this).data('course_id');
                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("addcart")}}',
                     data: {
                        'course_id': course_id,
                        'type' : 'course',
                     },
                     success: function(data) {
                        toastr[data.type](data.message);
                        if(data.type == 'success') {
                            $('#iscartitemexists').removeClass('d-none');
                            $('#cartitemtotal').text(data.total);
                            
                        }
                     },
                 });
             });

             $(document).on('click', '.paginations', function() {
                paginationId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    data: {
                        'page':$(this).data('page'),
                        'coursecategory': courseCategoryId == '' ? null : courseCategoryId,
                        'level': levelArr,
                        'is_paid': filterispaidarr,
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#coursepaginatepage').html(data);
                        $('.select_2').select2();
                        $('.common_btn').css('cursor','pointer');
                        $('.paginations').css('cursor','pointer');
                        if (sortvalue != '') {
                            $('#sortby').val(sortvalue);
                            $('#sortby').trigger('change.select2');
                        }
                       if ( courseCategoryId != '') {
                            $('.categories').css({'color':'white', 'border-color': 'white'});
                            $('#category_'+courseCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                       }
                    }
                });
            });

            $(document).on('click', '#nextpage', function() {
                nextpageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('nextpage'),
                    data: {
                        'coursecategory': courseCategoryId == '' ? null : courseCategoryId,
                        'level': levelArr,
                        'is_paid': filterispaidarr,
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#coursepaginatepage').html(data);
                        $('.select_2').select2();
                        $('.common_btn').css('cursor','pointer');
                        $('.paginations').css('cursor','pointer');
                        if (sortvalue != '') {
                            $('#sortby').val(sortvalue);
                            $('#sortby').trigger('change.select2');
                        }
                       if ( courseCategoryId != '') {
                            $('.categories').css({'color':'white', 'border-color': 'white'});
                            $('#category_'+courseCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                       }
                    }
                });
            });
            
            $(document).on('click', '#previouspage', function() {
                previouspageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('previouspage'),
                    data: {
                        'coursecategory': courseCategoryId == '' ? null : courseCategoryId,
                        'level': levelArr,
                        'is_paid': filterispaidarr,
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#coursepaginatepage').html(data);
                        $('.select_2').select2();
                        $('.common_btn').css('cursor','pointer');
                        $('.paginations').css('cursor','pointer');
                        if (sortvalue != '') {
                            $('#sortby').val(sortvalue);
                            $('#sortby').trigger('change.select2');
                        }
                       if ( courseCategoryId != '') {
                            $('.categories').css({'color':'white', 'border-color': 'white'});
                            $('#category_'+courseCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                       }
                    }
                });
            });
        });
    </script>
@endpush
