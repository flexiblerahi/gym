<div class="col-12 mt_35 mb-5 mb-lg-0" id="coursepagination">
    <div class="wsus__pagination">
        <nav aria-label="Page navigation example">
            <ul class="pagination">
                <li class="page-item">
                    <a class="page-link" data-previouspage="{{ $courses->previousPageUrl() }}" id="previouspage" aria-label="{{__('Previous')}}"><i class="far fa-angle-double-left"></i></a>
                </li>
                @foreach ($courses->links()->elements[0] as $key => $pagelink)
                    <li class="page-item @if($courses->currentPage() == $key) active @endif"><a class="page-link paginations" data-page="{{$key}}" id="paginate_{{$key}}">{{ $key }} </a></li>
                @endforeach
                <li class="page-item">
                    <a class="page-link" data-nextpage="{{ $courses->nextPageUrl() }}" id="nextpage" aria-label="{{__('Next')}}"><i class="far fa-angle-double-right"></i></a>
                </li>
            </ul>
        </nav>
    </div>
</div>
