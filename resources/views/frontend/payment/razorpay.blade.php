
<div class="tab-pane fade" id="razorpay" role="tabpanel" aria-labelledby="razorpay-tab">
    <div class="wsus_other_payment">
        <div class="RazorPay box">
            <div class="form-group">
                <label></label>
                <div class="col-sm-12 col-md-12">
                    @php 
                        $payable_amount = $total * ((PaymentGateway('razorpay_rate')) ? PaymentGateway('razorpay_rate') : 1);
                        $payable_amount = round($payable_amount, 2);
                    @endphp
                    <form
                        action="{{route('user.razorpay-pay', ['cart' => base64_encode(json_encode($cart)), 'tax' => $totaltax, 'total' => $payable_amount])}}"
                        method="POST">
                        @csrf
                        <script src="https://checkout.razorpay.com/v1/checkout.js" data-key="{{PaymentGateway('razorpay_key_id')}}"
                                                                            data-amount="{{$payable_amount * 100}}"
                                                                            data-currency="{{GetSetting('site_currency')}}"
                                                                            data-buttontext="{{__('Pay with Razorpay')}}"
                                                                            data-buttontext="{{__('Pay with Razorpay')}}"
                                                                            data-buttonclass="btn btn-success"
                                                                            data-name="{{GetSetting('site_name')}}"
                                                                            data-image="{{url(GetSetting('site_logo'))}}"
                                                                            data-prefill.name="" data-prefill.email=""
                                                                            data-theme.color="#0b29a2">
                        </script>
                        <input type="hidden" custom="Hidden Element" name="hidden">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>