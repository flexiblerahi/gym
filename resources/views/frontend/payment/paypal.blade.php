@php
    $payable_amount = $total * PaymentGateway('paypal_rate');
    $payable_amount = round($payable_amount, 2);
@endphp
<div class="tab-pane fade" id="paypal" role="tabpanel" aria-labelledby="paypal-tab">
    <div class="wsus_other_payment">
        <div class="PayPal box">
            <div class="form-group">
                    <a href="{{route('user.pay-with-paypal', ['cart' => base64_encode(json_encode($cart)), 'tax' => $totaltax, 'total' => $total])}}" type="submit" class="common_btn">{{__('frontend.Pay with paypal')}}</a>
            </div>
        </div>
    </div>
</div>