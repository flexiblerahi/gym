

<div class="tab-pane fade" id="sslcommerz" role="tabpanel"
    aria-labelledby="sslcommerz-tab">
    <div class="wsus_other_payment">
        @php 
            $payable_amount = $total * ((PaymentGateway('ssl_commerz_rate')) ? PaymentGateway('ssl_commerz_rate') : 1);
            $payable_amount = round($payable_amount, 2);
        @endphp
        <form action="{{ route('user.sslcommerz-pay', ['cart' => base64_encode(json_encode($cart)), 'tax' => $totaltax, 'total' => $payable_amount]) }}" method="POST" class="needs-validation">
            @csrf
            <button class="common_btn" type="submit">{{__('Continue to checkout (Hosted)')}}</button>
        </form>
    </div>
</div>