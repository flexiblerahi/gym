<div class="tab-pane fade show active" id="bank" role="tabpanel"
    aria-labelledby="bank">
    <div class="wsus__pay_card">
        <h5>{{__('we accept')}} :</h5>
        <img src="{{ asset('assets/frontend/images/payment_img.png') }}" alt="{{__('payment')}}" class="img-fluid w-100">
            <div class="row">
                <div class="wsus__single_input">
                    <textarea id="contact_message" cols="30" rows="3" disabled >{{PaymentGateway('bank_account_details')}}</textarea>
                </div>
                <div class="form-group">
                    <label>
                        {{trans('frontend.Transaction Information')}}
                    </label>
                    <div class="wsus__single_input">
                        <textarea id="bank_transaction_info" name="bank_transaction_info" cols="30" rows="5" placeholder="{{__('Enter Your Transaction Here')}}"></textarea>
                    </div>
                </div>
                <div class="col-12">
                    <button id="payment" class="common_btn">{{__('payment')}}</button>
                </div>
            </div>
        
    </div>
</div>
@push('scripts')
    
<script>
    $('#payment').on('click', function () {
        $.ajax({
            type: "post",
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            },
            url: "{{route('user.bank-pay')}}",
            data: {
                'cart' : '{{base64_encode(json_encode($cart))}}',
                'tax' : '{{$totaltax}}', 
                'total' : '{{$total}}',
                'bank_transaction_info' : $('#bank_transaction_info').val()
            },
            dataType: "json",
            success: function (response) {
                console.log('success :>> ', response);
            },
            error: function (response) {
                toastr.error(response.responseJSON.errors.bank_transaction_info[0])
            }
        });
    });
</script>

@endpush