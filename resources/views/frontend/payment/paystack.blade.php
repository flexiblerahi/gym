

<div class="tab-pane fade" id="paystack" role="tabpanel"
aria-labelledby="paystack-tab">
    <div class="wsus_other_payment">
        <div class="Paystack box">
            <div class="form-group">
                <label></label>
                <div class="col-sm-12 col-md-12">
                    <form id="paymentForm">
                        <div class="form-submit">
                            <button type="submit" class="common_btn"
                                onclick="payWithPaystack()">{{__('frontend.Pay with Paystack')}}</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@push('scripts')
    <script src="https://js.paystack.co/v1/inline.js"></script>
@php
    $payable_amount = $total * ((PaymentGateway('paystack_rate')) ? PaymentGateway('paystack_rate') : 1);
    $payable_amount = $payable_amount * 100;
    $payable_amount = round($payable_amount, 2);
@endphp
    <script>
        let paymentForm = document.getElementById('paymentForm');
        paymentForm.addEventListener("submit", payWithPaystack, false);

        function payWithPaystack(event) {
            event.preventDefault();
            let handler = PaystackPop.setup({
                key: "{{PaymentGateway('paystack_public_key')}}",
                email: "{{Auth::user()->email}}",
                amount: "{{$payable_amount}}",
                currency: "{{PaymentGateway('paystack_currency')}}",
                onClose: function() {
                    alert('Window closed.');
                },
                callback: function(response) {
                    let reference = response.reference;
                    let tnx_id = response.transaction;
                    window.location.href =
                        "{{route('user.paystack-pay', ['cart' => base64_encode(json_encode($cart)), 'tax' => $totaltax, 'total' => $payable_amount])}}&trx_id=" +
                        tnx_id + "&reference=" + reference;
                }
            });
            handler.openIframe();
        }
    </script>
@endpush