<div class="tab-pane fade" id="molly" role="tabpanel"
    aria-labelledby="stripe-tab">
    <div class="Mollie box">
        <div class="form-group">
            <label></label>
            <div class="col-sm-12 col-md-12">
                @php
                $payable_amount = $total * ((PaymentGateway('mollie_rate')) ? PaymentGateway('mollie_rate') : 1);
                $payable_amount = round($payable_amount, 2);
                @endphp
                <form
                    action="{{route('user.mollie-pay', ['cart' => base64_encode(json_encode($cart)), 'tax' => $totaltax, 'total' => $payable_amount])}}"
                    method="post">
                    @csrf
                    <button type="submit"
                        class="common_btn">{{__('frontend.Pay with Mollie')}}</button>
                </form>
            </div>
        </div>
    </div>
</div>