<div class="tab-pane fade" id="stripe" role="tabpanel"
    aria-labelledby="stripe-tab">
    <div class="wsus_other_payment">
        <form  role="form" 
            action="{{ route('user.stripe-pay', ['cart' => base64_encode(json_encode($cart)), 'tax' => $totaltax, 'total' => $total]) }}" 
            method="post" 
            class="require-validation"
            data-cc-on-file="false"
            data-stripe-publishable-key="{{PaymentGateway('stripe_api_publishable_key')}}"
            id="payment-form">
            @csrf
            <div class="row">
                <div class="col-xl-12">
                    <input type="text" id="card"  size='20'
                    class="card-number" placeholder="{{__('Card Number')}}">
                </div>
                <div class="col-xl-6">
                    <input type="text" id="expiremonth" class="card-expiry-month" placeholder="{{__('Exp: Month')}}">
                </div>
                <div class="col-xl-6">
                    <input type="text" id="expireyear" class="card-expiry-year" placeholder="{{__('Exp: Year')}}">
                </div>
                <div class="col-xl-12">
                    <input type="text" id="cvv" class="card-cvc" placeholder="{{__('CVV')}}">
                </div>
                <div class="col-xl-12">
                    <button id="confirm" class="common_btn">{{__('confirm')}}</button>
                </div>
            </div>
        </form>
    </div>
</div>

@push('scripts')
<script type="text/javascript" src="https://js.stripe.com/v2/"></script>
<script>
    
$(function() {
    var $form = $(".require-validation");
        
    $('form.require-validation').bind('submit', function(e) {
        var $form = $(".require-validation"),
        inputSelector = ['input[type=email]', 'input[type=password]',
                        'input[type=text]', 'input[type=file]',
                        'textarea'].join(', '),
        $inputs = $form.find('.required').find(inputSelector),
        $errorMessage = $form.find('div.error'),
        valid = true;
        $errorMessage.addClass('hide');
    
        $('.has-error').removeClass('has-error');
        $inputs.each(function(i, el) {
            var $input = $(el);
            if ($input.val() === '') {
                $input.parent().addClass('has-error');
                $errorMessage.removeClass('hide');
                e.preventDefault();
            }
     });
        
        if (!$form.data('cc-on-file')) {
            e.preventDefault();
            Stripe.setPublishableKey($form.data('stripe-publishable-key'));
            Stripe.createToken({
            number: $('.card-number').val(),
            cvc: $('.card-cvc').val(),
            exp_month: $('.card-expiry-month').val(),
            exp_year: $('.card-expiry-year').val()
            }, stripeResponseHandler);
        }
        
    });

    function stripeResponseHandler(status, response) {
            if (response.error) {
                $('.error')
                    .removeClass('hide')
                    .find('.alert')
                    .text(response.error.message);
            } else {
                var token = response['id'];
                $form.find('input[type=text]').empty();
                $form.append("<input type='hidden' name='stripeToken' value='" + token + "'/>");
                $form.get(0).submit();
            }
        }
        
    });
</script>
@endpush