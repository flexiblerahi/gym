
<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('payment')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="{{route('payment')}}">{{__('payment')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__payment mt_120 xs_mt_70">
    <div class="container">
        <div class="row">
            <div class="col-lg-2 col-md-4">
                <div class="wsus__payment_item" id="sticky_sidebar2">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active" id="bank-tab" data-bs-toggle="pill"
                                data-bs-target="#bank" type="button" role="tab" aria-controls="bank"
                                aria-selected="true">{{__('Bank')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="paypal-tab" data-bs-toggle="pill"
                                data-bs-target="#paypal" type="button" role="tab"
                                aria-controls="paypal" aria-selected="false">{{__('PayPal')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="stripe-tab" data-bs-toggle="pill"
                                data-bs-target="#stripe" type="button" role="tab"
                                aria-controls="stripe" aria-selected="false">{{__('Stripe')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="razorpay-tab" data-bs-toggle="pill"
                                data-bs-target="#razorpay" type="button" role="tab"
                                aria-controls="razorpay" aria-selected="false">{{__('Razorpay')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="paystack-tab" data-bs-toggle="pill"
                                data-bs-target="#paystack" type="button" role="tab"
                                aria-controls="paystack" aria-selected="false">{{__('Paystack')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="perfectmoney-tab" data-bs-toggle="pill"
                                data-bs-target="#perfectmoney" type="button" role="tab"
                                aria-controls="perfectmoney" aria-selected="false">{{__('perfectmoney')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="molly-tab" data-bs-toggle="pill"
                                data-bs-target="#molly" type="button" role="tab"
                                aria-controls="molly" aria-selected="false">{{__('mollie')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link" id="sslcommerz-tab" data-bs-toggle="pill"
                                data-bs-target="#sslcommerz" type="button" role="tab"
                                aria-controls="sslcommerz" aria-selected="false">{{__('sslcommerz')}}</button>
                        </li>
                    </ul>
                </div>
            </div>
            <div class="col-lg-6 col-md-8">
                <div class="wsus__payment_info">
                    <div class="tab-content" id="pills-tabContent">
                        @include('frontend.payment.bank')
                        @include('frontend.payment.stripe')
                        @include('frontend.payment.paypal')
                        @include('frontend.payment.razorpay')
                        @include('frontend.payment.molly')
                        @include('frontend.payment.paystack')
                        @include('frontend.payment.sslcommerz')
                    </div>
                </div>
            </div>
            <div class="col-lg-4">
                <div class="sidebar_item summary" id="sticky_sidebar">
                    <h4>{{__('order summary')}}</h4>
                    <ul>
                        <li>{{__('Subtotals:')}} <span>{{currencyPosition($subtotal)}}</span></li>
                        <li>{{__('shipping fee')}} (+): <span>{{currencyPosition($shipping['cost'])}}</span></li>
                        <li>{{__('tax')}} (+): <span>{{currencyPosition($totaltax)}}</span></li>
                        <li>{{__('coupon')}} (-): <span>
                        @if($coupon['coupon']['type'] == $coupon['inprice'] )
                            {{currencyPosition($coupon['coupon']['amount'])}}
                        @else
                            {{$coupon['coupon']['amount'].'%'}}
                        @endif</span></li>
                        <li><b>{{__('total')}}</b><b>{{currencyPosition($total)}}</b></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>