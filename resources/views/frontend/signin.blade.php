<section class="wsus__signin mt_130 xs_mt_80">
    <div class="container">
        <div class="row">
            <div id="registerform" class="col-xl-5 m-auto">
                <div class="wsus__login_form">
                    <ul class="nav nav-pills" id="pills-tab" role="tablist">
                        <li class="nav-item" role="presentation">
                            <button class="nav-link active text-white" id="pills-home-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                                aria-selected="true">{{__('Sign in')}}</button>
                        </li>
                        <li class="nav-item" role="presentation">
                            <button class="nav-link text-white" id="pills-profile-tab" data-bs-toggle="pill"
                                data-bs-target="#pills-profile" type="button" role="tab" aria-controls="pills-profile"
                                aria-selected="false">{{__('Sign Up')}}</button>
                        </li>
                    </ul>
                    <div class="tab-content" id="pills-tabContent">
                        <div class="tab-pane fade show active" id="pills-home" role="tabpanel"
                            aria-labelledby="pills-home-tab">
                            <form>
                                <label>{{__('Email Address')}}</label>
                                <div class="wsus__single_input">
                                    <input type="email" name="email" id="email">
                                    <span id="emailerr" class="text-danger"></span>
                                </div>
                                <label>{{__('password')}} <a href="" id="forgetPassword">{{__('forget
                                        password')}}</a></label>
                                <div class="wsus__single_input">
                                    <input type="password" name="password" id="password">
                                    <span id="showpassword"><i class="far fa-eye-slash"></i></span>
                                    <span id="passworderr" class="text-danger"></span>
                                </div>
                                <a id="loginbtn" class="common_btn">{{__('sign in')}} <i class="far fa-arrow-right"></i></a>
                                <a class="common_btn d-none" id="showspin"><i class="fa fa-circle-o-notch fa-spin " ></i></a>
                              </form>
                        </div>
                        <div class="tab-pane fade" id="pills-profile" role="tabpanel"
                            aria-labelledby="pills-profile-tab">
                            <form>
                                <label>{{__('name')}}</label>
                                <div class="wsus__single_input">
                                    <input type="text" id="name" name="name">
                                    <span id="namerr" class="text-danger"></span>
                                </div>
                                <label>{{__('Email Address')}}</label>
                                <div class="wsus__single_input">
                                    <input type="email" id="remail" name="email">
                                    <span id="remailerr" class="text-danger"></span>
                                </div>
                                <label>{{__('password')}}</label>
                                <div class="wsus__single_input">
                                    <input type="password" name="password" id="rpassword">
                                    <span id="showrpassword"><i class="far fa-eye-slash"></i></span>
                                </div>
                                <span id="rpassworderr" class="text-danger"></span>
                                <label>{{__('confirm password')}}</label>
                                <div class="wsus__single_input">
                                    <input type="password" id="confirm_password" name="confirm_password">
                                    <span id="showconfirmpassword"><i class="far fa-eye-slash"></i></span>
                                </div>
                                <span id="rconfirmpassworderr" class="text-danger"></span>
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" id="flexCheckDefault">
                                    <label class="form-check-label" for="flexCheckDefault">
                                        {{__('Are you agree to ')}} <a href="{{route('termcondition')}}">{{__('Terms of Condition')}}</a>
                                        {{__('and')}} <a href="{{route('privacy')}}">{{__('Privacy Policy')}}</a>
                                    </label>
                                </div>
                                <a id="submitbtn" class="common_btn">{{__('sign up')}} <i class="far fa-arrow-right"></i></a>
                                <a class="common_btn d-none" id="rshowspin"><i class="fa fa-circle-o-notch fa-spin " ></i></a>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div id="forgetpassword" class="col-xl-5 m-auto d-none">
                <div class="wsus__login_form">
                    <div class="tab-content" id="pills-tabContent">
                        <form name="login-form" class="clearfix" method="POST" action="{{route('user.forgot')}}">
                            @csrf
                            <div class="wsus__heading_area text-center mb_20">
                                <h4><i class="fa fa-key"></i> {{__('Reset Password')}}</h4>
                            </div>
                            <label>{{__('Email Address')}}</label>
                            <div class="wsus__single_input">
                                <input type="email" name="email" id="email">
                                <span id="emailerr" class="text-danger"></span>
                            </div>
                            <button type="submit" class="common_btn">{{__('Reset')}} <i
                                    class="far fa-arrow-right"></i></button>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script>
    $(function() {
            $('.common_btn').css('cursor','pointer');
            $('#loginbtn').on('click', function() {
                jQuery.ajax({
                    type: 'POST',
                    url: '{{ route('user.login') }}',
                    data: {
                        "_token": "{{ csrf_token() }}",
                        'email': $('#email').val(),
                        'password': $('#password').val() },
                    dataType: 'JSON',
                    beforeSend: function(){
                        // Show image container
                        $('#showspin').removeClass('d-none');
                        $('#loginbtn').addClass('d-none');
                    },
                    success: function(data) {
                        window.location = data.previousurl;
                        data.success ? toastr.success(data.success) : toastr.error(data.error);
                    },
                    error: function(data) {
                        let allerrors = null;
                        if(data.responseJSON.errors) {
                            allerrors = data.responseJSON.errors;
                            if(allerrors.password) toastr.error(allerrors.password[0])
                            if(allerrors.email)toastr.error(allerrors.email[0])
                        }
                    },
                    complete: function() {
                        // $('#showspin').addClass('d-none');
                        $('#showspin').addClass('d-none');
                        $('#loginbtn').removeClass('d-none');
                    }
                });
            });

            $('#submitbtn').on('click', function() {
                if($('#flexCheckDefault').is(':checked')) {
                    let registerurl = "{{ route('registrations.store') }}";
                    jQuery.ajax({
                        type: 'POST',
                        url: registerurl,
                        data: {
                            "_token": "{{ csrf_token() }}",
                            'name': $('#name').val(),
                            'email': $('#remail').val(),
                            'password': $('#rpassword').val(),
                            'confirm_password': $('#confirm_password').val() },
                        dataType: 'JSON',
                        beforeSend: function(){
                            // Show image container
                            $('#rshowspin').removeClass('d-none');
                            $('#submitbtn').addClass('d-none');
                        },
                        success: function(data) {
                            $('#name').val('');
                            $('#remail').val('');
                            $('#rpassword').val('');
                            $('#confirm_password').val('');
                            data.success ? toastr.success(data.success) : toastr.error(data.error);
                            window.location = "{{ route('signin') }}";
                        },
                        error: function(data) {
                            if(data.responseJSON.errors) {
                                const allerrors = data.responseJSON.errors; 
                                if(allerrors.confirm_password) toastr.error(allerrors.confirm_password[0])
                                if(allerrors.password) toastr.error(allerrors.password[0])        
                                if(allerrors.email) toastr.error(allerrors.email[0])
                                if(allerrors.name) toastr.error(allerrors.name[0])
                            }
                        },
                        complete: function() {
                            // $('#showspin').addClass('d-none');
                            $('#rshowspin').addClass('d-none');
                            $('#submitbtn').removeClass('d-none');
                        }
                    });
                } else toastr['error']('{{__("Please Click on Terms and condition and privacy policy.")}}')
            });

            $('#forgetPassword').on('click', function (e) { 
                e.preventDefault();
                $('#registerform').addClass('d-none');
                $('#forgetpassword').removeClass('d-none');
            });

            $('#sendbtn').on('click', function (e) {
                e.preventDefault();

                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("subscribe")}}',
                     data: {
                        'email': $('#mail').val(),
                     },
                     success: function(data) {
                        toastr[data.type](data.message);
                        if(data.type == 'success') {
                            $('#iscartitemexists').removeClass('d-none');
                            $('#cartitemtotal').text(data.total);
                        }
                     },
                 });
             });

            $(document).on('click', '#showpassword', function() {
                if ($('#password').attr("type") == "text") { 
                    $('#password').attr('type', 'password');
                    $('#showpassword i').addClass("fa-eye-slash");
                    $('#showpassword i').removeClass("fa-eye");
                } else if ($('#password').attr("type") == "password") {
                    $('#password').attr('type', 'text');
                    $('#showpassword i').removeClass("fa-eye-slash");
                    $('#showpassword i').addClass("fa-eye");
                }
            });

            $(document).on('click', '#showrpassword', function() {
                if ($('#rpassword').attr("type") == "text") { 
                    $('#rpassword').attr('type', 'password');
                    $('#showrpassword i').addClass("fa-eye-slash");
                    $('#showrpassword i').removeClass("fa-eye");
                } else if ($('#rpassword').attr("type") == "password") {
                    $('#rpassword').attr('type', 'text');
                    $('#showrpassword i').removeClass("fa-eye-slash");
                    $('#showrpassword i').addClass("fa-eye");
                }
            });

            $(document).on('click', '#showconfirmpassword', function () {  
                if ($('#confirm_password').attr("type") == "text") { 
                    $('#confirm_password').attr('type', 'password');
                    $('#showconfirmpassword i').addClass("fa-eye-slash");
                    $('#showconfirmpassword i').removeClass("fa-eye");
                } else if ($('#confirm_password').attr("type") == "password") {
                    $('#confirm_password').attr('type', 'text');
                    $('#showconfirmpassword i').removeClass("fa-eye-slash");
                    $('#showconfirmpassword i').addClass("fa-eye");
                }
            })
        });
</script>
@endpush