<section class="wsus_contact mt_130 xs_mt_80">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 m-auto">
                <form class="wsus__contact_area">
                    <div class="row">
                        <div class="col-xl-12">
                            <h2>{{__('Drop Us a Line')}}</h2>
                        </div>
                        <div class="col-xl-6">
                            <span id="namerr" class="text-danger"></span>
                            <div class="wsus__single_input">
                                <input id="contact_name" type="text" placeholder="{{__('Enter Your Name')}}">
                                <span><i class="fal fa-user-alt"></i></span>
                            </div>
                        </div>
                        <div class="col-xl-6">
                            <span id="emailerr" class="text-danger"></span>
                            <div class="wsus__single_input">
                                <input id="contact_email" type="email" placeholder="{{__('Enter Email Address')}}">
                                <span><i class="fal fa-envelope"></i></span>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <span id="phoneerr" class="text-danger"></span>
                            <div class="wsus__single_input">
                                <input id="contact_phone" type="text" placeholder="{{__('Enter Phone Nember')}}">
                                <span><i class="fas fa-phone-alt"></i></span>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="wsus__single_input">
                                <textarea id="contact_message" cols="3" rows="5" placeholder="{{__('Enter Your Message Here')}}"></textarea>
                                <span><i class="fal fa-edit"></i></span>
                            </div>
                            <button id="messagesubmit" class="common_btn">{{__('Submit Request')}} <i class="fas fa-paper-plane"></i></button>
                        </div>
                    </div>
                </form>
                <div class="wsus__contact_map mt_130 xs_mt_80">
                    <iframe
                        src="{{$googlemap->link}}"
                        width="600" height="500" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts')
    <script>
        $(function() {
            $('#messagesubmit').on('click', function(e) {
                e.preventDefault();
                let url = window.location.href;
                let formData = {
                    "_token": "{{ csrf_token() }}",
                    'name': $('#contact_name').val(),
                    'email': $('#contact_email').val(),
                    'phone': $('#contact_phone').val(),
                    'message': $('#contact_message').val()
                };
                console.log('formData :>> ', formData);
                
                jQuery.ajax({
                    type: 'POST',
                    url: '{{ route('contactmessage.store') }}',
                    data: formData,
                    dataType: 'JSON',
                    success: function(data) {
                        $('#contact_name').val('');
                        $('#contact_email').val('');
                        $('#contact_phone').val('');
                        $('#contact_message').val('');
                        data.success ? toastr.success(data.success) : toastr.error(data.error);
                    },
                    error: function(data) {
                        console.log('data error :>> ', data);
                        let allerrors = null;
                        if(data.responseJSON.errors) {
                            allerrors = data.responseJSON.errors;
                            if(allerrors.name) toastr.error(allerrors.name[0]);
                            if(allerrors.email) toastr.error(allerrors.email[0]);
                            if(allerrors.phone) toastr.error(allerrors.phone[0]);
                            if(allerrors.message) toastr.error(allerrors.message[0]);  
                        }
                    }
                });
            })
        });
    </script>
@endpush
