<div class="wsus__pagination mt_60">
    <nav aria-label="Page navigation example">
        <ul class="pagination">
            <li class="page-item">
                <a class="page-link" data-previouspage="{{ $blogcomments->previousPageUrl() }}" id="previouspage" aria-label="{{__('Previous')}}">
                    <i class="far fa-angle-double-left"></i>
                </a>
            </li>
            @foreach ($blogcomments->links()->elements[0] as $key => $pagelink)
                <li class="page-item @if($blogcomments->currentPage() == $key) active @endif"><a class="page-link paginations" data-page="{{$key}}" id="paginate_{{$key}}">{{ $key }}</a></li>
            @endforeach
            <li class="page-item">
                <a class="page-link" data-nextpage="{{ $blogcomments->nextPageUrl() }}" id="nextpage" aria-label="{{__('Next')}}">
                    <i class="far fa-angle-double-right"></i>
                </a>
            </li>
        </ul>
    </nav>
</div>