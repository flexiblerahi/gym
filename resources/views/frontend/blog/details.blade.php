<section class="wsus__breadcrumb" style="background: url({{asset(GetSetting('breadcumb'))}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('blog details')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('blog')}}</a></li>
                        <li><a href="">{{__('blog details')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__blog_details mt_105 xs_mt_55">
    <div class="container">
        <div class="row">
            <div class="col-xl-8">
                <div class="wsus__blog_details_area">
                    <div class="wsus__blog_det_img">
                        <img src="{{ asset($blogdetails->image) }}" alt="{{__('blog')}}" class="img-fluid w-100">
                    </div>
                    <div class="wsus__blog_det_text">
                        <ul class="top_icon d-flex fles-wrap">
                            <li><i class="far fa-calendar-alt"></i> {{__(date_format($blogdetails->created_at, 'd F Y'))}}</li>
                            <li><i class="fas fa-comment-dots"></i> {{($blogcomments->total())}} {{__('Comments')}}</li>
                            <li><i class="fas fa-user"></i> {{__('Admins')}}</li>
                        </ul>
                        <h4 class="title">{{($blogdetails->title)}}</h4>
                        {!! clean(html_entity_decode($blogdetails->description))!!}
                        @if($blogdetails->comment_status == App\Models\Blog::ACTIVE)
                            <div class="wsus__blog_comment_area">
                                @if ($blogcomments->total()>0)
                                    <h2>{{__('Comments -')}} <span>{{($blogcomments->total())}}</span></h2>
                                @endif
                                <div id="blogcommentpage">
                                    @include('frontend.blog.commentpage')
                                </div>
                            </div>
                            <div class="wsus__blog_comment_post">
                                <h2>{{__('Post a comment')}}</h2>
                                <form>
                                    <span id="namerr" class="text-danger"></span>
                                    <input id="comment_name" type="text" placeholder="{{__('Name*')}}">
                                    <span id="emailerr" class="text-danger"></span>
                                    <input id="comment_email" type="email" placeholder="{{__('Email*')}}">
                                    <span id="cmderr" class="text-danger"></span>
                                    <textarea id="blog_comment" rows="5" placeholder="{{__('Write a comment')}}"></textarea>
                                    <button id="commentsubmit" class="common_btn">{{__('post a comment')}} <i class="fa fa-circle-o-notch fa-spin d-none" id="showspin"></i></button>
                                </form>
                            </div>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-xl-4">
                <div class="wsus__sidebar" id="sticky_sidebar">
                    <div class="row">
                        <div class="col-xl-12">
                            <div class="sidebar_item wsus__blog_serch">
                                <h3>{{__('Search blog')}}</h3>
                                <form action="{{route('blog.index')}}" method="GET">
                                    <input type="text" name="blog_name" placeholder="{{__('Search')}}">
                                    <button type="submit" class="common_btn"><i class="far fa-search"></i></button>
                                </form>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="sidebar_item wsus__blog_categori pb_20">
                                <h3>{{__('Categories')}}</h3>
                                <ul>
                                    @foreach ($blogcategories as $blogcategory )
                                         <li><a href="{{route('blog.index', ['category_id'=> $blogcategory->id])}}">{{($blogcategory->name)}} <span>{{($blogcategory->getBlogs->count())}}</span></a></li>     
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                        <div class="col-xl-12">
                            <div class="sidebar_item">
                                <h3>{{__('News & Blogs')}}</h3>
                                <ul class="wss__sedebar_blog_item">    
                                    @foreach ($newblogs as $newblog )             
                                        <li>
                                            <img src="{{ asset($newblog->image) }}" alt="{{__('blog')}}" class="img-fluid w-100">
                                            <div class="text">
                                                <a href="{{ route('blog.show',$newblog->slug) }}">{{($newblog->title)}}</a>
                                                <p><i class="far fa-calendar-alt"></i>{{__(date_format($newblog->created_at, 'F d, Y'))}}</p>
                                            </div>
                                        </li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts')
    <script>
        $(function() {
            $('#commentsubmit').on('click', function(e) {
                e.preventDefault();
                let url = window.location.href;
                let spliturl = url.split('/');
                let formData = {
                    "_token": "{{ csrf_token() }}",
                    'name': $('#comment_name').val(),
                    'email': $('#comment_email').val(),
                    'comment': $('#blog_comment').val(),
                    'slug': spliturl[spliturl.length - 1]
                };
                console.log('formData :>> ', formData);
                
                jQuery.ajax({
                    type: 'POST',
                    url: '{{ route('blogcomment.store') }}',
                    data: formData,
                    dataType: 'JSON',
                    beforeSend: function() {
                        // Show image container
                        $('#showspin').removeClass('d-none');
                    },
                    success: function(data) {
                        $('#comment_name').val('');
                        $('#comment_email').val('');
                        $('#blog_comment').val('');
                        data.success ? toastr.success(data.success) : toastr.error(data.error);
                    },
                    error: function(data) {
                        let allerrors = null;
                        if(data.responseJSON.errors) {
                            allerrors = data.responseJSON.errors;
                            if(allerrors.name) toastr.error(allerrors.name[0])
                            if(allerrors.email) toastr.error(allerrors.email[0])
                            if(allerrors.comment) toastr.error(allerrors.comment[0])       
                        }
                    },
                    complete: function() {
                        $('#showspin').addClass('d-none');
                    }
                });
            })

            $(document).on('click', '.paginations', function() {
                commentpaginationId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    data: {
                        'page':$(this).data('page'),
                    },
                    success: function(data) {
                        $('#blogcommentpage').html(data);
                    }
                });
            });

            $(document).on('click', '#nextpage', function() {
                nextpageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('nextpage'),
                    data: {
                        'page':$(this).data('page'),
                    },
                    success: function(data) {
                        $('#blogcommentpage').html(data);
                    }
                });
            });
            
            $(document).on('click', '#previouspage', function() {
                previouspageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('previouspage'),
                    data: {
                        'page':$(this).data('page'),
                    },
                    success: function(data) {
                        $('#blogcommentpage').html(data);
                    }
                });
            });
        });
    </script>
@endpush
