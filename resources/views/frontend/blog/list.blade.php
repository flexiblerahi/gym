<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('blog grid')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="{{route('blog.index')}}">{{__('blog grid')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__blog_grid mt_105 xs_mt_50">
    <div class="container">
        <div class="row">
            @foreach ($blogs as $blog )
            <div class="col-xl-4">
                <div class="wsus__single_blog">
                    <div class="wsus__blog_img">
                        <img src="{{ asset($blog->image) }}" alt="{{__('blog')}}" class="img-fluid w-100">
                    </div>
                    <div class="wsus__blog_text">
                        <ul class="wsus__blog_header">
                            <li> {{__(date_format($blog->created_at, 'd, F Y'))}}</li>
                            <li>{{__("Author")}}</li>
                        </ul>
                        <a href="{{ route('blog.show',$blog->slug) }}">{{($blog->title)}}</a>
                        {!! clean(Str::limit(html_entity_decode($blog->description), 50, ' ...')) !!}
                    </div>
                </div>
            </div>   
            @endforeach
            @if($blogs->count()>=1)
                <div class="col-12 mt_35">
                    <div class="wsus__pagination">
                        <nav aria-label="Page navigation example">
                            <ul class="pagination">
                                <li class="page-item">
                                    <a class="page-link" href="{{ $blogs->previousPageUrl() }}" aria-label="{{__('Previous')}}">
                                        <i class="far fa-angle-double-left"></i>
                                    </a>
                                </li>
                                @foreach ($blogs->links()->elements[0] as $key => $pagelink)
                                    @if(request()->category_id)
                                    <li class="page-item @if($blogs->currentPage() == $key) active @endif"><a class="page-link" href="{{ request()->url().'?category_id='.request()->category_id.'&page='.$key }}">{{ $key }}</a></li>
                                    @elseif(request()->blog_name)
                                    <li class="page-item @if($blogs->currentPage() == $key) active @endif"><a class="page-link" href="{{ request()->url().'?blog_name='.request()->blog_name.'&page='.$key }}">{{ $key }}</a></li>
                                    @else
                                    <li class="page-item @if($blogs->currentPage() == $key) active @endif"><a class="page-link" href="{{ $pagelink }}">{{ $key }}</a></li>
                                    @endif
                                @endforeach
                                <li class="page-item">
                                    <a class="page-link" href="{{ $blogs->nextPageUrl() }}" aria-label="{{__('Next')}}">
                                        <i class="far fa-angle-double-right"></i>
                                    </a>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            @else
                <h1 class="text-center">{{__('NO BLOG AVAILABE')}}</h1>
            @endif
        </div>
    </div>
</section>
