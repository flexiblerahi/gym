
<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('Services')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="{{route('services')}}">{{__('Services')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
@if (count($services)>0 || $sectiontitle)
    <section class="wsus__services mt_120 xs_mt_70">
        <div class="container">
            <div class="row">
                <div class="col-xl-6 m-auto">
                    <div class="wsus__heading_area text-center mb_35">
                        <h2>{{($sectiontitle)?$sectiontitle->servicetitle : ''}}</h2>
                        <p>{{($sectiontitle)?$sectiontitle->servicesubtitle : ''}}</p>
                    </div>
                </div>
            </div>
            <div class="row">
                @foreach ($services as $service)
                    <div class="col-xl-4 col-sm-6 col-lg-4">
                        <div class="wsus__single_services">
                            <span><i class="{{ $service->icon }}"></i></span>
                            <h4>{{__($service->title)}}</h4>
                            <div id="service{{ $service->id }}">
                                {!! Str::limit(html_entity_decode($service->description), 50, '...') !!}
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
    </section>
@endif