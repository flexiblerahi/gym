
@extends('frontend.layouts.master')
@section('content')
    @include('cookieConsent::index')
    <div id="wrapper" class="clearfix">
        @include('frontend.layouts.menus.menuStyleOne')
        @foreach ($pagecomponents as $item)
            @include($item->view)
        @endforeach
        @include('frontend.layouts.footers.footerStyleOne')
        <div class="wsus__scroll_btn">
            <i class="fas fa-chevron-up"></i>
        </div>
    </div>
@endsection
