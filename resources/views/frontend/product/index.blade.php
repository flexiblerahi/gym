@if(Session::has('errormsg') ) 
    @push('scripts')
        <script>
            toastr.error('{{ Session::get("errormsg")}}');
        </script>
    @endpush
    {{Session::forget('errormsg')}}
@endif
<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{ __('product') }}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a>{{__('product')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
 <section class="wsus__product mt_130 xs_mt_80">
     <div class="container">
         <div class="row">
             <div class="col-xl-9" id="paginaterender">
                 @include('frontend.product.productpaginate')
             </div>
             <div class="col-xl-3">
                 <div class="wsus__sidebar" id="sticky_sidebar">
                     <div class="row">
                         <div class="col-xl-12">
                             <div class="sidebar_item wsus__blog_serch product_search">
                                 <h3>{{__('Search Product')}}</h3>
                                 <form>
                                     <input type="text" id="searchinput" placeholder="{{__('Search')}}">
                                     <button class="common_btn" id="searchproductbtn"><i class="far fa-search"></i></button>
                                 </form>
                             </div>
                         </div>
                         <div class="col-xl-12">
                            <div class="sidebar_item wsus__blog_categori product_category pb_20">
                                <h3>{{__('Categories')}}</h3>
                                <ul>
                                    @foreach ($categories as $category)
                                        <li class="product_categories" id="{{$category->id}}"><a class="categories" id="category_{{$category->id}}">{{$category->name}}</a></li>
                                    @endforeach
                                </ul>
                            </div>
                        </div>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section>
 @push('scripts')
     <script>
         $(function() {
            $('.paginations').css('cursor','pointer');
            $('.categories').css('cursor', 'pointer');
            $('.addcart').css('cursor', 'pointer');
            let productCategoryId = '';
            let sortvalue='';
            $(document).on('change', '#sortby', function () {  
                sortvalue = $(this).val();
                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("product.index")}}',
                     data: {
                        'category_id': productCategoryId,
                        'product': $('#searchinput').val() == '' ? null : $('#searchinput').val(),
                        'sortby': sortvalue == '' ? null : sortvalue
                     },
                     success: function(data) {
                        $('#paginaterender').html(data);
                        $('.select_2').select2();
                        $('.paginations').css('cursor','pointer');
                        $('.addcart').css('cursor', 'pointer');
                        $('#sortby').val(sortvalue);
                        $('#sortby').trigger('change.select2');
                
                     },
                     error: function(data) {
                        console.log('error', data);
                     }
                });
            });

            $(document).on('click', '#searchproductbtn', function(e) {
                e.preventDefault();
                sortvalue = $('#sortby').val();
                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("product.index")}}',
                     data: {
                        'category_id': productCategoryId,
                        'product': $('#searchinput').val() == '' ? null : $('#searchinput').val(),
                        'sortby': sortvalue == '' ? null : sortvalue
                     },
                     success: function(data) {
                        $('#paginaterender').html(data);
                        $('.select_2').select2();
                        $('.paginations').css('cursor','pointer');
                        $('.addcart').css('cursor', 'pointer');
                        $('#sortby').val(sortvalue);
                        $('#sortby').trigger('change.select2');
                
                     },
                     error: function(data) {
                        console.log('error', data);
                     }
                });
            })

            $('.product_categories').on('click', function () {  
                productCategoryId = $(this).attr('id');
                console.log('sortvalue :>> ', sortvalue);
                $('.categories').css({'color':'white', 'border-color': 'white'});
                $('#category_'+productCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                console.log('search product :>> ', $('#searchinput').val()); 
                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("product.index")}}',
                     data: {
                        'category_id': productCategoryId,
                        'product': $('#searchinput').val() == '' ? null : $('#searchinput').val(),
                        'sortby': sortvalue == '' ? null : sortvalue
                     },
                     success: function(data) {
                         $('#paginaterender').html(data);
                         $('.select_2').select2();
                         $('.paginations').css('cursor','pointer');
                         $('.addcart').css('cursor', 'pointer');
                         if(sortvalue!=''){
                             $('#sortby').val(sortvalue);
                             $('#sortby').trigger('change.select2');
                         }
                     },
                     error: function(data) {
                         let allerrors = null;
                         if (data.responseJSON.errors) {
                             allerrors = data.responseJSON.errors;
                             if (allerrors.email) $('#emailerr').text(allerrors.email[0])
                             if (allerrors.password) $('#passworderr').text(allerrors.password[0])
                         }
                     }
                 });
             });

             $(document).on('click', '.paginations', function() {
                paginationId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url: window.location.href,
                    data: {
                        'page':$(this).data('page'),
                        'category_id': productCategoryId,
                        'product': $('#searchinput').val() == '' ? null : $('#searchinput').val(),
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#paginaterender').html(data);
                        $('.select_2').select2();
                        $('.paginations').css('cursor','pointer');
                        $('.addcart').css('cursor', 'pointer');
                        if (sortvalue != '') {
                            $('#sortby').val(sortvalue);
                            $('#sortby').trigger('change.select2');
                        }
                       if ( productCategoryId != '') {
                        $('.categories').css({'color':'white', 'border-color': 'white'});
                        $('#category_'+productCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                       }
                    }
                });
             });

             $(document).on('click', '#previouspage', function() {
                nextpageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('previouspage'),
                    data: {
                        'category_id': productCategoryId,
                        'product': $('#searchinput').val() == '' ? null : $('#searchinput').val(),
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#paginaterender').html(data);
                        $('.select_2').select2();
                        $('.paginations').css('cursor','pointer');
                        $('.addcart').css('cursor', 'pointer');
                        if (sortvalue != '') {
                            $('#sortby').val(sortvalue);
                            $('#sortby').trigger('change.select2');
                        }
                       if ( productCategoryId != '') {
                        $('.categories').css({'color':'white', 'border-color': 'white'});
                        $('#category_'+productCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                       }
                    }
                });
             });

             $(document).on('click', '#nextpage', function() {
                nextpageId = $(this).attr('id');
                $.ajax({
                    type: "GET",
                    url:$(this).data('nextpage'),
                    data: {
                        'category_id': productCategoryId,
                        'product': $('#searchinput').val() == '' ? null : $('#searchinput').val(),
                        'sortby': sortvalue == '' ? null : sortvalue
                    },
                    success: function(data) {
                        $('#paginaterender').html(data);
                        $('.select_2').select2();
                        $('.paginations').css('cursor','pointer');
                        $('.addcart').css('cursor', 'pointer');
                        if (sortvalue != '') {
                            $('#sortby').val(sortvalue);
                            $('#sortby').trigger('change.select2');
                        }
                       if ( productCategoryId != '') {
                        $('.categories').css({'color':'white', 'border-color': 'white'});
                        $('#category_'+productCategoryId).css({'color':'#9BB70D', 'border-color':'#9BB70D'});
                       }
                    }
                });
             });

             $('#searchinput').on('keyup', delay(function(e) {
                 console.log('Time elapsed!', this.value);
                console.log('productCategoryId :>> ', productCategoryId);
                 let searchproduct = this.value;
                 jQuery.ajax({
                     type: 'GET',
                     url: '{{route("product.index")}}',
                     data: {
                         'category_id': productCategoryId,
                         'product': searchproduct,
                        'sortby': sortvalue == '' ? null : sortvalue
                     },
                     success: function(data) {
                         $('#paginaterender').html(data);
                         $('.select_2').select2();
                         $('.paginations').css('cursor','pointer');
                         $('.addcart').css('cursor', 'pointer');
                         if(sortvalue!=''){
                             $('#sortby').val(sortvalue);
                             $('#sortby').trigger('change.select2');
                         }
                     },
                     error: function(data) {
                         let allerrors = null;
                         if (data.responseJSON.errors) {
                             allerrors = data.responseJSON.errors;
                             if (allerrors.email) $('#emailerr').text(allerrors.email[0])
                             if (allerrors.password) $('#passworderr').text(allerrors.password[0])
                         }
                     }
                 });
             }, 500));

             $(document).on('click', '.addcart' , function () {  
                let product_id = $(this).data('product_id');
                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("addcart")}}',
                     data: {
                        'product_id': product_id,
                        'type' : 'product',
                        'quantity' : 1,
                     },
                     success: function(data) {
                        toastr[data.type](data.message);
                        if(data.type == 'success') {
                            $('#iscartitemexists').removeClass('d-none');
                            $('#cartitemtotal').text(data.total);
                        }
                     },
                     error: function(data) {
                        console.log('error data :>> ', data);
                     }
                 });
             });
         });

         function delay(callback, ms) {
             var timer = 0;
             return function() {
                 var context = this,
                     args = arguments;
                 clearTimeout(timer);
                 timer = setTimeout(function() {
                     callback.apply(context, args);
                 }, ms || 0);
             };
         }
     </script>
 @endpush
