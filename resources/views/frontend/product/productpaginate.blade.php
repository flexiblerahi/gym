<div class="row">
    <div class="col-xl-12">
        <div class="wsus__product_topbar">
            <p>{{__('Showing' . ' ' . $products->firstItem() . '-' . $products->lastItem() . ' ' . 'of' . ' ' . $products->total() . ' ' . 'Results')}}</p>
            <div class="wsus__select_area">
                <select class="select_2" id="sortby">
                    <option value="" >{{__('short by') }}</option>
                    <option value="price_asc">{{__('Sort by Price (low to high)')}}</option>
                    <option value="price_desc">{{__('Sort by Price (High to high)')}}</option>
                    <option value="name_asc">{{__('Sort by Name (A to Z)')}}</option>
                    <option value="name_desc">{{__('Sort by Name (Z to A)')}}</option>
                </select>
            </div>
        </div>
    </div>
    @foreach ($products as $product)
        @php 
            $productimage = explode(',', $product->image); 
        @endphp
        <div class="col-xl-4">
            <div class="wsus_product">
                <a href="{{ route('product.show', $product->slug) }}" class="wsus_product_img">
                    <img src="{{ asset($productimage[0]) }}" alt="{{__('product')}}" class="img-fluid w-100">
                </a>
                <div class="wsus_product_text">
                    <a class="title" href="{{ route('product.show', $product->slug) }}"> {{__($product->name)}}</a>      
                    @if(intval($product->discount_price)> 0)          
                        <h2><del> {{currencyPosition($product->price)}} </del>&nbsp;{{currencyPosition($product->discount_price)}}</h2> 
                    @else
                        <h2>{{currencyPosition($product->price)}}</h2>
                    @endif
                    <ul>
                        <li><a class="common_btn addcart" data-product_id="{{$product->id}}">{{__('add to cart')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    @endforeach
    @if($products->count()>=1)
        <div class="col-12 mt_35">
            <div class="wsus__pagination">
                <nav aria-label="Page navigation example">
                    <ul class="pagination">
                        <li class="page-item">
                            <a class="page-link" data-previouspage="{{ $products->previousPageUrl() }}" id="previouspage" aria-label="{{__('Previous')}}"><i class="far fa-angle-double-left"></i></a>
                        </li>
                        @foreach ($products->links()->elements[0] as $key => $pagelink)
                            <li class="page-item @if($products->currentPage() == $key) active @endif"><a class="page-link paginations" data-page="{{$key}}" id="paginate_{{$key}}">{{ $key }}</a></li>
                        @endforeach
                        <li class="page-item">
                            <a class="page-link" data-nextpage="{{ $products->nextPageUrl() }}" id="nextpage" aria-label="{{__('Next')}}"><i class="far fa-angle-double-right"></i></a>
                        </li> 
                    </ul>
                </nav>
            </div>
        </div>
    @else
        <h1 class="text-center">{{__('PRODUCT NOT AVAILABE')}}</h1>
    @endif
</div>
