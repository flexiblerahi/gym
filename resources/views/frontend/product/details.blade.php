<section class="wsus__breadcrumb" style="background: url({{asset(GetSetting('breadcumb'))}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('product details')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('product.index')}}">{{__('products')}}</a></li>
                        <li><a href="">{{__('product details')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__product_details mt_130 xs_mt_80">
    <div class="container">
        <div class="row">
            <div class="col-xl-10 m-auto">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="wsus__pro_zoom_area">
                            <div class="exzoom hidden" id="exzoom">
                                <div class="exzoom_img_box">
                                    <ul class='exzoom_img_ul'>
                                        @foreach ( $productimage = explode(',', $product->image) as $image)
                                        <li><img class="zoom ing-fluid w-100" src="{{ asset($image) }}" alt="{{__('product')}}"></li>
                                        @endforeach
                                    </ul>
                                </div>
                                <div class="exzoom_nav"></div>
                                <p class="exzoom_btn">
                                    <a href="javascript:void(0);" class="exzoom_prev_btn"> <i class="far fa-chevron-left"></i> </a>
                                    <a href="javascript:void(0);" class="exzoom_next_btn"> <i class="far fa-chevron-right"></i> </a>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7">
                        <div class="wsus__pro_det_area">
                            <h3>{{($product->name)}}</h3>
                            <p class="rating">
                                @php
                                    $totaltrate = 5;
                                    $productrating = $productreview->avg('rating') + 1;
                                @endphp
                                @while ($totaltrate > 0)
                                    @php
                                        $productrating--;
                                        $totaltrate--;
                                    @endphp
                                    @if ($productrating >= 0.1 && $productrating < 1)
                                        <i class="fas fa-star-half-alt"></i>
                                    @elseif($productrating >= 1)
                                        <i class="fas fa-star"></i>
                                    @else
                                        <i class="fal fa-star"></i>
                                    @endif
                                @endwhile
                                <span> @if ($productreview->avg('rating') > 0) {{__(round($productreview->avg('rating'), 1))}} @endif </span>
                            </p>
                            @if (intval($product->discount_price)> 0)          
                                <h2><del> {{currencyPosition($product->price)}} </del>&nbsp;{{currencyPosition($product->discount_price)}}</h2> 
                            @else
                                <h2>{{currencyPosition($product->price)}}</h2>
                            @endif
                            <h4> <span> {{__('Stock:')}}</span> {{($product->stock > 0) ? 'Available' : 'Out Of Stock'}} </h4>
                            <h4><span> {{__('SKU:')}}</span> {{($product->sku)}}</h4>
                            <div class="quentity_area">
                                <h4>{{__('Quantity:')}}</h4>
                                <div class="quentity">
                                    <div class="quentity_btn">
                                        <button id="removeqtn">-</button>
                                        <input id="quantity" type="text" placeholder="1">
                                        <button id="addqtn">+</button>
                                    </div>
                                    {{-- <form class="select_number">
                                        <input class="number_area" id="quantity" type="number" min="1" max="100" value="1" />
                                    </form> --}}
                                </div>
                                <ul class="d-flex flex-wrap">
                                    <li><a class="common_btn addcart" data-product_id="{{$product->id}}">{{__('add to cart')}}</a></li>
                                </ul>
                            </div>
                            @if ($product->category_id != null)
                                <h4><span> {{__('Category:')}}</span> {{$product->getCategory->name}}</h4>
                            @endif
                            
                            <div class="share_area">
                                <h4>{{__('share:')}}</h4>
                                <ul class="d-flex flex-wrap">
                                    <li><a href="https://www.facebook.com/sharer/sharer.php?u={{url()->current()}}"><i class="fab fa-facebook-f"></i></a></li>
                                    <li><a href="https://www.linkedin.com/sharing/share-offsite/?url={{url()->current()}}"><i class="fab fa-linkedin-in"></i></a></li>
                                    <li><a href="http://www.twitter.com/share?url={{url()->current()}}"><i class="fab fa-twitter"></i></a></li>
                                    <li><a href="http://pinterest.com/pin/create/bookmarklet?url={{url()->current()}}"><i class="fab fa-pinterest-p"></i></a></li>
                                    <li><a href="https://vk.com/share.php?url={{url()->current()}}"><i class="fab fa-vk"></i></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-12">
                        <div class="wsus__pro_description mt_80 xs_mt_60">
                            <ul class="nav nav-pills" id="pills-tab" role="tablist">
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link btn btn-dark active" id="pills-home-tab" data-bs-toggle="pill" data-bs-target="#pills-home" type="button" role="tab" aria-controls="pills-home"
                                        aria-selected="true">{{__('description')}}</button>
                                </li>
                                <li class="nav-item" role="presentation">
                                    <button class="nav-link btn btn-dark " id="pills-contact-tab" data-bs-toggle="pill" data-bs-target="#pills-contact" type="button" role="tab" aria-controls="pills-contact"
                                        aria-selected="false">{{__('reviews')}} ({{__($productreview->where('parent_id', 0)->count())}})</button>
                                </li>
                            </ul>
                            <div class="tab-content " id="pills-tabContent">
                                <div class="tab-pane fade show active" id="pills-home" role="tabpanel" aria-labelledby="pills-home-tab">
                                    <div class="description_text">
                                       {!! htmlspecialchars_decode(nl2br($product->description)) !!}
                                      </div>
                                </div>
                            
                                <div class="tab-pane fade" id="pills-contact" role="tabpanel" aria-labelledby="pills-contact-tab">
                                    <div class="description_text">
                                        @if ($productreview->total()>0)
                                        <h3>{{__('Reviews')}} - {{$productreview->total()}}</h3>    
                                        @endif
                                        <div class="row">
                                            <div class="col-xl-12" id="productcommentpage">
                                                @include('frontend.product.commentpage')
                                            </div>
                                            <div class="col-12">
                                                <div class="wsus__blog_comment_post">
                                                    <h2>{{__('Leave A Review')}}</h2>
                                                    <form>
                                                        <div class="row">
                                                            <div class="col-12">
                                                                <div class="wsus__select_review">
                                                                    <p>
                                                                        <span>{{__('Your Rating')}}:</span>
                                                                        <i id="rate_1" class="fas fa-star rating" data-rate="1"></i>
                                                                        <i id="rate_2" class="fas fa-star rating" data-rate="2"></i>
                                                                        <i id="rate_3" class="fas fa-star rating" data-rate="3"></i>
                                                                        <i id="rate_4" class="fas fa-star rating" data-rate="4"></i>
                                                                        <i id="rate_5" class="fas fa-star rating" data-rate="5"></i>
                                                                    </p>
                                                                </div>
                                                            </div>
                                                            <input type="hidden" class="product_id" id="id" value="{{$product->id}}">
                                                            <div class="col-xl-12">
                                                                <textarea rows="5" id="review_comment" placeholder="{{__('Write a comment') }}"></textarea>
                                                                <button id="review_submit" type="submit" class="common_btn">{{__('submit review')}}</button>
                                                            </div>
                                                        </div>
                                                    </form>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@push('scripts')
<script>
    $(function() {
        $('.common_btn').css('cursor','pointer');
        let quantity = 1;
        let regExp = /[a-zA-Z]/g;

// var testString = "john";
            
// if(regExp.test(testString)){
//   /* do something if letters are found in your string */
// } else {
//   /* do something if letters are not found in your string */
// }
        $('#quantity').val(quantity);
        $(document).on('click', '#addqtn', function () {  
            quantity = quantity + 1;
            if(quantity>=100)  toastr.error('{{__("Can not add more than 100.")}}');
            $('#quantity').val(quantity);
        })

        $(document).on('click', '#removeqtn', function () { 
            quantity = quantity - 1;
            if(quantity<1) toastr.error('{{__("Undefind Quantity")}}');
            $('#quantity').val(quantity);
            
        })

        $(document).on('keyup', '#quantity', function () { 
            if(!regExp.test($(this).val())) {
                quantity = $(this).val(); 
                if(quantity>=100) toastr.error('{{__("Can not add more than 100.")}}');
                else if(quantity<1) toastr.error('{{__("Undefind Quantity")}}');
                quantity = $(this).val(); 
            } else toastr.error('{{__("Undefind Quantity")}}');
            
        })

        $(document).on('click', '.addcart' , function () {  
            let product_id = $(this).data('product_id');
            if(!regExp.test($(this).val())) toastr.error('{{__("Undefind Quantity")}}');
            else if(quantity>=100) toastr.error('{{__("Can not add more than 100.")}}');
            else if(quantity<1) toastr.error('{{__("Undefind Quantity")}}');
            else {
                jQuery.ajax({
                        type: 'GET',
                        url: '{{route("addcart")}}',
                        data: {
                        'product_id': product_id,
                        'type' : 'product',
                        'quantity' :quantity,
                        },
                        success: function(data) {
                            toastr[data.type](data.message);
                            if(data.type == 'success') {
                                $('#iscartitemexists').removeClass('d-none');
                                $('#cartitemtotal').text(data.total);
                            }
                        }
                    });
                }
        });
        //Rating
        let rating = 0;
        $('.rating').on('click', function() {
            let rate = $(this).attr('data-rate');
            rating = rate;
            $('.rating').removeClass('text-warning');
            for (i = 0; i <= rate; i++) {
                $('#rate_' + i).addClass('text-warning');
            }
        });
        //Product Review
        $('#review_submit').on('click', function(e) {
            e.preventDefault();
            let userId = @json($userid);
            let productId = $('#id').val();
            let formData = {
                "_token": "{{ csrf_token() }}",
                'user_id': userId, 
                'comment': $('#review_comment').val(),
                'rating': rating,
                'product_id': productId,
                'url': window.location.href
            };
            jQuery.ajax({
                type: 'POST',
                url: '{{ route('user.productreview') }}',
                data: formData,
                dataType: 'JSON',
                success: function(data) {
                    $('#review_comment').val('');
                    data.success ? toastr.success(data.success) : toastr.error(data.error);
                },
                error: function(data) {
                    if (data.responseJSON.error== "Unauthenticated.") {
                        window.location = "{{route('signin')}}";
                    }
                    let allerrors = null;
                    if(data.responseJSON.errors) {
                        allerrors = data.responseJSON.errors;
                        if(allerrors.comment) toastr.error(allerrors.comment[0]); 
                    }
                }
            });
        })

        $(document).on('click', '.paginations', function() {
            commentpaginationId = $(this).attr('id');
            $.ajax({
                type: "GET",
                url: window.location.href,
                data: {
                    'page':$(this).data('page'),
                },
                success: function(data) {
                    $('#productcommentpage').html(data);
                }
            });
        });

        $(document).on('click', '#nextpage', function() {
            nextpageId = $(this).attr('id');
            $.ajax({
                type: "GET",
                url:$(this).data('nextpage'),
                data: {
                    'page':$(this).data('page'),
                },
                success: function(data) {
                    $('#productcommentpage').html(data);
                }
            });
        });
        
        $(document).on('click', '#previouspage', function() {
            previouspageId = $(this).attr('id');
            $.ajax({
                type: "GET",
                url:$(this).data('previouspage'),
                data: {
                    'page':$(this).data('page'),
                },
                success: function(data) {
                    $('#productcommentpage').html(data);
                }
            });
        });
    });

    function delay(callback, ms) {
        var timer = 0;
        return function() {
            var context = this,
                args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function() {
                callback.apply(context, args);
            }, ms || 0);
        };
    }
</script>
@endpush