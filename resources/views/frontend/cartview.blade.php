<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('cart view')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="{{route('cartview')}}">{{__('cart view')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__cart_view mt_120 xs_mt_70">
    <div class="container">
        <div class="row">
            <div class="col-xl-8 col-lg-8">
                <div class="wsus__cart_item">
                    <div class="table-responsive">
                        <table class="table" id="prcourse">
                            <thead>
                                <tr>
                                    <th class="categories"><a id="clear" class="clear_cart">{{__('clear cart')}}</a></th>
                                    <th class="price">{{__('Price')}}</th>
                                    <th class="quentity">{{__('Quantity')}}</th>
                                    <th class="total">{{__('Total')}}</th>
                                </tr>
                            </thead>
                            <tbody id="items">
                                @foreach ($courses as $course )
                                @php
                                    $image = explode(',',$course['image']);
                                @endphp
                                <tr>
                                    <td class="categories">
                                        <div class="img">
                                            <img src="{{ asset($image[0]) }}" alt="{{__('package')}}" class="img-fluid w-100">
                                            <a class="deletecourse" id="course_{{$course['id']}}" data-course_id="{{$course['id']}}"><i class="far fa-times"></i></a>
                                        </div>
                                        <div class="text">
                                            <h4>{{$course['name']}}</h4>
                                            <p>{{__('Is Paid')}} : @if ($course['is_paid'] == App\Models\Course::PAID) {{__('Paid')}} @else {{__('Free')}} @endif</p>
                                        </div>
                                    </td>
                                    <td>
                                        <h5 id="price_{{$course['id']}}">&nbsp;@if($course['is_paid'] == App\Models\Course::PAID) {{currencyPosition($course['price'])}} @else {{__('Free')}} @endif</h5>
                                    </td>
                                    <td>
                                        <div class="text-white">
                                            <span class="qtn" id="qtn_{{$course['id']}}" data-course_id="{{ $course['id']}}">1</span>
                                        </div>
                                    </td>
                                    <td class="total">
                                        <h5 id="course_total_{{$course['id']}}">@if($course['is_paid'] == App\Models\Course::PAID) {{currencyPosition($course['price'])}} @else {{__('Free')}} @endif</h5>
                                    </td>
                                </tr>
                                @endforeach
                                @foreach ($products as $product )
                                    @php
                                        $image = explode(',',$product['image']);
                                    @endphp
                                    <tr>
                                        <td class="categories">
                                            <div class="img">
                                                <img src="{{ asset($image[0]) }}" alt="{{__('package')}}" class="img-fluid w-100">
                                                <a class="deleteproduct" id="product_{{$product['id']}}" data-product_id="{{$product['id']}}"><i class="far fa-times"></i></a>
                                            </div>
                                            <div class="text">
                                                <h4>{{$product['name']}}</h4>
                                                @if ($product['category'])
                                                <p>{{__('Category')}}&nbsp;:&nbsp;{{$product['category']}}</p>
                                                @endif
                                            </div>
                                        </td>
                                        <td>
                                            <h5 id="price_{{$product['id']}}">&nbsp;{{currencyPosition($product['price']) }}</h5>
                                        </td>
                                        <td>
                                            <div class="text-white">
                                                <span><button class="btn btn-dark qtn_minus" data-product_id="{{ $product['id']}}">-</button></span>
                                                <span class="qtn" id="product_qtn_{{$product['id']}}" data-product_id="{{ $product['id']}}">{{$product['quantity']}}</span>
                                                <span><button class="btn btn-dark qtn_add" data-product_id="{{ $product['id']}}">+</button></span>
                                            </div>
                                        </td>
                                        <td class="total">
                                            <h5 id="product_total_{{$product['id']}}">&nbsp;{{currencyPosition(intval($product['price']) * intval($product['quantity'])) }}</h5>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
            <div class="col-xl-4 col-lg-4">
                <div class="wsus__sidebar cart_sedebar">
                    <div class="row">
                        <div class="col-xl-12">
                            <h4>{{__('Cart Totals')}}</h4>
                            <div class="sidebar_item">
                                <ul>
                                    <li>{{__('Subtotals')}}&nbsp;:&nbsp;<span id="subtotal"></span></li>
                                    <li class="d-none" id="coupon">{{__('Coupon')}}&nbsp;:&nbsp;<span id="couponprice"></span>
                                    </li>
                                    <li>{{__('Totals')}}&nbsp;:&nbsp;<span id="total"></span></li>
                                </ul>
                                <div class="accordion accordion-flush" id="accordionFlushExample">
                                    <div class="accordion-item">
                                        <h2 class="accordion-header" id="flush-headingOne" data-bs-toggle="collapse"
                                            data-bs-target="#flush-collapseOne" aria-expanded="false"
                                            aria-controls="flush-collapseOne">
                                            <div class="form-check">
                                                <input class="form-check-input" type="checkbox" id="couponcode" value="">
                                                <label class="form-check-label collapsed" for="flexCheckDefault" type="button">{{__('Have Coupone Code')}}</label>
                                            </div>
                                        </h2>
                                        <div id="flush-collapseOne" class="accordion-collapse collapse" aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                            <div class="accordion-body">
                                                <form>
                                                    <input type="text" id="code" name="code" placeholder="{{__('Code')}}">
                                                    <button id="apply_coupon"
                                                        class="common_btn">{{__('apply')}}</button>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                    <span id="couponerror" class="error text-danger"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xl-12 shipping_calculate">
                            <h4>{{__('Calculate Shipping')}}</h4>
                            <div class="sidebar_item mb-0">
                                @if($products)
                                    @foreach ($shippings as $shipping)
                                    <div class="form-check">
                                        <input class="form-check-input shippingadd" type="radio" name="shippings"
                                            id="shipping_{{$shipping->id}}" value="{{$shipping->id}}" @if($shipping->id == $shippings->first()->id) checked @endif>
                                        <label class="form-check-label" for="shipping_{{$shipping->id}}">
                                            {{$shipping->title}} ({{currencyPosition($shipping->cost)}})
                                            <span>({{$shipping->short_description}})</span>
                                        </label>
                                    </div>
                                    @endforeach
                                @endif
                                <div id="proceedbtn">

                                </div>
                                <div id="loaderbtn" class="d-none" >
                                    <button class="common_btn"><i class="fa fa-circle-o-notch fa-spin" id="showspin"></i></button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@push('scripts')
<script>
    function currencyPosition(amount) {
        let currency = @json($currency);
        return (currency.site_currency_direction == currency.is_left) ? currency.site_currency_icon + amount.toFixed(2) : amount.toFixed(2) + currency.site_currency_icon;
    }

    $(function() {
        $('.common_btn').css('cursor','pointer');
        let products = @json($products);
        
        let courses = @json($courses);
        let shippings = @json($shippings);

        let is_paid = @json($is_paid);
        let producttotal = 0;
        let coursetotal = 0;
        let shipping = shippings[0];
        let coupon = null;
        let grand_total = 0;
        
        products.map(product => {
            product.price = parseInt(product.price);
            product.quantity = parseInt(product.quantity);
            product.stock = parseInt(product.stock) - product.quantity;
            producttotal = producttotal + (product.price * product.quantity);
        });

        courses.map(course=> {
            if(is_paid['free'] != course.is_paid) coursetotal = coursetotal + parseInt(course.price);
        });
        grand_total = producttotal+coursetotal;
        console.log('grand_total :>> ', grand_total);
        (grand_total>0) ? $('#subtotal').text(currencyPosition(parseInt(grand_total))) : $('#subtotal').text('Free');
        (grand_total>0) ? $('#total').text(currencyPosition(parseInt(grand_total))) : $('#total').text('Free');
        if(grand_total<1) $('#accordionFlushExample').addClass('d-none');

        if(grand_total > 1) {
            $('#proceedbtn').append('<a id="proceedcheckout" class="common_btn">{{__("Procced To Cheackout")}}</a>');
            $('.common_btn').css('cursor','pointer');
        }
        else {
            $('#proceedbtn').append('<a id="purchesfree" class="common_btn">{{__("Purchase Item")}}</a>');
            $('.common_btn').css('cursor','pointer');
        } 

        $(document).on('click', '#purchesfree', function() {
            let user = @json(auth()->user());
            if(!user) window.location.assign('{{route("signin")}}')
            else {
                $.ajax({
                    type: "POST",
                    url: "{{route('freepurches')}}", 
                    beforeSend: function() {
                        // Show image container
                        $('#proceedbtn').addClass('d-none');
                        $('#loaderbtn').removeClass('d-none');
                    },
                    data: {
                        "_token": "{{ csrf_token() }}",
                        "coupon": coupon,
                        "shipping": shipping,
                        "courses" : courses,
                        "products" : products
                    },
                    dataType: "JSON",
                    success: function (response) {
                        toastr.success(response.msg)
                        window.location.assign('{{route("user.dashboard")}}')
                    }
                });
            }
        })

        $(document).on('click', '#proceedcheckout', function() {
            $.ajax({
                type: "POST",
                url: "{{route('proceedcheckout')}}",
                data: {
                    "_token": "{{ csrf_token() }}",
                    "coupon": coupon,
                    "shipping": shipping,
                    "courses" : courses,
                    "products" : products
                },
                dataType: "JSON",
                success: function (response) {
                    (response.products) ? window.location.assign('{{route("checkout")}}') : window.location.assign('{{route("payment")}}')
                }
            });
        });

        $('.qtn_add').on('click', function() { 
            const product_id = $(this).data('product_id'); // add quantity
            let total = 0;
            products.map(product => {
                if(product.id == product_id && product.stock > 0) {
                    product.quantity = product.quantity + 1;
                    product.stock = product.stock - 1;
                    $('#product_qtn_'+product_id).text(product.quantity);
                    $('#product_total_'+product_id).text(currencyPosition(product.price * product.quantity));
                }
                total = total + (product.price * product.quantity);
            });
            producttotal = total;
            total = total + coursetotal;
            $('#subtotal').text(currencyPosition(total));
            total =total + parseInt(shipping.cost);
            if (coupon) total = (coupon.coupon.type == coupon.inprice) ? (total-parseInt(coupon.coupon.amount)) : (total - ((total/100) * parseInt(coupon.coupon.amount)));
            $('#total').text(currencyPosition(total));
        });

        $('.qtn_minus').on('click', function() {
            const product_id = $(this).data('product_id'); //substract quantity
            let total = 0;
            products.map(product => {
                if((product.id == product_id) && (product.quantity >1)) {
                    product.quantity = product.quantity - 1;
                    product.stock = product.stock + 1;
                    $('#product_qtn_'+product_id).text(product.quantity);
                    $('#product_total_'+product_id).text(currencyPosition(product.price * product.quantity));
                }
                price= product.price * product.quantity;
                total = total + price;
            });
            producttotal = total;
            total = total + coursetotal;
            $('#subtotal').text(currencyPosition(total));
            total =total + parseInt(shipping.cost);
            if (coupon) total = (coupon.coupon.type == coupon.inprice) ? (total-parseInt(coupon.coupon.amount)) : (total - ((total/100) * parseInt(coupon.coupon.amount)));
            $('#total').text(currencyPosition(total));
        });

        $('.deleteproduct').on('click', function() {
            let thisproduct = $(this).data('product_id');
            let total = 0;
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "{{ route('removecart') }}",
                data: {
                    'product_id': $(this).data('product_id'),
                    'type': 'product'
                },
                success:function(data) {
                    (data.total < 1) ? $('#iscartitemexists').addClass('d-none') : $('#cartitemtotal').text((data.total));
                }
            });

            products = products.filter(product => product.id != $(this).data('product_id'));

            products.map(product=> total = total + (product.price * product.quantity));
            
            $(this).closest("tr").remove(); //table row remove
            producttotal = total;
            total = producttotal + coursetotal;
            $('#subtotal').text(currencyPosition(total));
            total = total + parseInt(shipping.cost);
            if (coupon) total = (coupon.coupon.type == coupon.inprice) ? (total-parseInt(coupon.coupon.amount)) : (total - ((total/100) * parseInt(coupon.coupon.amount)));
            $('#total').text(currencyPosition(total));
        });

        $('.deletecourse').on('click', function () {
            let total = 0;
            courses = courses.filter(course => course.id != $(this).data('course_id') && is_paid['free'] != course.is_paid);
            courses.map(course=> total = total + course.price);

            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "{{ route('removecart') }}",
                data: {
                    'course_id': $(this).data('course_id'),
                    'type': 'course'
                },
                success:function(data) {
                    (data.total < 1) ? $('#iscartitemexists').addClass('d-none') : $('#cartitemtotal').text((data.total));
                }
            });

            $(this).closest("tr").remove(); //table row remove
            coursetotal = total;
            total = coursetotal + producttotal;
            $('#subtotal').text(currencyPosition(total));
            total =total + parseInt(shipping.cost);
            if (coupon) total = (coupon.coupon.type == coupon.inprice) ? (total-parseInt(coupon.coupon.amount)) : (total - ((total/100) * parseInt(coupon.coupon.amount)));
            $('#total').text(currencyPosition(total));
        });
            
        $('.clear_cart').on('click', function () {
            $('#items').remove();
            $('#total').text((0));
            $('#subtotal').text((0));
            $.ajax({
                type: 'GET',
                dataType: 'json',
                url: "{{ route('removecart') }}",
                success:function(data) {
                    $('#iscartitemexists').addClass('d-none');
                }
            });
        });

        $('.shippingadd').on('click', function () {
            let total= 0;
            shipping = shippings.find(shipping => shipping.id == $(this).val());
            total = producttotal + coursetotal + parseInt(shipping.cost);
            if (coupon) total = (coupon.coupon.type == coupon.inprice) ? (total-parseInt(coupon.coupon.amount)) : (total - ((total/100) * parseInt(coupon.coupon.amount)));
            $('#total').text(currencyPosition(total));
        });

        $('#apply_coupon').on('click', function (e) {
            e.preventDefault();
            let formData = { 'code' : $('#code').val()};
            $.ajax({
                    type: 'GET',
                    dataType: 'json',
                    data: formData,
                    url: "{{ route('couponapply') }}",
                    success:function(data){
                        if(data.error) toastr.error(data.error);
                        else {
                            coupon = data;
                            const total = coursetotal + producttotal + parseInt(shipping.cost);
                            $('#coupon').removeClass("d-none");
                            $('#code').val('');
                            toastr.success('{{__("Succesfully Found")}}');
                            (coupon.coupon.type == coupon.inprice) ? $('#couponprice').text(currencyPosition(coupon.coupon.amount)) : $('#couponprice').text((coupon.coupon.amount+'%'));
                            const totalWithCoupon = (coupon.coupon.type == coupon.inprice) ? (total-parseInt(coupon.coupon.amount)) : (total - ((total/100) * parseInt(coupon.coupon.amount)));
                            $('#total').text(currencyPosition(totalWithCoupon));
                        }
                    }
                });
        });
    });
</script>
@endpush