<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta name="viewport"
    content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <meta name="csrf-token" content="{{csrf_token()}}" />
    <title>{{ __('GymSuit') }}</title>
    @method('meta')
    @include('frontend.layouts.styles')
    @stack('style')
</head>
<body class="home_2">

    @yield('content')
    
    @include('frontend.layouts.scripts')
    @stack('scripts')
</body>
</html>
