<footer class="footer_2 mt_130 xs_mt_80 pt_130 xs_pt_80">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-md-8 col-lg-3 col-xl-3">
                <div class="wsus__footer_content">
                    <a href="{{ route('home') }}" class="footer_logo">
                        <img src="{{ url(GetSetting('site_logo')) }}" alt="{{__('logo')}}" class="img-fluid">
                    </a>
                    @foreach (@App\Models\FooterContactItem::get() as $item)
                        <p><i class="{{$item->icon}}"></i> {{$item->content}}</p>                       
                    @endforeach
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <div class="wsus__footer_content">
                    <h3>{{ __('company') }}</h3>
                    <ul>
                        <li><a href="{{ route('about') }}">{{__('About us')}}</a></li>
                        <li><a href="{{ route('termcondition') }}">{{__('Terms')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <div class="wsus__footer_content">
                    <h3>{{ __('Information') }}</h3>
                    <ul>
                        <li><a href="{{ route('faq') }}">{{__('FAQ')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <div class="wsus__footer_content">
                    <h3>{{ __('services') }}</h3>
                    <ul>
                        <li><a href="{{ route('blog.index') }}">{{__('Blog')}}</a></li>
                        <li><a href="{{ route('termcondition') }}">{{__('Terms')}}</a></li>
                        <li><a href="{{ route('privacy') }}">{{__('Privacy Policy')}}</a></li>
                        <li><a href="{{ route('contact') }}">{{__('Contact')}}</a></li>
                    </ul>
                </div>
            </div>
            <div class="col-sm-6 col-md-4 col-lg-2 col-xl-2">
                <div class="wsus__footer_content">
                    <h3>{{__('Serious stuff')}}</h3>
                    <ul>
                        <li><a href="{{ route('privacy') }}">{{__('Privacy')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
        <div class="row justify-content-center mt_100 xs_mt_10 mb_100 xs_mb_15 margin_md">
            <div class="col-xl-8 col-lg-8">
                <div class="wsus__footer_content">
                    <form>
                        <input type="email" id="mail" placeholder="{{__('Enter your Email for Subscription.')}}">
                        <button class="common_btn" id="subscriptionmail">{{__('send')}} <i class="fa fa-circle-o-notch fa-spin d-none" id="showspin"></i></button>
                    </form>
                    <ul class="footer_link d-flex flex-wrap justify-content-center">
                        @php
                            $links = getSocialLinks('footer');
                        @endphp
                        @foreach ($links as $item )
                            <li><a href="https://{{$item->link}}"><i class="{{$item->icon}}"></i></a></li>
                        @endforeach
                    </ul>
                </div>
            </div>
        </div>
    </div>
</footer>

@push('scripts')
<script>
    $(function() {
            $('#subscriptionmail').on('click', function (e) {
                e.preventDefault();  
                jQuery.ajax({
                     type: 'GET',
                     url: '{{route("subscribe")}}',
                     data: {
                        'email': $('#mail').val(),
                     },
                     beforeSend: function(){
                        // Show image container
                        $('#showspin').removeClass('d-none');
                    },
                     success: function(data) {
                         toastr[data.type](data.message);
                         $('#mail').val('');
                        if(data.type == 'success') {
                            $('#iscartitemexists').removeClass('d-none');
                            $('#cartitemtotal').text(data.total);
                        }
                     },
                     error: function (data) {  
                        toastr['error'](data.responseJSON.errors.email[0])
                     },
                     complete: function() {
                        $('#showspin').addClass('d-none');
                    },
                     
                 });
             });
            
        });
</script>
@endpush