<nav class="navbar navbar-expand-lg main_menu">
    <div class="container border_area">
        <a class="navbar-brand" href="{{ route('home') }}">
            <img src="{{asset(GetSetting('site_logo'))}}" alt="{{__('GymSuit')}}" class="img-fluid">
        </a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse"
            data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
            aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav ms-auto ">
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('home') ? 'active' : '' }}" aria-current="page" href="{{ route('home') }}">{{__('Home')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('services') ? 'active' : '' }}" href="{{ route('services') }}" >{{__('services')}}</a>
                </li> 
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('product.index') ? 'active' : '' }}" href="{{route('product.index')}}">{{__('product')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('about') ? 'active' : '' }}" href="{{ route('about') }}">{{__('about us')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('blog.index') ? 'active' : '' }}" href="{{ route('blog.index') }}">{{__('blog')}}</a>
                </li>
                <li class="nav-item">
                    <a class="nav-link">{{__('pages')}} <i class="far fa-angle-down"></i></a>
                    <ul class="dropdown">
                        <li><a class="{{ request()->routeIs('course.index') ? 'active' : '' }}" href="{{ route('course.index') }}">{{__('courses')}}</a></li>
                        <li><a class="{{ request()->routeIs('faq') ? 'active' : '' }}" href="{{ route('faq') }}">{{__('FAQ')}}</a></li>
                        <li><a class="{{ request()->routeIs('privacy') ? 'active' : '' }}" href="{{ route('privacy') }}">{{__('privacy Policy')}}</a></li>
                        <li><a class="{{ request()->routeIs('termcondition') ? 'active' : '' }}" href="{{ route('termcondition') }}">{{__('terms and condition')}}</a></li>
                    </ul>
                </li>
                <li class="nav-item">
                    <a class="nav-link {{ request()->routeIs('contact') ? 'active' : '' }}" href="{{ route('contact') }}">{{__('contact us')}}</a>
                </li>
                
                <li class="nav-item @if(!Session::has('products') && !Session::has('courses')) d-none @endif" id="iscartitemexists">
                    @php 
                        $totalproduct = Session::get('products')  == null ? 0 : Session::get('products')->count();
                        $totalcourse = Session::get('courses') == null ? 0 : Session::get('courses')->count();
                    @endphp
                    <a class="nav-link" href="{{route('cartview')}}"><i class="fal fa-cart-plus" aria-hidden="true"></i> <span id="cartitemtotal">{{ $totalproduct + $totalcourse }}</span></a>
                </li>
            </ul>
        </div>
    </div>
</nav>