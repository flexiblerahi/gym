 <!-- add for another template -->
 <link href="{{asset('assets/frontend/css/all.min.css')}}" rel="stylesheet" type="text/css">
 <link href="{{ asset('assets/frontend/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/frontend/css/spacing.css')}}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/frontend/css/venobox.min.css')}}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/frontend/css/slick.css')}}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/frontend/css/circlos.css')}}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/frontend/css/colorfulTab.css')}}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/frontend/css/select2.min.css')}}" rel="stylesheet" type="text/css">
 {{-- <link href="{{asset('assets/frontend/css/jquery.nice-number.min.css')}}" rel="stylesheet" type="text/css"> --}}
 <link href="{{asset('assets/frontend/css/jquery.exzoom.css')}}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/frontend/css/style.css')}}" rel="stylesheet" type="text/css">
 
 @if(GetSetting('site_direction') != 'LTR')
    <link href="{{asset('assets/frontend/css/rtl.css')}}" rel="stylesheet" type="text/css">
 @endif
 <link href="{{asset('assets/frontend/css/responsive.css')}}" rel="stylesheet" type="text/css">
 <link href="{{asset('assets/toastr/toastr.min.css')}}" rel="stylesheet" type="text/css">