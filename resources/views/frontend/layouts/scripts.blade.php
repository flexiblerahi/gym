
<!-- external javascripts -->
<script src="{{ asset('assets/frontend/js/jquery-3.6.0.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/bootstrap.bundle.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/Font-Awesome.js')}}"></script>
<script src="{{ asset('assets/frontend/js/venobox.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/jquery.waypoints.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/jquery.countup.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/slick.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/circlos.js')}}"></script>
<script src="{{ asset('assets/frontend/js/colorfulTab.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/jquery.exzoom.js')}}"></script>
<script src="{{ asset('assets/frontend/js/select2.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/sticky_sidebar.js') }}"></script>
<script src="{{ asset('assets/admin/js/sweetalert.js')}}"></script>
<script src="{{ asset('assets/toastr/toastr.min.js')}}"></script>
<script src="{{ asset('assets/frontend/js/main.js')}}"></script>

@include('sweetalert::alert')
