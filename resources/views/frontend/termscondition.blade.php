
<section class="wsus__breadcrumb" style="background: url({{GetSetting('breadcumb')}});">
    <div class="wsus__breadcrumb_overlay">
        <div class="container">
            <div class="row">
                <div class="col-12">
                    <h1>{{__('terms and condition')}}</h1>
                    <ul class="d-flex flex-wrap">
                        <li><a href="{{route('home')}}">{{__('home')}}</a></li>
                        <li><a href="">{{__('terms and condition')}}</a></li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="wsus__terms_condition mt_120 xs_mt_70">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="wsus__terms_con_text">
                    <h2>{{__('Terms & Condition')}}</h2>
                    <div id="terms">
                        @if ($termcondition)
                            {!!html_entity_decode($termcondition->content)!!}
                        @endif
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>