@php
$page_title = trans('frontend.Verify Email');
@endphp
@extends('frontend.layouts.master')
@push('style')
    <style>
        .mt-6 {
            margin-top: 4rem !important;
        }
    </style>
@endpush
@section('content')
    @include('cookieConsent::index')
    <div id="wrapper" class="clearfix">
        <!-- Header -->
        {{-- @include(getHeader()) --}}
        @include('frontend.layouts.menus.menuStyleOne')
        <!-- Start main-content -->
        <div class="main-content-area">
            <div class="container">
                <div class="row d-flex justify-content-center">
                    <div class="col-md-8 mt-5">
                       <div class="card mt-6 h-100 text-center">
                         <div class="card-body  p-5 shadow-sm">
                            @if (session('resent'))
                                <div class="alert alert-success" role="alert">
                                    {{trans('frontend.A fresh verification link has been sent to your email address.')}}
                                </div>
                            @endif
                        <p style="color: black">{{__('frontend.Before proceeding, please check your email for a verification link. If you did not receive the email,')}}</p>
                        <form action="{{route('verification.resend')}}" method="POST" class="d-inline">
                            @csrf
                            <button type="submit" class="d-inline btn btn-link p-0">
                               {{trans('frontend.click here to request another')}}
                            </button>
                        </form>
                         </div>
                       </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- end main-content -->
        @include('frontend.layouts.footers.footerStyleOne')
        <a class="scrollToTop" ><i class="fa fa-angle-up"></i></a>
    </div>
    <!-- end wrapper -->
@endsection
