
    <!DOCTYPE html>
    <head>
        <!-- Meta Tags -->
        <meta name="viewport" content="width=device-width,initial-scale=1.0" />
        <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
        <!-- Page Title -->
        <title>@yield('title')</title>
        @include('frontend.layouts.styles')
    </head>
    <body>
        
        <section class="wsus__404">
            <div class="container">
                <div class="row">
                    <div class="col-xl-6 m-auto">
                        <div class="wsus__404_text">
                            <h2>@yield('code')</h2>
                            <h4>{{__('Oops! Look likes something going wrong')}}</h4>
                            <p>@yield('message')</p>
                            <a href="{{url('/')}}" class="common_btn">{{__('go to home')}}</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </body>
    </html>








