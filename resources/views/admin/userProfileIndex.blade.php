@php
$page_title = trans('Admin | Profile');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
        .input-group-text,
        select.form-control:not([size]):not([multiple]),
        .form-control:not(.form-control-sm):not(.form-control-lg) {
            font-size: 14px;
            padding: 10px 15px;
            height: 48px;
        }

    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Profile')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left"></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card">
                            <div class="card-body">
                                <form action="{{route('admin.user-profile.store')}}" method="POST"
                                    enctype="multipart/form-data">
                                    @csrf
                                    @method('POST')
                                    @include('admin.layouts.mediaInput', [
                                    'inputTitle' => trans('Avatar'),
                                    'inputName' => 'avatar',
                                    'inputData' => $user->avatar,
                                    'multiInput' => false,
                                    'buttonText' => trans('Select'),
                                    ])
                                    <div class="form-group">
                                        <label for="">{{__('admin.Name')}}</label>
                                        <input type="text" class="form-control" name="name"
                                            value="{{@$user->name}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">{{__('admin.Email')}}</label>
                                        <input type="email" class="form-control" name="email"
                                            value="{{@$user->email}}" required>
                                    </div>
                                    <div class="form-group">
                                        <label for="">{{__('admin.Password')}}</label>
                                        <input type="password" class="form-control" name="password"
                                            value="">
                                    </div>
                                    <div class="form-group">
                                        <label for="">{{__('admin.Confirm Password')}}</label>
                                        <input type="password" class="form-control" name="confirm_password"
                                            value="">
                                    </div>
                                    <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('admin.Save')}} </button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @push('script')
        @include('admin.layouts.mediaJs')
    @endpush
    @include('admin.layouts.mediaModal')
@endsection
