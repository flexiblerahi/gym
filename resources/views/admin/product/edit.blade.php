@php
$page_title = trans('Admin | Edit product');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
    <!-- Include stylesheet -->
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Edit Product')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.product.update', $product->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="name">{{__('admin.Product Name')}}</label>
                                <input type="text" class="form-control" name="name" id="name"
                                    value="{{$product->name}}">
                            </div>
                            <div class="form-group">
                                <label for="slug">{{__('admin.Slug')}}</label>
                                <input type="text" class="form-control" name="slug" id="slug"
                                    value="{{$product->slug}}">
                            </div>

                            <div class="form-group">
                                <label for="category_id">{{__('admin.Categories')}}</label>
                                <select name="category_id" id="category_id" class="form-control select2">
                                    @foreach ($categories as $category)
                                        <option value={{$category->id}} @if($category->id == $product->category_id)
                                            selected
                                        @endif>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @include('admin.layouts.mediaInput', [
                                'inputTitle' => trans('Image'),
                                'inputName' => 'media',
                                'inputData' => $product->image,
                                'multiInput' => true,
                                'buttonText' => trans('Select Images'),
                            ])
                            <div class="form-group">
                                <label for="">{{__('admin.Full Description')}}</label>
                                <textarea class="form-control h-100" name="description" id="editor"
                                    rows="8">{!! clean(html_entity_decode($product->description)) !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('Price')}}</label>
                                <input type="number" class="form-control" name="price" value="{{$product->price}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Discount Price (Optional)')}}</label>
                                <input type="number" class="form-control" name="discount_price"
                                    value="{{$product->discount_price}}">
                            </div>
                            <div class="form-group">
                                <label for="brand">{{__('admin.Brand (Optional)')}}</label>
                                <input type="text" class="form-control" name="brand" id="brand" value="{{$product->brand}}">
                            </div>
                            <div class="form-group">
                                <label for="stock">{{__('admin.Stock (Optional)')}}</label>
                                <input type="number" class="form-control" name="stock" id="stock" value="{{$product->stock}}">
                            </div>
                            <div class="form-group">
                                <label for="sku">{{__('admin.SKU')}}</label>
                                <input type="text" class="form-control" name="sku" id="sku" value="{{$product->sku}}">
                            </div>
                            <div class="form-group">
                                <div class="control-label">{{__('admin.Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="status" class="custom-switch-input" @if ($product->status == App\Models\Product::ACTIVE)
                                    checked
                                    @endif>
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('admin.OFF')}} / {{trans('admin.ON')}}</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" href="#" role="button"> {{trans('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    @include('admin.layouts.mediaJs')
    <script>
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) return false;
        });
    </script>
@endpush
