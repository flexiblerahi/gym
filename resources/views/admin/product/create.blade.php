@php
$page_title = trans('Admin | Add product');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
    <!-- Include stylesheet -->
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Add Product')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.product.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="name">{{__('admin.Product Name')}}</label>
                                <input type="text" class="form-control" name="name" id="name">
                            </div>
                            <div class="form-group">
                                <label for="slug">{{__('admin.Slug')}}</label>
                                <input type="text" class="form-control" name="slug" id="slug">
                            </div>
                            <div class="form-group">
                                <label for="category_id">{{__('admin.Categories')}}</label>
                                <select name="category_id" id="category_id" class="form-control select2">
                                    @foreach ($categories as $category)
                                        <option value={{$category->id}}>{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @include('admin.layouts.mediaInput', [
                                'inputTitle' => trans('Image'),
                                'inputName' => 'media',
                                'inputData' => null,
                                'multiInput' => true,
                                'buttonText' => trans('Select Images'),
                            ])
                            <div class="form-group">
                                <label for="">{{__('Description')}}</label>
                                <textarea class="form-control h-100" name="description" id="editor"
                                    rows="8"></textarea>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Price')}}</label>
                                <input type="number" class="form-control" name="price">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Discount Price (Optional)')}} </label>
                                <input type="number" class="form-control" name="discount_price">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Brand (Optional)')}}</label>
                                <input type="text" class="form-control" name="brand">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Stock (Optional)')}}</label>
                                <input type="number" class="form-control" name="stock">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.SKU')}}</label>
                                <input type="text" class="form-control" name="sku">
                            </div>
                            <div class="form-group">
                                <div class="control-label">{{__('admin.Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="status" checked class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('admin.OFF')}} / {{trans('admin.ON')}}</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block"  role="button"> {{trans('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    <script>
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) return false;
        });
    </script>
    @include('admin.layouts.mediaJs')
@endpush
