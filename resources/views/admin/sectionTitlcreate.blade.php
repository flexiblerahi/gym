@php
$page_title = trans('Admin | Section Title Subtitle');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('Section Tilte Subtitle')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i class="fas fa-arrow-circle-left"></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        @can('sectiontitle-update')
                        <form action="{{route('admin.sectitontitle.update', 1)}}" method="POST">
                            @csrf
                            @method('PUT')
                        @endcan
                            <div class="form-group">
                                <label for="">{{__('Service Title')}}</label>
                                <input type="text" class="form-control" name="servicetitle" value="{{($sectiontitle)?$sectiontitle->servicetitle : ''}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('Service Sub Title')}}</label>
                                <textarea type="text" rows="5" class="form-control h-100" name="servicesubtitle">{{($sectiontitle)?$sectiontitle->servicesubtitle : ''}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('Training Title')}}</label>
                                <input type="text" class="form-control" name="trainingtitle" value="{{($sectiontitle)?$sectiontitle->trainingtitle : ''}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('Training Sub Title')}}</label>
                                <textarea type="text" rows="5" class="form-control h-100" name="trainingsubtitle">{{($sectiontitle)?$sectiontitle->trainingsubtitle : ''}}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('Blog Title')}}</label>
                                <input type="text" class="form-control" name="blogtitle" value="{{($sectiontitle)?$sectiontitle->blogtitle : ''}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('Blog Sub Title')}}</label>
                                <textarea type="text" rows="5" class="form-control h-100" name="blogsubtitle">{{($sectiontitle)?$sectiontitle->blogsubtitle : ''}}</textarea>
                            </div>
                            @can('sectiontitle-update')
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('admin.Save')}} </button>
                        </form>
                        @endcan
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection

