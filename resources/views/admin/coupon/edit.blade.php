@php
$page_title = trans('Admin | Edit Coupon');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
@endpush
@extends('admin.layouts.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('Edit coupon')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left"></i> {{__('Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.coupon.update',$coupon->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">{{__('Name')}}</label>
                                        <input type="text" class="form-control" name="name" id="name" value="{{$coupon->name}}" required>
                                    </div>  
                                </div>
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">{{__('Amount')}}</label>
                                        <input type="text" class="form-control" name="amount" id="amount" value="{{$coupon->amount}}" required>
                                    </div>  
                                </div>
                            </div> 
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="">{{__('Code')}}</label>
                                        <input type="text" class="form-control" name="code" value="{{$coupon->code}}">
                                    </div>
                                </div>
                                <div class="col">
                                    <div class="mb-3">
                                      <label for="type" class="form-label">{{__('Type')}}</label>
                                      <select class="form-control" name="type" id="type">
                                        <option value="{{App\Models\Coupon::INPRICE}}" @if ($coupon->type ==  App\Models\Coupon::INPRICE) selected @endif>{{__('In Price')}}</option>
                                        <option value="{{App\Models\Coupon::INPERCENTAGE}}" @if ($coupon->type ==  App\Models\Coupon::INPERCENTAGE) selected @endif> {{__('In Percentage')}}</option>
                                      </select>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <div class="control-label">{{__('Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="status" class="custom-switch-input" @if ($coupon->status == $coupon->active) checked @endif>
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('OFF')}} / {{__('ON')}}</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{__('Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    <script>
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) return false;
        });
    </script>
@endpush
