<li class="{{request()->routeIs('admin.dashboard') ? 'active': ''}}">
    <a class="nav-link" href="{{ route('admin.dashboard') }}">
        <i class="fas fa-fire"></i>
        <span>{{__('admin.Dashboard')}}</span>
    </a>
</li>
@canany(['blog-index', 'blog-create', 'blog-category-index', 'blog-comment-index'])
    <li class="nav-item dropdown {{request()->is('admin/blog*') ? 'active': ''}}">
        <a href="#" class="nav-link has-dropdown">
            <i class="fas fa-edit"></i><span>{{__('admin.Blog')}}</span>
        </a>
        <ul class="dropdown-menu">
            @can('blog-category-index')
                <li class="{{(request()->routeIs('admin.blog-category.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.blog-category.index') }}">{{__('admin.Categories')}}</a>
                </li>
            @endcan
            @can('blog-index')
                <li class="{{(request()->routeIs('admin.blog.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.blog.index') }}">{{__('admin.Blogs')}}</a>
                </li>
            @endcan
            @can('blog-comment-index')
                <li class="{{(request()->routeIs('admin.blog-comment.index')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.blog-comment.index') }}">{{__('admin.Comments')}}</a>
                </li>
            @endcan
        </ul>
    </li>
@endcanany 
@canany(['course-index', 'course-create', 'course-edit', 'course-delete'])
    <li class="nav-item dropdown {{request()->is('admin/course*') ? 'active': ''}}">
        <a href="#" class="nav-link has-dropdown">
            <i class="fas fa-edit"></i><span>{{__('Courses')}}</span>
        </a>
        <ul class="dropdown-menu">
            @can('course-category-index')
                <li class="{{(request()->routeIs('admin.coursecategory.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.coursecategory.index') }}">{{__('Category List')}}</a>
                </li>
            @endcan
            @can('course-index')
                <li class="{{(request()->routeIs('admin.course.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.course.index') }}">{{__('List')}}</a>
                </li>
            @endcan
        </ul>
    </li>
@endcanany
@canany(['about-index', 'faq-index', 'terms-of-use-index', 'privacy-policy-index'])
    <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown"> 
            <i class="fas fa-copy"></i><span>{{__('admin.Pages')}}</span>
        </a>
        <ul class="dropdown-menu">
            @can('about-index')
                <li class="{{(request()->routeIs('admin.about.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.about.index') }}">{{__('admin.About')}}</a>
                </li>
            @endcan
            @can('faq-index')
                <li class="{{(request()->routeIs('admin.faq.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.faq.index') }}">{{__('admin.Faq')}}</a>
                </li>
            @endcan
            @can('terms-of-use-index')
                <li class="{{(request()->routeIs('admin.terms-of-use.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.terms-of-use.index') }}">{{__('admin.Terms of use')}}</a>
                </li>
            @endcan
            @can('privacy-policy-index')
                <li class="{{(request()->routeIs('admin.privacy-policy.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.privacy-policy.index') }}">{{__('admin.Privacy policy')}}</a>
                </li>
            @endcan
        </ul>
    </li>
@endcanany
<li class="nav-item dropdown ">
    <a href="#" class="nav-link has-dropdown"> 
        <i class="fas fa-puzzle-piece"></i><span>{{__('Home Sections')}}</span>
    </a>
    <ul class="dropdown-menu">
        <li class="{{(request()->routeIs('admin.trainer.*')) ? 'active': ''}}">
            <a href="{{ route('admin.trainer.index') }}" class="nav-link">
                {{__('Trainer')}}
            </a>
        </li>
        @can('ourprocess-index')
            <li class="{{(request()->routeIs('admin.ourprocess.*')) ? 'active': ''}}">
                <a href="{{ route('admin.ourprocess.index') }}" class="nav-link">
                    {{__('Our Process')}}
                </a>
            </li>
        @endcan
        @can('sectiontitle-index')
            <li class="{{(request()->routeIs('admin.sectitontitle.*')) ? 'active': ''}}">
                <a href="{{ route('admin.sectitontitle.index') }}" class="nav-link">
                    {{__('Others')}}
                </a>
            </li>
        @endcan
    </ul>
</li>
@canany(['product-index', 'product-category-index', 'order-index', 'product-review-index', 'shop-discount-index', 'transaction-index',
                'shipping-country-index', 'shop-settings-index'])
    <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown">
            <i class="fas fa-store"></i><span>{{__('admin.Shop')}}</span>
        </a>
        <ul class="dropdown-menu">
            @can('product-index')
                <li class="{{(request()->routeIs('admin.product.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.product.index') }}">{{__('admin.Products')}}</a>
                </li>
            @endcan
            @can('product-category-index')
                <li class="{{(request()->routeIs('admin.product-category.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.product-category.index') }}">{{__('admin.Categories')}}</a>
                </li>
            @endcan
            @can('order-index')
                <li class="{{(request()->routeIs('admin.order.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.order.index') }}">{{__('admin.Orders')}}</a>
                </li>
            @endcan
            @can('product-review-index')
                <li class="{{(request()->routeIs('admin.product-review.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.product-review.index') }}">{{__('admin.Reviews')}}</a>
                </li>
            @endcan
            @can('transaction-index')
                <li class="{{(request()->routeIs('admin.transaction.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.transaction.index') }}">{{__('admin.Transactions')}}</a>
                </li>
            @endcan
            @can('shipping-country-index')
                <li class="{{(request()->routeIs('admin.shipping-country.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.shipping-country.index') }}">{{__('admin.Shipping country')}}</a>
                </li>
            @endcan
            <li class="{{(request()->routeIs('admin.shipping.*')) ? 'active': ''}}">
                <a class="nav-link" href="{{ route('admin.shipping.index') }}">{{__('Shipping Cost')}}</a>
            </li>
            <li class="{{(request()->routeIs('admin.coupon.*')) ? 'active': ''}}">
                <a class="nav-link" href="{{ route('admin.coupon.index') }}">{{__('Coupon')}}</a>
            </li>
            @can('shop-settings-index')
                <li class="{{(request()->routeIs('admin.shop-setting.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.shop-setting.index') }}">{{__('admin.Shop setting')}}</a>
                </li>
            @endcan
        </ul>
    </li>
@endcanany
@can('contact-message-index')
    <li class="{{(request()->routeIs('admin.contact-messages.*')) ? 'active': ''}}">
        <a class="nav-link" href="{{ route('admin.contact-messages.index') }}"><i class="fas fa-comment"></i>
            <span>{{__('admin.Contact Messages')}}</span>
        </a>
    </li>
@endcan