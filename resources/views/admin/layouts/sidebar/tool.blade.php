@canany(['settings-index', 'role-index', 'user-index','media-manager-index','subscriber-index', 'send-email-index'])
    @can('settings-index')
        <li class="{{(request()->routeIs('admin.settings')) ? 'active': ''}}">
            <a class="nav-link" href="{{ route('admin.settings') }}"><i class="fas fa-cog"></i>
                <span>{{__('admin.Settings')}}</span>
            </a>
        </li>
    @endcan
    @can('role-index')
        <li class="{{(request()->routeIs('admin.roles.*')) ? 'active': ''}}">
            <a class="nav-link" href="{{ route('admin.roles.index') }}"><i class="fas fa-key"></i>
                <span>{{__('admin.Roles')}}</span>
            </a>
        </li>
    @endcan
    @can('user-index')
        <li class="{{(request()->routeIs('admin.users.*')) ? 'active': ''}}">
            <a class="nav-link" href="{{ route('admin.users.index') }}"><i class="fas fa-user"></i>
                <span>{{__('admin.Users')}}</span>
            </a>
        </li>
    @endcan
    @can('media-manager-index')
        <li class="{{(request()->routeIs('admin.showmanager')) ? 'active': ''}}">
            <a class="nav-link" href="{{ route('admin.showmanager') }}"><i class="fas fa-images"></i>
                <span>{{ trans('admin.Media manager') }}</span>
            </a>
        </li>
    @endcan
    @can('subscriber-index')
        <li class="{{(request()->routeIs('admin.subscriber.*')) ? 'active': ''}}">
            <a class="nav-link" href="{{ route('admin.subscriber.index') }}"><i class="fas fa-mail-bulk"></i>
                <span>{{__('admin.Email subscribers')}}</span>
            </a>
        </li>
    @endcan
    @can('send-email-index')
        <li class="{{(request()->routeIs('admin.send-email.*')) ? 'active': ''}}">
            <a class="nav-link" href="{{ route('admin.send-email.index') }}"><i class="fas fa-envelope"></i>
                <span>{{__('admin.Send email')}}</span>
            </a>
        </li>
    @endcan
@endcanany