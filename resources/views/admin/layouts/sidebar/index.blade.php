<div class="main-sidebar">
    <aside id="sidebar-wrapper">
        <div class="sidebar-brand px-5 mb-4">
            <a href="{{ url('/admin/dashboard') }}">
                <img class="w-100 mt-2" src="{{ url(GetSetting('site_logo')) }}">
            </a>
        </div>
        <div class="sidebar-brand sidebar-brand-sm">
            <a href="{{ url('/admin/dashboard') }}">
                <img src="{{ url(GetSetting('site_favicon')) }}">
            </a>
        </div>
        <ul class="sidebar-menu">
            <li class="menu-header">{{__('admin.Basics')}}</li>
            @include('admin.layouts.sidebar.basic')
            <li class="menu-header">{{__('admin.Contents')}}</li>
            @include('admin.layouts.sidebar.content')
            <li class="menu-header">{{__('admin.Tools')}}</li>
            @include('admin.layouts.sidebar.tool')
        </ul>
    </aside>
</div>
