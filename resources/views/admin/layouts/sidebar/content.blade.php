@canany(['testimonial-index', 'service-index', 'slider-index', 'sponsor-index'])
    @can('testimonial-index')
        <li class="nav-item {{(request()->routeIs('admin.testimonial.*')) ? 'active': ''}}">
            <a href="{{ route('admin.testimonial.index') }}" class="nav-link">
                <i class="fas fa-comments"></i><span>{{__('admin.Testimonials')}}</span>
            </a>
        </li>
    @endcan
    @can('service-index')
        <li class="nav-item {{(request()->routeIs('admin.service.*')) ? 'active': ''}}">
            <a href="{{ route('admin.service.index') }}" class="nav-link">
                <i class="fas fa-cogs"></i><span>{{__('admin.Services')}}</span>
            </a>
        </li>
    @endcan
    @can('slider-index')
        <li class="nav-item {{(request()->routeIs('admin.slider.*')) ? 'active': ''}}">
            <a href="{{ route('admin.slider.index') }}" class="nav-link">
                <i class="fas fa-sliders-h"></i><span>{{__('admin.Slider')}}</span>
            </a>
        </li>
    @endcan
@endcanany
@canany(['footer-contact-item-index', 'footer-information-index', 'social-link-index'])
    <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown">
            <i class="fas fa-edit"></i><span>{{__('admin.Footer')}}</span>
        </a>
        <ul class="dropdown-menu">
            @can('footer-contact-item-index')
                <li class="{{(request()->routeIs('admin.footer-contact-item.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.footer-contact-item.index') }}">
                        {{trans('admin.Contact')}}
                    </a>
                </li>
            @endcan
            @can('social-link-index')
                <li class="{{(request()->routeIs('admin.social-link.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.social-link.index') }}">
                        {{trans('Social media')}}
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcanany
@canany(['admin-language-index', 'frontend-language-index'])
    <li class="nav-item dropdown">
        <a href="#" class="nav-link has-dropdown"> 
            <i class="fas fa-globe-asia"></i><span>{{__('admin.Language')}}</span>
        </a>
        <ul class="dropdown-menu">
            @can('frontend-language-index')
                <li class="{{(request()->routeIs('admin.frontend-language.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.frontend-language.index') }}">
                        {{trans('admin.Frontend')}}
                    </a>
                </li>
            @endcan
            @can('admin-language-index')
                <li class="{{(request()->routeIs('admin.admin-language.*')) ? 'active': ''}}">
                    <a class="nav-link" href="{{ route('admin.admin-language.index') }}">
                        {{trans('admin.Admin')}}
                    </a>
                </li>
            @endcan
        </ul>
    </li>
@endcanany