@php
$page_title = trans('Admin | Our Process contents');
@endphp
@push('style')
<link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
<style>
    .form-control-file {
        padding: 5px;
        border: 1px solid #e4e6fc;
        border-radius: 5px;
        background: #fdfdff;
    }
    .input-group-text,
    select.form-control:not([size]):not([multiple]),
    .form-control:not(.form-control-sm):not(.form-control-lg) {
        font-size: 14px;
        padding: 10px 15px;
        height: 48px;
    }
</style>
@endpush
@extends('admin.layouts.master')
@section('content')
{{-- Main Content --}}
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>{{__('Our Process')}}</h1>
        </div>
        <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                class="fas fa-arrow-circle-left    "></i> {{__('admin.Back')}}</a>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @can('ourprocess-update')
                    <form action="{{route('admin.ourprocess.update', 1)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                    @endcan
                        <div class="form-group">
                            <label for="title">{{__('admin.Title')}}</label>
                            <input type="text" class="form-control" name="title" id="title" value="{{($ourprocess)?$ourprocess->title : ''}}">
                        </div>
                        <div class="form-group">
                            <label for="description">{{__('admin.Description')}}</label>
                            <textarea class="form-control h-100" name="description" id="description" rows="4">{{$ourprocess ? $ourprocess->description: ''}}</textarea>
                        </div>
                        @php
                            $items =($ourprocess)? json_decode($ourprocess->items) : array();              
                        @endphp
                        @forelse ( $items as $key => $item )
                            <div class="add_item delete_item_add">
                                <div class="form-group">
                                    <h4>{{__('Our Process Item')}}</h4>
                                    <div class="form-group">
                                        <label for="">{{__('Icon')}}</label>
                                        <div id="{{"icon".$key}}" name="itemicon[]"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <label for="">{{__('Title')}}</label>
                                            <input type="text" class="form-control" name="itemtitle[]" value="{{$item->title}}">
                                        </div>
                                        <div class="col-md-2 pt-4">
                                            <span class="btn btn-danger removemore"><i class="fa fa-minus-circle fa-lg"></i></span> 
                                            <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        @empty
                            <div class="add_item">
                                <div class="form-group">
                                    <h4>{{__('Our Process Item')}}</h4>
                                    <div class="form-group">
                                        <label for="">{{__('Icon')}}</label>
                                        <div id="icon0" name="itemicon[]">
                                            <input type="hidden" name="icons[]">
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <label for="">{{__('Title')}}</label>
                                            <input type="text" class="form-control" name="itemtitle[]" value="">
                                        </div>
                                        <div class="col-md-2 pt-4">
                                            <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        @endforelse
                     @can('ourprocess-update')
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{__('admin.Save')}}</button>              
                    </form>
                    @endcan
                </div>
            </div>
        </div>
    </section>
</div>

<div class="form-group d-none">
    <div class="our_process_item_add" id="our_process_item_add">
         <div class="delete_item_add" id="delete_item_add">
             <h4>{{__('Our Process Item')}}</h4>
             <div class="form-group">
                <label for="">{{__('Icon')}}</label>
                <div id="demoicon" name="itemicon[]">
                    <input type="hidden" name="icons[]">
                </div>
            </div>
            
             <div class="row">
                 <div class="col-md-10">
                     <label for="">{{__('Title')}}</label>
                     <input type="text" class="form-control" name="itemtitle[]">
                 </div>
                 <div class="col-md-2 pt-4">
                     <span class="btn btn-danger removemore"><i class="fa fa-minus-circle fa-lg"></i></span>
                     <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                     </span>
                 </div>
             </div>
             <div class="mt-3"></div>
         </div>
    </div>
 </div>
 @push('script')
    <script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js')}}"></script>
    <script>
        let icons = @json($items);
        let iconcount = icons.length;
        if(iconcount>0) {
            icons.map((icon, index)=> {
                $('#icon'+index).iconpicker({
                    align: 'left', // Only in div tag
                    arrowClass: 'btn-primary',
                    arrowPrevIconClass: 'fas fa-angle-left',
                    arrowNextIconClass: 'fas fa-angle-right',
                    cols: 5,
                    footer: true,
                    header: true,
                    icon: icon.icon,
                    value: icon.icon,
                    iconset: 'fontawesome5',
                    labelHeader: '{0} of {1} pages',
                    labelFooter: '{0} - {1} of {2} icons',
                    placement: 'bottom', // Only in button tag
                    rows: 5,
                    search: true,
                    searchText: icon.icon,
                    selectedClass: 'btn-primary',
                    unselectedClass: 'btn-dark',
                });
                $('#icon'+index).find('input[type=hidden]').each(function() {
                    $(this).val(icon.icon);
                });
            });
        } else addIcon('#icon'+iconcount);
        
        $(document).ready(function() {
            let counter = 0;
            $(document).on("click",".addemore",function() {
                iconcount++;
                let our_process_item_add = $('#our_process_item_add').html();
                $(this).closest(".add_item").append(our_process_item_add.replace('id="demoicon"', 'id="icon'+iconcount+'"'));
                counter++;
                addIcon('#icon'+iconcount);
            });
            $(document).on("click",'.removemore',function(event){
                $(this).closest(".delete_item_add").remove();
                counter -= 1
            });
        });

        function addIcon(iconid) { 
            $(iconid).iconpicker({
                align: 'left', // Only in div tag
                arrowClass: 'btn-primary',
                arrowPrevIconClass: 'fas fa-angle-left',
                arrowNextIconClass: 'fas fa-angle-right',
                cols: 5,
                footer: true,
                header: true,
                iconset: 'fontawesome5',
                labelHeader: '{0} of {1} pages',
                labelFooter: '{0} - {1} of {2} icons',
                placement: 'bottom', // Only in button tag
                rows: 5,
                search: true,
                searchText: 'Search',
                selectedClass: 'btn-primary',
                unselectedClass: 'btn-dark',
            })
        }
    </script>
    @include('admin.layouts.mediaJs')
@endpush
@endsection



