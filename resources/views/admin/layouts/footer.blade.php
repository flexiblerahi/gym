<footer class="main-footer">
    <div class="footer-left">
      {{trans("Copyright © 2022")}} <div class="bullet"></div> {{trans('admin.Developed By Websolutionus')}}
    </div>
    <div class="footer-right">
      {{__('1.0.0')}}
    </div>
</footer>
