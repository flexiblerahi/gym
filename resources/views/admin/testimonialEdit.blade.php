@php
$page_title = trans('Admin | Edit testimonial');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Edit Testimonial')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.testimonial.update', $testimonial->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            @include('admin.layouts.mediaInput', [
                                'inputTitle' => trans('Image'),
                                'inputName' => 'image',
                                'inputData' => $testimonial->image,
                                'multiInput' => false,
                                'buttonText' => trans('Select'),
                            ])
                            <div class="form-group">
                                <label for="">{{__('admin.Name')}}</label>
                                <input type="text" class="form-control" name="name"
                                    value="{{$testimonial->name}}" required>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Designation')}}</label>
                                <input type="text" class="form-control" name="designation"
                                    value="{{$testimonial->designation}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Comment')}}</label>
                                <textarea class="form-control h-100" name="comment"
                                    rows="3">{!! filterTag(clean($testimonial->comment)) !!}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('scripts')
    @include('admin.layouts.mediaJs')
@endpush
