@php
$page_title = trans('Admin | Edit Trainer');
@endphp
@push('style')
<link rel="stylesheet" href="{{ asset('assets/admin/css/mediamanager.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-iconpicker.min.css') }}">
<style>
    .form-control-file {
        padding: 5px;
        border: 1px solid #e4e6fc;
        border-radius: 5px;
        background: #fdfdff;
    }
</style>
@endpush
@extends('admin.layouts.master')
@section('content')
{{-- Main Content --}}
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>{{ __('Update Trainer Information') }}</h1>
        </div>
        <a class="btn btn-primary mb-4" href="{{ URL::previous() }}" role="button"><i
                class="fas fa-arrow-circle-left"></i> {{__('admin.Back')}}</a>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.trainer.update',$trainer->id) }}"  method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    @include('admin.layouts.mediaInput', [
                                    'inputTitle' => trans('Image'),
                                    'inputName' => 'image',
                                    'inputData' => $trainer->image,
                                    'multiInput' => false,
                                    'buttonText' => trans('Select'),
                                    ])
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="">{{__('Name')}}</label>
                                    <input type="text" class="form-control" name="name" value="{{$trainer->name}}">
                                </div>
                            </div>
                        </div>
                        @php
                        $items =($trainer)? json_decode($trainer->social_item) : array();              
                        @endphp
                        @foreach ( $items as $key=> $item )
                            <div class="add_trainer_item delete_trainer_item">
                                <div class="form-group">
                                    <h4>{{__('Social Media')}}</h4>
                                    <div class="form-group">
                                        <label for="">{{__('Icon')}}</label>
                                        <div id="{{"icon".$key}}" name="socialicon[]"></div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <label for="">{{__('Social Link')}}</label>
                                            <input type="text" class="form-control" name="sociallink[]" value="{{$item->sociallink}}" >
                                        </div>
                                        <div class="col-md-2 pt-4" >
                                            <span class="btn btn-danger removemore"><i class="fa fa-minus-circle fa-lg"></i></span> 
                                            <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        @endforeach
                        <button type="submit" class="btn btn-primary btn-block" role="button">{{__('Save')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
<div class="form-group d-none">
    <div class="trainer_item_add" id="trainer_item_add">
        <div class="delete_trainer_item" id="delete_trainer_item">
            <h4>{{__('Social Media')}}</h4>
            <div class="form-group">
                <label for="">{{__('Icon')}}</label>
                <div id="demoicon" name="socialicon[]">
                    <input type="hidden" name="icons[]">
                </div>
            </div>
            <div class="row">
                <div class="col-md-10">
                    <label for="">{{__('Social Link')}}</label>
                    <input type="text" class="form-control" name="sociallink[]" value="">
                </div>
                <div class="col-md-2 pt-4" >
                    <span class="btn btn-danger removemore"><i class="fa fa-minus-circle fa-lg"></i></span>
                    <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                    </span>
                </div>
            </div>
            <div class="mt-3"></div>
        </div>
    </div>
</div>
@include('admin.layouts.mediaModal')
@endsection
@push('script')
<script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js')}}"></script>
    <script>
        let icons = @json($items);
        console.log('icons :>> ', icons);
        let iconcount = icons.length;
        if(iconcount>0) {
            icons.map((icon, index)=> {
                $('#icon'+index).iconpicker({
                    align: 'left', // Only in div tag
                    arrowClass: 'btn-primary',
                    arrowPrevIconClass: 'fas fa-angle-left',
                    arrowNextIconClass: 'fas fa-angle-right',
                    cols: 5,
                    footer: true,
                    header: true,
                    icon: icon.socialicon,
                    value: icon.socialicon,
                    iconset: 'fontawesome5',
                    labelHeader: '{0} of {1} pages',
                    labelFooter: '{0} - {1} of {2} icons',
                    placement: 'bottom', // Only in button tag
                    rows: 5,
                    search: true,
                    searchText: icon.socialicon,
                    selectedClass: 'btn-primary',
                    unselectedClass: 'btn-dark',
                });
                $('#icon'+index).find('input[type=hidden]').each(function() {
                    $(this).val(icon.socialicon);
                });
            });
        } else addIcon('#icon'+iconcount);
        
        $(document).ready(function() {
            let counter = 0;
            $(document).on("click",".addemore",function() {
                iconcount++;
                let trainer_item_add = $('#trainer_item_add').html();
                $(this).closest(".add_trainer_item").append(trainer_item_add.replace('id="demoicon"', 'id="icon'+iconcount+'"'));
                counter++;
                addIcon('#icon'+iconcount);
            });
            $(document).on("click",'.removemore',function(event){
                $(this).closest(".delete_trainer_item").remove();
                counter -= 1
            });
        });

        function addIcon(iconid) { 
            $(iconid).iconpicker({
                align: 'left', // Only in div tag
                arrowClass: 'btn-primary',
                arrowPrevIconClass: 'fas fa-angle-left',
                arrowNextIconClass: 'fas fa-angle-right',
                cols: 5,
                footer: true,
                header: true,
                iconset: 'fontawesome5',
                labelHeader: '{0} of {1} pages',
                labelFooter: '{0} - {1} of {2} icons',
                placement: 'bottom', // Only in button tag
                rows: 5,
                search: true,
                searchText: 'Search',
                selectedClass: 'btn-primary',
                unselectedClass: 'btn-dark',
            })
        }
    </script>
@include('admin.layouts.mediaJs')
@endpush