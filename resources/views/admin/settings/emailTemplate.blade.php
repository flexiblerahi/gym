<div class="tab-pane fade" id="email-template" role="tabpanel"
                                        aria-labelledby="email-template-tab">
    <div class="card border">
        <div class="card-body">
            <div class="table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th>{{__('admin.Name')}}</th>
                            <th>{{__('admin.Subject')}}</th>
                            <th>{{__('admin.Action')}}</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach (App\Models\EmailTemplate::all() as $template)
                            <tr>
                                <td scope="row">{{$loop->iteration}}</td>
                                <td>{{$template->name}}</td>
                                <td>{{$template->subject}}</td>
                                <td>
                                    <a class="btn btn-primary"
                                        href="{{route('admin.email-template.edit', $template->id)}}"
                                        role="button"> {{trans('admin.Edit')}} </a>
                                </td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>