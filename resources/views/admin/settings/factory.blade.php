<div class="tab-pane fade" id="factory-settings" role="tabpanel"
                                        aria-labelledby="factory-settings">
    <div class="card border">
        <div class="card-body">
            <div class="row">
                <div class="col-md-12">
                    <a class="btn btn-primary btn-lg btn-block mb-4"
                        href="{{route('admin.backup')}}" role="button">
                        <i class="fas fa-database"></i>
                        {{trans('admin.Backup')}}</a>
                </div>
                <div class="col-md-12">
                    <a class="btn btn-primary btn-lg btn-block mb-4 reset"
                        href="{{route('admin.reset')}}" role="button">
                        <i class="fas fa-database"></i>
                        {{trans('admin.Reset Database')}}</a>
                </div>
                <div class="col-12">
                    <form action="{{route('admin.restore')}}" method="POST"
                        enctype="multipart/form-data" class="border p-4">
                        @csrf
                        <div class="form-group">
                            <label for="">{{__('admin.Restore')}}</label>
                            <div class="border p-4"><input type="file" class="form-control-file" name="backup" >
                            </div>
                            <button type="submit" class="btn btn-primary btn-block mt-4">
                            <i class="fas fa-sync"></i>
                            {{trans('admin.Restore')}}
                            </button>
                        </div>
                    </form>
                </div>
                <div class="col-md-12 mt-4">
                    @if(checkMaintenance()) 
                    <a class="btn btn-primary btn-lg btn-block mb-4"
                        href="{{route('admin.maintenance')}}" role="button"> 
                        <i class="fas fa-wrench"></i> {{trans('admin.Maintenance Mode')}} </a>
                    @else
                    <a class="btn btn-second-secondary btn-lg btn-block mb-4"
                        href="{{route('admin.maintenance')}}" role="button"> 
                        <i class="fas fa-wrench"></i> {{trans('admin.Maintenance Mode')}} </a>
                    @endif
                    
                </div>
            </div>
        </div>
    </div>
</div>