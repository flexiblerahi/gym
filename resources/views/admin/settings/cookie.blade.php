<div class="tab-pane fade" id="cookie-consent" role="tabpanel"
                                        aria-labelledby="cookie-consent-tab">
    <form action="{{route('admin.cookiesettings')}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card border">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.Cookie Consent')}}</label>
                            <select class="form-control" name="cookie_status">
                                <option @if(CookieSetting('cookie_status') == 1) selected @endif value="1">{{__('admin.Enable')}}
                                </option>
                                <option @if(CookieSetting('cookie_status') == 0) selected @endif value="0">{{__('admin.Disable')}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.Cookie Consent Button')}}</label>
                            <input type="text" class="form-control"
                                name="cookie_button"
                                value="{{CookieSetting('cookie_button')}}">
                        </div>
                    </div>
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('admin.Cookie Consent Confirmation Text')}}</label>
                            <textarea type="text" class="form-control h-50"
                                name="cookie_confirmation">{{filterTag(clean(CookieSetting('cookie_confirmation')))}}</textarea>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">{{__('admin.Save')}}</button>
            </div>
        </div>
    </form>
</div>