<div class="tab-pane fade" id="pagination-settings" role="tabpanel"
                                        aria-labelledby="pagination-settings-tab">
    <form action="{{route('admin.paginationsettings')}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card border">
            <div class="card-body">
                <div class="form-group">
                    <label>{{__('Blog per-page paginate data')}}</label>
                    <input type="number" class="form-control" name="blog_pagination"
                        value="{{$setting ? $setting->blog_pagination : '2'}}">
                </div>
                <div class="form-group">
                    <label>{{__('Blog comment per-page paginate data')}}</label>
                    <input type="number" class="form-control" name="blog_comment_pagination"
                        value="{{$setting ? $setting->blog_comment_pagination : '2'}}">
                </div>
                <div class="form-group">
                    <label>{{__('Course per-page paginate data')}}</label>
                    <input type="number" class="form-control" name="course_pagination"
                        value="{{$setting ? $setting->course_pagination : '2'}}">
                </div>
                <div class="form-group">
                    <label>{{__('Product per-page paginate data')}}</label>
                    <input type="number" class="form-control" name="product_pagination"
                        value="{{$setting ? $setting->product_pagination : '2'}}">
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">{{__('admin.Save')}}</button>
            </div>
        </div>
    </form>
</div>