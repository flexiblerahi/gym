<div class="tab-pane fade" id="google-map" role="tabpanel"
                                            aria-labelledby="google-map-tab">
    <form action="{{route('admin.mapsettings')}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card border">
            <div class="card-body">
                <div class="form-group">
                    <label>{{__('admin.Map Embed Link')}}</label>
                    <input type="text" class="form-control" name="link"
                        value="{{App\Models\GoogleMap::first()->link}}">
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">{{__('admin.Save')}}</button>
            </div>
        </div>
    </form>
</div>