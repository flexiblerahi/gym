<div class="tab-pane fade" id="payment-gateway" role="tabpanel"
                                        aria-labelledby="payment-gateway-tab">
    <form action="{{route('admin.paymentsettings')}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card border">
            <div class="card-body">
                <ul class="nav nav-pills" id="myTab" role="tablist">
                    <li class="nav-item">
                        <a class="nav-link active" id="paypal-tab" data-toggle="tab"
                            href="#paypal" role="tab" aria-controls="paypal"
                            aria-selected="true">{{__('admin.Paypal')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="stripe-tab" data-toggle="tab"
                            href="#stripe" role="tab" aria-controls="stripe"
                            aria-selected="false">{{__('admin.Stripe')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="razorpay-tab" data-toggle="tab"
                            href="#razorpay" role="tab" aria-controls="razorpay"
                            aria-selected="false">{{__('admin.RazorPay')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="paystack-tab" data-toggle="tab"
                            href="#paystack" role="tab" aria-controls="paystack"
                            aria-selected="false">{{__('admin.Paystack')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="mollie-tab" data-toggle="tab"
                            href="#mollie" role="tab" aria-controls="mollie"
                            aria-selected="false">{{__('admin.Mollie')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="sslcommerz-tab"
                            data-toggle="tab" href="#sslcommerz" role="tab"
                            aria-controls="sslcommerz"
                            aria-selected="false">{{__('Ssl-Commerz')}}</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="bank-tab" data-toggle="tab"
                            href="#bank" role="tab" aria-controls="bank"
                            aria-selected="false">{{__('admin.Bank')}}</a>
                    </li>
                </ul>
                <div class="tab-content mt-4" id="myTabContent">
                    <div class="tab-pane fade show active" id="paypal" role="tabpanel"
                        aria-labelledby="paypal-tab">
                        <div class="form-group">
                            <label>{{__('admin.Paypal Mode')}}</label>
                            <select class="form-control" name="paypal_api_mode">
                                <option @if (PaymentGateway('paypal_api_mode') == 'sandbox') selected @endif value="sandbox">Sandbox
                                </option>
                                <option @if (PaymentGateway('paypal_api_mode') == 'live') selected @endif value="live">Live
                                </option>
                            </select>
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.Paypal Api Client ID')}}</label>
                            <input type="text" class="form-control"
                                name="paypal_api_client"
                                value="{{PaymentGateway('paypal_api_client')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.Paypal Api Secret')}}</label>
                            <input type="text" class="form-control"
                                name="paypal_api_secret"
                                value="{{PaymentGateway('paypal_api_secret')}}">
                        </div>

                        <div class="form-group">
                            <label for="">{{__('admin.Country')}}</label>
                            <select class="form-control" name="paypal_country">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Countries() as $item => $val)
                                    <option @if ($item == PaymentGateway('paypal_country')) selected @endif
                                    value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Currency')}}</label>
                                <select class="form-control" name="paypal_currency">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                    @foreach (Currencies() as $item => $val)
                                    <option @if ($item == PaymentGateway('paypal_currency')) selected @endif value="{{$item}}"> {{$val}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Currency rate per')}} ( {{GetSetting('site_currency')}})</label>
                                <input type="text" class="form-control" value="{{PaymentGateway('paypal_rate')}}" name="paypal_rate">
                            </div>
                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="paypal_api_status"
                                    @if (PaymentGateway('paypal_api_status') == 1) checked @endif
                                    class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF / ON')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="stripe" role="tabpanel"
                        aria-labelledby="stripe-tab">
                        <div class="form-group">
                            <label>{{__('admin.Stripe Publishable Key')}}</label>
                            <input type="text" class="form-control"
                                name="stripe_api_publishable_key"
                                value="{{PaymentGateway('stripe_api_publishable_key')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.Stripe Secret Key')}}</label>
                            <input type="text" class="form-control"
                                name="stripe_api_secret_key"
                                value="{{PaymentGateway('stripe_api_secret_key')}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Country')}}</label>
                            <select class="form-control" name="stripe_country">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Countries() as $item => $val)
                                    <option @if ($item == PaymentGateway('stripe_country')) selected @endif
                                    value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Currency')}}</label>
                                <select class="form-control" name="stripe_currency">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                    @foreach (Currencies() as $item => $val)
                                    <option @if ($item == PaymentGateway('stripe_currency')) selected @endif value="{{$item}}"> {{$val}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Currency rate per')}} ({{GetSetting('site_currency')}})</label>
                                <input type="text"
                                class="form-control" value="{{PaymentGateway('stripe_rate')}}" name="stripe_rate">
                            </div>

                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="stripe_api_status"
                                    @if (PaymentGateway('stripe_api_status') == 1) checked @endif
                                    class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF / ON')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="razorpay" role="tabpanel"
                        aria-labelledby="razorpay-tab">
                        <div class="form-group">
                            <label>{{__('admin.Razorpay Key ID')}}</label>
                            <input type="text" class="form-control"
                                name="razorpay_key_id"
                                value="{{PaymentGateway('razorpay_key_id')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.Razorpay Key Secret')}}</label>
                            <input type="text" class="form-control"
                                name="razorpay_key_secret"
                                value="{{PaymentGateway('razorpay_key_secret')}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Country')}}</label>
                            <select class="form-control" name="razorpay_country">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Countries() as $item => $val)
                                    <option @if ($item == PaymentGateway('razorpay_country')) selected @endif
                                    value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Currency')}}</label>
                            <select class="form-control" name="razorpay_currency">
                            <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Currencies() as $item => $val)
                                <option @if ($item == PaymentGateway('razorpay_currency')) selected @endif value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Currency rate per')}} ({{GetSetting('site_currency')}})</label>
                            <input type="text"
                            class="form-control" value="{{PaymentGateway('razorpay_rate')}}" name="razorpay_rate">
                        </div>
                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="razorpay_status"
                                    @if (PaymentGateway('razorpay_status') == 1) checked @endif
                                    class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF / ON')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="mollie" role="tabpanel"
                        aria-labelledby="mollie-tab">
                        <div class="form-group">
                            <label>{{__('admin.Mollie Email')}}</label>
                            <input type="text" class="form-control"
                                name="mollie_email"
                                value="{{PaymentGateway('mollie_email')}}" >
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.Mollie API Key')}}</label>
                            <input type="text" class="form-control"
                                name="mollie_api_key"
                                value="{{PaymentGateway('mollie_api_key')}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Country')}}</label>
                            <select class="form-control" name="mollie_country">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Countries() as $item => $val)
                                    <option @if ($item == PaymentGateway('mollie_country')) selected @endif
                                    value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Currency')}}</label>
                                <select class="form-control" name="mollie_currency">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                    @foreach (Currencies() as $item => $val)
                                    <option @if ($item == PaymentGateway('mollie_currency')) selected @endif value="{{$item}}"> {{$val}} </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Currency rate per')}} ({{GetSetting('site_currency')}})</label>
                                <input type="text"
                                class="form-control" value="{{PaymentGateway('mollie_rate')}}" name="mollie_rate">
                            </div>
                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="mollie_status"
                                    @if (PaymentGateway('mollie_status') == 1) checked @endif
                                    class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF / ON')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="paystack" role="tabpanel"
                        aria-labelledby="paystack-tab">
                        <div class="form-group">
                            <label>{{__('admin.Paystack Public Key')}}</label>
                            <input type="text" class="form-control"
                                name="paystack_public_key"
                                value="{{PaymentGateway('paystack_public_key')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('admin.Paystack Secret Key')}}</label>
                            <input type="text" class="form-control"
                                name="paystack_secret_key"
                                value="{{PaymentGateway('paystack_secret_key')}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Country')}}</label>
                            <select class="form-control" name="paystack_country">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Countries() as $item => $val)
                                    <option @if ($item == PaymentGateway('paystack_country')) selected @endif
                                    value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Currency')}}</label>
                            <select class="form-control" name="paystack_currency">
                            <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Currencies() as $item => $val)
                                <option @if ($item == PaymentGateway('paystack_currency')) selected @endif value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Currency rate per')}} ({{GetSetting('site_currency')}})</label>
                            <input type="text"
                            class="form-control" value="{{PaymentGateway('paystack_rate')}}" name="paystack_rate">
                        </div>
                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="paystack_status"
                                    @if (PaymentGateway('paystack_status') == 1) checked @endif
                                    class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF / ON')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="bank" role="tabpanel"
                        aria-labelledby="bank-tab">
                        <div class="form-group">
                            <label>{{__('admin.Bank Account Information')}}</label>
                            <textarea rows="10" class="form-control h-100"
                                name="bank_account_details"
                                >{{filterTag(clean(PaymentGateway('bank_account_details')))}}</textarea>
                        </div>

                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="bank_account_status"
                                    @if (PaymentGateway('bank_account_status') == 1) checked @endif
                                    class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF / ON')}}</span>
                            </label>
                        </div>
                    </div>
                    <div class="tab-pane fade" id="sslcommerz" role="tabpanel"
                        aria-labelledby="sslcommerz-tab">
                        <div class="form-group">
                            <label>{{__('STORE ID')}}</label>
                            <input type="text" class="form-control"
                                name="ssl_commerz_store_id"
                                value="{{PaymentGateway('ssl_commerz_store_id')}}">
                        </div>
                        <div class="form-group">
                            <label>{{__('STORE PASSWORD')}}</label>
                            <input type="text" class="form-control"
                                name="ssl_commerz_store_password"
                                value="{{PaymentGateway('ssl_commerz_store_password')}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('ssl_commerz_country')}}</label>
                            <select class="form-control" name="ssl_commerz_country">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Countries() as $item => $val)
                                    <option @if ($item == PaymentGateway('ssl_commerz_country')) selected @endif
                                    value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Currency')}}</label>
                            <select class="form-control" name="ssl_commerz_currency">
                                <option>--- {{trans('admin.Select')}} ----</option>
                                @foreach (Currencies() as $item => $val)
                                    <option @if ($item == PaymentGateway('ssl_commerz_currency')) selected @endif value="{{$item}}"> {{$val}} </option>
                                @endforeach
                            </select>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Currency rate per')}} ( {{GetSetting('site_currency')}})</label>
                            <input type="text" class="form-control" value="{{PaymentGateway('ssl_commerz_rate')}}" name="ssl_commerz_rate">
                        </div>
                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="ssl_commerz_status"
                                    @if (PaymentGateway('ssl_commerz_status') == 1) checked @endif
                                    class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF / ON')}}</span>
                            </label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">{{__('admin.Save')}}</button>
            </div>
        </div>
    </form>
</div>