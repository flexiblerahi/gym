<div class="tab-pane fade" id="email-settings" role="tabpanel"
                                        aria-labelledby="email-settings-tab">
    <form action="{{route('admin.mailsettings')}}" method="POST">
        @csrf
        @method('PUT')
        <div class="card border">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('admin.Mail Host')}}</label>
                            <input type="text" class="form-control" name="mail_host"
                                value="{{MailConfig('mail_host')}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.Sender Name')}}</label>
                            <input type="text" class="form-control"
                                name="mail_sent_from"
                                value="{{MailConfig('mail_sent_from')}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.Sender Email')}}</label>
                            <input type="text" class="form-control"
                                name="mail_sent_from_email"
                                value="{{MailConfig('mail_sent_from_email')}}">
                        </div>
                    </div>

                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.SMTP Username')}}</label>
                            <input type="text" class="form-control"
                                name="smtp_username"
                                value="{{MailConfig('smtp_username')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.SMTP Password')}}</label>
                            <input type="text" class="form-control"
                                name="smtp_password"
                                value="{{MailConfig('smtp_password')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.Mail Port')}}</label>
                            <input type="text" class="form-control" name="mail_port"
                                value="{{MailConfig('mail_port')}}">
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>{{__('admin.Mail Encription')}}</label>
                            <select class="form-control" name="mail_encryption">
                                <option @if (MailConfig('mail_encryption') == 'tls') selected @endif value="tls">{{__('admin.TLS')}}
                                </option>
                                <option @if (MailConfig('mail_encryption') == 'ssl') selected @endif value="ssl">{{__('admin.SSL')}}
                                </option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">{{__('admin.Save')}}</button>
            </div>
        </div>
    </form>
</div>