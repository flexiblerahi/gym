<div class="tab-pane fade show active" id="general-settings" role="tabpanel"
                                        aria-labelledby="general-settings-tab">
    <form action="{{route('admin.generalsettings')}}" method="POST"
        enctype="multipart/form-data">
        @csrf
        @method('PUT')
        <div class="card border">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>{{__('admin.Site Name')}}</label>
                            <input type="text" class="form-control" name="site_name"
                                value="{{GetSetting('site_name')}}" >
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>{{__('admin.Currency')}}</label>
                            <select class="form-control" name="site_currency">
                                @foreach (currencies() as $key => $currency)
                                    <option @if (GetSetting('site_currency') == $key) selected @endif
                                        value="{{$key}}">
                                        {{$key}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>{{__('admin.Currency Icon')}}</label>
                            <input type="text" class="form-control"
                                name="site_currency_icon"
                                value="{{GetSetting('site_currency_icon')}}">
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{__('Site Currency Direction')}}</label>
                            <select class="form-control" name="site_currency_direction">
                                <option @if (GetSetting('site_currency_direction') == App\Models\Setting::RIGHT) selected 
                                @endif value="{{ App\Models\Setting::RIGHT}}">{{__('RIGHT')}}
                                </option>
                                <option @if (GetSetting('site_currency_direction') == App\Models\Setting::LEFT) selected 
                                @endif value="{{ App\Models\Setting::LEFT}}">{{__('LEFT')}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>{{__('admin.Site Direction')}}</label>
                            <select class="form-control" name="site_direction">
                                <option @if (GetSetting('site_direction') == 'RTL') selected @endif value="RTL">{{__('admin.RTL')}}
                                </option>
                                <option @if (GetSetting('site_direction') == 'LTR') selected @endif value="LTR">{{__('admin.LTR')}}
                                </option>
                            </select>
                        </div>
                    </div>
                    <div class="col-md-3">
                        <div class="form-group">
                            <label>{{__('admin.Time Zone')}}</label>
                            <select class="form-control select2" name="site_time_zone">
                                @foreach (DateTimeZone::listIdentifiers() as $zone)
                                    <option @if (GetSetting('site_time_zone') == $zone) selected @endif
                                        value="{{$zone}}">
                                        {{$zone}}</option>
                                @endforeach
                            </select>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        @include('admin.layouts.mediaInput', [
                        'inputTitle' => trans('Site Logo'),
                        'inputName'  => 'site_logo',
                        'inputData'  => GetSetting('site_logo'),
                        'multiInput' => false,
                        'buttonText' => trans('Select'),
                        ])
                        @include('admin.layouts.mediaInput', [
                        'inputTitle' => trans('Site Favicon'),
                        'inputName'  => 'site_favicon',
                        'inputData'  => GetSetting('site_favicon'),
                        'multiInput' => false,
                        'buttonText' => trans('Select'),
                        ])
                        @include('admin.layouts.mediaInput', [
                            'inputTitle' => trans('Section Icon'),
                            'inputName'  => 'section_icon',
                            'inputData'  => GetSetting('section_icon'),
                            'multiInput' => false,
                            'buttonText' => trans('Select'),
                        ])
                        @include('admin.layouts.mediaInput', [
                            'inputTitle' => trans('Footer Logo'),
                            'inputName'  => 'footer_logo',
                            'inputData'  => GetSetting('footer_logo'),
                            'multiInput' => false,
                            'buttonText' => trans('Select'),
                        ])
                        @include('admin.layouts.mediaInput', [
                            'inputTitle' => trans('Breadcumb'),
                            'inputName'  => 'breadcumb',
                            'inputData'  => GetSetting('breadcumb'),
                            'multiInput' => false,
                            'buttonText' => trans('Select'),
                        ])
                        @include('admin.layouts.mediaInput', [
                            'inputTitle' => trans('Testimonial Background'),
                            'inputName'  => 'testimonial_background',
                            'inputData'  => GetSetting('testimonial_background'),
                            'multiInput' => false,
                            'buttonText' => trans('Select'),
                        ])
                        @include('admin.layouts.mediaInput', [
                            'inputTitle' => trans('User Avatar'),
                            'inputName'  => 'user_avatar',
                            'inputData'  => GetSetting('user_avatar'),
                            'multiInput' => false,
                            'buttonText' => trans('Select'),
                        ])
                    </div>
                </div>
            </div>
        </div>
        <div class="card border">
            <div class="card-body">
                <button type="submit" class="btn btn-primary btn-block">{{__('admin.Save')}}</button>
            </div>
        </div>
    </form>
</div>