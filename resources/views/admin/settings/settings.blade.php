@php
    $page_title = trans('Admin | Settings');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-colorpicker.css')}}" />
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="modal fade" id="preloader" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
          <div class="modal-content">
            <div class="modal-header">
              <h5 class="modal-title" id="exampleModalLongTitle">{{__('Loading')}}</h5>
              <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                <span aria-hidden="true">&times;</span>
              </button>
            </div>
            <div class="modal-body">
                <div class="d-flex justify-content-center d-none">
                    <div class="spinner-border" role="status">
                    </div>
                </div>
            </div>
          </div>
        </div>
    </div>
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Settings')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button">
                <i class="fas fa-arrow-circle-left"></i> {{trans('admin.Back')}}
            </a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3">
                                <ul class="nav flex-column nav-pills mt-2" id="myTab" role="tablist">
                                    <li class="nav-item mb-2">
                                        <a class="nav-link border active" id="general-settings-tab" data-toggle="tab"
                                            href="#general-settings" role="tab" aria-controls="general-settings"
                                            aria-selected="true">{{__('admin.General Settings')}}</a>
                                    </li>
                                    <li class="nav-item mb-2">
                                        <a class="nav-link border" id="cookie-consent-tab" data-toggle="tab"
                                            href="#cookie-consent" role="tab" aria-controls="cookie-consent"
                                            aria-selected="false">{{__('admin.Cookie Consent')}}</a>
                                    </li>
                                    <li class="nav-item mb-2">
                                        <a class="nav-link border" id="payment-gateway-tab" data-toggle="tab"
                                            href="#payment-gateway" role="tab" aria-controls="payment-gateway"
                                            aria-selected="false">{{__('admin.Payment Gateway')}}</a>
                                    </li>
                                    <li class="nav-item mb-2">
                                        <a class="nav-link border" id="google-map-tab" data-toggle="tab" href="#google-map"
                                            role="tab" aria-controls="google-map" aria-selected="false">{{__('admin.Google Map')}}</a>
                                    </li>
                                    <li class="nav-item mb-2">
                                        <a class="nav-link border" id="email-settings-tab" data-toggle="tab"
                                            href="#email-settings" role="tab" aria-controls="email-settings"
                                            aria-selected="false">{{__('admin.Email Settings')}}</a>
                                    </li>
                                    <li class="nav-item mb-2">
                                        <a class="nav-link border" id="email-template-tab" data-toggle="tab"
                                            href="#email-template" role="tab" aria-controls="email-template"
                                            aria-selected="false">{{__('admin.Email Template')}}</a>
                                    </li>

                                    <li class="nav-item mb-2">
                                        <a class="nav-link border" id="pagination-settings-tab" data-toggle="tab"
                                            href="#pagination-settings" role="tab" aria-controls="pagination-settings"
                                            aria-selected="true">{{__('admin.Pagination Settings')}}</a>
                                    </li>

                                    <li class="nav-item mb-2">
                                        <a class="nav-link border" id="factory-settings-tab" data-toggle="tab"
                                            href="#factory-settings" role="tab" aria-controls="factory-settings"
                                            aria-selected="false">{{__('admin.Factory Settings')}}</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-9">
                                <div class="tab-content" id="myTabContent">
                                    @include('admin.settings.general')
                                    @include('admin.settings.cookie')
                                    @include('admin.settings.payment')
                                    @include('admin.settings.map')
                                    @include('admin.settings.pagination')
                                    @include('admin.settings.emailSetting')
                                    @include('admin.settings.emailTemplate')
                                    @include('admin.settings.factory')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    <script src="{{asset('assets/admin/js/bootstrap-colorpicker.min.js')}}"></script>
    <script>
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) return false;
        });

        $(function() {
            $('.cp-1').colorpicker();
            $('.cp-prev-1').css('background-color', $('.cp-1').val().toString());
            $('.cp-1').on('colorpickerChange', function(event) {
                $('.cp-prev-1').css('background-color', event.color.toString());
            });
            $('.cp-2').colorpicker();
            $('.cp-prev-2').css('background-color', $('.cp-2').val().toString());
            $('.cp-2').on('colorpickerChange', function(event) {
                $('.cp-prev-2').css('background-color', event.color.toString());
            });
        });
    </script>
    <script>
        $(document).on('click', '.reset', function (e) {
            e.preventDefault();
            var url = $(this).attr('href');
            Swal.fire({
            title: '{{trans('admin.Are you sure?')}}',
            text: "{{trans('admin.You wont be able to revert this!')}}",
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: '{{trans('admin.Reset!')}}'
            }).then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "GET",
                        url: url,
                        beforeSend: function(){
                            // Show image container
                            $('#preloader').modal('show');
                        },
                        success: function (response) {
                            Swal.fire(
                                '{{trans('admin.Successful!')}}',
                                '{{trans('admin.Reset completed.')}}',
                                '{{trans('admin.success')}}'
                            )
                            .then((result)=>{
                                location.reload()
                            })
                        },
                        complete:function(data){
                            // Hide image container
                            $('#preloader').modal('hide');
                        }
                    });
                }
            })
        });
    </script>
    @include('admin.layouts.mediaJs')
@endpush

