@php
    $page_title = trans('Admin | Edit FAQ');
@endphp
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Edit FAQ')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.faq.update', $faq->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="question">{{__('admin.Question')}}</label>
                                <input type="text" class="form-control" name="question" id="question" value="{{$faq->question}}">
                            </div>
                            <div class="form-group">
                                <label for="answer">{{__('admin.Answer')}}</label>
                                <textarea type="text" rows="5" class="form-control h-100" name="answer" id="answer">{{ $faq->answer }}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection