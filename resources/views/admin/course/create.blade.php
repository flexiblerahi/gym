@php
$page_title = trans('Admin | Add Course');
@endphp
@push('style')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/mediamanager.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-iconpicker.min.css') }}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ __('Add Course') }}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{ URL::previous() }}" role="button"><i class="fas fa-arrow-circle-left"></i> {{__('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin.course.store') }}" enctype="multipart/form-data" method="POST">
                            @csrf
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="name">{{__('Course Name')}}</label>
                                        <input type="text" class="form-control" name="name" id="name">
                                    </div>
                                    <div class="form-group">
                                        <label for="slug">{{__('Slug')}}</label>
                                        <input type="text" class="form-control" name="slug" id="slug">
                                    </div>
                                </div>
                            </div>
                             <div class="row">
                                <div class="col">
                                    @include('admin.layouts.mediaInput', [
                                        'inputTitle' => trans('Image'),
                                        'inputName' => 'image',
                                        'inputData' => null,
                                        'multiInput' => false,
                                        'buttonText' => trans('Select'),
                                    ])
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="price">{{__('Price')}}</label>
                                        <input type="number" class="form-control" name="price" id="price" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="offer_price">{{__('Offer Price')}}</label>
                                        <input type="number" class="form-control" name="offer_price" id="offer_price" value="0">
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="is_paid">{{__('Is Paid')}}</label>
                                        <select class="form-control" name="is_paid" id="is_paid">
                                            <option value="{{ App\Models\Course::PAID }}">{{__('Paid')}}</option>
                                            <option value="{{ App\Models\Course::NONPAID }}">{{__('Free')}}</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="col">
                                    <div class="form-group">
                                        <label for="">{{__('Course Category')}}</label>
                                        <select class="form-control select2" name="coursecategory_id" id="coursecategory_id">
                                            <option value="">{{__('Select Course Category')}}</option>
                                            @foreach ($courseCategories as $courseCategory)
                                                <option value="{{ $courseCategory->id }}">{{ $courseCategory->name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col">
                                    <div class="form-group">
                                        <label for="is_paid">{{__('Level')}}</label>
                                        <select class="form-control" name="level" id="level">
                                            <option value="{{ App\Models\Course::BEGINNER }}">{{__('Beginner')}}</option>
                                            <option value="{{ App\Models\Course::INTERMEDIATE }}">{{__('Intermediate')}}</option>
                                            <option value="{{ App\Models\Course::ADVANCE }}">{{__('Advance')}}</option>
                                            <option value="{{ App\Models\Course::PRO }}">{{__('Pro')}}</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="mb-3">
                              <label for="" class="form-label">{{__('Description')}}</label>
                              <textarea class="form-control h-100" name="description" id="editor" rows="4"></textarea>
                            </div>
                            <div class="form-group">
                                <div class="control-label">{{__('Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="status" checked class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('admin.OFF')}} / {{__('admin.ON')}}</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{__('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    <script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
    @include('admin.layouts.mediaJs')
@endpush
