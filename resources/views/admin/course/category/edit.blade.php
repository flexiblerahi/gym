@php
$page_title = trans('Admin | Edit Course Category');
@endphp
@push('style')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/mediamanager.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-iconpicker.min.css') }}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('Edit Course Category')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{ URL::previous() }}" role="button"><i class="fas fa-arrow-circle-left"></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{ route('admin.coursecategory.update',  $coursecategory->id) }}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">{{__('Name')}}</label>
                                <input type="text" class="form-control" name="name" value="{{ $coursecategory->name }}" required>
                            </div>
                            <div class="form-group">
                                <div class="control-label">{{__('Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="status" @if($coursecategory->status == App\Models\Coursecategory::ACTIVE)
                                        checked
                                    @endif  class="custom-switch-input">
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('admin.OFF')}} / {{trans('admin.ON')}}</span>
                                </label>
                            </div> 
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('Update')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    <script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
    @include('admin.layouts.mediaJs')
@endpush
