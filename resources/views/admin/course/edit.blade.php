@php
$page_title = trans('Admin | Edit Course');
@endphp
@push('style')
<link rel="stylesheet" href="{{ asset('assets/admin/css/mediamanager.css') }}">
<link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-iconpicker.min.css') }}">
<style>
    .form-control-file {
        padding: 5px;
        border: 1px solid #e4e6fc;
        border-radius: 5px;
        background: #fdfdff;
    }
</style>
@endpush
@extends('admin.layouts.master')
@section('content')
{{-- Main Content --}}
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>{{ __('Update Course') }}</h1>
        </div>
        <a class="btn btn-primary mb-4" href="{{ URL::previous() }}" role="button">
            <i class="fas fa-arrow-circle-left"></i> {{__('admin.Back')}}
        </a>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <form action="{{ route('admin.course.update', $course->id) }}" enctype="multipart/form-data" method="POST">
                        @csrf
                        @method('PUT')
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="name">{{__('Course Name')}}</label>
                                    <input type="text" class="form-control" name="name" id="name" value="{{ $course->name }}">
                                </div>
                                <div class="form-group">
                                    <label for="slug">{{__('Slug')}}</label>
                                    <input type="text" class="form-control" name="slug" id="slug" value="{{ $course->slug }}">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                @include('admin.layouts.mediaInput', [
                                'inputTitle' => trans('Image'),
                                'inputName' => 'image',
                                'inputData' => $course->image,
                                'multiInput' => false,
                                'buttonText' => trans('Select'),
                                ])
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="price">{{__('Price')}}</label>
                                    <input type="number" class="form-control" value="{{ $course->price }}" name="price" id="price">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="offer_price">{{__('Offer Price')}}</label>
                                    <input type="number" class="form-control" value="{{ $course->offer_price }}" name="offer_price" id="offer_price">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="is_paid">{{__('Is Paid')}}</label>
                                    <select class="form-control" name="is_paid" id="is_paid">
                                        <option @if ($course->is_paid == App\Models\Course::PAID) selected @endif
                                            value="{{ App\Models\Course::PAID }}">{{__('Paid')}}</option>
                                        <option @if ($course->is_paid == App\Models\Course::NONPAID) selected @endif
                                            value="{{ App\Models\Course::NONPAID }}">{{__('Free')}}</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col">
                                <div class="form-group">
                                    <label for="">{{__('Course Category')}}</label>
                                    <select class="select2" name="coursecategory_id" id="coursecategory_id">
                                        <option value="">{{__('Select Course Category')}}</option>
                                        @foreach ($courseCategories as $courseCategory)
                                            <option value="{{ $courseCategory->id }}" 
                                            @if ($course->coursecategory_id ==$courseCategory->id)
                                                Selected
                                            @endif>{{ $courseCategory->name }}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col">
                                <div class="form-group">
                                    <label for="level">{{__('Level')}}</label>
                                    <select class="form-control" name="level" id="level">
                                        <option @if ($course->level == App\Models\Course::BEGINNER) selected @endif
                                            value="{{ App\Models\Course::BEGINNER }}">{{__('Beginner')}}</option>
                                        <option @if ($course->level == App\Models\Course::INTERMEDIATE) selected @endif
                                            value="{{ App\Models\Course::INTERMEDIATE }}">{{__('Intermediate')}}
                                        </option>
                                        <option @if ($course->level == App\Models\Course::ADVANCE) selected @endif
                                            value="{{ App\Models\Course::ADVANCE }}">{{__('Advance')}}</option>
                                        <option @if ($course->level == App\Models\Course::PRO) selected @endif value="{{
                                            App\Models\Course::PRO }}">{{__('Pro')}}</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Full Description')}}</label>
                            <textarea class="form-control h-100" name="description" id="editor" rows="4">
                                {!! clean(html_entity_decode($course->description)) !!}
                            </textarea>
                        </div>
                        <div class="form-group">
                            <div class="control-label">{{__('Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="status" 
                                    @if($course->status == App\Models\Course::ACTIVE)
                                        checked
                                    @endif
                                class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF')}} / {{trans('admin.ON')}}</span>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" role="button">
                            {{trans('Update')}} 
                        </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@include('admin.layouts.mediaModal')
@endsection
@push('script')
<script type="text/javascript" src="{{ asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js') }}"></script>
@include('admin.layouts.mediaJs')
@endpush