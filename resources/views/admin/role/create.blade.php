@php
$page_title = trans('Admin | Create Role');
@endphp
@push('style')
    <link rel="stylesheet" href="{{ asset('assets/admin/css/mediamanager.css') }}">
    <link rel="stylesheet" href="{{ asset('assets/admin/css/bootstrap-iconpicker.min.css') }}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{ trans('admin.Create Role') }}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{ URL::previous() }}" role="button"><i
                    class="fas fa-arrow-circle-left    "></i> {{ trans('admin.Back') }}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(['route' => 'admin.roles.store', 'method' => 'POST']) !!}
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>{{__('Name')}}</label>
                                    {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control']) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>{{__('Permission')}}</label>
                                    <br />
                                    <div class="form-group">
                                        <div class="form-check">
                                            <input type="checkbox" name="checkall" id="checkall" class="form-check-input"
                                                onClick="check_uncheck_checkbox(this.checked);" />
                                            <label class="form-check-label" for="checkall">
                                                {{__('all-permissions')}}
                                            </label>
                                        </div>
                                        @foreach ($permission as $key => $value)
                                            @if($key%4 == 0) 
                                                @php
                                                    $valuename = str_replace('-index', '', $value->name);
                                                @endphp
                                                <h4 class="mt-3" id="{{$valuename}}">
                                                    <input type="checkbox" class="form-check-input mt-2"
                                                        onClick="group_checkbox('{{$valuename}}');" />
                                                    <span class="mt-2">{{ucwords(str_replace('index', '', str_replace('-', ' ', $value->name)))}}</span>
                                                </h4>
                                            @endif 
                                            <div class="form-check ml-4">
                                                {{ Form::checkbox('permission[]', $value->id, false, ['class' => 'form-check-input '.$valuename, 'id' => $value->id]) }}
                                                <label class="form-check-label" for="{{ $value->id }}">
                                                    {{ $value->name }}
                                                </label>
                                            </div>
                                        @endforeach
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">{{__('Submit')}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection

@push('script')
    <script>
        function group_checkbox(value) {
            $('.'+value).each(function() {
                this.checked = true;
            })
        }
        function check_uncheck_checkbox(isChecked) {
            if (isChecked) {
                $('input[name="permission[]"]').each(function() {
                    this.checked = true;
                });
            } else {
                $('input[name="permission[]"]').each(function() {
                    this.checked = false;
                });
            }
        }
        $('input[name="permission[]"]').on("click", function() {
            if ($(this).is(":not(:checked)"))
                $('#checkall').prop('checked', false);
        });
    </script>
    @include('admin.layouts.mediaJs')
@endpush
