@php
$page_title = trans('Admin | About page contents');
@endphp
@push('style')
<link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
<style>
    .form-control-file {
        padding: 5px;
        border: 1px solid #aaa;
        border-radius: 5px;
        background: #fdfdff;
    }

    .input-group-text,
    select.form-control:not([size]):not([multiple]),
    .form-control:not(.form-control-sm):not(.form-control-lg) {
        font-size: 14px;
        padding: 10px 15px;
        height: 48px;
    }
</style>
@endpush
@extends('admin.layouts.master')
@section('content')
{{-- Main Content --}}
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>{{__('admin.About')}}</h1>
        </div>
        <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.about.update', 1)}}" method="POST" enctype="multipart/form-data">
                        @csrf
                        @method('PUT')
                        <div class="form-group">
                            <label for="">{{__('admin.Icon')}}</label>
                            <div id="icon995" name="icon"></div>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Title')}}</label>
                            <input type="text" class="form-control" name="title" value="{{($about)?$about->title : ''}}">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Subtitle')}}</label>
                            <input type="text" class="form-control" name="subtitle" value="{{($about)?$about->subtitle : ''}}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Description')}}</label>
                            <textarea class="form-control h-100" name="description" rows="3"
                                id="editor">{!! ($about) ? clean(html_entity_decode($about->description)) : '' !!}</textarea>
                        </div>

                        @include('admin.layouts.mediaInput', [
                        'inputTitle' => trans('First Image'),
                        'inputName' => 'first_image',
                        'inputData' => @($about)?$about->first_image : '',
                        'multiInput' => false,
                        'buttonText' => trans('Select'),
                        ])

                        @include('admin.layouts.mediaInput', [
                        'inputTitle' => trans('Second Image'),
                        'inputName' => 'second_image',
                        'inputData' => @($about)?$about->second_image : '',
                        'multiInput' => false,
                        'buttonText' => trans('Select'),
                        ])

                        @include('admin.layouts.mediaInput', [
                        'inputTitle' => trans('Third Image'),
                        'inputName' => 'third_image',
                        'inputData' => @($about)?$about->third_image : '',
                        'multiInput' => false,
                        'buttonText' => trans('Select'),
                        ])
                        <div class="form-group">
                            <label for="">{{__('admin.Bottom Text')}}</label>
                            <input type="text" class="form-control" name="bottom_text" value="{{($about)?$about->bottom_text : ''}}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="">{{__('First Video')}}</label>
                            <input type="text" class="form-control" name="first_video" value="{{($about)?$about->first_video : ''}}"
                            >
                        </div>
                        <div class="form-group">
                            <label for="">{{__('Second Video')}}</label>
                            <input type="text" class="form-control" name="second_video" value="{{($about)?$about->second_video : ''}}">
                        </div>
                        @php
                             $client_section = $about ? json_decode($about->client_section) :  null;
                        @endphp
                        <div class="form-group">
                            <h4>{{__('Client Section')}}</h4>
                            <div class="form-group">
                                <label for="">{{__('Client Section Title')}}</label>
                                <input type="text" class="form-control" name="clienttitle" value="{{$client_section ? $client_section->title : ''}}">
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="">{{__('Client Section Subtitle')}}</label>
                                    <input type="text" class="form-control" name="clientsubtitle" value="{{$client_section ? $client_section->subtitle : ''}}">
                                </div>
                            </div>
                        </div>
                        @php
                        $items =($about)? json_decode($about->item) : array();              
                        @endphp
                       @forelse ( $items as $key => $item )
                            <div class="add_item delete_item_add">
                                <div class="form-group">
                                    <h4>{{__('About Item')}}</h4>
                                    <div class="form-group">
                                        <label for="">{{__('Icon')}}</label>
                                        <div id="{{"icon".$key}}" name="itemicon[]"></div>
                                    </div>
                                    <div class="form-group">
                                        <label for="">{{__('About Item Title')}}</label>
                                        <input type="text" class="form-control" name="itemtitle[]" value="{{($item->title)}}">
                                    </div>
                                    <div class="row">
                                        <div class="col-md-10">
                                            <label for="">{{__('Subtitle')}}</label>
                                            <input type="text" class="form-control" name="itemsubtitle[]" value="{{($item->subtitle)}}">
                                        </div>
                                        <div class="col-md-2 pt-4">
                                            <span class="btn btn-danger removemore"><i class="fa fa-minus-circle fa-lg"></i></span> 
                                            <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                                            </span>
                                        </div>
                                    </div>
                                </div>
                            </div>  
                        @empty
                        <div class="add_item">
                            <div class="form-group">
                                <h4>{{__('About Item')}}</h4>
                                <div class="form-group">
                                    <label for="">{{__('Icon')}}</label>
                                    <div id="icon0" name="itemicon[]">
                                        <input type="hidden" name="icons[]">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="">{{__('About Item Title')}}</label>
                                    <input type="text" class="form-control" name="itemtitle[]">
                                </div>
                                <div class="row">
                                    <div class="col-md-10">
                                        <label for="">{{__('Subtitle')}}</label>
                                        <input type="text" class="form-control" name="itemsubtitle[]">
                                    </div>
                                    <div class="col-md-2 pt-4" >
                                        <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </div>  
                        @endforelse             
                        <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('admin.Save')}}</button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>

<div class="form-group d-none">
    <div class="about_item_add" id="about_item_add">
         <div class="delete_item_add" id="delete_item_add">
             <h4>{{__('About Item')}}</h4>
             <div class="form-group">
                <label for="">{{__('Icon')}}</label>
                <div id="demoicon" name="itemicon[]">
                    <input type="hidden" name="icons[]">
                </div>
            </div>
             <div class="form-group">
                 <label for="">{{__('Title')}}</label>
                 <input type="text" class="form-control"  name="itemtitle[]">
             </div>
             <div class="row">
                 <div class="col-md-10">
                     <label for="">{{__('Subtitle')}}</label>
                     <input type="text" class="form-control" name="itemsubtitle[]">
                 </div>
                 <div class="col-md-2 pt-4">
                     <span class="btn btn-danger removemore"><i class="fa fa-minus-circle fa-lg"></i></span>
                     <span id="addemore" class="btn btn-success addemore"><i class="fa fa-plus-circle fa-lg"></i>
                     </span>
                 </div>
             </div>
             <div class="mt-3"></div>
         </div>
    </div>
</div>
@push('script')
<script type="text/javascript" src="{{asset('assets/admin/js/bootstrap-iconpicker.bundle.min.js')}}"></script>
<script>
    let icons = @json($items);
    let about = @json($about);
    console.log('about :>> ', about);
    let iconcount = icons.length;
    about ? addIcon('#icon995', about.icon) : addIcon('#icon995');
    if(iconcount>0) {
        icons.map((icon, index)=> {
            addIcon('#icon'+index, icon.icon)
            $('#icon'+index).iconpicker({
                
            });
            $('#icon'+index).find('input[type=hidden]').each(function() {
                $(this).val(icon.icon);
            });
        });
    } else addIcon('#icon'+iconcount);
    
    $(document).ready(function() {
        let counter = 0;
        $(document).on("click",".addemore",function() {
            iconcount++;
            let about_item_add = $('#about_item_add').html();
            $(this).closest(".add_item").append(about_item_add.replace('id="demoicon"', 'id="icon'+iconcount+'"'));
            counter++;
            addIcon('#icon'+iconcount);
        });
        $(document).on("click",'.removemore',function(event){
            $(this).closest(".delete_item_add").remove();
            counter -= 1
        });
    });

    function addIcon(iconid, icon =null) { 
        $(iconid).iconpicker({
            align: 'left', // Only in div tag
            arrowClass: 'btn-primary',
            arrowPrevIconClass: 'fas fa-angle-left',
            arrowNextIconClass: 'fas fa-angle-right',
            cols: 5,
            footer: true,
            header: true,
            iconset: 'fontawesome5',
            labelHeader: '{0} of {1} pages',
            labelFooter: '{0} - {1} of {2} icons',
            placement: 'bottom', // Only in button tag
            icon: icon ? icon : '',
            value: icon ? icon : '',
            rows: 5,
            search: true,
            searchText: icon ? icon : 'Search',
            selectedClass: 'btn-primary',
            unselectedClass: 'btn-dark',
        })
    }
</script>
@include('admin.layouts.mediaJs')
@endpush
@include('admin.layouts.mediaModal')
@endsection