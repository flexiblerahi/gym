@php
$page_title = trans('Admin | Order');
@endphp
@extends('admin.layouts.master')
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>{{__('Order Information')}}</h1>
        </div>
        <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
        <div class="section-body">
            <div class="card bg-white">
                <div class="card-header">
                    {{trans('Billing Information')}}
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <tbody>
                            <tr>
                                <td>{{__('Name')}}</td>
                                <td colspan="2">{{$order->address->billing_name}}</td>
                            </tr>
                            <tr>
                                <td>{{__('Phone')}}</td>
                                <td colspan="2">{{$order->address->billing_phone}}</td>
                            </tr>
                            <tr>
                                <td>{{__('Email')}}</td>
                                <td colspan="2">{{$order->address->billing_email}}</td>
                            </tr>
                            <tr>
                                <td>{{__('Address')}}</td>
                                <td colspan="2" rowspan="3">
                                    {{trans('City : ') . $order->address->billing_city}},
                                    {{trans('Post Code : ') . $order->address->billing_post_code}},
                                    {{trans('Country : ') . $order->address->billing_country}}
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
            @if ($order->address->is_shipping == APP\Models\OrderAddress::DIFFERENTSHIPPING)
                <div class="card bg-white">
                    <div class="card-header">
                        {{trans('Shipping Information')}}
                    </div>
                    <div class="card-body">
                        <table class="table table-bordered">
                            <tbody>
                                <tr>
                                    <td>{{__('Name')}}</td>
                                    <td colspan="2">{{$order->address->shipping_name}}</td>
                                </tr>
                                <tr>
                                    <td>{{__('Phone')}}</td>
                                    <td colspan="2">{{$order->address->shipping_phone}}</td>
                                </tr>
                                <tr>
                                    <td>{{__('Email')}}</td>
                                    <td colspan="2">{{$order->address->shipping_email}}</td>
                                </tr>
                                <tr>
                                    <td>{{__('Address')}}</td>
                                    <td colspan="2" rowspan="3">
                                        {{trans('City : ') . $order->address->shipping_city}},
                                        {{trans('Post Code : ') . $order->address->shipping_post_code}},
                                        {{trans('Country : ') . $order->address->shipping_country}}
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            @endif
            @if ($order->bank_info)
            <div class="card bg-white">
                <div class="card-header">
                    {{trans('admin.Bank Information')}}
                </div>
                <div class="card-body">
                    <div class="form-group">
                      <textarea class="form-control h-100" name="" id="" rows="3">{!! filterTag(clean($order->bank_info)) !!}</textarea>
                    </div>

                </div>
            </div>
            @endif
            <div class="card">
                <div class="card-header">
                    {{trans('admin.Order Details')}}
                </div>
                <div class="card-body">
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>{{__('admin.Item')}}</th>
                                <th>{{__('admin.Quantity')}}</th>
                                <th>{{__('admin.Unit Price')}}</th>
                                <th>{{__('admin.Sub Total')}}</th>
                            </tr>
                        </thead>
                        <tbody>
                        @foreach ($order->items as $item)
                            <tr>
                                <td>
                                    <div class="order-item d-flex">
                                        <div class="order-item-name">
                                            @if (@$item->type ==  App\Models\OrderItem::COURSE)
                                                <b>{{__('Name:')}}&nbsp;{{$item->course->name}}</b>
                                            @else
                                                <b>{{__('Name:')}}&nbsp;{{$item->product->name}}</b>
                                            @endif
                                            
                                            <div>
                                                @if (@$item->type ==  App\Models\OrderItem::COURSE)
                                                <b>{{__('Item Name:')}}&nbsp;{{__('Course')}}</b>
                                                @else
                                                <b>{{__('Item Type:')}}&nbsp;{{__('Product')}}</b>
                                                @endif
                                            </div>
                                        </div>
                                    </div>
                                </td>
                                <td>{{@$item->item_qty}}</td>
                                <td>  {{ currencyPosition ( $item->item_price)}}
                                </td>
                                <td>
                                    @if (@$item->type ==  App\Models\OrderItem::COURSE)
                                    {{ currencyPosition ($item->item_price)}}
                                    @else
                                    {{ currencyPosition ($item->item_price  * @$item->item_qty)}}
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                            <tr>
                                <td scope="row" colspan="3">{{__('admin.Tax')}}</td>
                                <td class="font-weight-bold">
                                    {{ currencyPosition ($order->tax)}}
                                </td>
                            </tr>
                            <tr>
                                <td scope="row" colspan="3">{{__('admin.Shipping fee')}}</td>
                                <td class="font-weight-bold">
                                    {{ currencyPosition ($order->shipping_fee)}}
                                </td>
                            </tr>
                            <tr>
                                <td class="font-weight-bold" scope="row" colspan="3">{{__('admin.Total')}}</td>
                                <td class="font-weight-bold">
                                    {{ currencyPosition ($order->total_amount)}}
                                </td>
                            </tr>
                            <tr>
                                <form action="{{route('admin.order.update', $order->id)}}" method="POST">
                                    <td colspan="1">
                                        {{trans('admin.Order Status')}} :
                                    </td>
                                    @csrf
                                    @method('PUT')
                                    <td colspan="2">
                                        <div class="form-group pt-4">
                                            <select class="form-control" name="status" >
                                                <option value="pending" @if ($order->status == 'pending') selected @endif>{{__('admin.Pending ')}}</option>
                                                <option value="shipping" @if ($order->status == 'shipping') selected @endif>{{__('admin.Shipping')}}</option>
                                                <option value="cancelled" @if ($order->status == 'cancelled') selected @endif>{{__('admin.Cancelled')}}</option>
                                                <option value="completed" @if ($order->status == 'completed') selected @endif>{{__('admin.Complete')}}</option>
                                            </select>
                                        </div>
                                    </td>
                                    <td>
                                        <button type="submit" class="btn btn-primary btn-block">{{__('admin.Update')}}</button>
                                    </td>
                                </form>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </section>
</div>
@endsection