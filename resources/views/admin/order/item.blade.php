
@foreach ($order->items as $item)
    <div class="order-item d-flex">
        <div class="order-item-name">
            @if ($item->type ==  App\Models\OrderItem::COURSE)
                <b>{{__('Item Name:')}}&nbsp;{{$item->course->name}}</b>
            @else
                <b>{{__('Item Name:')}}&nbsp;{{$item->product->name}}</b>
            @endif
            <br>
            @if ($item->type ==  App\Models\OrderItem::COURSE)
                <b>{{__('Item Name:')}}&nbsp;{{__('Course')}}</b>
            @else
                <b>{{__('Item Type:')}}&nbsp;{{__('Product')}}</b>
            @endif
            <br>
            <small>
                {{$item->item_qty}} x {{GetSetting('site_currency_icon')}} {{$item->item_price}}
            </small>
        </div>
    </div>
@endforeach
