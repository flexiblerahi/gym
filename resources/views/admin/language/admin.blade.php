@php
$page_title = trans('Admin | Admin language');
@endphp
@extends('admin.layouts.master')
@section('content')
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Admin Language')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <form action="{{route('admin.admin-language.update',1)}}" method="POST">
                    @csrf
                    @method('PUT')
                    <div class="card">
                        <div class="card-body">
                            <table class="table table-striped   table-bordered w-100">
                                <thead class="thead-inverse border-bottom">
                                    <tr>
                                        <th class="w-50">{{__('admin.Content Name')}}</th>
                                        <th class="w-50">{{__('admin.Localization')}}</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach ($language as $key => $value)
                                        <tr>
                                            <td>{{$key}}</td>
                                            <td>
                                                <div class="form-group mt-4">
                                                    <input type="text" class="form-control" name="{{$key}}" value="{{$value}}" required>
                                                </div>
                                            </td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <button type="submit" class="btn btn-primary btn-block"> {{trans('admin.Save')}} </button>
                        </div>
                    </div>
                </form>
            </div>
        </section>
    </div>
@endsection

