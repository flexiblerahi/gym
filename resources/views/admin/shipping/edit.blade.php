@php
$page_title = trans('Admin | Edit Shipping Info');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
    <!-- Include stylesheet -->
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('Edit shipping')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left"></i> {{__('Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.shipping.update',$shipping->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">{{__('Name')}}</label>
                                <input type="text" class="form-control" name="title" value="{{$shipping->title}}">
                            </div>  
                            <div class="form-group">
                                <label for="">{{__('Cost')}}</label>
                                <input type="text" class="form-control" name="cost" value="{{$shipping->cost}}">
                            </div> 
                            <div class="form-group">
                                <label for="">{{__('admin.Description')}}</label>
                                <textarea class="form-control h-100" name="short_description" rows="8">{{$shipping->short_description}}</textarea>
                            </div>
                            <div class="form-group">
                                <div class="control-label">{{__('Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="status" class="custom-switch-input" @if ($shipping->status == $shipping->active) checked @endif>
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('OFF')}} / {{__('ON')}}</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{__('Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    <script>
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13)  return false;
        });
    </script>
@endpush
