@php
$page_title = trans('Admin | Terms of use');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
    <!-- Include stylesheet -->

@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Terms Of Use')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left"></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.terms-of-use.store')}}" method="POST">
                            @csrf
                            <div class="form-group">
                                <label for="">{{__('admin.Content')}}</label>
                                <textarea class="form-control h-100" name="content" id="editor"
                                    rows="8">{!! ($tos) ? clean(html_entity_decode($tos->content)) : '' !!}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('script')
@endpush
