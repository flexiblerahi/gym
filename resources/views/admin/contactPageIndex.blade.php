@php
$page_title = trans('Admin | Contact Page');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }
    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Contact Page')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.contact-page.update', $contact->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">{{__('admin.Form Title')}}</label>
                                <input type="text" class="form-control" name="form_title" value="{{$contact->form_title}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Form Subtitle')}}</label>
                                <input type="text" class="form-control" name="form_subtitle" value="{{$contact->form_subtitle}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Contact Title')}}</label>
                                <input type="text" class="form-control" name="contact_title" value="{{$contact->contact_title}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Contact Subtitle')}}</label>
                                <input type="text" class="form-control" name="contact_subtitle" value="{{$contact->contact_subtitle}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Phone')}}</label>
                                <input type="text" class="form-control" name="phone" value="{{$contact->phone}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Email')}}</label>
                                <input type="email" class="form-control" name="email" value="{{$contact->email}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Address')}}</label>
                                <input type="text" class="form-control" name="address" value="{{$contact->address}}">
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button">{{__('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    @include('admin.layouts.mediaJs')
@endpush
