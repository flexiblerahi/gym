@php
$page_title = trans('Admin | Edit blog');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Edit Blog')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <form action="{{route('admin.blog.update', $blog->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">{{__('admin.Title')}}</label>
                                <input type="text" class="form-control" name="title"
                                    value="{{$blog->title}}" required>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Slug')}}</label>
                                <input type="text" class="form-control" name="slug"
                                    value="{{$blog->slug}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Category')}}</label>
                                <select class="form-control" name="category_id"  required>
                                    <option disabled selected>{{__('Select a category')}}</option>
                                    @foreach (App\Models\BlogCategory::all() as $category)
                                        <option @if ($category->id == $blog->category_id)
                                            selected
                                            @endif value="{{$category->id}}">{{$category->name}}</option>
                                    @endforeach
                                </select>
                            </div>
                            @include('admin.layouts.mediaInput', [
                            'inputTitle' => trans('Image'),
                            'inputName' => 'blog_image',
                            'inputData' => $blog->image,
                            'multiInput' => false,
                            'buttonText' => trans('Select'),
                            ])
                            <div class="form-group">
                                <label for="">{{__('admin.Description')}}</label>
                                <textarea class="form-control h-100" name="description" id="editor"
                                    rows="8">{!! clean(html_entity_decode($blog->description)) !!}</textarea>
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.SEO Title')}}</label>
                                <input type="text" class="form-control" name="seo_title"
                                    value="{{$blog->seo_title}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.SEO Description')}}</label>
                                <textarea class="form-control h-100" name="seo_description"
                                    rows="4">{!! filterTag(clean($blog->seo_description)) !!}</textarea>
                            </div>

                            <div class="form-group">
                                <div class="control-label">{{__('admin.Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="status" class="custom-switch-input" @if ($blog->status == 1)
                                    checked
                                    @endif>
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('admin.OFF')}} / {{trans('admin.ON')}}</span>
                                </label>
                            </div>
                            <div class="form-group">
                                <div class="control-label">{{__('Comment Status')}}</div>
                                <label class="custom-switch pl-0 mt-2">
                                    <input type="checkbox" name="comment_status" class="custom-switch-input" @if ($blog->comment_status == 1)
                                    checked
                                    @endif>
                                    <span class="custom-switch-indicator"></span>
                                    <span class="custom-switch-description">{{__('Close')}} / {{trans('Active')}}</span>
                                </label>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block" role="button"> {{trans('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    @include('admin.layouts.mediaJs')
    <script>
        $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) return false;
        });
    </script>
@endpush
