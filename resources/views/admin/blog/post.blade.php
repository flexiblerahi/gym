@php
$page_title = trans('Admin | Post a blog');
@endphp
@push('style')
<link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
<link rel="stylesheet" href="{{asset('assets/admin/css/tagsinput.css')}}">
<style>
    .form-control-file {
        padding: 5px;
        border: 1px solid #aaa;
        border-radius: 5px;
        background: #fdfdff;
    }
</style>
@endpush
@extends('admin.layouts.master')
@section('content')
<div class="main-content">
    <section class="section">
        <div class="section-header">
            <h1>{{__('admin.Post a Blog')}}</h1>
        </div>
        <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <form action="{{route('admin.blog.store')}}" method="POST">
                        @csrf
                        <div class="form-group">
                            <label for="">{{__('admin.Title')}}</label>
                            <input type="text" class="form-control" name="title" id="title">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Slug')}}</label>
                            <input type="text" class="form-control" name="slug" id="slug">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.Category')}}</label>
                            <select class="form-control" name="category_id">
                                <option disabled selected>{{__('admin.Select a Category')}}</option>
                                @foreach (App\Models\BlogCategory::all() as $category)
                                <option value="{{$category->id}}">{{$category->name}}</option>
                                @endforeach
                            </select>
                        </div>
                        @include('admin.layouts.mediaInput', [
                        'inputTitle' => trans('Image'),
                        'inputName' => 'blog_image',
                        'inputData' => null,
                        'multiInput' => false,
                        'buttonText' => trans('Select'),
                        ])
                        <div class="form-group">
                            <label for="">{{__('admin.Description')}}</label>
                            <textarea class="form-control h-100" name="description" id="editor" rows="8"></textarea>
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.SEO Title')}}</label>
                            <input type="text" class="form-control" name="seo_title">
                        </div>
                        <div class="form-group">
                            <label for="">{{__('admin.SEO Description')}}</label>
                            <textarea class="form-control h-100" name="seo_description" rows="4"></textarea>
                        </div>
                        <div class="form-group">
                            <div class="control-label">{{__('admin.Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="status" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('admin.OFF')}} /
                                    {{trans('admin.ON')}}</span>
                            </label>
                        </div>
                        <div class="form-group">
                            <div class="control-label">{{__('Comment Status')}}</div>
                            <label class="custom-switch pl-0 mt-2">
                                <input type="checkbox" name="comment_status" class="custom-switch-input">
                                <span class="custom-switch-indicator"></span>
                                <span class="custom-switch-description">{{__('Close')}} / {{trans('Active')}}</span>
                            </label>
                        </div>
                        <button type="submit" class="btn btn-primary btn-block" role="button">
                            {{trans('admin.Save')}} </button>
                    </form>
                </div>
            </div>
        </div>
    </section>
</div>
@include('admin.layouts.mediaModal')
@endsection
@push('script')
<script>
    $("form").bind("keypress", function(e) {
            if (e.keyCode == 13) {
                return false;
            }
        });
</script>
@include('admin.layouts.mediaJs')

@endpush