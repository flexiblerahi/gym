@php
$page_title = trans('Admin | Add User');
@endphp
@push('style')
    <link rel="stylesheet" href="{{asset('assets/admin/css/mediamanager.css')}}">
    <link rel="stylesheet" href="{{asset('assets/admin/css/bootstrap-iconpicker.min.css')}}">
    <style>
        .form-control-file {
            padding: 5px;
            border: 1px solid #aaa;
            border-radius: 5px;
            background: #fdfdff;
        }

    </style>
@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Add User')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left"></i> {{trans('admin.Back')}}</a>
            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        {!! Form::open(['route' => 'admin.users.store', 'method' => 'POST']) !!}
                        @include('admin.layouts.mediaInput', [
                            'inputTitle' => trans('Image'),
                            'inputName' => 'avatar',
                            'inputData' => null,
                            'multiInput' => false,
                            'buttonText' => trans('Select'),
                        ])
                        <div class="row">
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>{{__('Name')}}</label>
                                    {!! Form::text('name', null, ['placeholder' => 'Name', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>{{__('Email')}}</label>
                                    {!! Form::email('email', null, ['placeholder' => 'Email', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>{{__('Password')}}</label>
                                    {!! Form::password('password', ['placeholder' => 'Password', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>{{__('Confirm Password')}}</label>
                                    {!! Form::password('confirm-password', ['placeholder' => 'Confirm Password', 'class' => 'form-control', 'autocomplete' => 'off']) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12">
                                <div class="form-group">
                                    <label>{{__('Role')}}</label>
                                    {!! Form::select('roles[]', $roles, [], ['class' => 'form-control', 'multiple', 'id' => 'multiple-select']) !!}
                                </div>
                            </div>
                            <div class="col-xs-12 col-sm-12 col-md-12 text-center">
                                <button type="submit" class="btn btn-primary">{{__("Submit")}}</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </section>
    </div>
    @include('admin.layouts.mediaModal')
@endsection
@push('script')
    <script>
        $(document).ready(function() {
            $('#multiple-select').select2();
        });
    </script>
    @include('admin.layouts.mediaJs')
@endpush
