@php
$page_title = trans('Admin | Edit email template');
@endphp
@push('style')

@endpush
@extends('admin.layouts.master')
@section('content')
    {{-- Main Content --}}
    <div class="main-content">
        <section class="section">
            <div class="section-header">
                <h1>{{__('admin.Edit Email Template')}}</h1>
            </div>
            <a class="btn btn-primary mb-4" href="{{URL::previous()}}" role="button"><i
                    class="fas fa-arrow-circle-left    "></i> {{trans('admin.Back')}}</a>

            <div class="section-body">
                <div class="card">
                    <div class="card-body">
                        <label for="">{{__('admin.Variables')}}</label>
                        <table class="table table-striped table-bordered table-inverse w-100 mb-4">
                            <thead class="thead-inverse border-bottom">
                                <tr>
                                    <th>{{__('admin.Variabe')}}</th>
                                    <th>{{__('admin.Meaning')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach ($variables as $variable)
                                    <tr>
                                        <td>{{$variable->variable}}</td>
                                        <td>{{$variable->meaning}}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        <form action="{{route('admin.email-template.update', $email_template->id)}}" method="POST">
                            @csrf
                            @method('PUT')
                            <div class="form-group">
                                <label for="">{{__('admin.Subject')}}</label>
                                <input type="text" class="form-control" name="subject"
                                    value="{{$email_template->subject}}">
                            </div>
                            <div class="form-group">
                                <label for="">{{__('admin.Description')}}</label>
                                <textarea class="form-control h-100" name="description" id="editor"
                                    rows="3">{!! clean(html_entity_decode($email_template->description)) !!}</textarea>
                            </div>
                            <button type="submit" class="btn btn-primary btn-block"> {{trans('admin.Save')}} </button>
                        </form>
                    </div>
                </div>
            </div>
        </section>
    </div>
@endsection
@push('script')
@endpush
