<!DOCTYPE html>
<html lang="en">
<head>
    <!-- Meta Tags -->
    <meta name="viewport"
    content="width=device-width, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0, user-scalable=no, target-densityDpi=device-dpi" />
    <meta http-equiv="content-type" content="text/html; charset=UTF-8" />
    <title>{{ __('GymSuit') }}</title>
    @include('frontend.layouts.styles')
</head>
<body class="home_2">

    @yield('code')
    
</body>
</html>

