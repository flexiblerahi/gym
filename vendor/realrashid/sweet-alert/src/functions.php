<?php

if (!function_exists('alert')) {

    function alert($title = '', $message = '', $type = '')
    {
        $alert = app('alert');
        if (!is_null($title)) {
            return $alert->alert($title, $message, $type);
        }
        return $alert;
    }
}

if (!function_exists('toast')) {
    function toast($title = '', $type = null, $position = 'bottom-right')
    {
        $alert = app('alert');
        if (!is_null($title)) {
            return $alert->toast($title, $type, $position);
        }
        return $alert;
    }
}
